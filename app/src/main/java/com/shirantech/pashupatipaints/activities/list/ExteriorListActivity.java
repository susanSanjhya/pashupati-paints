package com.shirantech.pashupatipaints.activities.list;

import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.ProgressBar;

import com.actionbarsherlock.app.SherlockActivity;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.shirantech.pashupatipaints.R;
import com.shirantech.pashupatipaints.activities.list.exteriors.Exterior1TabletActivity;
import com.shirantech.pashupatipaints.activities.list.exteriors.Exterior1V2Activity;
import com.shirantech.pashupatipaints.activities.list.exteriors.Exterior2TabletActivity;
import com.shirantech.pashupatipaints.activities.list.exteriors.Exterior2V2Activity;
import com.shirantech.pashupatipaints.activities.list.exteriors.Exterior3TabletActivity;
import com.shirantech.pashupatipaints.activities.list.exteriors.Exterior3V2Activity;
import com.shirantech.pashupatipaints.adapter.RoomCategoryListAdapter;
import com.shirantech.pashupatipaints.controller.GetRoomCategoriesTask;
import com.shirantech.pashupatipaints.interfaces.ModelListGetter;
import com.shirantech.pashupatipaints.model.RoomCategory;
import com.shirantech.pashupatipaints.util.Utility;

import java.util.List;

/**
 * Created by susan on 1/5/15.
 */
public class ExteriorListActivity extends SherlockActivity implements AdapterView.OnItemClickListener, ModelListGetter<RoomCategory> {
    private GridView gridViewExteriorList;
    private ProgressBar progressBarExteriorLoading;
    private ImageLoader imageLoader = ImageLoader.getInstance();
    private String[] exteriorsDrawableArray = {
            "Exterior/exterior1/exterior1_main.png",
            "Exterior/exterior2/exterior2_main.png",
            "Exterior/exterior3/exterior3_main.png"
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        requestWindowFeature(android.view.Window.FEATURE_NO_TITLE);
        if (Build.VERSION.SDK_INT <= Build.VERSION_CODES.HONEYCOMB) {
            getSupportActionBar().hide();
        }
        if (Utility.isTablet(this)) {
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
        } else {
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        }
        setContentView(R.layout.activity_exterior_list);

        imageLoader.init(ImageLoaderConfiguration.createDefault(this));
        progressBarExteriorLoading = (ProgressBar) findViewById(R.id.progressbar_exterior_loading);

        gridViewExteriorList = (GridView) findViewById(R.id.gridview_exterior);
        gridViewExteriorList.setOnItemClickListener(this);
        findViewById(R.id.home).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(ExteriorListActivity.this, PainingWaysActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
            }
        });

        new GetRoomCategoriesTask(this, this, "exteriors").execute(exteriorsDrawableArray);
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        switch (position) {
            case 0:
                if (Utility.isTablet(this)){
                    Intent exterior1Intent = new Intent(ExteriorListActivity.this, Exterior1TabletActivity.class);
                    exterior1Intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(exterior1Intent);
                } else {
                    Intent exterior1Intent = new Intent(ExteriorListActivity.this, Exterior1V2Activity.class);
                    exterior1Intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(exterior1Intent);
                }
                break;
            case 1:
                if(Utility.isTablet(this)) {
                    Intent exterior2Intent = new Intent(ExteriorListActivity.this, Exterior2TabletActivity.class);
                    exterior2Intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(exterior2Intent);
                } else {
                    Intent exterior2Intent = new Intent(ExteriorListActivity.this, Exterior2V2Activity.class);
                    exterior2Intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(exterior2Intent);
                }
                break;
            case 2:
                if(Utility.isTablet(this)) {
                    Intent exterior3Intent = new Intent(ExteriorListActivity.this, Exterior3TabletActivity.class);
                    exterior3Intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(exterior3Intent);
                } else {
                    Intent exterior3Intent = new Intent(ExteriorListActivity.this, Exterior3V2Activity.class);
                    exterior3Intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(exterior3Intent);
                }
                break;
        }
    }

    @Override
    public void onTaskStarted() {
        progressBarExteriorLoading.setVisibility(View.VISIBLE);
    }

    @Override
    public void onTaskFinished(List<RoomCategory> modelList) {
        gridViewExteriorList.setAdapter(new RoomCategoryListAdapter(this, imageLoader, modelList));
        progressBarExteriorLoading.setVisibility(View.GONE);
    }
}
