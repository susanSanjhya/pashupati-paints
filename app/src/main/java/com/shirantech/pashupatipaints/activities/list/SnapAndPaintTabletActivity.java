package com.shirantech.pashupatipaints.activities.list;

import android.annotation.TargetApi;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffXfermode;
import android.graphics.drawable.BitmapDrawable;
import android.os.Build;
import android.os.Bundle;
import android.os.Vibrator;
import android.support.annotation.NonNull;
import android.support.v4.view.ViewPager;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewConfiguration;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.actionbarsherlock.view.Window;
import com.jess.ui.TwoWayAdapterView;
import com.jess.ui.TwoWayGridView;
import com.shirantech.pashupatipaints.R;
import com.shirantech.pashupatipaints.adapter.TabletColorGridPagerAdapter;
import com.shirantech.pashupatipaints.adapter.TabletColorHorizontalListAdapter;
import com.shirantech.pashupatipaints.adapter.UsedColorAdapter;
import com.shirantech.pashupatipaints.customwidgets.CustomBoldTextView;
import com.shirantech.pashupatipaints.fragments.BeigeFragment;
import com.shirantech.pashupatipaints.fragments.BlackFragment;
import com.shirantech.pashupatipaints.fragments.BlueFragment;
import com.shirantech.pashupatipaints.fragments.BrownFragment;
import com.shirantech.pashupatipaints.fragments.GrayFragment;
import com.shirantech.pashupatipaints.fragments.GreenFragment;
import com.shirantech.pashupatipaints.fragments.IndigoFragment;
import com.shirantech.pashupatipaints.fragments.OrangeFragment;
import com.shirantech.pashupatipaints.fragments.RedFragment;
import com.shirantech.pashupatipaints.fragments.VioletFragment;
import com.shirantech.pashupatipaints.fragments.WhiteFragment;
import com.shirantech.pashupatipaints.fragments.YellowFragment;
import com.shirantech.pashupatipaints.interfaces.ViewOnTouchListener;
import com.shirantech.pashupatipaints.model.PaintColor;
import com.shirantech.pashupatipaints.util.AppLog;
import com.shirantech.pashupatipaints.util.Global;
import com.shirantech.pashupatipaints.util.ImageUtils;
import com.shirantech.pashupatipaints.util.Utility;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Created by susan on 1/5/15.
 */
public class SnapAndPaintTabletActivity extends GraphicsActivity
        implements VioletFragment.onPaintSwatchSelectedListener, IndigoFragment.onPaintSwatchSelectedListener,
        BlueFragment.onPaintSwatchSelectedListener, GreenFragment.onPaintSwatchSelectedListener,
        YellowFragment.onPaintSwatchSelectedListener, OrangeFragment.onPaintSwatchSelectedListener,
        RedFragment.onPaintSwatchSelectedListener, BrownFragment.onPaintSwatchSelectedListener,
        BeigeFragment.onPaintSwatchSelectedListener, GrayFragment.onPaintSwatchSelectedListener,
        WhiteFragment.onPaintSwatchSelectedListener, BlackFragment.onPaintSwatchSelectedListener,
        View.OnClickListener, ViewOnTouchListener {

    private static final String CLASS_TAG = SnapAndPaintTabletActivity.class.getSimpleName();
    private int usedColorId;
    private Boolean isColorSelected = false;
    private int screenWidth = 0;
    private int xAdjustment = 0;
    private Paint mPaint;
    private RelativeLayout rlRoom;
    private ViewPager viewPagerColors;
    private int pagerPagePosition;
    private TabletColorGridPagerAdapter adapter;
    private TextView textViewColorName;
    private Global global;
    private Bitmap snappedBitmap;
    private List<PaintColor> globalPaintColorList;
    private SnapAndPaintView mv;
    private List<PaintColor> usedColorList;
    private Button buttonHideNShow;
    private List<PaintColor> selectedColorsList;
    private TwoWayGridView horizontalListView;
    private int y, x;
    private TabletColorHorizontalListAdapter paintColorAdapter;
    private List<PaintColor> tempElements;
    private Vibrator mVibrator;
    private long VIBRATE_DURATION = 35;
    private ListView listViewUsedColors;
    private LinearLayout linearLayoutPagerTab;
    private CustomBoldTextView imageButtonPreviousPage;
    private CustomBoldTextView imageButtonNextPage;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        if (Build.VERSION.SDK_INT <= Build.VERSION_CODES.HONEYCOMB) {
            getSupportActionBar().hide();
        }
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);

        setContentView(R.layout.activity_snap_and_paint);
        try {
            ViewConfiguration configuration = ViewConfiguration.get(this);
            Field menuKeyField = ViewConfiguration.class.getDeclaredField("sHasPermanentMenuKey");
            if (menuKeyField != null) {
                menuKeyField.setAccessible(true);
                menuKeyField.setBoolean(configuration, false);
            }
        } catch (NoSuchFieldException e) {
            AppLog.showLog(CLASS_TAG, e.getLocalizedMessage());
        } catch (IllegalAccessException e) {
            AppLog.showLog(CLASS_TAG, e.getLocalizedMessage());
        }

        rlRoom = (RelativeLayout) findViewById(R.id.rl_snap_and_paint);


        listViewUsedColors = (ListView) findViewById(R.id.listview_used_colors);

        global = (Global) getApplication();
        globalPaintColorList = global.getGlobalPaintColorList();
        snappedBitmap = global.getSnapAndPaintBitmap();
        mv = new SnapAndPaintView(this, snappedBitmap);

        rlRoom.addView(mv);

        textViewColorName = (TextView) findViewById(R.id.textview_color_name);

        linearLayoutPagerTab = (LinearLayout) findViewById(R.id.linear_layout_pager_tab);
        viewPagerColors = (ViewPager) findViewById(R.id.viewpager_colors);
        adapter = new TabletColorGridPagerAdapter(getSupportFragmentManager());
        textViewColorName.setText(adapter.getPageTitle(0));
        viewPagerColors.setAdapter(adapter);
        linearLayoutPagerTab.setBackground(Utility.getCustomShape(Color.parseColor("#7F00FF")));
        viewPagerColors.setOnPageChangeListener(new ViewPager.SimpleOnPageChangeListener() {
            @Override
            public void onPageSelected(int position) {
                pagerPagePosition = position;
                textViewColorName.setText(adapter.getPageTitle(position));
                switch (position) {
                    case 0:
                        linearLayoutPagerTab.setBackground(Utility.getCustomShape(Color.parseColor("#7F00FF")));
                        textViewColorName.setTextColor(Color.BLACK);
                        imageButtonNextPage.setTextColor(Color.BLACK);
                        imageButtonPreviousPage.setTextColor(Color.BLACK);
                        imageButtonPreviousPage.setVisibility(View.INVISIBLE);
                        imageButtonNextPage.setVisibility(View.VISIBLE);
                        break;
                    case 1:
                        linearLayoutPagerTab.setBackground(Utility.getCustomShape(Color.parseColor("#4b0082")));
                        textViewColorName.setTextColor(Color.BLACK);
                        imageButtonNextPage.setTextColor(Color.BLACK);
                        imageButtonPreviousPage.setTextColor(Color.BLACK);
                        imageButtonPreviousPage.setVisibility(View.VISIBLE);
                        imageButtonNextPage.setVisibility(View.VISIBLE);
                        break;
                    case 2:
                        linearLayoutPagerTab.setBackground(Utility.getCustomShape(Color.BLUE));
                        textViewColorName.setTextColor(Color.BLACK);
                        imageButtonNextPage.setTextColor(Color.BLACK);
                        imageButtonPreviousPage.setTextColor(Color.BLACK);
                        imageButtonPreviousPage.setVisibility(View.VISIBLE);
                        imageButtonNextPage.setVisibility(View.VISIBLE);
                        break;
                    case 3:
                        linearLayoutPagerTab.setBackground(Utility.getCustomShape(Color.GREEN));
                        textViewColorName.setTextColor(Color.BLACK);
                        imageButtonNextPage.setTextColor(Color.BLACK);
                        imageButtonPreviousPage.setTextColor(Color.BLACK);
                        imageButtonPreviousPage.setVisibility(View.VISIBLE);
                        imageButtonNextPage.setVisibility(View.VISIBLE);
                        break;
                    case 4:
                        linearLayoutPagerTab.setBackground(Utility.getCustomShape(Color.parseColor("#FFD700")));
                        textViewColorName.setTextColor(Color.BLACK);
                        imageButtonNextPage.setTextColor(Color.BLACK);
                        imageButtonPreviousPage.setTextColor(Color.BLACK);
                        imageButtonPreviousPage.setVisibility(View.VISIBLE);
                        imageButtonNextPage.setVisibility(View.VISIBLE);
                        break;
                    case 5:
                        linearLayoutPagerTab.setBackground(Utility.getCustomShape(Color.parseColor("#FFA500")));
                        textViewColorName.setTextColor(Color.BLACK);
                        imageButtonNextPage.setTextColor(Color.BLACK);
                        imageButtonPreviousPage.setTextColor(Color.BLACK);
                        imageButtonPreviousPage.setVisibility(View.VISIBLE);
                        imageButtonNextPage.setVisibility(View.VISIBLE);
                        break;
                    case 6:
                        linearLayoutPagerTab.setBackground(Utility.getCustomShape(Color.RED));
                        textViewColorName.setTextColor(Color.BLACK);
                        imageButtonNextPage.setTextColor(Color.BLACK);
                        imageButtonPreviousPage.setTextColor(Color.BLACK);
                        imageButtonPreviousPage.setVisibility(View.VISIBLE);
                        imageButtonNextPage.setVisibility(View.VISIBLE);
                        break;
                    case 7:
                        linearLayoutPagerTab.setBackground(Utility.getCustomShape(Color.parseColor("#f4a460")));
                        textViewColorName.setTextColor(Color.BLACK);
                        imageButtonNextPage.setTextColor(Color.BLACK);
                        imageButtonPreviousPage.setTextColor(Color.BLACK);
                        imageButtonPreviousPage.setVisibility(View.VISIBLE);
                        imageButtonNextPage.setVisibility(View.VISIBLE);
                        break;
                    case 8:
                        linearLayoutPagerTab.setBackground(Utility.getCustomShape(Color.parseColor("#F5F5DC")));
                        textViewColorName.setTextColor(Color.BLACK);
                        imageButtonNextPage.setTextColor(Color.BLACK);
                        imageButtonPreviousPage.setTextColor(Color.BLACK);
                        imageButtonPreviousPage.setVisibility(View.VISIBLE);
                        imageButtonNextPage.setVisibility(View.VISIBLE);
                        break;
                    case 9:
                        linearLayoutPagerTab.setBackground(Utility.getCustomShape(Color.parseColor("#d3d3d3")));
                        textViewColorName.setTextColor(Color.BLACK);
                        imageButtonNextPage.setTextColor(Color.BLACK);
                        imageButtonPreviousPage.setTextColor(Color.BLACK);
                        imageButtonPreviousPage.setVisibility(View.VISIBLE);
                        imageButtonNextPage.setVisibility(View.VISIBLE);
                        break;
                    case 10:
                        linearLayoutPagerTab.setBackground(Utility.getCustomShape(Color.WHITE));
                        textViewColorName.setTextColor(Color.BLACK);
                        imageButtonNextPage.setTextColor(Color.BLACK);
                        imageButtonPreviousPage.setTextColor(Color.BLACK);
                        imageButtonPreviousPage.setVisibility(View.VISIBLE);
                        imageButtonNextPage.setVisibility(View.VISIBLE);

                        break;
                    case 11:
                        linearLayoutPagerTab.setBackground(Utility.getCustomShape(Color.BLACK));
                        textViewColorName.setTextColor(Color.WHITE);
                        imageButtonNextPage.setTextColor(Color.WHITE);
                        imageButtonPreviousPage.setTextColor(Color.WHITE);
                        imageButtonPreviousPage.setVisibility(View.VISIBLE);
                        imageButtonNextPage.setVisibility(View.GONE);
                        break;
                }
            }
        });


        imageButtonPreviousPage = (CustomBoldTextView) findViewById(R.id.button_previous_page);
        imageButtonNextPage = (CustomBoldTextView) findViewById(R.id.button_next_page);
        buttonHideNShow = (Button) findViewById(R.id.button_hide_and_show_hlv);
        ImageButton imageButtonRefresh = (ImageButton) findViewById(R.id.imagebutton_paint_refresh);
        ImageButton imageButtonSave = (ImageButton) findViewById(R.id.imagebutton_paint_save);
        ImageButton imageButtonShare = (ImageButton) findViewById(R.id.imagebutton_paint_share);
        ImageButton imageButtonErase = (ImageButton) findViewById(R.id.imagebutton_paint_undo);
        findViewById(R.id.home).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                switch (v.getId()) {
                    case R.id.home:
                        finish();
                        break;
                }
            }
        });
        CustomBoldTextView imageButtonGoToGallery = (CustomBoldTextView) findViewById(R.id.imagebutton_go_to_gallery);

        imageButtonGoToGallery.setOnClickListener(this);
        imageButtonErase.setOnClickListener(this);
        imageButtonShare.setOnClickListener(this);
        imageButtonSave.setOnClickListener(this);
        imageButtonRefresh.setOnClickListener(this);
        buttonHideNShow.setOnClickListener(this);
        imageButtonNextPage.setOnClickListener(this);
        imageButtonPreviousPage.setOnClickListener(this);
        imageButtonPreviousPage.setVisibility(View.INVISIBLE);

        horizontalListView = (TwoWayGridView) findViewById(R.id.hlv_selected_colors);
        selectedColorsList = new ArrayList<PaintColor>();
        usedColorList = new ArrayList<PaintColor>();
        if (globalPaintColorList != null) {
            if (globalPaintColorList.size() > 0) {
                globalPaintColorList.clear();
            }
        }
        final RelativeLayout.LayoutParams params = (RelativeLayout.LayoutParams) buttonHideNShow.getLayoutParams();
        buttonHideNShow.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent event) {
                switch (event.getAction()) {
                    case MotionEvent.ACTION_DOWN:
                        AppLog.showLog(CLASS_TAG, "Action Down entered");
                        x = (int) event.getX();
                        y = (int) event.getY();
                        break;
                    case MotionEvent.ACTION_MOVE:
                        AppLog.showLog(CLASS_TAG, "Action Move entered:::");

                        if (y < event.getY() && horizontalListView.getVisibility() == View.VISIBLE) {
                            AppLog.showLog(CLASS_TAG, "Hide layout() old x: " + x + " new x: " + event.getX()
                                    + " old y: " + y + " new y: " + event.getY());

                            Animation slideDown = AnimationUtils.loadAnimation(SnapAndPaintTabletActivity.this, R.anim.slide_down);
                            slideDown.setAnimationListener(new Animation.AnimationListener() {
                                @Override
                                public void onAnimationStart(Animation animation) {

                                }

                                @Override
                                public void onAnimationEnd(Animation animation) {
                                    params.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM);
                                    horizontalListView.setVisibility(View.GONE);
                                    buttonHideNShow.setText("^");
                                }

                                @Override
                                public void onAnimationRepeat(Animation animation) {

                                }
                            });
                            buttonHideNShow.startAnimation(slideDown);
                            horizontalListView.startAnimation(slideDown);
                        } else if (y > event.getY() && horizontalListView.getVisibility() != View.VISIBLE) {
                            AppLog.showLog(CLASS_TAG, "Show layout() old x: " + x + " new x: " + event.getX()
                                    + " old y: " + y + " new y: " + event.getY());
                            final Animation slideUp = AnimationUtils.loadAnimation(SnapAndPaintTabletActivity.this, R.anim.slide_up);
                            slideUp.setAnimationListener(new Animation.AnimationListener() {
                                @TargetApi(Build.VERSION_CODES.JELLY_BEAN_MR1)
                                @Override
                                public void onAnimationStart(Animation animation) {
                                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) {
                                        params.removeRule(RelativeLayout.ALIGN_PARENT_BOTTOM);
                                    } else {
                                        params.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM, 0);
                                    }
                                    params.addRule(RelativeLayout.ABOVE, R.id.hlv_selected_colors);
                                }


                                @Override
                                public void onAnimationEnd(Animation animation) {
                                    horizontalListView.setVisibility(View.VISIBLE);
                                    buttonHideNShow.setText("v");
                                }

                                @Override
                                public void onAnimationRepeat(Animation animation) {

                                }
                            });
                            buttonHideNShow.startAnimation(slideUp);
                            horizontalListView.startAnimation(slideUp);
                        }
                        break;
                    case MotionEvent.ACTION_UP:
                        x = (int) event.getX();
                        y = (int) event.getY();
                        break;
                }
                return false;
            }
        });
        mVibrator = (Vibrator) this.getSystemService(Context.VIBRATOR_SERVICE);
        horizontalListView.setOnItemLongClickListener(new TwoWayAdapterView.OnItemLongClickListener() {

            @Override
            public boolean onItemLongClick(TwoWayAdapterView<?> parent, View view, final int position, long id) {
                mVibrator.vibrate(VIBRATE_DURATION);
                TabletColorHorizontalListAdapter.ViewHolder holder = (TabletColorHorizontalListAdapter.ViewHolder) view.getTag();
                holder.imageButtonDelete.setVisibility(View.VISIBLE);
                holder.imageButtonDelete.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        final PaintColor pc = paintColorAdapter.getItem(position);
                        selectedColorsList.remove(pc);
                        tempElements.remove(pc);
                        paintColorAdapter = new TabletColorHorizontalListAdapter(SnapAndPaintTabletActivity.this,
                                tempElements, SnapAndPaintTabletActivity.this);
                        horizontalListView.setAdapter(paintColorAdapter);
                        paintColorAdapter.notifyDataSetChanged();
                    }
                });
                return true;
                /*final PaintColor pc = paintColorAdapter.getItem(position);

                AlertDialog.Builder builder = new AlertDialog.Builder(SnapAndPaintTabletActivity.this);
                builder.setMessage(
                        "Delete selected color?")
                        .setCancelable(false)
                                // Prevents user to use "back button"
                        .setPositiveButton("Delete",
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int id) {
                                        AppLog.showLog(CLASS_TAG, "OK clicked!! id: " + id);
                                        *//* delete *//*
                                        selectedColorsList.remove(pc);
                                        tempElements.remove(pc);
                                        paintColorAdapter = new TabletColorHorizontalListAdapter(SnapAndPaintTabletActivity.this,
                                                tempElements, SnapAndPaintTabletActivity.this);
                                        horizontalListView.setAdapter(paintColorAdapter);
                                        paintColorAdapter.notifyDataSetChanged();
                                    }
                                }
                        )
                        .setNegativeButton("Cancel",
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int id) {
                                        AppLog.showLog(CLASS_TAG, "cancel clicked!! id: " + id);
                                        dialog.cancel();
                                    }
                                }
                        );
                builder.show();
                return false;*/
            }
        });
//        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.HONEYCOMB) {
//            horizontalListView.setOnTouchListener(new ChoiceTouchListener());
//        }
        horizontalListView.setOnItemClickListener(new TwoWayAdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(TwoWayAdapterView<?> parent, View view, int position, long id) {
                if (((TabletColorHorizontalListAdapter.ViewHolder) view.getTag()).imageButtonDelete.getVisibility() == View.VISIBLE) {
                    ((TabletColorHorizontalListAdapter.ViewHolder) view.getTag()).imageButtonDelete.setVisibility(View.GONE);
                }
                PaintColor paintColor = (PaintColor) horizontalListView.getAdapter().getItem(position);
                AppLog.showLog(CLASS_TAG, "grid selected item:: " + paintColor.getName());
                sendToParent(paintColor);
            }
        });
    }

    @Override
    public void onViewTouch(PaintColor paintColor) {
        findViewById(R.id.imageview_selected_color)
                .setBackground(Utility.getCustomShape(Color.parseColor(paintColor.getColorCode())));
        ((TextView) findViewById(R.id.textview_selected_color_name))
                .setText(paintColor.getName());
        ((TextView) findViewById(R.id.textview_selected_color_code))
                .setText(paintColor.getColorCode());
        sendToParent(paintColor);
    }

    private void hideOrShowColorListView() {
        final RelativeLayout.LayoutParams params = (RelativeLayout.LayoutParams) buttonHideNShow.getLayoutParams();
        if (horizontalListView.getVisibility() == View.VISIBLE) {
            Animation slideDown = AnimationUtils.loadAnimation(this, R.anim.slide_down);
            slideDown.setAnimationListener(new Animation.AnimationListener() {
                @Override
                public void onAnimationStart(Animation animation) {

                }

                @Override
                public void onAnimationEnd(Animation animation) {
                    params.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM);
                    horizontalListView.setVisibility(View.GONE);
                    buttonHideNShow.setText("^");
                }

                @Override
                public void onAnimationRepeat(Animation animation) {

                }
            });
            horizontalListView.startAnimation(slideDown);

        } else {

            Animation slideUp = AnimationUtils.loadAnimation(this, R.anim.slide_up);
            slideUp.setAnimationListener(new Animation.AnimationListener() {
                @Override
                public void onAnimationStart(Animation animation) {

                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) {
                        params.removeRule(RelativeLayout.ALIGN_PARENT_BOTTOM);
                    } else {
                        params.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM, 0);
                    }
                    params.addRule(RelativeLayout.ABOVE, R.id.hlv_selected_colors);
                }

                @Override
                public void onAnimationEnd(Animation animation) {
                    horizontalListView.setVisibility(View.VISIBLE);
                    buttonHideNShow.setText("v");
                }

                @Override
                public void onAnimationRepeat(Animation animation) {

                }
            });

            horizontalListView.startAnimation(slideUp);
        }
    }

    private void sendToParent(PaintColor paintColor) {
        isColorSelected = true;

        mPaint = new Paint();
        mPaint.setAntiAlias(true);
        mPaint.setDither(true);
        mPaint.setAlpha(0x00);
        mPaint.setColor(-1);
        mPaint.setStyle(Paint.Style.STROKE);
        mPaint.setStrokeJoin(Paint.Join.ROUND);
        mPaint.setStrokeCap(Paint.Cap.ROUND);
        mPaint.setStrokeWidth(12);

        //set the paint color to paint
        mPaint.setColor(Color.parseColor(paintColor.getColorCode()));
        mPaint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.SRC));
        mPaint.setAlpha(0x00);


        Float[] rgb = ImageUtils.hexToRgb(paintColor.getColorCode());

        float redScale = rgb[0];
        float greenScale = rgb[1];
        float blueScale = rgb[2];

        AppLog.showLog(CLASS_TAG,
                "we get:::::: redScale:: " + redScale + " greenScale:: " + greenScale + " blueScale:: " + blueScale);
    }

    @Override
    public void onPaintSelected(PaintColor paintColor) {
        for (PaintColor p : selectedColorsList) {
            AppLog.showLog(CLASS_TAG, "in for loop:: paint-color id passed:: " + paintColor.getId());
            AppLog.showLog(CLASS_TAG, "selected color list in loop:: " + selectedColorsList.size() + " p id:: " + p.getId());
            if (p.getId().equals(paintColor.getId()) || p.getId().intValue() == paintColor.getId().intValue()) {
                Toast.makeText(getApplicationContext(), "The color is already selected.", Toast.LENGTH_SHORT).show();
                return;
            }
        }
        selectedColorsList.add(paintColor);
        AppLog.showLog(CLASS_TAG, "selectedColorsList size:: " + selectedColorsList.size());

        //global = (Global)getApplication();
        global.setGlobalPaintColorList(selectedColorsList);

        //AUTO SCROLL THE SELECTED COLORS!!
        tempElements = new ArrayList<PaintColor>(selectedColorsList);
        Collections.reverse(tempElements);

        paintColorAdapter = new TabletColorHorizontalListAdapter(getApplicationContext(), tempElements, this);
        horizontalListView.setAdapter(paintColorAdapter);
        paintColorAdapter.notifyDataSetChanged();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.button_next_page:
                if (pagerPagePosition >= adapter.getCount()) {
                    return;
                }

                viewPagerColors.setCurrentItem(viewPagerColors.getCurrentItem() + 1, true);
                textViewColorName.setText(adapter.getPageTitle(viewPagerColors.getCurrentItem()));
                switch (pagerPagePosition) {
                    case 0:
                        linearLayoutPagerTab.setBackground(Utility.getCustomShape(Color.parseColor("#7F00FF")));
                        textViewColorName.setTextColor(Color.BLACK);
                        imageButtonNextPage.setTextColor(Color.BLACK);
                        imageButtonPreviousPage.setTextColor(Color.BLACK);
                        imageButtonPreviousPage.setVisibility(View.INVISIBLE);
                        imageButtonNextPage.setVisibility(View.VISIBLE);
                        break;
                    case 1:
                        linearLayoutPagerTab.setBackground(Utility.getCustomShape(Color.parseColor("#4b0082")));
                        textViewColorName.setTextColor(Color.BLACK);
                        imageButtonNextPage.setTextColor(Color.BLACK);
                        imageButtonPreviousPage.setTextColor(Color.BLACK);
                        imageButtonPreviousPage.setVisibility(View.VISIBLE);
                        imageButtonNextPage.setVisibility(View.VISIBLE);
                        break;
                    case 2:
                        linearLayoutPagerTab.setBackground(Utility.getCustomShape(Color.BLUE));
                        textViewColorName.setTextColor(Color.BLACK);
                        imageButtonNextPage.setTextColor(Color.BLACK);
                        imageButtonPreviousPage.setTextColor(Color.BLACK);
                        imageButtonPreviousPage.setVisibility(View.VISIBLE);
                        imageButtonNextPage.setVisibility(View.VISIBLE);
                        break;
                    case 3:
                        linearLayoutPagerTab.setBackground(Utility.getCustomShape(Color.GREEN));
                        textViewColorName.setTextColor(Color.BLACK);
                        imageButtonNextPage.setTextColor(Color.BLACK);
                        imageButtonPreviousPage.setTextColor(Color.BLACK);
                        imageButtonPreviousPage.setVisibility(View.VISIBLE);
                        imageButtonNextPage.setVisibility(View.VISIBLE);
                        break;
                    case 4:
                        linearLayoutPagerTab.setBackground(Utility.getCustomShape(Color.parseColor("#FFD700")));
                        textViewColorName.setTextColor(Color.BLACK);
                        imageButtonNextPage.setTextColor(Color.BLACK);
                        imageButtonPreviousPage.setTextColor(Color.BLACK);
                        imageButtonPreviousPage.setVisibility(View.VISIBLE);
                        imageButtonNextPage.setVisibility(View.VISIBLE);
                        break;
                    case 5:
                        linearLayoutPagerTab.setBackground(Utility.getCustomShape(Color.parseColor("#FFA500")));
                        textViewColorName.setTextColor(Color.BLACK);
                        imageButtonNextPage.setTextColor(Color.BLACK);
                        imageButtonPreviousPage.setTextColor(Color.BLACK);
                        imageButtonPreviousPage.setVisibility(View.VISIBLE);
                        imageButtonNextPage.setVisibility(View.VISIBLE);
                        break;
                    case 6:
                        linearLayoutPagerTab.setBackground(Utility.getCustomShape(Color.RED));
                        textViewColorName.setTextColor(Color.BLACK);
                        imageButtonNextPage.setTextColor(Color.BLACK);
                        imageButtonPreviousPage.setTextColor(Color.BLACK);
                        imageButtonPreviousPage.setVisibility(View.VISIBLE);
                        imageButtonNextPage.setVisibility(View.VISIBLE);
                        break;
                    case 7:
                        linearLayoutPagerTab.setBackground(Utility.getCustomShape(Color.parseColor("#f4a460")));
                        textViewColorName.setTextColor(Color.BLACK);
                        imageButtonNextPage.setTextColor(Color.BLACK);
                        imageButtonPreviousPage.setTextColor(Color.BLACK);
                        imageButtonPreviousPage.setVisibility(View.VISIBLE);
                        imageButtonNextPage.setVisibility(View.VISIBLE);
                        break;
                    case 8:
                        linearLayoutPagerTab.setBackground(Utility.getCustomShape(Color.parseColor("#F5F5DC")));
                        textViewColorName.setTextColor(Color.BLACK);
                        imageButtonNextPage.setTextColor(Color.BLACK);
                        imageButtonPreviousPage.setTextColor(Color.BLACK);
                        imageButtonPreviousPage.setVisibility(View.VISIBLE);
                        imageButtonNextPage.setVisibility(View.VISIBLE);
                        break;
                    case 9:
                        linearLayoutPagerTab.setBackground(Utility.getCustomShape(Color.parseColor("#d3d3d3")));
                        textViewColorName.setTextColor(Color.BLACK);
                        imageButtonNextPage.setTextColor(Color.BLACK);
                        imageButtonPreviousPage.setTextColor(Color.BLACK);
                        imageButtonPreviousPage.setVisibility(View.VISIBLE);
                        imageButtonNextPage.setVisibility(View.VISIBLE);
                        break;
                    case 10:
                        linearLayoutPagerTab.setBackground(Utility.getCustomShape(Color.WHITE));
                        textViewColorName.setTextColor(Color.BLACK);
                        imageButtonNextPage.setTextColor(Color.BLACK);
                        imageButtonPreviousPage.setTextColor(Color.BLACK);
                        imageButtonPreviousPage.setVisibility(View.VISIBLE);
                        imageButtonNextPage.setVisibility(View.VISIBLE);

                        break;
                    case 11:
                        linearLayoutPagerTab.setBackground(Utility.getCustomShape(Color.BLACK));
                        textViewColorName.setTextColor(Color.WHITE);
                        imageButtonNextPage.setTextColor(Color.WHITE);
                        imageButtonPreviousPage.setTextColor(Color.WHITE);
                        imageButtonPreviousPage.setVisibility(View.VISIBLE);
                        imageButtonNextPage.setVisibility(View.GONE);
                        break;
                }
                break;
            case R.id.button_previous_page:
                if (pagerPagePosition < 0) {
                    return;
                }
                viewPagerColors.setCurrentItem(viewPagerColors.getCurrentItem() - 1, true);
                textViewColorName.setText(adapter.getPageTitle(viewPagerColors.getCurrentItem()));
                switch (pagerPagePosition) {
                    case 0:
                        linearLayoutPagerTab.setBackground(Utility.getCustomShape(Color.parseColor("#7F00FF")));
                        textViewColorName.setTextColor(Color.BLACK);
                        imageButtonNextPage.setTextColor(Color.BLACK);
                        imageButtonPreviousPage.setTextColor(Color.BLACK);
                        imageButtonPreviousPage.setVisibility(View.INVISIBLE);
                        imageButtonNextPage.setVisibility(View.VISIBLE);
                        break;
                    case 1:
                        linearLayoutPagerTab.setBackground(Utility.getCustomShape(Color.parseColor("#4b0082")));
                        textViewColorName.setTextColor(Color.BLACK);
                        imageButtonNextPage.setTextColor(Color.BLACK);
                        imageButtonPreviousPage.setTextColor(Color.BLACK);
                        imageButtonPreviousPage.setVisibility(View.VISIBLE);
                        imageButtonNextPage.setVisibility(View.VISIBLE);
                        break;
                    case 2:
                        linearLayoutPagerTab.setBackground(Utility.getCustomShape(Color.BLUE));
                        textViewColorName.setTextColor(Color.BLACK);
                        imageButtonNextPage.setTextColor(Color.BLACK);
                        imageButtonPreviousPage.setTextColor(Color.BLACK);
                        imageButtonPreviousPage.setVisibility(View.VISIBLE);
                        imageButtonNextPage.setVisibility(View.VISIBLE);
                        break;
                    case 3:
                        linearLayoutPagerTab.setBackground(Utility.getCustomShape(Color.GREEN));
                        textViewColorName.setTextColor(Color.BLACK);
                        imageButtonNextPage.setTextColor(Color.BLACK);
                        imageButtonPreviousPage.setTextColor(Color.BLACK);
                        imageButtonPreviousPage.setVisibility(View.VISIBLE);
                        imageButtonNextPage.setVisibility(View.VISIBLE);
                        break;
                    case 4:
                        linearLayoutPagerTab.setBackground(Utility.getCustomShape(Color.parseColor("#FFD700")));
                        textViewColorName.setTextColor(Color.BLACK);
                        imageButtonNextPage.setTextColor(Color.BLACK);
                        imageButtonPreviousPage.setTextColor(Color.BLACK);
                        imageButtonPreviousPage.setVisibility(View.VISIBLE);
                        imageButtonNextPage.setVisibility(View.VISIBLE);
                        break;
                    case 5:
                        linearLayoutPagerTab.setBackground(Utility.getCustomShape(Color.parseColor("#FFA500")));
                        textViewColorName.setTextColor(Color.BLACK);
                        imageButtonNextPage.setTextColor(Color.BLACK);
                        imageButtonPreviousPage.setTextColor(Color.BLACK);
                        imageButtonPreviousPage.setVisibility(View.VISIBLE);
                        imageButtonNextPage.setVisibility(View.VISIBLE);
                        break;
                    case 6:
                        linearLayoutPagerTab.setBackground(Utility.getCustomShape(Color.RED));
                        textViewColorName.setTextColor(Color.BLACK);
                        imageButtonNextPage.setTextColor(Color.BLACK);
                        imageButtonPreviousPage.setTextColor(Color.BLACK);
                        imageButtonPreviousPage.setVisibility(View.VISIBLE);
                        imageButtonNextPage.setVisibility(View.VISIBLE);
                        break;
                    case 7:
                        linearLayoutPagerTab.setBackground(Utility.getCustomShape(Color.parseColor("#f4a460")));
                        textViewColorName.setTextColor(Color.BLACK);
                        imageButtonNextPage.setTextColor(Color.BLACK);
                        imageButtonPreviousPage.setTextColor(Color.BLACK);
                        imageButtonPreviousPage.setVisibility(View.VISIBLE);
                        imageButtonNextPage.setVisibility(View.VISIBLE);
                        break;
                    case 8:
                        linearLayoutPagerTab.setBackground(Utility.getCustomShape(Color.parseColor("#F5F5DC")));
                        textViewColorName.setTextColor(Color.BLACK);
                        imageButtonNextPage.setTextColor(Color.BLACK);
                        imageButtonPreviousPage.setTextColor(Color.BLACK);
                        imageButtonPreviousPage.setVisibility(View.VISIBLE);
                        imageButtonNextPage.setVisibility(View.VISIBLE);
                        break;
                    case 9:
                        linearLayoutPagerTab.setBackground(Utility.getCustomShape(Color.parseColor("#d3d3d3")));
                        textViewColorName.setTextColor(Color.BLACK);
                        imageButtonNextPage.setTextColor(Color.BLACK);
                        imageButtonPreviousPage.setTextColor(Color.BLACK);
                        imageButtonPreviousPage.setVisibility(View.VISIBLE);
                        imageButtonNextPage.setVisibility(View.VISIBLE);
                        break;
                    case 10:
                        linearLayoutPagerTab.setBackground(Utility.getCustomShape(Color.WHITE));
                        textViewColorName.setTextColor(Color.BLACK);
                        imageButtonNextPage.setTextColor(Color.BLACK);
                        imageButtonPreviousPage.setTextColor(Color.BLACK);
                        imageButtonPreviousPage.setVisibility(View.VISIBLE);
                        imageButtonNextPage.setVisibility(View.VISIBLE);

                        break;
                    case 11:
                        linearLayoutPagerTab.setBackground(Utility.getCustomShape(Color.BLACK));
                        textViewColorName.setTextColor(Color.WHITE);
                        imageButtonNextPage.setTextColor(Color.WHITE);
                        imageButtonPreviousPage.setTextColor(Color.WHITE);
                        imageButtonPreviousPage.setVisibility(View.VISIBLE);
                        imageButtonNextPage.setVisibility(View.GONE);
                        break;
                }
                break;
            case R.id.button_hide_and_show_hlv:
                hideOrShowColorListView();
                break;
            case R.id.imagebutton_paint_undo://Erase function.
                erase();
                break;
            case R.id.imagebutton_paint_refresh:
                AlertDialog.Builder builder = new AlertDialog.Builder(this);
                builder.setTitle("Reset Paintings");
                builder.setMessage("Want to reset all the painting details?");
                builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        refreshPaintStatus();
                        dialogInterface.dismiss();
                    }
                });
                builder.setNegativeButton("No", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dialogInterface.dismiss();
                    }
                });
                builder.create().show();
                break;
            case R.id.imagebutton_paint_save:
                if (usedColorList.size() <= 0) {
                    Toast.makeText(this, "Please paint the image to save", Toast.LENGTH_SHORT)
                            .show();
                    return;
                }
                AlertDialog.Builder builder1 = new AlertDialog.Builder(this);
                builder1.setTitle("Save Image");
                builder1.setMessage("Do you want to save image?");
                builder1.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        ImageUtils.save(SnapAndPaintTabletActivity.this, rlRoom, usedColorList);
                        dialogInterface.dismiss();
                    }
                });
                builder1.setNegativeButton("No", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dialogInterface.dismiss();
                    }
                });
                builder1.create().show();
                break;
            case R.id.imagebutton_paint_share:
                Bitmap bm;
                rlRoom.setDrawingCacheEnabled(true);
                rlRoom.buildDrawingCache();
                bm = rlRoom.getDrawingCache();
                Bitmap bmCopy = bm.copy(Bitmap.Config.ARGB_8888, false);
                rlRoom.destroyDrawingCache();

                ((Global) getApplication()).setSnapAndPaintBitmap(bmCopy);
                startActivity(new Intent(this, ImagePreviewActivity.class));
              /*  AlertDialog.Builder imageDialog = new AlertDialog.Builder(this);
                LayoutInflater inflater = (LayoutInflater) this.getSystemService(LAYOUT_INFLATER_SERVICE);

                View layout = inflater.inflate(R.layout.layout_paint_preview,
                        (ViewGroup) findViewById(R.id.layout_root));
                ImageView image = (ImageView) layout.findViewById(R.id.fullimage);
                image.setImageBitmap(bmCopy);
                imageDialog.setView(layout);
                imageDialog.setPositiveButton("OK", new DialogInterface.OnClickListener() {

                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }

                });


                imageDialog.create();
                imageDialog.show();
                if (usedColorList.size() <= 0) {
                    Toast.makeText(this, "Please paint the image to share", Toast.LENGTH_SHORT)
                            .show();
                    return;
                }
                AlertDialog.Builder builder2 = new AlertDialog.Builder(this);
                builder2.setTitle("Share Image");
                builder2.setMessage("Want to share image?");
                builder2.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        ImageUtils.share(SnapAndPaintTabletActivity.this,
                                rlRoom, usedColorList);
                        dialogInterface.dismiss();
                    }
                });
                builder2.setNegativeButton("No", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dialogInterface.dismiss();
                    }
                });
                builder2.create().show();*/
                break;
            case R.id.imagebutton_paint_redo:
                //Not necessary.
                break;
            case R.id.imagebutton_go_to_gallery:
                Intent intentGallery = new Intent(SnapAndPaintTabletActivity.this,
                        GalleryActivity.class);
                intentGallery.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intentGallery);
                break;
        }
    }

    private void refreshPaintStatus() {
        rlRoom.removeAllViews();
        mv = new SnapAndPaintView(this, snappedBitmap);
        selectedColorsList.clear();
        paintColorAdapter = new TabletColorHorizontalListAdapter(getApplicationContext(), selectedColorsList, this);
        horizontalListView.setAdapter(paintColorAdapter);
        usedColorList.clear();
        mPaint = new Paint();
        mPaint.setAntiAlias(true);
        mPaint.setDither(true);
        mPaint.setAlpha(0x80);
        mPaint.setColor(getResources().getColor(android.R.color.transparent));
        mPaint.setStyle(Paint.Style.STROKE);
        mPaint.setStrokeJoin(Paint.Join.ROUND);
        mPaint.setStrokeCap(Paint.Cap.ROUND);
        mPaint.setStrokeWidth(12);
        listViewUsedColors.setAdapter(new UsedColorAdapter(this, usedColorList));
        rlRoom.addView(mv);
    }

    private void erase() {
        try {
            if (mPaint != null) {
                mPaint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.CLEAR));
            } else {
                Toast.makeText(getApplicationContext(), "Nothing to erase!", Toast.LENGTH_SHORT).show();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @SuppressWarnings("deprecation")
    public class SnapAndPaintView extends View {

        private Bitmap mBitmap, bmp, bm;
        private Canvas mCanvas;
        private Path mPath;
        private Paint mBitmapPaint;

        public SnapAndPaintView(Context c, Bitmap mbmp) {
            super(c);
            mPath = new Path();
            mBitmapPaint = new Paint(Paint.DITHER_FLAG);
            bmp = mbmp;

            xAdjustment = (screenWidth - bmp.getWidth()) / 2;
        }

        @Override
        protected void onSizeChanged(int w, int h, int oldw, int oldh) {
            super.onSizeChanged(w, h, oldw, oldh);

            mBitmap = Bitmap.createBitmap(bmp.copy(Bitmap.Config.ARGB_8888, true));
            mv.setBackgroundDrawable(new BitmapDrawable(mBitmap));//set background here

            bm = Bitmap.createBitmap(w, h, Bitmap.Config.ARGB_8888);

            AppLog.showLog(CLASS_TAG, "original bitmap width:: " + mBitmap.getWidth() + " height:: " + mBitmap.getHeight());
            mCanvas = new Canvas(bm);
        }

        @Override
        protected void onDraw(Canvas canvas) {
            super.onDraw(canvas);
            canvas.drawBitmap(bm, 0, 0, mBitmapPaint);

            if (isColorSelected) {
                usedColorId = mPaint.getColor();
            }

        }

        private float mX, mY;
        private static final float TOUCH_TOLERANCE = 4;

        private void touch_start(float x, float y) {
            mPath.reset();
            mPath.moveTo(x, y);
            mX = x;
            mY = y;

            try {
                mCanvas.drawPath(mPath, mPaint);
            } catch (Exception e) {
                AppLog.showLog(CLASS_TAG, "touch start error: " + e.getLocalizedMessage());
            }
        }

        private void touch_move(float x, float y) {
            float dx = Math.abs(x - mX);
            float dy = Math.abs(y - mY);
            if (dx >= TOUCH_TOLERANCE || dy >= TOUCH_TOLERANCE) {
                mPath.quadTo(mX, mY, (x + mX) / 2, (y + mY) / 2);
                mX = x;
                mY = y;
            }
            try {
                mCanvas.drawPath(mPath, mPaint);
            } catch (Exception e) {
                AppLog.showLog(CLASS_TAG, "touch move error: " + e.getLocalizedMessage());
            }

        }

        private void touch_up() {

            //GET THE COLOT AND SAVE TO DATABASE!!!
            Boolean isNewSelectedColor = true;

            //CONVERT COLOR INT TO HASH
            String colorHash = String.format("#%06X", 0xFFFFFF & usedColorId);
            AppLog.showLog(CLASS_TAG, "color Hash value:: " + colorHash);

            for (PaintColor p : usedColorList)
                try {
                    if (p.getColorCode().equalsIgnoreCase(colorHash)) {
                        isNewSelectedColor = false;
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                }

            if (isNewSelectedColor) {
                PaintColor pc = new PaintColor();
                pc.setColorCode(colorHash);

                String colorName = null;

                //set corresponding color name for colorHash!!
                for (PaintColor aGlobalPaintColorList : global.globalPaintColorList) {
                    if (aGlobalPaintColorList.getColorCode().equalsIgnoreCase(colorHash)) {
                        colorName = aGlobalPaintColorList.getName();
                    }
                }
                pc.setName(colorName);
                if (pc.getName() != null && !pc.getName().isEmpty()) {
                    usedColorList.add(pc);
                }
                listViewUsedColors.setAdapter(new UsedColorAdapter(SnapAndPaintTabletActivity.this, usedColorList));
            }

            try {
                mPath.lineTo(mX, mY);
                // commit the path to our offscreen
                mCanvas.drawPath(mPath, mPaint);
                // kill this so we don't double draw
                mPath.reset();

                mPaint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.SRC));
                mPaint.setAlpha(0x80);
                mPaint.setMaskFilter(null);

            } catch (Exception e) {
                AppLog.showLog(CLASS_TAG, "touch up error: " + e.getLocalizedMessage());
            }

        }

        @Override
        public boolean onTouchEvent(@NonNull MotionEvent event) {
            float x = event.getX();
            float y = event.getY();

            switch (event.getAction()) {
                case MotionEvent.ACTION_DOWN:
                    touch_start(x, y);
                    invalidate();
                    break;
                case MotionEvent.ACTION_MOVE:
                    touch_move(x, y);
                    invalidate();
                    break;
                case MotionEvent.ACTION_UP:
                    touch_up();
                    invalidate();
                    break;
            }
            return true;
        }
    }
}