package com.shirantech.pashupatipaints.activities.list.exteriors;

import android.content.Intent;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.view.DragEvent;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnDragListener;
import android.view.View.OnTouchListener;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.actionbarsherlock.app.ActionBar;
import com.actionbarsherlock.app.SherlockFragmentActivity;
import com.actionbarsherlock.view.Menu;
import com.actionbarsherlock.view.MenuItem;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.shirantech.pashupatipaints.R;
import com.shirantech.pashupatipaints.activities.list.ColorChooserActivity;
import com.shirantech.pashupatipaints.fragments.SelectedColorsFragment;
import com.shirantech.pashupatipaints.model.PaintColor;
import com.shirantech.pashupatipaints.util.AppLog;
import com.shirantech.pashupatipaints.util.AppText;
import com.shirantech.pashupatipaints.util.Global;
import com.shirantech.pashupatipaints.util.ImageUtils;

import java.util.List;

public class ExteriorTemplate3Activity extends SherlockFragmentActivity implements SelectedColorsFragment.onSelectedColorSelectedListener {

    private static final String CLASS_TAG = ExteriorTemplate3Activity.class.getSimpleName();
    private ImageView ivMainIma, ivWall1, ivWall2, ivWall3, ivWall4, ivWall5, ivWall6, ivWall7, ivWall8, ivWall9, ivWall10, ivWall11, ivWall12;
    private float redScale = 1f;
    private float greenScale = 0f;
    private float blueScale = 0f;
    private Boolean isColorClicked = false;


    private int ivWall1Red = -1, ivWall1Green = -1, ivWall1Blue = -1;
    private int ivWall2Red = -1, ivWall2Green = -1, ivWall2Blue = -1;
    private int ivWall3Red = -1, ivWall3Green = -1, ivWall3Blue = -1;
    private int ivWall4Red = -1, ivWall4Green = -1, ivWall4Blue = -1;
    private int ivWall5Red = -1, ivWall5Green = -1, ivWall5Blue = -1;
    private int ivWall6Red = -1, ivWall6Green = -1, ivWall6Blue = -1;
    private int ivWall7Red = -1, ivWall7Green = -1, ivWall7Blue = -1;
    private int ivWall8Red = -1, ivWall8Green = -1, ivWall8Blue = -1;
    private int ivWall9Red = -1, ivWall9Green = -1, ivWall9Blue = -1;
    private int ivWall10Red = -1, ivWall10Green = -1, ivWall10Blue = -1;
    private int ivWall11Red = -1, ivWall11Green = -1, ivWall11Blue = -1;
    private int ivWall12Red = -1, ivWall12Green = -1, ivWall12Blue = -1;


    RelativeLayout parent;

    Global global;
    private ImageLoader imageLoader = ImageLoader.getInstance();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_exteriors3);

        imageLoader.init(ImageLoaderConfiguration.createDefault(this));

        final ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setTitle("");

        parent = (RelativeLayout) findViewById(R.id.roomLayout_exterior3);

        global = (Global) getApplication();
        List<PaintColor> globalPaintColorList = global.getGlobalPaintColorList();

        AppLog.showLog(CLASS_TAG, "size of the global paintcolor list at Drawingroom2 :: " + globalPaintColorList.size());

        ivMainIma = (ImageView) findViewById(R.id.iv_exterior3_main_image);
        ivWall1 = (ImageView) findViewById(R.id.iv_exterior3_1);
        ivWall2 = (ImageView) findViewById(R.id.iv_exterior3_2);
        ivWall3 = (ImageView) findViewById(R.id.iv_exterior3_3);
        ivWall4 = (ImageView) findViewById(R.id.iv_exterior3_4);
        ivWall5 = (ImageView) findViewById(R.id.iv_exterior3_5);
        ivWall6 = (ImageView) findViewById(R.id.iv_exterior3_6);
        ivWall7 = (ImageView) findViewById(R.id.iv_exterior3_7);
        ivWall8 = (ImageView) findViewById(R.id.iv_exterior3_8);
        ivWall9 = (ImageView) findViewById(R.id.iv_exterior3_9);
        ivWall10 = (ImageView) findViewById(R.id.iv_exterior3_10);
        ivWall11 = (ImageView) findViewById(R.id.iv_exterior3_11);
        ivWall12 = (ImageView) findViewById(R.id.iv_exterior3_12);

        ImageUtils.loadImageUsingLoader(imageLoader, ivMainIma, "assets://" + AppText.EXTERIOR3_LOCATION_MAIN_IMAGE);
        ImageUtils.loadImageUsingLoader(imageLoader, ivWall1, "assets://" + AppText.EXTERIOR3_LOCATION_IMAGE1);
        ImageUtils.loadImageUsingLoader(imageLoader, ivWall2, "assets://" + AppText.EXTERIOR3_LOCATION_IMAGE2);
        ImageUtils.loadImageUsingLoader(imageLoader, ivWall3, "assets://" + AppText.EXTERIOR3_LOCATION_IMAGE3);
        ImageUtils.loadImageUsingLoader(imageLoader, ivWall4, "assets://" + AppText.EXTERIOR3_LOCATION_IMAGE4);
        ImageUtils.loadImageUsingLoader(imageLoader, ivWall5, "assets://" + AppText.EXTERIOR3_LOCATION_IMAGE5);
        ImageUtils.loadImageUsingLoader(imageLoader, ivWall6, "assets://" + AppText.EXTERIOR3_LOCATION_IMAGE6);
        ImageUtils.loadImageUsingLoader(imageLoader, ivWall7, "assets://" + AppText.EXTERIOR3_LOCATION_IMAGE7);
        ImageUtils.loadImageUsingLoader(imageLoader, ivWall8, "assets://" + AppText.EXTERIOR3_LOCATION_IMAGE8);
        ImageUtils.loadImageUsingLoader(imageLoader, ivWall9, "assets://" + AppText.EXTERIOR3_LOCATION_IMAGE9);
        ImageUtils.loadImageUsingLoader(imageLoader, ivWall10, "assets://" + AppText.EXTERIOR3_LOCATION_IMAGE10);
        ImageUtils.loadImageUsingLoader(imageLoader, ivWall11, "assets://" + AppText.EXTERIOR3_LOCATION_IMAGE11);
        ImageUtils.loadImageUsingLoader(imageLoader, ivWall12, "assets://" + AppText.EXTERIOR3_LOCATION_IMAGE12);

        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.HONEYCOMB) {
            //drag and drop!!
            ivWall1.setOnDragListener(new ChoiceDragListener());
        }
        ivWall1.setOnTouchListener(new OnTouchListener() {

            @Override
            public boolean onTouch(View v, MotionEvent event) {

                if (isColorClicked) {
                    int[] colors = ImageUtils.startPainting(ExteriorTemplate3Activity.this, ImageUtils.EXTERIOR_TEMPLATE_3, (int) event.getX(),
                            (int) event.getY(), ivMainIma, parent,
                            new float[]{redScale, greenScale, blueScale},
                            ivWall1, ivWall2, ivWall3, ivWall4, ivWall5, ivWall6, ivWall7, ivWall8, ivWall9,
                            ivWall10, ivWall11, ivWall12);
                    int ivId = colors[3];
                    if (ivId == R.id.iv_exterior3_1) {
                        ivWall1Red = colors[0];
                        ivWall1Green = colors[1];
                        ivWall1Blue = colors[2];
                    } else if (ivId == R.id.iv_exterior3_2) {
                        ivWall2Red = colors[0];
                        ivWall2Green = colors[1];
                        ivWall2Blue = colors[2];
                    } else if (ivId == R.id.iv_exterior3_3) {
                        ivWall3Red = colors[0];
                        ivWall3Green = colors[1];
                        ivWall3Blue = colors[2];
                    } else if (ivId == R.id.iv_exterior3_4) {
                        ivWall4Red = colors[0];
                        ivWall4Green = colors[1];
                        ivWall4Blue = colors[2];
                    } else if (ivId == R.id.iv_exterior3_5) {
                        ivWall5Red = colors[0];
                        ivWall5Green = colors[1];
                        ivWall5Blue = colors[2];
                    } else if (ivId == R.id.iv_exterior3_6) {
                        ivWall6Red = colors[0];
                        ivWall6Green = colors[1];
                        ivWall6Blue = colors[2];
                    } else if (ivId == R.id.iv_exterior3_7) {
                        ivWall7Red = colors[0];
                        ivWall7Green = colors[1];
                        ivWall7Blue = colors[2];
                    } else if (ivId == R.id.iv_exterior3_8) {
                        ivWall8Red = colors[0];
                        ivWall8Green = colors[1];
                        ivWall8Blue = colors[2];
                    } else if (ivId == R.id.iv_exterior3_9) {
                        ivWall9Red = colors[0];
                        ivWall9Green = colors[1];
                        ivWall9Blue = colors[2];
                    } else if (ivId == R.id.iv_exterior3_10) {
                        ivWall10Red = colors[0];
                        ivWall10Green = colors[1];
                        ivWall10Blue = colors[2];
                    } else if (ivId == R.id.iv_exterior3_11) {
                        ivWall11Red = colors[0];
                        ivWall11Green = colors[1];
                        ivWall11Blue = colors[2];
                    } else if (ivId == R.id.iv_exterior3_12) {
                        ivWall12Red = colors[0];
                        ivWall12Green = colors[1];
                        ivWall12Blue = colors[2];
                    }

                } else {
                    Toast.makeText(getApplicationContext(), "Please select the color below to paint the rooms", Toast.LENGTH_SHORT).show();
                }


                return false;
            }
        });

        //Determine density
        DisplayMetrics metrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(metrics);
        int density = metrics.densityDpi;

        if (density == DisplayMetrics.DENSITY_HIGH) {
            AppLog.showLog(CLASS_TAG, "DENSITY_HIGH... Density is " + String.valueOf(density));
        } else if (density == DisplayMetrics.DENSITY_MEDIUM) {
            AppLog.showLog(CLASS_TAG, "DENSITY_MEDIUM... Density is " + String.valueOf(density));
        } else if (density == DisplayMetrics.DENSITY_LOW) {
            AppLog.showLog(CLASS_TAG, "DENSITY_LOW... Density is " + String.valueOf(density));
        } else {
            AppLog.showLog(CLASS_TAG, "Density is neither HIGH, MEDIUM OR LOW.  Density is " + String.valueOf(density));

            parent.setScaleX(2.0f);
            parent.setScaleY(2.0f);

            parent.post(new Runnable() {

                @Override
                public void run() {
                    int h, w;
                    h = parent.getHeight();
                    w = parent.getWidth();

                    RelativeLayout.LayoutParams tableLP = new RelativeLayout.LayoutParams(w * 2, h * 2);
                    tableLP.addRule(RelativeLayout.CENTER_IN_PARENT, 1);
                    tableLP.addRule(RelativeLayout.ALIGN_PARENT_TOP, 1);
                    parent.setLayoutParams(tableLP);

                }
            });
        }

    }


    @Override
    public void onColorSelected(PaintColor paintColor) {
        isColorClicked = true;

        Float[] rgb = ImageUtils.hexToRgb(paintColor.getColorCode());

        redScale = rgb[0];
        greenScale = rgb[1];
        blueScale = rgb[2];

        AppLog.showLog(CLASS_TAG, "redScale:: " + redScale +
                " greenScale:: " + greenScale + " blueScale:: " + blueScale);


    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getSupportMenuInflater().inflate(R.menu.common_menu, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {

            case R.id.share:
                ImageUtils.shareTemplateImage(ExteriorTemplate3Activity.this, global,
                        ImageUtils.EXTERIOR_TEMPLATE_3, parent,
                        ivWall1Red, ivWall1Green, ivWall1Blue, ivWall2Red, ivWall2Green, ivWall2Blue, ivWall3Red, ivWall3Green, ivWall3Blue,
                        ivWall4Red, ivWall4Green, ivWall4Blue, ivWall5Red, ivWall5Green, ivWall5Blue, ivWall6Red, ivWall6Green, ivWall6Blue,
                        ivWall7Red, ivWall7Green, ivWall7Blue, ivWall8Red, ivWall8Green, ivWall8Blue, ivWall9Red, ivWall9Green, ivWall9Blue,
                        ivWall10Red, ivWall10Green, ivWall10Blue, ivWall11Red, ivWall11Green, ivWall11Blue, ivWall12Red, ivWall12Green, ivWall12Blue);
                break;

            case R.id.menu_save:
                ImageUtils.saveTemplateImage(ExteriorTemplate3Activity.this, global,
                        ImageUtils.EXTERIOR_TEMPLATE_3, parent, ivWall1Red, ivWall1Green, ivWall1Blue, ivWall2Red, ivWall2Green, ivWall2Blue, ivWall3Red, ivWall3Green, ivWall3Blue,
                        ivWall4Red, ivWall4Green, ivWall4Blue, ivWall5Red, ivWall5Green, ivWall5Blue, ivWall6Red, ivWall6Green, ivWall6Blue,
                        ivWall7Red, ivWall7Green, ivWall7Blue, ivWall8Red, ivWall8Green, ivWall8Blue, ivWall9Red, ivWall9Green, ivWall9Blue,
                        ivWall10Red, ivWall10Green, ivWall10Blue, ivWall11Red, ivWall11Green, ivWall11Blue, ivWall12Red, ivWall12Green, ivWall12Blue);
                break;

            case android.R.id.home:

                Intent colorChooserIntenta = new Intent(getApplicationContext(), ColorChooserActivity.class);
                colorChooserIntenta.putExtra("COLOR CHOOSER SOURCE", "TEMPLATE");
                colorChooserIntenta.putExtra("POSITION", 2);
                colorChooserIntenta.putExtra("TYPE", "exterior");
                colorChooserIntenta.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(colorChooserIntenta);
                break;

            case R.id.add_more_colors:
                Intent colorChooserIntent = new Intent(getApplicationContext(), ColorChooserActivity.class);
                colorChooserIntent.putExtra("COLOR CHOOSER SOURCE", "TEMPLATE");
                colorChooserIntent.putExtra("POSITION", 2);
                colorChooserIntent.putExtra("TYPE", "exterior");
                colorChooserIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(colorChooserIntent);

                break;

            default:
                break;
        }
        return super.onOptionsItemSelected(item);
    }


    /**
     * DragListener will handle dragged views being dropped on the drop area
     * - only the drop action will have processing added to it as we are not
     * - amending the default behavior for other parts of the drag process
     */
    private class ChoiceDragListener implements OnDragListener {

        @Override
        public boolean onDrag(View v, DragEvent event) {
            switch (event.getAction()) {
                case DragEvent.ACTION_DRAG_STARTED:
                    //no action necessary
                    break;
                case DragEvent.ACTION_DRAG_ENTERED:
                    //no action necessary
                    break;
                case DragEvent.ACTION_DRAG_EXITED:
                    //no action necessary
                    break;
                case DragEvent.ACTION_DROP:
                    int[] colors = ImageUtils.startPainting(ExteriorTemplate3Activity.this, ImageUtils.EXTERIOR_TEMPLATE_3, (int) event.getX(),
                            (int) event.getY(), ivMainIma, parent,
                            new float[]{redScale, greenScale, blueScale},
                            ivWall1, ivWall2, ivWall3, ivWall4, ivWall5, ivWall6, ivWall7, ivWall8, ivWall9,
                            ivWall10, ivWall11, ivWall12);
                    int ivId = colors[3];
                    if (ivId == R.id.iv_exterior3_1) {
                        ivWall1Red = colors[0];
                        ivWall1Green = colors[1];
                        ivWall1Blue = colors[2];
                    } else if (ivId == R.id.iv_exterior3_2) {
                        ivWall2Red = colors[0];
                        ivWall2Green = colors[1];
                        ivWall2Blue = colors[2];
                    } else if (ivId == R.id.iv_exterior3_3) {
                        ivWall3Red = colors[0];
                        ivWall3Green = colors[1];
                        ivWall3Blue = colors[2];
                    } else if (ivId == R.id.iv_exterior3_4) {
                        ivWall4Red = colors[0];
                        ivWall4Green = colors[1];
                        ivWall4Blue = colors[2];
                    } else if (ivId == R.id.iv_exterior3_5) {
                        ivWall5Red = colors[0];
                        ivWall5Green = colors[1];
                        ivWall5Blue = colors[2];
                    } else if (ivId == R.id.iv_exterior3_6) {
                        ivWall6Red = colors[0];
                        ivWall6Green = colors[1];
                        ivWall6Blue = colors[2];
                    } else if (ivId == R.id.iv_exterior3_7) {
                        ivWall7Red = colors[0];
                        ivWall7Green = colors[1];
                        ivWall7Blue = colors[2];
                    } else if (ivId == R.id.iv_exterior3_8) {
                        ivWall8Red = colors[0];
                        ivWall8Green = colors[1];
                        ivWall8Blue = colors[2];
                    } else if (ivId == R.id.iv_exterior3_9) {
                        ivWall9Red = colors[0];
                        ivWall9Green = colors[1];
                        ivWall9Blue = colors[2];
                    } else if (ivId == R.id.iv_exterior3_10) {
                        ivWall10Red = colors[0];
                        ivWall10Green = colors[1];
                        ivWall10Blue = colors[2];
                    } else if (ivId == R.id.iv_exterior3_11) {
                        ivWall11Red = colors[0];
                        ivWall11Green = colors[1];
                        ivWall11Blue = colors[2];
                    } else if (ivId == R.id.iv_exterior3_12) {
                        ivWall12Red = colors[0];
                        ivWall12Green = colors[1];
                        ivWall12Blue = colors[2];
                    }


                    //handle the dragged view being dropped over a drop view
                    View view = (View) event.getLocalState();

                    //stop displaying the view where it was before it was dragged
                    ////view.setVisibility(View.INVISIBLE);

                    //view dragged item is being dropped on
                    ImageView dropTarget = (ImageView) v;

                    //view being dragged and dropped
                    LinearLayout dropped = (LinearLayout) view;


                    //if an item has already been dropped here, there will be a tag
                    Object tag = dropTarget.getTag();

                    //set the tag in the target view being dropped on - to the ID of the view being dropped
                    dropTarget.setTag(dropped.getId());

                    break;
                case DragEvent.ACTION_DRAG_ENDED:
                    //no action necessary
                    break;
                default:
                    break;
            }
            return true;
        }
    }

}


