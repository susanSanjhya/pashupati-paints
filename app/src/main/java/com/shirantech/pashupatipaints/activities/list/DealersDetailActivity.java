package com.shirantech.pashupatipaints.activities.list;

import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.ProgressBar;

import com.actionbarsherlock.app.SherlockFragmentActivity;
import com.actionbarsherlock.view.MenuItem;
import com.shirantech.pashupatipaints.R;
import com.shirantech.pashupatipaints.adapter.DealersTabletListAdapter;
import com.shirantech.pashupatipaints.controller.GetDealerForTabletTask;
import com.shirantech.pashupatipaints.database.dao.DealerDao;
import com.shirantech.pashupatipaints.fragments.DealerFilterFragment;
import com.shirantech.pashupatipaints.fragments.DealerInfoFragment;
import com.shirantech.pashupatipaints.interfaces.ModelListGetter;
import com.shirantech.pashupatipaints.interfaces.OnFilterChangeListener;
import com.shirantech.pashupatipaints.model.PaintDealer;
import com.shirantech.pashupatipaints.util.AppLog;
import com.shirantech.pashupatipaints.util.Utility;

import java.sql.SQLException;
import java.util.List;

public class DealersDetailActivity extends SherlockFragmentActivity
        implements View.OnClickListener, OnFilterChangeListener, AdapterView.OnItemClickListener {


    private static final String CLASS_TAG = DealersDetailActivity.class.getSimpleName();

    private List<PaintDealer> dealerList;
    private GridView gridViewDealers;
    private ProgressBar progressBarDealerLoading;
    private LinearLayout linearLayoutSearch;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        if (Build.VERSION.SDK_INT <= Build.VERSION_CODES.HONEYCOMB) {
            getSupportActionBar().hide();
        }
        AppLog.showLog(CLASS_TAG, "is tablet? " + Utility.isTablet(this));
        if (Utility.isTablet(this)) {
            AppLog.showLog(CLASS_TAG, "is tablet");
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
        /*getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);*/
        } else {
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        }
        setContentView(R.layout.activity_dealers_detail);

        linearLayoutSearch = (LinearLayout) findViewById(R.id.linear_layout_search);
        linearLayoutSearch.setOnClickListener(this);
        gridViewDealers = (GridView) findViewById(R.id.gridview_dealers);
        progressBarDealerLoading = (ProgressBar) findViewById(R.id.progressbar_dealer_loading);
        ImageButton buttonHome = (ImageButton) findViewById(R.id.home);
        buttonHome.setOnClickListener(this);
        gridViewDealers.setOnItemClickListener(this);

        new GetDealerForTabletTask(this, new ModelListGetter<PaintDealer>() {
            @Override
            public void onTaskStarted() {
                progressBarDealerLoading.setVisibility(View.VISIBLE);
            }

            @Override
            public void onTaskFinished(List<PaintDealer> modelList) {
                progressBarDealerLoading.setVisibility(View.GONE);
                dealerList = modelList;
                if (modelList.size() > 0) {
                    showView(modelList);
                } else {
                    AppLog.showLog(CLASS_TAG, "dealerList for Category1 is null.");
                }
            }
        }).execute();

    }

    private void showResultForTablet(String query) {
        DealerDao dealerDao = new DealerDao(getApplicationContext());
        try {
            List<PaintDealer> searchedList = dealerDao.searchDealer((query != null ? query : "@@@@"));

            if (searchedList != null) {
                DealersTabletListAdapter dealersListAdapter = new DealersTabletListAdapter(this, searchedList);
                gridViewDealers.setAdapter(dealersListAdapter);
            }
        } catch (SQLException e) {
            AppLog.showLog(CLASS_TAG, "Dealer get Query error:" + e.getLocalizedMessage());
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        super.onOptionsItemSelected(item);

        switch (item.getItemId()) {
            case android.R.id.home:
                Intent intent = new Intent(this, StarterV2Activity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent);
                break;

            default:
                break;
        }

        return true;
    }

    private void showView(List<PaintDealer> dealerList) {
        DealersTabletListAdapter dealersListAdapter = new DealersTabletListAdapter(this, dealerList);
        gridViewDealers.setAdapter(dealersListAdapter);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.home:
                if (Utility.isTablet(this)) {
                    Intent intent = new Intent(this, StarterTabletActivity.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(intent);
                } else {
                    Intent intent = new Intent(this, StarterV2Activity.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(intent);
                }
                break;
            case R.id.linear_layout_search:
                DealerFilterFragment filterFragment = new DealerFilterFragment();
                filterFragment.show(getSupportFragmentManager(), "Dealer Search");
                break;
            case R.id.image_button_delete:

                showView(dealerList);
                break;
        /*    case R.id.textview_dealer_filter:
                DealerFilterFragment dealerFilterFragment = new DealerFilterFragment();
                dealerFilterFragment.show(getSupportFragmentManager(), "Filter By");
                break;*/

        }
    }

    /* @Override
     public void onFilterFinished(String branchName, String districtName, String cityName) {
         AppLog.showLog(CLASS_TAG, "OnFilterFinished() Branch: " + branchName
                 + ", District: " + districtName + ", City: " + cityName);
         new GetDealerByBranchDistrictCityTask(DealersDetailActivity.this, new ModelListGetter<PaintDealer>() {
             @Override
             public void onTaskStarted() {
                 progressBarDealerLoading.setVisibility(View.VISIBLE);
                 gridViewDealers.setAdapter(null);
             }

             @Override
             public void onTaskFinished(List<PaintDealer> modelList) {
                 progressBarDealerLoading.setVisibility(View.GONE);
                 AppLog.showLog(CLASS_TAG, "OnFilterFinished() modelList size: " + modelList.size());
                 showView(modelList);
             }
         }).execute(branchName, districtName, cityName);
     }
 */
    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        PaintDealer paintDealer = (PaintDealer) parent.getAdapter().getItem(position);
        DealerInfoFragment fragment = new DealerInfoFragment();
        Bundle bundle = new Bundle();
        bundle.putParcelable("DEALER_DETAIL", paintDealer);
        fragment.setArguments(bundle);
        fragment.show(getSupportFragmentManager(), "Dealer Detail");
    }

    @Override
    public void onFilterFinished(String toSearch) {
        showResultForTablet(toSearch + "*");
    }
}
