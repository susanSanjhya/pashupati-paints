package com.shirantech.pashupatipaints.activities.list;

import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;

import com.actionbarsherlock.app.SherlockFragmentActivity;
import com.shirantech.pashupatipaints.R;
import com.shirantech.pashupatipaints.customwidgets.carousel.CarouselDataItem;
import com.shirantech.pashupatipaints.customwidgets.carousel.CarouselView;
import com.shirantech.pashupatipaints.customwidgets.carousel.CarouselViewAdapter;
import com.shirantech.pashupatipaints.util.AppLog;
import com.shirantech.pashupatipaints.util.Global;
import com.shirantech.pashupatipaints.util.Utility;

import java.util.ArrayList;

public class StarterActivityV2 extends SherlockFragmentActivity implements AdapterView.OnItemClickListener {
    private static final String CLASS_TAG = StarterActivityV2.class.getSimpleName();
    Global m_Inst = Global.getInstance();
    ArrayList<CarouselDataItem> carouselItemList;
    private int coverFlowPosition;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Global.getInstance().InitGUIFrame(this);
        if (Utility.isTablet(this)) {
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
        } else {
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        }
        setContentView(R.layout.activity_starterv2);
        getSupportActionBar().hide();
        AppLog.showLog(CLASS_TAG, "which device " + getResources().getString(R.string.whichDevice));
        carouselItemList = new ArrayList<CarouselDataItem>();
        carouselItemList.add(new CarouselDataItem(R.drawable.start_painting, 0, "Start\nPainting"));
        carouselItemList.add(new CarouselDataItem(R.drawable.paint_calculator, 0, "Paint\nCalculator"));
        carouselItemList.add(new CarouselDataItem(R.drawable.authorized_dealers, 0, "Authorized\nDealers"));
        carouselItemList.add(new CarouselDataItem(R.drawable.ic_gallery, 0, "Gallery"));

        final CarouselView coverFlow = (CarouselView) findViewById(R.id.carousel);

        CarouselViewAdapter m_carouselAdapter = new CarouselViewAdapter(this, carouselItemList, m_Inst.Scale(getResources().getInteger(R.integer.carousel_view_image_width)), m_Inst.Scale(300));
        coverFlow.setAdapter(m_carouselAdapter);
        coverFlow.setSpacing(-1 * m_Inst.Scale(150));
        coverFlow.setSelection(100 / 2, true);
        coverFlow.setUnselectedAlpha(0.5f);
       coverFlow.setMaxRotationAngle(30);
        coverFlow.setAnimationDuration(1000);

        int childCount = coverFlow.getChildCount();
        AppLog.showLog(CLASS_TAG, "Child Count " + childCount);
        AppLog.showLog(CLASS_TAG, "Current child " + coverFlowPosition);
        coverFlow.setOnItemClickListener(this);
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        Log.e(StarterActivityV2.class.getSimpleName(), "Clicked on " + position);
        if (position >= carouselItemList.size()) {
            position = position % carouselItemList.size();
        }
        switch (position) {
            case 0:
                Intent paintingWaysIntent = new Intent(getApplicationContext(), PainingWaysActivity.class);

                startActivity(paintingWaysIntent);
                break;
            case 1:
                Intent paintCalculatorIntent = new Intent(getApplicationContext(), PaintCalculatorTabletActivity.class);
                startActivity(paintCalculatorIntent);
                break;
            case 2:
                Intent dealersDetailIntent = new Intent(getApplicationContext(), DealersDetailActivity.class);

                startActivity(dealersDetailIntent);
                break;
            case 3:
                Intent galleryIntent = new Intent(getApplicationContext(), GalleryActivity.class);

                startActivity(galleryIntent);
                break;
        }
    }
}
