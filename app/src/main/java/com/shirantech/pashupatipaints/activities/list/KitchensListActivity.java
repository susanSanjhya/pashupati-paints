package com.shirantech.pashupatipaints.activities.list;

import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.GridView;

import com.actionbarsherlock.app.SherlockActivity;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.shirantech.pashupatipaints.R;
import com.shirantech.pashupatipaints.activities.list.kitchens.Kitchens1TabletActivity;
import com.shirantech.pashupatipaints.activities.list.kitchens.Kitchens1V2Activity;
import com.shirantech.pashupatipaints.activities.list.kitchens.Kitchens2TabletActivity;
import com.shirantech.pashupatipaints.activities.list.kitchens.Kitchens2V2Activity;
import com.shirantech.pashupatipaints.activities.list.kitchens.Kitchens3TabletActivity;
import com.shirantech.pashupatipaints.activities.list.kitchens.Kitchens3V2Activity;
import com.shirantech.pashupatipaints.activities.list.kitchens.Kitchens4TabletActivity;
import com.shirantech.pashupatipaints.activities.list.kitchens.Kitchens4V2Activity;
import com.shirantech.pashupatipaints.activities.list.kitchens.Kitchens5TabletActivity;
import com.shirantech.pashupatipaints.activities.list.kitchens.Kitchens5V2Activity;
import com.shirantech.pashupatipaints.activities.list.kitchens.Kitchens6TabletActivity;
import com.shirantech.pashupatipaints.activities.list.kitchens.Kitchens6V2Activity;
import com.shirantech.pashupatipaints.activities.list.kitchens.Kitchens7TabletActivity;
import com.shirantech.pashupatipaints.activities.list.kitchens.Kitchens7V2Activity;
import com.shirantech.pashupatipaints.adapter.RoomCategoryListAdapter;
import com.shirantech.pashupatipaints.controller.GetRoomCategoriesTask;
import com.shirantech.pashupatipaints.interfaces.ModelListGetter;
import com.shirantech.pashupatipaints.model.RoomCategory;
import com.shirantech.pashupatipaints.util.Utility;

import java.util.List;

/**
 * Created by susan on 12/12/14.
 */
public class KitchensListActivity extends SherlockActivity implements ModelListGetter<RoomCategory>, AdapterView.OnItemClickListener {
    private String[] drawableArray = new String[]{
            "Kitchen/kitchen1/kitchen1_main.png",
            "Kitchen/kitchen2/kitchen2_main.png",
            "Kitchen/kitchen3/kitchen3_main.png",
            "Kitchen/kitchen4/kitchen4_main.png",
            "Kitchen/kitchen5/kitchen5_main.png",
            "Kitchen/kitchen6/kitchen6_main.png",
            "Dining/dining1/dining1_main.png"
    };
    private ImageLoader imageLoader = ImageLoader.getInstance();
    private GridView gridViewExteriors;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        requestWindowFeature(Window.FEATURE_NO_TITLE);
        if (Build.VERSION.SDK_INT <= Build.VERSION_CODES.HONEYCOMB) {
            getSupportActionBar().hide();
        }
        if (Utility.isTablet(this)) {
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
        } else {
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        }
        setContentView(R.layout.fragment_exteriors);
        imageLoader.init(ImageLoaderConfiguration.createDefault(this));
        findViewById(R.id.home).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(KitchensListActivity.this, PainingWaysActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
            }
        });
        gridViewExteriors = (GridView) findViewById(R.id.gridview_exterior);
        gridViewExteriors.setOnItemClickListener(this);
        new GetRoomCategoriesTask(this, this, "kitchens").execute(drawableArray);
    }

    @Override
    public void onTaskStarted() {

    }

    @Override
    public void onTaskFinished(List<RoomCategory> modelList) {
        gridViewExteriors.setAdapter(new RoomCategoryListAdapter(this, imageLoader, modelList));
    }

    @Override
    public void onItemClick(AdapterView<?> adapterView, View view, int position, long l) {
        switch (position) {
            case 0:
                if(Utility.isTablet(this)) {
                    Intent kitchen1Intent = new Intent(this, Kitchens1TabletActivity.class);
                    kitchen1Intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(kitchen1Intent);
                } else {
                    Intent kitchen1Intent = new Intent(this, Kitchens1V2Activity.class);
                    kitchen1Intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(kitchen1Intent);
                }
                break;
            case 1:
                if(Utility.isTablet(this)) {
                    Intent kitchen2Intent = new Intent(this, Kitchens2TabletActivity.class);
                    kitchen2Intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(kitchen2Intent);
                } else {
                    Intent kitchen2Intent = new Intent(this, Kitchens2V2Activity.class);
                    kitchen2Intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(kitchen2Intent);
                }
                break;
            case 2:
                if(Utility.isTablet(this)) {
                    Intent kitchen3Intent = new Intent(this, Kitchens3TabletActivity.class);
                    kitchen3Intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(kitchen3Intent);
                } else {
                    Intent kitchen3Intent = new Intent(this, Kitchens3V2Activity.class);
                    kitchen3Intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(kitchen3Intent);
                }
                break;
            case 3:
                if(Utility.isTablet(this)) {
                    Intent kitchen4Intent = new Intent(this, Kitchens4TabletActivity.class);
                    kitchen4Intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(kitchen4Intent);
                } else {
                    Intent kitchen4Intent = new Intent(this, Kitchens4V2Activity.class);
                    kitchen4Intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(kitchen4Intent);
                }
                break;
            case 4:
                if(Utility.isTablet(this)) {
                    Intent kitchen5Intent = new Intent(this, Kitchens5TabletActivity.class);
                    kitchen5Intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(kitchen5Intent);
                } else {
                    Intent kitchen5Intent = new Intent(this, Kitchens5V2Activity.class);
                    kitchen5Intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(kitchen5Intent);
                }
                break;
            case 5:
                if(Utility.isTablet(this)) {
                    Intent kitchen6Intent = new Intent(this, Kitchens6TabletActivity.class);
                    kitchen6Intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(kitchen6Intent);
                }else{
                    Intent kitchen6Intent = new Intent(this, Kitchens6V2Activity.class);
                    kitchen6Intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(kitchen6Intent);
                }
                break;
            case 6:
                if(Utility.isTablet(this)) {
                    Intent kitchen7Intent = new Intent(this, Kitchens7TabletActivity.class);
                    kitchen7Intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(kitchen7Intent);
                }else{
                    Intent kitchen7Intent = new Intent(this, Kitchens7V2Activity.class);
                    kitchen7Intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(kitchen7Intent);
                }
                break;
        }
    }
}
