package com.shirantech.pashupatipaints.activities.list;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.os.Vibrator;
import android.support.v4.view.ViewPager;
import android.view.View;
import android.view.Window;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.Toast;

import com.actionbarsherlock.app.SherlockFragmentActivity;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.shirantech.pashupatipaints.R;
import com.shirantech.pashupatipaints.adapter.GalleryPagerAdapter;
import com.shirantech.pashupatipaints.adapter.PaintedColorAdapter;
import com.shirantech.pashupatipaints.controller.GetPaintedImagesTask;
import com.shirantech.pashupatipaints.customwidgets.TouchImageView;
import com.shirantech.pashupatipaints.database.dao.PaintedImageDao;
import com.shirantech.pashupatipaints.fragments.GalleryFragment;
import com.shirantech.pashupatipaints.interfaces.ModelListGetter;
import com.shirantech.pashupatipaints.model.PaintedImage;
import com.shirantech.pashupatipaints.model.WallDescription;
import com.shirantech.pashupatipaints.util.AppLog;
import com.shirantech.pashupatipaints.util.ImageUtils;

import java.util.List;

/**
 * Created by susan on 7/9/15.
 */
public class GalleryV2Activity extends SherlockFragmentActivity
        implements ModelListGetter<PaintedImage>, GalleryFragment.OnGalleryClickListener {

    private static final java.lang.String CLASS_TAG = GalleryV2Activity.class.getSimpleName();
    private Vibrator mVibrator;
    private ImageLoader imageLoader = ImageLoader.getInstance();
    private LinearLayout linearLayoutImageGallery;
    private TouchImageView imageViewGallery;
    private ListView listViewUsedColors;
    public ViewPager pager;
    private boolean isPictureClicked = false;
    private List<PaintedImage> paintedImagesList;
    private GalleryPagerAdapter galleryClassAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        if (Build.VERSION.SDK_INT <= Build.VERSION_CODES.HONEYCOMB) {
            getSupportActionBar().hide();
        }
        setContentView(R.layout.activity_gallery_v2);
        imageLoader.init(ImageLoaderConfiguration.createDefault(this));
        mVibrator = (Vibrator) this.getSystemService(Context.VIBRATOR_SERVICE);

        linearLayoutImageGallery = (LinearLayout) findViewById(R.id.linear_layout_image_gallery);
        imageViewGallery = (TouchImageView) findViewById(R.id.image_view_gallery);
        listViewUsedColors = (ListView) findViewById(R.id.list_view_used_colors);

        pager = (ViewPager) findViewById(R.id.view_pager_carouse_gallery);

        findViewById(R.id.home).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(GalleryV2Activity.this, StarterV2Activity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
            }
        });

        new GetPaintedImagesTask(this, this).execute();
    }

    @Override
    public void onTaskStarted() {

    }

    @Override
    public void onTaskFinished(List<PaintedImage> paintedImageList) {
        paintedImagesList = paintedImageList;
        if (paintedImagesList != null && paintedImagesList.size() > 0) {
            galleryClassAdapter = new GalleryPagerAdapter(getSupportFragmentManager(), this, paintedImagesList);
            pager.setAdapter(galleryClassAdapter);
            pager.setCurrentItem(0, true);
            pager.setOnPageChangeListener(galleryClassAdapter);
            pager.setOffscreenPageLimit(3);
            pager.setPageMargin(-1 * (getResources().getInteger(R.integer.gallery_pager_margin)));
        } else {
            AppLog.showLog(CLASS_TAG, "List is empty");
            Toast.makeText(getApplicationContext(), "There is no saved image yet.", Toast.LENGTH_SHORT)
                    .show();
        }
    }

    @Override
    public void onGalleryClicked(int position) {
        isPictureClicked = true;
        pager.setVisibility(View.GONE);
        linearLayoutImageGallery.setVisibility(View.VISIBLE);
        ImageUtils.loadImageUsingLoader(imageLoader, imageViewGallery, "file:///mnt/sdcard/PashupatiPaints/"
                + paintedImagesList.get(position).getName() + ".png");
        List<WallDescription> wallDescriptionList;
        wallDescriptionList = paintedImagesList.get(position).getWallDescriptionList();
        listViewUsedColors.setAdapter(new PaintedColorAdapter(GalleryV2Activity.this, wallDescriptionList));
    }

    @Override
    public void onGalleryLongClicked(int position) {
        long VIBRATE_DURATION = 30;
        mVibrator.vibrate(VIBRATE_DURATION);

        final PaintedImage pc = paintedImagesList.get(position);

        AlertDialog.Builder builder = new AlertDialog.Builder(GalleryV2Activity.this);
        builder.setMessage(
                "Delete image from Gallery?")
                // Prevents user to use "back button"
                .setCancelable(false)
                .setPositiveButton("Delete",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                AppLog.showLog(CLASS_TAG, "OK clicked!! id: " + id);

                                deletedPaintedImageRecord(pc);
                            }
                        }
                )
                .setNegativeButton("Cancel",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                AppLog.showLog(CLASS_TAG, "cancel clicked!! id: " + id);
                                dialog.cancel();
                            }
                        }
                );
        builder.show();
    }

    private void deletedPaintedImageRecord(PaintedImage c) {
        try {
            int position = paintedImagesList.indexOf(c);
            PaintedImageDao paintedImageDao = new PaintedImageDao(this);
            paintedImageDao.deletePaintedImage(c);
            galleryClassAdapter.remove(c); // remove from adapter
            galleryClassAdapter.clearAll();
            paintedImagesList.clear();
            paintedImagesList = paintedImageDao.getAllPaintedImages();
            if (paintedImagesList.size() <= 0) {
                Toast.makeText(getApplicationContext(), "There is no saved image yet.", Toast.LENGTH_SHORT)
                        .show();
                pager.setAdapter(null);
                pager.setAdapter(new GalleryPagerAdapter(getSupportFragmentManager(), this, paintedImagesList));
//                galleryClassAdapter.notifyDataSetChanged();
            } else {
                pager.setAdapter(null);
                pager.setAdapter(new GalleryPagerAdapter(getSupportFragmentManager(), this, paintedImagesList));
                if(position != 0) {
                    pager.setCurrentItem(position - 1);
                }else{
                    pager.setCurrentItem(0);
                }
//                galleryClassAdapter.notifyDataSetChanged(); // notify to UI
            }
            Toast.makeText(this, "Record deleted!!", Toast.LENGTH_SHORT).show();

        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @Override
    public void onBackPressed() {
        if (isPictureClicked) {
            isPictureClicked = false;
            pager.setVisibility(View.VISIBLE);
            linearLayoutImageGallery.setVisibility(View.GONE);
        } else {
            finish();
        }
    }
}
