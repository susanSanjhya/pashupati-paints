package com.shirantech.pashupatipaints.activities.list;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentPagerAdapter;

import com.actionbarsherlock.app.ActionBar;
import com.actionbarsherlock.app.SherlockFragmentActivity;
import com.actionbarsherlock.view.MenuItem;
import com.shirantech.pashupatipaints.R;
import com.shirantech.pashupatipaints.adapter.RoomCategoryPagerAdapter;
import com.shirantech.pashupatipaints.util.AppLog;

public class RoomCategoryActivity extends SherlockFragmentActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_room_category);
        AppLog.showLog(RoomCategoryActivity.class.getSimpleName(), "RoomCategoryActivity started.");

        getSupportActionBar().setTitle("");
        final ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);
        FragmentPagerAdapter adapter = new RoomCategoryPagerAdapter(getSupportFragmentManager());
//        ViewPager pager = (ViewPager) findViewById(R.id.pager_room);
//        pager.setAdapter(adapter);
//        TabPageIndicator indicator = (TabPageIndicator) findViewById(R.id.indicator_room);
//        indicator.setViewPager(pager);
//        pager.setCurrentItem(0);

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        super.onOptionsItemSelected(item);

        switch (item.getItemId()) {
            case android.R.id.home:
                Intent intent = new Intent(this, PainingWaysActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
                break;

            default:
                break;
        }

        return true;
    }

}
