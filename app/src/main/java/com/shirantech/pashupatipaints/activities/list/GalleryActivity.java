package com.shirantech.pashupatipaints.activities.list;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.os.Build;
import android.os.Bundle;
import android.os.Vibrator;
import android.view.View;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.AdapterView.OnItemLongClickListener;
import android.widget.GridView;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.Toast;

import com.actionbarsherlock.app.SherlockFragmentActivity;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.shirantech.pashupatipaints.R;
import com.shirantech.pashupatipaints.adapter.GalleryCarouselAdapter;
import com.shirantech.pashupatipaints.adapter.PaintedColorAdapter;
import com.shirantech.pashupatipaints.adapter.TabletGalleryImageAdapter;
import com.shirantech.pashupatipaints.controller.GetPaintedImagesTask;
import com.shirantech.pashupatipaints.customwidgets.TouchImageView;
import com.shirantech.pashupatipaints.customwidgets.carousel.CarouselView;
import com.shirantech.pashupatipaints.database.dao.PaintedImageDao;
import com.shirantech.pashupatipaints.interfaces.ModelListGetter;
import com.shirantech.pashupatipaints.model.PaintedImage;
import com.shirantech.pashupatipaints.model.WallDescription;
import com.shirantech.pashupatipaints.util.AppLog;
import com.shirantech.pashupatipaints.util.Global;
import com.shirantech.pashupatipaints.util.ImageUtils;
import com.shirantech.pashupatipaints.util.Utility;

import java.util.List;

public class GalleryActivity extends SherlockFragmentActivity implements ModelListGetter<PaintedImage>, View.OnClickListener {

    private static final String CLASS_TAG = GalleryActivity.class.getSimpleName();
    //    private ProgressBar pb;
    private List<PaintedImage> paintedImagesList;
    CarouselView carouselView;
    private ListView imageListView, listViewGallery;
    private ImageView imageViewGalleryPicture;
    private GridView gridViewColorUsed;
    private GalleryCarouselAdapter galleryClassAdapter;
    private TabletGalleryImageAdapter tabletGalleryImageAdapter;
    private LinearLayout linearLayoutImageGallery;
    private TouchImageView imageViewGallery;
    private ListView listViewUsedColors;
    private boolean isPictureClicked = false;

    private Vibrator mVibrator;
    private static final int VIBRATE_DURATION = 35;
    private ImageLoader imageLoader = ImageLoader.getInstance();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        requestWindowFeature(Window.FEATURE_NO_TITLE);
        if (Build.VERSION.SDK_INT <= Build.VERSION_CODES.HONEYCOMB) {
            getSupportActionBar().hide();
        }
        if (Utility.isTablet(this)) {
            AppLog.showLog(CLASS_TAG, "is tablet");
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
        /*getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);*/
        } else {
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        }
        setContentView(R.layout.activity_gallery);
        AppLog.showLog(CLASS_TAG, "Gallery Activity Started");
        imageLoader.init(ImageLoaderConfiguration.createDefault(this));
        mVibrator = (Vibrator) this.getSystemService(Context.VIBRATOR_SERVICE);
        if (!Utility.isTablet(this)) {
            /*imageListView = (ListView) findViewById(R.id.imageList);

            imageListView.setOnItemClickListener(new OnItemClickListener() {

                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                    AppLog.showLog(CLASS_TAG, "clicked position:: " + position + " class id:: " + paintedImagesList.get(position).getName());
                    Intent i = new Intent(getApplicationContext(), FullImageActivity.class);
                    i.putExtra("imageName", paintedImagesList.get(position).getName());
                    startActivity(i);
                }
            });


            // On a long press, we vibrate so the user gets some feedback.

            imageListView.setOnItemLongClickListener(new OnItemLongClickListener() {

                @Override
                public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {

                    mVibrator.vibrate(VIBRATE_DURATION);

                    final PaintedImage pc = galleryClassAdapter.getItem(position);

                    AlertDialog.Builder builder = new AlertDialog.Builder(GalleryActivity.this);
                    builder.setMessage(
                            "Delete image from Gallery?")
                            .setCancelable(false)
                                    // Prevents user to use "back button"
                            .setPositiveButton("Delete",
                                    new DialogInterface.OnClickListener() {
                                        public void onClick(DialogInterface dialog, int id) {
                                            AppLog.showLog(CLASS_TAG, "OK clicked!! id: " + id);
                                        *//* delete *//*
                                            deletedPaintedImageRecord(pc);
                                        }
                                    }
                            )
                            .setNegativeButton("Cancel",
                                    new DialogInterface.OnClickListener() {
                                        public void onClick(DialogInterface dialog, int id) {
                                            AppLog.showLog(CLASS_TAG, "cancel clicked!! id: " + id);
                                            dialog.cancel();
                                        }
                                    }
                            );
                    builder.show();
                    return false;

                }
            });

            final ActionBar actionBar = getSupportActionBar();
            actionBar.setDisplayHomeAsUpEnabled(true);
            actionBar.setTitle("");*/

            linearLayoutImageGallery = (LinearLayout) findViewById(R.id.linear_layout_image_gallery);
            imageViewGallery = (TouchImageView) findViewById(R.id.image_view_gallery);
            listViewUsedColors = (ListView) findViewById(R.id.list_view_used_colors);

            carouselView = (CarouselView) findViewById(R.id.carousel);
            carouselView.setSpacing(-1 * Global.getInstance().Scale(100));
            carouselView.setSelection(0, true);
            carouselView.setAnimationDuration(1000);
            carouselView.setMaxRotationAngle(30);

            carouselView.setOnItemClickListener(new OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                    isPictureClicked = true;
                    PaintedImage paintedImage = (PaintedImage) parent.getAdapter().getItem(position);
                    carouselView.setVisibility(View.GONE);
                    linearLayoutImageGallery.setVisibility(View.VISIBLE);
                    ImageUtils.loadImageUsingLoader(imageLoader, imageViewGallery, "file:///mnt/sdcard/PashupatiPaints/"
                            + paintedImage.getName() + ".png");
                    List<WallDescription> wallDescriptionList;
                    wallDescriptionList = paintedImage.getWallDescriptionList();
                    listViewUsedColors.setAdapter(new PaintedColorAdapter(GalleryActivity.this, wallDescriptionList));
                }
            });

            carouselView.setOnItemLongClickListener(new OnItemLongClickListener() {
                @Override
                public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {
                    mVibrator.vibrate(VIBRATE_DURATION);

                    final PaintedImage pc = (PaintedImage) galleryClassAdapter.getItem(position);

                    AlertDialog.Builder builder = new AlertDialog.Builder(GalleryActivity.this);
                    builder.setMessage(
                            "Delete image from Gallery?")
                            .setCancelable(false)
                                    // Prevents user to use "back button"
                            .setPositiveButton("Delete",
                                    new DialogInterface.OnClickListener() {
                                        public void onClick(DialogInterface dialog, int id) {
                                            AppLog.showLog(CLASS_TAG, "OK clicked!! id: " + id);
                                        /* delete */
                                            deletedPaintedImageRecord(pc);
                                        }
                                    }
                            )
                            .setNegativeButton("Cancel",
                                    new DialogInterface.OnClickListener() {
                                        public void onClick(DialogInterface dialog, int id) {
                                            AppLog.showLog(CLASS_TAG, "cancel clicked!! id: " + id);
                                            dialog.cancel();
                                        }
                                    }
                            );
                    builder.show();
                    return false;
                }
            });
        } else {

            gridViewColorUsed = (GridView) findViewById(R.id.gridview_gallery_color_used);
            imageViewGalleryPicture = (ImageView) findViewById(R.id.imageview_gallery_picture);
            imageViewGalleryPicture.setOnClickListener(this);
            listViewGallery = (ListView) findViewById(R.id.listview_gallery);
            listViewGallery.setOnItemLongClickListener(new OnItemLongClickListener() {
                @Override
                public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {
                    mVibrator.vibrate(VIBRATE_DURATION);

                    final PaintedImage pc = tabletGalleryImageAdapter.getItem(position);

                    AlertDialog.Builder builder = new AlertDialog.Builder(GalleryActivity.this);
                    builder.setMessage(
                            "Delete image from Gallery?")
                            .setCancelable(false)
                                    // Prevents user to use "back button"
                            .setPositiveButton("Delete",
                                    new DialogInterface.OnClickListener() {
                                        public void onClick(DialogInterface dialog, int id) {
                                            AppLog.showLog(CLASS_TAG, "OK clicked!! id: " + id);
                                        /* delete */
                                            deletedPaintedImageRecord(pc);
                                        }
                                    }
                            )
                            .setNegativeButton("Cancel",
                                    new DialogInterface.OnClickListener() {
                                        public void onClick(DialogInterface dialog, int id) {
                                            AppLog.showLog(CLASS_TAG, "cancel clicked!! id: " + id);
                                            dialog.cancel();
                                        }
                                    }
                            );
                    builder.show();
                    return false;
                }
            });

            listViewGallery.setOnItemClickListener(new OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                    PaintedImage paintedImage = paintedImagesList.get(i);
                    ImageUtils.loadImageUsingLoader(imageLoader, imageViewGalleryPicture,
                            "file:///mnt/sdcard/PashupatiPaints/" + paintedImage.getName() + ".png");
                    List<WallDescription> wallDescriptionList;
                    wallDescriptionList = paintedImage.getWallDescriptionList();
                    gridViewColorUsed.setAdapter(new PaintedColorAdapter(GalleryActivity.this, wallDescriptionList));
                }
            });


        }
        ImageButton buttonHome = (ImageButton) findViewById(R.id.home);
        buttonHome.setOnClickListener(this);
//        pb = (ProgressBar) findViewById(R.id.progressBar_images_saved);
        new GetPaintedImagesTask(this, this).execute();
    }

    private boolean wallPainted(WallDescription wall1) {
        return !wall1.getColorName().equalsIgnoreCase(getString(R.string.text_wall_not_painted));
    }

    private void deletedPaintedImageRecord(PaintedImage c) {
        try {

            PaintedImageDao paintedImageDao = new PaintedImageDao(this);
            paintedImageDao.deletePaintedImage(c);
            if (!Utility.isTablet(this)) {
                galleryClassAdapter.remove(c); // remove from adapter
                paintedImagesList = paintedImageDao.getAllPaintedImages();
                if (paintedImagesList.size() <= 0) {
                    Toast.makeText(getApplicationContext(), "There is no saved image yet.", Toast.LENGTH_SHORT)
                            .show();
                    carouselView.setAdapter(new GalleryCarouselAdapter(this, imageLoader, paintedImagesList, Global.getInstance().Scale(400), Global.getInstance().Scale(300)));
                } else {
                    galleryClassAdapter.notifyDataSetChanged(); // notify to UI
                }
            } else {
                tabletGalleryImageAdapter.remove(c);
                tabletGalleryImageAdapter.notifyDataSetChanged();
            }
            Toast.makeText(this, "Record deleted!!", Toast.LENGTH_SHORT).show();

        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @Override
    public void onTaskStarted() {
//        pb.setVisibility(View.VISIBLE);
    }

    @Override
    public void onTaskFinished(List<PaintedImage> paintedImageList) {
        paintedImagesList = paintedImageList;
        if (paintedImagesList != null && paintedImagesList.size() > 0) {
//            pb.setVisibility(View.GONE);
            if (!Utility.isTablet(this)) {
                galleryClassAdapter = new GalleryCarouselAdapter(this, imageLoader, paintedImagesList, Global.getInstance().Scale(400), Global.getInstance().Scale(300));
                carouselView.setAdapter(galleryClassAdapter);
//                carouselView.setSelection(paintedImageList.size() / 2, true);
            } else {
                tabletGalleryImageAdapter = new TabletGalleryImageAdapter(GalleryActivity.this, paintedImageList);
                listViewGallery.setAdapter(tabletGalleryImageAdapter);
                PaintedImage paintedImage = paintedImagesList.get(0);
                ImageUtils.loadImageUsingLoader(imageLoader, imageViewGalleryPicture,
                        "file:///mnt/sdcard/PashupatiPaints/" + paintedImage.getName() + ".png");
                List<WallDescription> wallDescriptionList;
                wallDescriptionList = paintedImage.getWallDescriptionList();
                gridViewColorUsed.setAdapter(new PaintedColorAdapter(GalleryActivity.this, wallDescriptionList));
            }
        } else {
//            pb.setVisibility(View.GONE);
            AppLog.showLog(CLASS_TAG, "List is empty");
            Toast.makeText(getApplicationContext(), "There is no saved image yet.", Toast.LENGTH_SHORT)
                    .show();
        }
    }

    @Override
    public void onBackPressed() {
        if (isPictureClicked) {
            isPictureClicked = false;
            carouselView.setVisibility(View.VISIBLE);
            linearLayoutImageGallery.setVisibility(View.GONE);
        } else {
            finish();
        }
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.home:
                if (Utility.isTablet(this)) {
                    Intent intent = new Intent(this, StarterTabletActivity.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(intent);
                } else {
                    if (isPictureClicked) {
                        isPictureClicked = false;
                        carouselView.setVisibility(View.VISIBLE);
                        linearLayoutImageGallery.setVisibility(View.GONE);
                    } else {
                        Intent intent = new Intent(this, StarterActivityV2.class);
                        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        startActivity(intent);
                    }
                }
                break;
            case R.id.imageview_gallery_picture:
                Bitmap bitmap = ((BitmapDrawable) imageViewGalleryPicture.getDrawable()).getBitmap();
                ((Global) getApplication()).setSnapAndPaintBitmap(bitmap);
                startActivity(new Intent(this, ImagePreviewActivity.class));
                break;
        }
    }
}
