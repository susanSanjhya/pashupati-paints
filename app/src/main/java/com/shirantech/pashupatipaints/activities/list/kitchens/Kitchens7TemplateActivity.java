package com.shirantech.pashupatipaints.activities.list.kitchens;

import android.content.Intent;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.view.DragEvent;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnDragListener;
import android.view.View.OnTouchListener;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.actionbarsherlock.app.ActionBar;
import com.actionbarsherlock.app.SherlockFragmentActivity;
import com.actionbarsherlock.view.Menu;
import com.actionbarsherlock.view.MenuItem;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.shirantech.pashupatipaints.R;
import com.shirantech.pashupatipaints.activities.list.ColorChooserActivity;
import com.shirantech.pashupatipaints.fragments.SelectedColorsFragment;
import com.shirantech.pashupatipaints.model.PaintColor;
import com.shirantech.pashupatipaints.util.AppLog;
import com.shirantech.pashupatipaints.util.AppText;
import com.shirantech.pashupatipaints.util.Global;
import com.shirantech.pashupatipaints.util.ImageUtils;

import java.util.List;

public class Kitchens7TemplateActivity extends SherlockFragmentActivity
        implements SelectedColorsFragment.onSelectedColorSelectedListener {

    private static final String CLASS_TAG = Kitchens7TemplateActivity.class.getSimpleName();
    private ImageView ivMainImage, ivLeftWall, ivTop, ivRightWall, ivBottom, ivCorner;
    private float redScale = 1f, greenScale = 0f, blueScale = 0f;
    private Boolean isColorClicked = false;

    private int leftRed = -1, leftGreen = -1, leftBlue = -1;
    private int ceilingRed = -1, ceilingGreen = -1, ceilingBlue = -1;
    private int rightRed = -1, rightGreen = -1, rightBlue = -1;
    private int bottomRed = -1, bottomGreen = -1, bottomBlue = -1;
    private int cornerRed = -1, cornerGreen = -1, cornerBlue = -1;

    RelativeLayout parent;

    Global global;

    private ImageLoader imageLoader = ImageLoader.getInstance();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_kitchen_room7);

        imageLoader.init(ImageLoaderConfiguration.createDefault(this));

        final ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setTitle("");

        parent = (RelativeLayout) findViewById(R.id.roomLayout_kitchen_7);

        global = (Global) getApplication();
        List<PaintColor> globalPaintColorList = global.getGlobalPaintColorList();

        System.out
                .println("size of the global paintcolor list at Drawingroom2 :: "
                        + globalPaintColorList.size());

        ivMainImage = (ImageView) findViewById(R.id.iv_kitchen7_main_image);
        ivLeftWall = (ImageView) findViewById(R.id.iv_kitchen7_left_wall);
        ivTop = (ImageView) findViewById(R.id.iv_kitchen7_ceiling);
        ivRightWall = (ImageView) findViewById(R.id.iv_kitchen7_right_wall);
        ivBottom = (ImageView) findViewById(R.id.iv_kitchen7_bottom);
        ivCorner = (ImageView) findViewById(R.id.iv_kitchen7_corner);

        /*ivMainImage.setImageBitmap(ImageUtils.getBitmapFromAsset(
                getApplicationContext(), AppText.KITCHEN7_LOCATION_MAIN, 1));
        ivLeftWall.setImageBitmap(ImageUtils.getBitmapFromAsset(
                getApplicationContext(), AppText.KITCHEN7_LOCATION_LEFT, 1));
        ivTop.setImageBitmap(ImageUtils.getBitmapFromAsset(
                getApplicationContext(), AppText.KITCHEN7_LOCATION_TOP, 1));
        ivRightWall.setImageBitmap(ImageUtils.getBitmapFromAsset(
                getApplicationContext(), AppText.KITCHEN7_LOCATION_RIGHT, 1));
        ivBottom.setImageBitmap(ImageUtils.getBitmapFromAsset(
                getApplicationContext(), AppText.KITCHEN7_LOCATION_BOTTOM, 1));
        ivCorner.setImageBitmap(ImageUtils.getBitmapFromAsset(
                getApplicationContext(), AppText.KITCHEN7_LOCATION_CORNER, 1));*/

        ImageUtils.loadImageUsingLoader(imageLoader, ivMainImage, "assets://" + AppText.KITCHEN7_LOCATION_MAIN);
        ImageUtils.loadImageUsingLoader(imageLoader, ivLeftWall, "assets://" + AppText.KITCHEN7_LOCATION_LEFT);
        ImageUtils.loadImageUsingLoader(imageLoader, ivTop, "assets://" + AppText.KITCHEN7_LOCATION_TOP);
        ImageUtils.loadImageUsingLoader(imageLoader, ivRightWall, "assets://" + AppText.KITCHEN7_LOCATION_RIGHT);
        ImageUtils.loadImageUsingLoader(imageLoader, ivBottom, "assets://" + AppText.KITCHEN7_LOCATION_BOTTOM);
        ImageUtils.loadImageUsingLoader(imageLoader, ivCorner, "assets://" + AppText.KITCHEN7_LOCATION_CORNER);

        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.HONEYCOMB) {
            // drag and drop!!
            ivCorner.setOnDragListener(new ChoiceDragListener());
        }
        ivCorner.setOnTouchListener(new OnTouchListener() {

            @Override
            public boolean onTouch(View v, MotionEvent event) {

                if (isColorClicked) {
                    int[] colors = ImageUtils.startPainting(Kitchens7TemplateActivity.this, ImageUtils.KITCHEN_TEMPLATE_7,
                            (int) event.getX(), (int) event.getY(), ivMainImage, parent,
                            new float[]{redScale, greenScale, blueScale},
                            ivLeftWall, ivTop, ivRightWall, ivBottom, ivCorner);
                    int ivId = colors[3];
                    if (ivId == R.id.iv_kitchen7_left_wall) {
                        leftRed = colors[0];
                        leftGreen = colors[1];
                        leftBlue = colors[2];
                    } else if (ivId == R.id.iv_kitchen7_ceiling) {
                        ceilingRed = colors[0];
                        ceilingGreen = colors[1];
                        ceilingBlue = colors[2];
                    } else if (ivId == R.id.iv_kitchen7_right_wall) {
                        rightRed = colors[0];
                        rightGreen = colors[1];
                        rightBlue = colors[2];
                    } else if (ivId == R.id.iv_kitchen7_bottom) {
                        bottomRed = colors[0];
                        bottomGreen = colors[1];
                        bottomBlue = colors[2];
                    } else if (ivId == R.id.iv_kitchen7_corner) {
                        cornerRed = colors[0];
                        cornerGreen = colors[1];
                        cornerBlue = colors[2];
                    }

                } else {
                    Toast.makeText(getApplicationContext(),
                            "Please select the color below to paint the rooms",
                            Toast.LENGTH_SHORT).show();
                }

                return false;
            }
        });

        // Determine density
        DisplayMetrics metrics = new DisplayMetrics();

        getWindowManager().getDefaultDisplay().getMetrics(metrics);
        int density = metrics.densityDpi;
        if (density == DisplayMetrics.DENSITY_HIGH) {
            AppLog.showLog(CLASS_TAG, "DENSITY_HIGH... Density is "
                    + String.valueOf(density));
        } else if (density == DisplayMetrics.DENSITY_MEDIUM) {
            AppLog.showLog(CLASS_TAG, "DENSITY_MEDIUM... Density is "
                    + String.valueOf(density));
        } else if (density == DisplayMetrics.DENSITY_LOW) {
            AppLog.showLog(CLASS_TAG, "DENSITY_LOW... Density is "
                    + String.valueOf(density));
        } else {
            AppLog.showLog(CLASS_TAG, "Density is neither HIGH, MEDIUM OR LOW.  Density is "
                    + String.valueOf(density));
            parent.setScaleX(2.0f);
            parent.setScaleY(2.0f);
            parent.post(new Runnable() {

                @Override
                public void run() {
                    int h, w;
                    h = parent.getHeight();
                    w = parent.getWidth();
                    RelativeLayout.LayoutParams tableLP = new RelativeLayout.LayoutParams(
                            w * 2, h * 2);
                    tableLP.addRule(RelativeLayout.CENTER_IN_PARENT, 1);
                    tableLP.addRule(RelativeLayout.ALIGN_PARENT_TOP, 1);
                    parent.setLayoutParams(tableLP);
                }
            });
        }

    }

    @Override
    public void onColorSelected(PaintColor paintColor) {
        isColorClicked = true;
        Float[] rgb = ImageUtils.hexToRgb(paintColor.getColorCode());
        redScale = rgb[0];
        greenScale = rgb[1];
        blueScale = rgb[2];
        AppLog.showLog(CLASS_TAG, "  we get:::::: redScale:: "
                + redScale + " greenScale:: " + greenScale + " blueScale:: "
                + blueScale);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getSupportMenuInflater().inflate(R.menu.common_menu, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {

            case R.id.share:
                ImageUtils.shareTemplateImage(Kitchens7TemplateActivity.this, global,
                        ImageUtils.KITCHEN_TEMPLATE_7, parent,
                        leftRed, leftGreen, leftBlue,
                        ceilingRed, ceilingGreen, ceilingBlue,
                        rightRed, rightGreen, rightBlue,
                        bottomRed, bottomGreen, bottomBlue,
                        cornerRed, cornerGreen, cornerBlue);
                break;

            case R.id.menu_save:
                ImageUtils.saveTemplateImage(Kitchens7TemplateActivity.this, global,
                        ImageUtils.KITCHEN_TEMPLATE_7, parent,
                        leftRed, leftGreen, leftBlue,
                        ceilingRed, ceilingGreen, ceilingBlue,
                        rightRed, rightGreen, rightBlue,
                        bottomRed, bottomGreen, bottomBlue,
                        cornerRed, cornerGreen, cornerBlue);
                break;

            case android.R.id.home:
                Intent colorChooserIntenta = new Intent(getApplicationContext(),
                        ColorChooserActivity.class);
                colorChooserIntenta.putExtra("COLOR CHOOSER SOURCE", "TEMPLATE");
                colorChooserIntenta.putExtra("POSITION", 6);
                colorChooserIntenta.putExtra("TYPE", "kitchen");
                colorChooserIntenta.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(colorChooserIntenta);
                break;

            case R.id.add_more_colors:
                Intent colorChooserIntent = new Intent(getApplicationContext(),
                        ColorChooserActivity.class);
                colorChooserIntent.putExtra("COLOR CHOOSER SOURCE", "TEMPLATE");
                colorChooserIntent.putExtra("POSITION", 6);
                colorChooserIntent.putExtra("TYPE", "kitchen");
                colorChooserIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(colorChooserIntent);

                break;

            default:
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    /**
     * DragListener will handle dragged views being dropped on the drop area -
     * only the drop action will have processing added to it as we are not -
     * amending the default behavior for other parts of the drag process
     */
    private class ChoiceDragListener implements OnDragListener {

        @Override
        public boolean onDrag(View v, DragEvent event) {
            switch (event.getAction()) {
                case DragEvent.ACTION_DRAG_STARTED:
                    // no action necessary
                    break;
                case DragEvent.ACTION_DRAG_ENTERED:
                    // no action necessary
                    break;
                case DragEvent.ACTION_DRAG_EXITED:
                    // no action necessary
                    break;
                case DragEvent.ACTION_DROP:
                    int[] colors = ImageUtils.startPainting(Kitchens7TemplateActivity.this, ImageUtils.KITCHEN_TEMPLATE_7,
                            (int) event.getX(), (int) event.getY(), ivMainImage, parent,
                            new float[]{redScale, greenScale, blueScale},
                            ivLeftWall, ivTop, ivRightWall, ivBottom, ivCorner);
                    int ivId = colors[3];
                    if (ivId == R.id.iv_kitchen7_left_wall) {
                        leftRed = colors[0];
                        leftGreen = colors[1];
                        leftBlue = colors[2];
                    } else if (ivId == R.id.iv_kitchen7_ceiling) {
                        ceilingRed = colors[0];
                        ceilingGreen = colors[1];
                        ceilingBlue = colors[2];
                    } else if (ivId == R.id.iv_kitchen7_right_wall) {
                        rightRed = colors[0];
                        rightGreen = colors[1];
                        rightBlue = colors[2];
                    } else if (ivId == R.id.iv_kitchen7_bottom) {
                        bottomRed = colors[0];
                        bottomGreen = colors[1];
                        bottomBlue = colors[2];
                    } else if (ivId == R.id.iv_kitchen7_corner) {
                        cornerRed = colors[0];
                        cornerGreen = colors[1];
                        cornerBlue = colors[2];
                    }
                    // handle the dragged view being dropped over a drop view
                    View view = (View) event.getLocalState();

                    // stop displaying the view where it was before it was dragged
                    // //view.setVisibility(View.INVISIBLE);

                    // view dragged item is being dropped on
                    ImageView dropTarget = (ImageView) v;

                    // view being dragged and dropped
                    LinearLayout dropped = (LinearLayout) view;

                    // if an item has already been dropped here, there will be a tag
                    Object tag = dropTarget.getTag();

                    // set the tag in the target view being dropped on - to the ID
                    // of the view being dropped
                    dropTarget.setTag(dropped.getId());

                    break;
                case DragEvent.ACTION_DRAG_ENDED:
                    // no action necessary
                    break;
                default:
                    break;
            }
            return true;
        }
    }

}
