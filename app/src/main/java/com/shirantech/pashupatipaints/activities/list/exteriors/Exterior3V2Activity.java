package com.shirantech.pashupatipaints.activities.list.exteriors;

import android.app.AlertDialog;
import android.content.ClipData;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Build;
import android.os.Bundle;
import android.os.Vibrator;
import android.util.DisplayMetrics;
import android.view.DragEvent;
import android.view.MotionEvent;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.GridView;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.actionbarsherlock.app.SherlockFragmentActivity;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.shirantech.pashupatipaints.R;
import com.shirantech.pashupatipaints.activities.list.ColorChooserActivity;
import com.shirantech.pashupatipaints.activities.list.GalleryActivity;
import com.shirantech.pashupatipaints.activities.list.GalleryV2Activity;
import com.shirantech.pashupatipaints.activities.list.ImagePreviewActivity;
import com.shirantech.pashupatipaints.adapter.ColorPickerAdapter;
import com.shirantech.pashupatipaints.adapter.TabletColorHorizontalListAdapter;
import com.shirantech.pashupatipaints.customwidgets.ColorDragShadowBuilder;
import com.shirantech.pashupatipaints.fragments.PaintInfoFragment;
import com.shirantech.pashupatipaints.fragments.SelectedColorsFragment;
import com.shirantech.pashupatipaints.interfaces.ViewOnTouchListener;
import com.shirantech.pashupatipaints.model.PaintColor;
import com.shirantech.pashupatipaints.model.PaintColorHistory;
import com.shirantech.pashupatipaints.util.AppLog;
import com.shirantech.pashupatipaints.util.AppText;
import com.shirantech.pashupatipaints.util.Global;
import com.shirantech.pashupatipaints.util.ImageUtils;
import com.shirantech.pashupatipaints.util.Utility;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 * Created by susan on 4/6/15.
 */
@SuppressWarnings("deprecation")
public class Exterior3V2Activity extends SherlockFragmentActivity implements SelectedColorsFragment.onSelectedColorSelectedListener, View.OnClickListener, ViewOnTouchListener {

    private static final String CLASS_TAG = Exterior3V2Activity.class.getSimpleName();
    private ImageLoader imageLoader = ImageLoader.getInstance();
    private RelativeLayout parent;
    private Global global;
    private List<PaintColor> globalPaintColorList;
    private ImageView ivMainImage, ivWall1, ivWall2, ivWall3, ivWall4,
            ivWall5, ivWall6, ivWall7, ivWall8, ivWall9, ivWall10, ivWall11, ivWall12;
    private TextView textViewSelectedColorCode, textViewSelectedColorName;
    private View viewSelectedColor;
    private List<PaintColor> usedColorList;
    private boolean isColorClicked = false;
    private float redScale = 1f;
    private float greenScale = 0f, blueScale = 0f;
    private boolean isDonePainting = false;
    private GridView gridViewSelectedColors;
    private int ivWall1Red = -1, ivWall1Green = -1, ivWall1Blue = -1;
    private int ivWall2Red = -1, ivWall2Green = -1, ivWall2Blue = -1;
    private int ivWall3Red = -1, ivWall3Green = -1, ivWall3Blue = -1;
    private int ivWall4Red = -1, ivWall4Green = -1, ivWall4Blue = -1;
    private int ivWall5Red = -1, ivWall5Green = -1, ivWall5Blue = -1;
    private int ivWall6Red = -1, ivWall6Green = -1, ivWall6Blue = -1;
    private int ivWall7Red = -1, ivWall7Green = -1, ivWall7Blue = -1;
    private int ivWall8Red = -1, ivWall8Green = -1, ivWall8Blue = -1;
    private int ivWall9Red = -1, ivWall9Green = -1, ivWall9Blue = -1;
    private int ivWall10Red = -1, ivWall10Green = -1, ivWall10Blue = -1;
    private int ivWall11Red = -1, ivWall11Green = -1, ivWall11Blue = -1;
    private int ivWall12Red = -1, ivWall12Green = -1, ivWall12Blue = -1;
    private List<PaintColorHistory> redoColorHistoryList = new ArrayList<PaintColorHistory>();
    private List<PaintColorHistory> undoColorHistoryList = new ArrayList<PaintColorHistory>();
    private Vibrator mVibrator;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(android.view.Window.FEATURE_NO_TITLE);
        if (Build.VERSION.SDK_INT <= Build.VERSION_CODES.HONEYCOMB) {
            getSupportActionBar().hide();
        }
        setContentView(R.layout.activity_exteriors3);
        imageLoader.init(ImageLoaderConfiguration.createDefault(this));
        parent = (RelativeLayout) findViewById(R.id.roomLayout_exterior3);

        global = (Global) getApplication();
        globalPaintColorList = global.getGlobalPaintColorList();
        ivMainImage = (ImageView) findViewById(R.id.iv_exterior3_main_image);
        ivWall1 = (ImageView) findViewById(R.id.iv_exterior3_1);
        ivWall2 = (ImageView) findViewById(R.id.iv_exterior3_2);
        ivWall3 = (ImageView) findViewById(R.id.iv_exterior3_3);
        ivWall4 = (ImageView) findViewById(R.id.iv_exterior3_4);
        ivWall5 = (ImageView) findViewById(R.id.iv_exterior3_5);
        ivWall6 = (ImageView) findViewById(R.id.iv_exterior3_6);
        ivWall7 = (ImageView) findViewById(R.id.iv_exterior3_7);
        ivWall8 = (ImageView) findViewById(R.id.iv_exterior3_8);
        ivWall9 = (ImageView) findViewById(R.id.iv_exterior3_9);
        ivWall10 = (ImageView) findViewById(R.id.iv_exterior3_10);
        ivWall11 = (ImageView) findViewById(R.id.iv_exterior3_11);
        ivWall12 = (ImageView) findViewById(R.id.iv_exterior3_12);

        ImageUtils.loadImageUsingLoader(imageLoader, ivMainImage, "assets://" + AppText.EXTERIOR3_LOCATION_MAIN_IMAGE);
        ImageUtils.loadImageUsingLoader(imageLoader, ivWall1, "assets://" + AppText.EXTERIOR3_LOCATION_IMAGE1);
        ImageUtils.loadImageUsingLoader(imageLoader, ivWall2, "assets://" + AppText.EXTERIOR3_LOCATION_IMAGE2);
        ImageUtils.loadImageUsingLoader(imageLoader, ivWall3, "assets://" + AppText.EXTERIOR3_LOCATION_IMAGE3);
        ImageUtils.loadImageUsingLoader(imageLoader, ivWall4, "assets://" + AppText.EXTERIOR3_LOCATION_IMAGE4);
        ImageUtils.loadImageUsingLoader(imageLoader, ivWall5, "assets://" + AppText.EXTERIOR3_LOCATION_IMAGE5);
        ImageUtils.loadImageUsingLoader(imageLoader, ivWall6, "assets://" + AppText.EXTERIOR3_LOCATION_IMAGE6);
        ImageUtils.loadImageUsingLoader(imageLoader, ivWall7, "assets://" + AppText.EXTERIOR3_LOCATION_IMAGE7);
        ImageUtils.loadImageUsingLoader(imageLoader, ivWall8, "assets://" + AppText.EXTERIOR3_LOCATION_IMAGE8);
        ImageUtils.loadImageUsingLoader(imageLoader, ivWall9, "assets://" + AppText.EXTERIOR3_LOCATION_IMAGE9);
        ImageUtils.loadImageUsingLoader(imageLoader, ivWall10, "assets://" + AppText.EXTERIOR3_LOCATION_IMAGE10);
        ImageUtils.loadImageUsingLoader(imageLoader, ivWall11, "assets://" + AppText.EXTERIOR3_LOCATION_IMAGE11);
        ImageUtils.loadImageUsingLoader(imageLoader, ivWall12, "assets://" + AppText.EXTERIOR3_LOCATION_IMAGE12);

        Button imageButtonAddMoreColor = (Button) findViewById(R.id.image_button_add_more_color);
        imageButtonAddMoreColor.setOnClickListener(this);

        ImageButton imageButtonRefresh = (ImageButton) findViewById(R.id.imagebutton_paint_refresh);
        ImageButton imageButtonSave = (ImageButton) findViewById(R.id.imagebutton_paint_save);
        ImageButton imageButtonShare = (ImageButton) findViewById(R.id.imagebutton_paint_share);
        ImageButton imageButtonUndo = (ImageButton) findViewById(R.id.imagebutton_paint_undo);
        ImageButton imageButtonRedo = (ImageButton) findViewById(R.id.imagebutton_paint_redo);
        Button imageButtonInfo = (Button) findViewById(R.id.imagebutton_info);
        TextView imageButtonGoToGallery = (TextView) findViewById(R.id.imagebutton_go_to_gallery);
        ImageButton imageButtonBack = (ImageButton) findViewById(R.id.home);

        imageButtonInfo.setOnClickListener(this);
        imageButtonBack.setOnClickListener(this);
        imageButtonGoToGallery.setOnClickListener(this);
        imageButtonRedo.setOnClickListener(this);
        imageButtonUndo.setOnClickListener(this);
        imageButtonShare.setOnClickListener(this);
        imageButtonSave.setOnClickListener(this);
        imageButtonRefresh.setOnClickListener(this);

        textViewSelectedColorCode = (TextView) findViewById(R.id.text_view_selected_color_code);
        textViewSelectedColorName = (TextView) findViewById(R.id.text_view_selected_color_name);
        viewSelectedColor = findViewById(R.id.view_selected_color);

        usedColorList = new ArrayList<PaintColor>();

        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.HONEYCOMB) {
            //drag and drop!!
            ivWall1.setOnDragListener(new ChoiceDragListener());
        }
        ivWall1.setOnTouchListener(new View.OnTouchListener() {

            @Override
            public boolean onTouch(View v, MotionEvent event) {

                if (isColorClicked) {
                    int[] colors = ImageUtils.startPainting(Exterior3V2Activity.this,
                            ImageUtils.EXTERIOR_TEMPLATE_3, (int) event.getX(),
                            (int) event.getY(), ivMainImage, parent,
                            new float[]{redScale, greenScale, blueScale},
                            ivWall1, ivWall2, ivWall3, ivWall4, ivWall5,
                            ivWall6, ivWall7, ivWall8, ivWall9,
                            ivWall10, ivWall11, ivWall12);
                    isDonePainting = true;
                    updatePaintStatus(colors);
                } else {
                    Toast.makeText(getApplicationContext(), "Please select the color below to paint the rooms", Toast.LENGTH_SHORT).show();
                }
                return false;
            }
        });

        //Determine density
        DisplayMetrics metrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(metrics);
        int density = metrics.densityDpi;

        if (density == DisplayMetrics.DENSITY_HIGH) {
            AppLog.showLog(CLASS_TAG, "DENSITY_HIGH... Density is " + String.valueOf(density));
        } else if (density == DisplayMetrics.DENSITY_MEDIUM) {
            AppLog.showLog(CLASS_TAG, "DENSITY_MEDIUM... Density is " + String.valueOf(density));
        } else if (density == DisplayMetrics.DENSITY_LOW) {
            AppLog.showLog(CLASS_TAG, "DENSITY_LOW... Density is " + String.valueOf(density));
        } else {
            AppLog.showLog(CLASS_TAG, "Density is neither HIGH, MEDIUM OR LOW.  Density is " + String.valueOf(density));
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        globalPaintColorList = ((Global) getApplication()).getGlobalPaintColorList();

        gridViewSelectedColors = (GridView) findViewById(R.id.selected_color_fragment);/*.findViewById(R.id.gridViewSelectedColors));*/
        gridViewSelectedColors.setAdapter(new TabletColorHorizontalListAdapter(this, globalPaintColorList, this));
        /*if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.HONEYCOMB) {
            gridViewSelectedColors.setOnTouchListener(new ChoiceTouchListener());
        }*/
        gridViewSelectedColors.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                PaintColor paintColor = (PaintColor) gridViewSelectedColors.getAdapter().getItem(position);
                AppLog.showLog(CLASS_TAG, "grid selected item:: " + paintColor.getName());
                sendToParent(paintColor);
            }
        });

        mVibrator = (Vibrator) this.getSystemService(Context.VIBRATOR_SERVICE);
        gridViewSelectedColors.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(final AdapterView<?> parent, View view, final int position, long id) {
                mVibrator.vibrate(30);
                TabletColorHorizontalListAdapter.ViewHolder holder = (TabletColorHorizontalListAdapter.ViewHolder) view.getTag();
                holder.imageButtonDelete.setVisibility(View.VISIBLE);
                holder.imageButtonDelete.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        final PaintColor pc = (PaintColor) parent.getAdapter().getItem(position);
                        globalPaintColorList.remove(pc);
                        gridViewSelectedColors.setAdapter(new TabletColorHorizontalListAdapter(Exterior3V2Activity.this,
                                globalPaintColorList, Exterior3V2Activity.this));
                        ((TabletColorHorizontalListAdapter)parent.getAdapter()).notifyDataSetChanged();
                    }
                });
                return true;
            }
        });
    }

    private void sendToParent(PaintColor paintColor) {
        isColorClicked = true;

        Float[] rgb = ImageUtils.hexToRgb(paintColor.getColorCode());

        redScale = rgb[0];
        greenScale = rgb[1];
        blueScale = rgb[2];
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
            viewSelectedColor.setBackground(Utility.getCustomShape(Color.parseColor(paintColor.getColorCode())));
        } else {
            viewSelectedColor.setBackgroundDrawable(Utility.getCustomShape(Color.parseColor(paintColor.getColorCode())));
        }
        textViewSelectedColorName.setText(paintColor.getName());
        textViewSelectedColorCode.setText(paintColor.getColorCode());
    }

    @Override
    public void onColorSelected(PaintColor paintColor) {
        isColorClicked = true;

        Float[] rgb = ImageUtils.hexToRgb(paintColor.getColorCode());

        redScale = rgb[0];
        greenScale = rgb[1];
        blueScale = rgb[2];
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
            viewSelectedColor.setBackground(Utility.getCustomShape(Color.parseColor(paintColor.getColorCode())));
        } else {
            viewSelectedColor.setBackgroundDrawable(Utility.getCustomShape(Color.parseColor(paintColor.getColorCode())));
        }
        textViewSelectedColorName.setText(paintColor.getName());
        textViewSelectedColorCode.setText(paintColor.getColorCode());
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.image_button_add_more_color:
                Intent colorChooserIntent = new Intent(this, ColorChooserActivity.class);
//                colorChooserIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                if (globalPaintColorList.size() <= 0) {
                    colorChooserIntent.putExtra("COLOR CHOOSER SOURCE", "STARTER");
                    colorChooserIntent.putExtra("POSITION", 0);
                    colorChooserIntent.putExtra("TYPE", "bedroom");
                } else {
                    colorChooserIntent.putExtra("COLOR CHOOSER SOURCE", "TEMPLATE");
                    colorChooserIntent.putExtra("POSITION", 0);
                    colorChooserIntent.putExtra("TYPE", "bedroom");
                }
                startActivity(colorChooserIntent);
                break;

            case R.id.home:
                global.getGlobalPaintColorList().clear();
                finish();
                break;
            case R.id.imagebutton_paint_undo:
                undoPainting();
                break;
            case R.id.imagebutton_paint_refresh:
                AlertDialog.Builder builder = new AlertDialog.Builder(this);
                builder.setTitle("Reset painting");
                builder.setMessage("Want to reset all the painting details?");
                builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        refreshPaintStatus();
                        dialogInterface.dismiss();
                    }
                });
                builder.setNegativeButton("No", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dialogInterface.dismiss();
                    }
                });
                builder.create().show();
                break;
            case R.id.imagebutton_paint_save:
                if (usedColorList.size() <= 0 || usedColorList.isEmpty()) {
                    Toast.makeText(this, "Please Paint the image to save", Toast.LENGTH_SHORT)
                            .show();
                    return;
                }
                AlertDialog.Builder builder1 = new AlertDialog.Builder(this);
                builder1.setTitle("Save Image");
                builder1.setMessage("Do you want to save image?");
                builder1.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        ImageUtils.saveTemplateImage(Exterior3V2Activity.this, global,
                                ImageUtils.EXTERIOR_TEMPLATE_3, parent,
                                ivWall1Red, ivWall1Green, ivWall1Blue,
                                ivWall2Red, ivWall2Green, ivWall2Blue,
                                ivWall3Red, ivWall3Green, ivWall3Blue,
                                ivWall4Red, ivWall4Green, ivWall4Blue,
                                ivWall5Red, ivWall5Green, ivWall5Blue,
                                ivWall6Red, ivWall6Green, ivWall6Blue,
                                ivWall7Red, ivWall7Green, ivWall7Blue,
                                ivWall8Red, ivWall8Green, ivWall8Blue,
                                ivWall9Red, ivWall9Green, ivWall9Blue,
                                ivWall10Red, ivWall10Green, ivWall10Blue,
                                ivWall11Red, ivWall11Green, ivWall11Blue,
                                ivWall12Red, ivWall12Green, ivWall12Blue);
                        dialogInterface.dismiss();
                    }
                });
                builder1.setNegativeButton("No", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dialogInterface.dismiss();
                    }
                });
                builder1.create().show();
                break;
            case R.id.imagebutton_paint_share:
                Bitmap bm;
                parent.setDrawingCacheEnabled(true);
                parent.buildDrawingCache();
                bm = parent.getDrawingCache();
                Bitmap bmCopy = bm.copy(Bitmap.Config.ARGB_8888, false);
                parent.destroyDrawingCache();
                Intent intent = new Intent(this, ImagePreviewActivity.class);
                ((Global) getApplication()).setSnapAndPaintBitmap(bmCopy);
                startActivity(intent);
                break;
            case R.id.imagebutton_paint_redo:
                redoPainting();
                break;
            case R.id.imagebutton_info:
//                showInfoDialog();
                showInfoActivity();
                break;
            case R.id.imagebutton_go_to_gallery:
                Intent intentGallery = new Intent(Exterior3V2Activity.this,
                        GalleryV2Activity.class);
                intentGallery.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intentGallery);
                break;
        }
    }

    @Override
    public void finish() {
        ((Global) getApplication()).getGlobalPaintColorList().clear();
        super.finish();
    }

    private void showInfoActivity() {
        AppLog.showLog(CLASS_TAG, "size of used color : " + usedColorList.size());
        if (usedColorList == null || usedColorList.size() <= 0) {
            Toast.makeText(this, "Info unavailable. Paint first.", Toast.LENGTH_SHORT)
                    .show();
            return;
        }
//        Intent infoActivity = new Intent(this, PaintInfoActivity.class);
        PaintInfoFragment fragment = new PaintInfoFragment();
        Bundle bundle = new Bundle();
        bundle.putParcelableArrayList("USED_COLOR_LIST", (ArrayList<PaintColor>) usedColorList);
        fragment.setArguments(bundle);
        fragment.show(getSupportFragmentManager(), "Paint Info");
//        infoActivity.putExtras(bundle);
//        startActivity(infoActivity);
    }

    private void redoPainting() {
        isDonePainting = false;
        if (redoColorHistoryList.size() - 1 < 0) {
            Toast.makeText(this, "redo unavailable.", Toast.LENGTH_SHORT)
                    .show();
        } else {
            int maxIndex = redoColorHistoryList.size() - 1;
            undoColorHistoryList.add(redoColorHistoryList.get(maxIndex));
            PaintColorHistory paintColorHistory = redoColorHistoryList.get(maxIndex);
            redoColorHistoryList.remove(maxIndex);
            Float[] colorScale = paintColorHistory.getColorCode();
            int imageId = paintColorHistory.getImageViewId();
            switch (imageId) {
                case R.id.iv_exterior3_1:
                    if (colorScale[0] == 0 && colorScale[1] == 0 && colorScale[2] == 0) {
                        ImageUtils.loadImageUsingLoader(imageLoader, ivWall1,
                                "assets://" + AppText.EXTERIOR3_LOCATION_IMAGE1);
                        updatePaintStatus(new int[]{0, 0, 0, R.id.iv_exterior3_1});
                    } else {
                        int[] colors = ImageUtils.undoPainting(this, ivWall1, AppText.EXTERIOR3_LOCATION_IMAGE1,
                                colorScale);
                        updatePaintStatus(colors);
                    }
                    break;
                case R.id.iv_exterior3_2:
                    if (colorScale[0] == 0 && colorScale[1] == 0 && colorScale[2] == 0) {
                        ImageUtils.loadImageUsingLoader(imageLoader, ivWall2,
                                "assets://" + AppText.EXTERIOR3_LOCATION_IMAGE2);
                        updatePaintStatus(new int[]{0, 0, 0, R.id.iv_exterior3_2});
                    } else {
                        int[] colors = ImageUtils.undoPainting(this, ivWall2, AppText.EXTERIOR3_LOCATION_IMAGE2,
                                colorScale);
                        updatePaintStatus(colors);
                    }
                    break;
                case R.id.iv_exterior3_3:
                    if (colorScale[0] == 0 && colorScale[1] == 0 && colorScale[2] == 0) {
                        ImageUtils.loadImageUsingLoader(imageLoader, ivWall3,
                                "assets://" + AppText.EXTERIOR3_LOCATION_IMAGE3);
                        updatePaintStatus(new int[]{0, 0, 0, R.id.iv_exterior3_3});
                    } else {
                        int[] colors = ImageUtils.undoPainting(this, ivWall3, AppText.EXTERIOR3_LOCATION_IMAGE3,
                                colorScale);
                        updatePaintStatus(colors);
                    }
                    break;
                case R.id.iv_exterior3_4:
                    if (colorScale[0] == 0 && colorScale[1] == 0 && colorScale[2] == 0) {
                        ImageUtils.loadImageUsingLoader(imageLoader, ivWall4,
                                "assets://" + AppText.EXTERIOR3_LOCATION_IMAGE4);
                        updatePaintStatus(new int[]{0, 0, 0, R.id.iv_exterior3_4});
                    } else {
                        int[] colors = ImageUtils.undoPainting(this, ivWall4, AppText.EXTERIOR3_LOCATION_IMAGE4,
                                colorScale);
                        updatePaintStatus(colors);
                    }
                    break;
                case R.id.iv_exterior3_5:
                    if (colorScale[0] == 0 && colorScale[1] == 0 && colorScale[2] == 0) {
                        ImageUtils.loadImageUsingLoader(imageLoader, ivWall5,
                                "assets://" + AppText.EXTERIOR3_LOCATION_IMAGE5);
                        updatePaintStatus(new int[]{0, 0, 0, R.id.iv_exterior3_5});
                    } else {
                        int[] colors = ImageUtils.undoPainting(this, ivWall5, AppText.EXTERIOR3_LOCATION_IMAGE5,
                                colorScale);
                        updatePaintStatus(colors);
                    }
                    break;
                case R.id.iv_exterior3_6:
                    if (colorScale[0] == 0 && colorScale[1] == 0 && colorScale[2] == 0) {
                        ImageUtils.loadImageUsingLoader(imageLoader, ivWall6,
                                "assets://" + AppText.EXTERIOR3_LOCATION_IMAGE6);
                        updatePaintStatus(new int[]{0, 0, 0, R.id.iv_exterior3_6});
                    } else {
                        int[] colors = ImageUtils.undoPainting(this, ivWall6, AppText.EXTERIOR3_LOCATION_IMAGE6,
                                colorScale);
                        updatePaintStatus(colors);
                    }
                    break;
                case R.id.iv_exterior3_7:
                    if (colorScale[0] == 0 && colorScale[1] == 0 && colorScale[2] == 0) {
                        ImageUtils.loadImageUsingLoader(imageLoader, ivWall7,
                                "assets://" + AppText.EXTERIOR3_LOCATION_IMAGE7);
                        updatePaintStatus(new int[]{0, 0, 0, R.id.iv_exterior3_7});
                    } else {
                        int[] colors = ImageUtils.undoPainting(this, ivWall7, AppText.EXTERIOR3_LOCATION_IMAGE7,
                                colorScale);
                        updatePaintStatus(colors);
                    }
                    break;
                case R.id.iv_exterior3_8:
                    if (colorScale[0] == 0 && colorScale[1] == 0 && colorScale[2] == 0) {
                        ImageUtils.loadImageUsingLoader(imageLoader, ivWall8,
                                "assets://" + AppText.EXTERIOR3_LOCATION_IMAGE8);
                        updatePaintStatus(new int[]{0, 0, 0, R.id.iv_exterior3_8});
                    } else {
                        int[] colors = ImageUtils.undoPainting(this, ivWall8, AppText.EXTERIOR3_LOCATION_IMAGE8,
                                colorScale);
                        updatePaintStatus(colors);
                    }
                    break;
                case R.id.iv_exterior3_9:
                    if (colorScale[0] == 0 && colorScale[1] == 0 && colorScale[2] == 0) {
                        ImageUtils.loadImageUsingLoader(imageLoader, ivWall9,
                                "assets://" + AppText.EXTERIOR3_LOCATION_IMAGE9);
                        updatePaintStatus(new int[]{0, 0, 0, R.id.iv_exterior3_9});
                    } else {
                        int[] colors = ImageUtils.undoPainting(this, ivWall9, AppText.EXTERIOR3_LOCATION_IMAGE9,
                                colorScale);
                        updatePaintStatus(colors);
                    }
                    break;
                case R.id.iv_exterior3_10:
                    if (colorScale[0] == 0 && colorScale[1] == 0 && colorScale[2] == 0) {
                        ImageUtils.loadImageUsingLoader(imageLoader, ivWall10,
                                "assets://" + AppText.EXTERIOR3_LOCATION_IMAGE10);
                        updatePaintStatus(new int[]{0, 0, 0, R.id.iv_exterior3_10});
                    } else {
                        int[] colors = ImageUtils.undoPainting(this, ivWall10, AppText.EXTERIOR3_LOCATION_IMAGE10,
                                colorScale);
                        updatePaintStatus(colors);
                    }
                    break;
                case R.id.iv_exterior3_11:
                    if (colorScale[0] == 0 && colorScale[1] == 0 && colorScale[2] == 0) {
                        ImageUtils.loadImageUsingLoader(imageLoader, ivWall11,
                                "assets://" + AppText.EXTERIOR3_LOCATION_IMAGE11);
                        updatePaintStatus(new int[]{0, 0, 0, R.id.iv_exterior3_11});
                    } else {
                        int[] colors = ImageUtils.undoPainting(this, ivWall11, AppText.EXTERIOR3_LOCATION_IMAGE11,
                                colorScale);
                        updatePaintStatus(colors);
                    }
                    break;
                case R.id.iv_exterior3_12:
                    if (colorScale[0] == 0 && colorScale[1] == 0 && colorScale[2] == 0) {
                        ImageUtils.loadImageUsingLoader(imageLoader, ivWall12,
                                "assets://" + AppText.EXTERIOR3_LOCATION_IMAGE12);
                        updatePaintStatus(new int[]{0, 0, 0, R.id.iv_exterior3_12});
                    } else {
                        int[] colors = ImageUtils.undoPainting(this, ivWall12, AppText.EXTERIOR3_LOCATION_IMAGE12,
                                colorScale);
                        updatePaintStatus(colors);
                    }
                    break;
            }
        }
    }

    private void refreshPaintStatus() {

        isColorClicked = false;
        redScale = 1f;
        greenScale = 0f;
        blueScale = 0f;
        usedColorList.clear();
        undoColorHistoryList.clear();
        redoColorHistoryList.clear();

        viewSelectedColor.setBackgroundDrawable(new ColorDrawable(android.R.color.transparent));
        textViewSelectedColorCode.setText("");
        textViewSelectedColorName.setText("");

        ImageUtils.loadImageUsingLoader(imageLoader, ivMainImage, "assets://" + AppText.EXTERIOR3_LOCATION_MAIN_IMAGE);
        ImageUtils.loadImageUsingLoader(imageLoader, ivWall1, "assets://" + AppText.EXTERIOR3_LOCATION_IMAGE1);
        ImageUtils.loadImageUsingLoader(imageLoader, ivWall2, "assets://" + AppText.EXTERIOR3_LOCATION_IMAGE2);
        ImageUtils.loadImageUsingLoader(imageLoader, ivWall3, "assets://" + AppText.EXTERIOR3_LOCATION_IMAGE3);
        ImageUtils.loadImageUsingLoader(imageLoader, ivWall4, "assets://" + AppText.EXTERIOR3_LOCATION_IMAGE4);
        ImageUtils.loadImageUsingLoader(imageLoader, ivWall5, "assets://" + AppText.EXTERIOR3_LOCATION_IMAGE5);
        ImageUtils.loadImageUsingLoader(imageLoader, ivWall6, "assets://" + AppText.EXTERIOR3_LOCATION_IMAGE6);
        ImageUtils.loadImageUsingLoader(imageLoader, ivWall7, "assets://" + AppText.EXTERIOR3_LOCATION_IMAGE7);
        ImageUtils.loadImageUsingLoader(imageLoader, ivWall8, "assets://" + AppText.EXTERIOR3_LOCATION_IMAGE8);
        ImageUtils.loadImageUsingLoader(imageLoader, ivWall9, "assets://" + AppText.EXTERIOR3_LOCATION_IMAGE9);
        ImageUtils.loadImageUsingLoader(imageLoader, ivWall10, "assets://" + AppText.EXTERIOR3_LOCATION_IMAGE10);
        ImageUtils.loadImageUsingLoader(imageLoader, ivWall11, "assets://" + AppText.EXTERIOR3_LOCATION_IMAGE11);
        ImageUtils.loadImageUsingLoader(imageLoader, ivWall12, "assets://" + AppText.EXTERIOR3_LOCATION_IMAGE12);

        globalPaintColorList.clear();
        ((TabletColorHorizontalListAdapter)gridViewSelectedColors.getAdapter()).notifyDataSetChanged();
    }

    private void undoPainting() {
        isDonePainting = false;
        if (undoColorHistoryList.size() <= 0) {
            Toast.makeText(this, "undo unavailable.", Toast.LENGTH_SHORT)
                    .show();
        } else {
            int maxIndex = undoColorHistoryList.size() - 1;
            redoColorHistoryList.add(undoColorHistoryList.get(maxIndex));
            undoColorHistoryList.remove(maxIndex);
            maxIndex = undoColorHistoryList.size() - 1;
            if (maxIndex < 0) {
                Toast.makeText(this, "undo unavailable.", Toast.LENGTH_SHORT)
                        .show();
                return;
            }
            PaintColorHistory paintColorHistory = undoColorHistoryList.get(maxIndex);
            Float[] colorScale = paintColorHistory.getColorCode();
            switch (paintColorHistory.getImageViewId()) {
                case R.id.iv_exterior3_1:
                    if (colorScale[0] == 0 && colorScale[1] == 0 && colorScale[2] == 0) {
                        ImageUtils.loadImageUsingLoader(imageLoader, ivWall1,
                                "assets://" + AppText.EXTERIOR3_LOCATION_IMAGE1);
                        updatePaintStatus(new int[]{0, 0, 0, R.id.iv_exterior3_1});
                    } else {
                        int[] colors = ImageUtils.undoPainting(this, ivWall1, AppText.EXTERIOR3_LOCATION_IMAGE1,
                                colorScale);
                        updatePaintStatus(colors);
                    }
                    break;
                case R.id.iv_exterior3_2:
                    if (colorScale[0] == 0 && colorScale[1] == 0 && colorScale[2] == 0) {
                        ImageUtils.loadImageUsingLoader(imageLoader, ivWall2,
                                "assets://" + AppText.EXTERIOR3_LOCATION_IMAGE2);
                        updatePaintStatus(new int[]{0, 0, 0, R.id.iv_exterior3_2});
                    } else {
                        int[] colors = ImageUtils.undoPainting(this, ivWall2, AppText.EXTERIOR3_LOCATION_IMAGE2,
                                colorScale);
                        updatePaintStatus(colors);
                    }
                    break;
                case R.id.iv_exterior3_3:
                    if (colorScale[0] == 0 && colorScale[1] == 0 && colorScale[2] == 0) {
                        ImageUtils.loadImageUsingLoader(imageLoader, ivWall3,
                                "assets://" + AppText.EXTERIOR3_LOCATION_IMAGE3);
                        updatePaintStatus(new int[]{0, 0, 0, R.id.iv_exterior3_3});
                    } else {
                        int[] colors = ImageUtils.undoPainting(this, ivWall3, AppText.EXTERIOR3_LOCATION_IMAGE3,
                                colorScale);
                        updatePaintStatus(colors);
                    }
                    break;
                case R.id.iv_exterior3_4:
                    if (colorScale[0] == 0 && colorScale[1] == 0 && colorScale[2] == 0) {
                        ImageUtils.loadImageUsingLoader(imageLoader, ivWall4,
                                "assets://" + AppText.EXTERIOR3_LOCATION_IMAGE4);
                        updatePaintStatus(new int[]{0, 0, 0, R.id.iv_exterior3_4});
                    } else {
                        int[] colors = ImageUtils.undoPainting(this, ivWall4, AppText.EXTERIOR3_LOCATION_IMAGE4,
                                colorScale);
                        updatePaintStatus(colors);
                    }
                    break;
                case R.id.iv_exterior3_5:
                    if (colorScale[0] == 0 && colorScale[1] == 0 && colorScale[2] == 0) {
                        ImageUtils.loadImageUsingLoader(imageLoader, ivWall5,
                                "assets://" + AppText.EXTERIOR3_LOCATION_IMAGE5);
                        updatePaintStatus(new int[]{0, 0, 0, R.id.iv_exterior3_5});
                    } else {
                        int[] colors = ImageUtils.undoPainting(this, ivWall5, AppText.EXTERIOR3_LOCATION_IMAGE5,
                                colorScale);
                        updatePaintStatus(colors);
                    }
                    break;
                case R.id.iv_exterior3_6:
                    if (colorScale[0] == 0 && colorScale[1] == 0 && colorScale[2] == 0) {
                        ImageUtils.loadImageUsingLoader(imageLoader, ivWall6,
                                "assets://" + AppText.EXTERIOR3_LOCATION_IMAGE6);
                        updatePaintStatus(new int[]{0, 0, 0, R.id.iv_exterior3_6});
                    } else {
                        int[] colors = ImageUtils.undoPainting(this, ivWall6, AppText.EXTERIOR3_LOCATION_IMAGE6,
                                colorScale);
                        updatePaintStatus(colors);
                    }
                    break;
                case R.id.iv_exterior3_7:
                    if (colorScale[0] == 0 && colorScale[1] == 0 && colorScale[2] == 0) {
                        ImageUtils.loadImageUsingLoader(imageLoader, ivWall7,
                                "assets://" + AppText.EXTERIOR3_LOCATION_IMAGE7);
                        updatePaintStatus(new int[]{0, 0, 0, R.id.iv_exterior3_7});
                    } else {
                        int[] colors = ImageUtils.undoPainting(this, ivWall7, AppText.EXTERIOR3_LOCATION_IMAGE7,
                                colorScale);
                        updatePaintStatus(colors);
                    }
                    break;
                case R.id.iv_exterior3_8:
                    if (colorScale[0] == 0 && colorScale[1] == 0 && colorScale[2] == 0) {
                        ImageUtils.loadImageUsingLoader(imageLoader, ivWall8,
                                "assets://" + AppText.EXTERIOR3_LOCATION_IMAGE8);
                        updatePaintStatus(new int[]{0, 0, 0, R.id.iv_exterior3_8});
                    } else {
                        int[] colors = ImageUtils.undoPainting(this, ivWall8, AppText.EXTERIOR3_LOCATION_IMAGE8,
                                colorScale);
                        updatePaintStatus(colors);
                    }
                    break;
                case R.id.iv_exterior3_9:
                    if (colorScale[0] == 0 && colorScale[1] == 0 && colorScale[2] == 0) {
                        ImageUtils.loadImageUsingLoader(imageLoader, ivWall9,
                                "assets://" + AppText.EXTERIOR3_LOCATION_IMAGE9);
                        updatePaintStatus(new int[]{0, 0, 0, R.id.iv_exterior3_9});
                    } else {
                        int[] colors = ImageUtils.undoPainting(this, ivWall9, AppText.EXTERIOR3_LOCATION_IMAGE9,
                                colorScale);
                        updatePaintStatus(colors);
                    }
                    break;
                case R.id.iv_exterior3_10:
                    if (colorScale[0] == 0 && colorScale[1] == 0 && colorScale[2] == 0) {
                        ImageUtils.loadImageUsingLoader(imageLoader, ivWall10,
                                "assets://" + AppText.EXTERIOR3_LOCATION_IMAGE10);
                        updatePaintStatus(new int[]{0, 0, 0, R.id.iv_exterior3_10});
                    } else {
                        int[] colors = ImageUtils.undoPainting(this, ivWall10, AppText.EXTERIOR3_LOCATION_IMAGE10,
                                colorScale);
                        updatePaintStatus(colors);
                    }
                    break;
                case R.id.iv_exterior3_11:
                    if (colorScale[0] == 0 && colorScale[1] == 0 && colorScale[2] == 0) {
                        ImageUtils.loadImageUsingLoader(imageLoader, ivWall11,
                                "assets://" + AppText.EXTERIOR3_LOCATION_IMAGE11);
                        updatePaintStatus(new int[]{0, 0, 0, R.id.iv_exterior3_11});
                    } else {
                        int[] colors = ImageUtils.undoPainting(this, ivWall11, AppText.EXTERIOR3_LOCATION_IMAGE11,
                                colorScale);
                        updatePaintStatus(colors);
                    }
                    break;
                case R.id.iv_exterior3_12:
                    if (colorScale[0] == 0 && colorScale[1] == 0 && colorScale[2] == 0) {
                        ImageUtils.loadImageUsingLoader(imageLoader, ivWall12,
                                "assets://" + AppText.EXTERIOR3_LOCATION_IMAGE12);
                        updatePaintStatus(new int[]{0, 0, 0, R.id.iv_exterior3_12});
                    } else {
                        int[] colors = ImageUtils.undoPainting(this, ivWall12, AppText.EXTERIOR3_LOCATION_IMAGE12,
                                colorScale);
                        updatePaintStatus(colors);
                    }
                    break;
            }
        }
    }

    private void updatePaintStatus(int[] colors) {
        int ivId = colors[3];
        if (ivId == R.id.iv_exterior3_1) {
            addToUndo(ivId);
            ivWall1Red = colors[0];
            ivWall1Green = colors[1];
            ivWall1Blue = colors[2];

            if (ivWall1Red == 0 && ivWall1Green == 0 && ivWall1Blue == 0) {
                Iterator<PaintColor> iterator = usedColorList.iterator();
                while (iterator.hasNext()) {
                    PaintColor paintColor = iterator.next();
                    if ("Wall 1".equalsIgnoreCase(paintColor.getTypeWall())) {
                        iterator.remove();
                    }
                }
                return;
            }
            String wall1ColorHash = ImageUtils.RgbToHex(ivWall1Red, ivWall1Green, ivWall1Blue);
            String wall1ColorName = null;
            for (PaintColor paintColor : global.globalPaintColorList) {
                if (wall1ColorHash.equalsIgnoreCase(paintColor.getColorCode())) {
                    wall1ColorName = paintColor.getName();
                }
            }

            PaintColor paintColor = new PaintColor();
            paintColor.setTypeWall("Wall 1");
            paintColor.setColorCode(wall1ColorHash);
            paintColor.setName(wall1ColorName);

            Iterator<PaintColor> iterator = usedColorList.iterator();
            while (iterator.hasNext()) {
                PaintColor paintColor1 = iterator.next();
                if ("Wall 1".equalsIgnoreCase(paintColor1.getTypeWall())) {
                    iterator.remove();
                }
            }
            usedColorList.add(paintColor);
        } else if (ivId == R.id.iv_exterior3_2) {
            addToUndo(ivId);
            ivWall2Red = colors[0];
            ivWall2Green = colors[1];
            ivWall2Blue = colors[2];

            if (ivWall2Red == 0 && ivWall2Green == 0 && ivWall2Blue == 0) {
                Iterator<PaintColor> iterator = usedColorList.iterator();
                while (iterator.hasNext()) {
                    PaintColor paintColor = iterator.next();
                    if ("Wall 2".equalsIgnoreCase(paintColor.getTypeWall())) {
                        iterator.remove();
                    }
                }
                return;
            }
            String wall2ColorHash = ImageUtils.RgbToHex(ivWall2Red, ivWall2Green, ivWall2Blue);
            String wall2ColorName = null;
            for (PaintColor paintColor : global.globalPaintColorList) {
                if (wall2ColorHash.equalsIgnoreCase(paintColor.getColorCode())) {
                    wall2ColorName = paintColor.getName();
                }
            }

            PaintColor paintColor = new PaintColor();
            paintColor.setTypeWall("Wall 2");
            paintColor.setColorCode(wall2ColorHash);
            paintColor.setName(wall2ColorName);

            Iterator<PaintColor> iterator = usedColorList.iterator();
            while (iterator.hasNext()) {
                PaintColor paintColor1 = iterator.next();
                if ("Wall 2".equalsIgnoreCase(paintColor1.getTypeWall())) {
                    iterator.remove();
                }
            }
            usedColorList.add(paintColor);
        } else if (ivId == R.id.iv_exterior3_3) {
            addToUndo(ivId);
            ivWall3Red = colors[0];
            ivWall3Green = colors[1];
            ivWall3Blue = colors[2];

            if (ivWall3Red == 0 && ivWall3Green == 0 && ivWall3Blue == 0) {
                Iterator<PaintColor> iterator = usedColorList.iterator();
                while (iterator.hasNext()) {
                    PaintColor paintColor = iterator.next();
                    if ("Wall 3".equalsIgnoreCase(paintColor.getTypeWall())) {
                        iterator.remove();
                    }
                }
                return;
            }
            String wall3ColorHash = ImageUtils.RgbToHex(ivWall3Red, ivWall3Green, ivWall3Blue);
            String wall3ColorName = null;
            for (PaintColor paintColor : global.globalPaintColorList) {
                if (wall3ColorHash.equalsIgnoreCase(paintColor.getColorCode())) {
                    wall3ColorName = paintColor.getName();
                }
            }

            PaintColor paintColor = new PaintColor();
            paintColor.setTypeWall("Wall 3");
            paintColor.setColorCode(wall3ColorHash);
            paintColor.setName(wall3ColorName);

            Iterator<PaintColor> iterator = usedColorList.iterator();
            while (iterator.hasNext()) {
                PaintColor paintColor1 = iterator.next();
                if ("Wall 3".equalsIgnoreCase(paintColor1.getTypeWall())) {
                    iterator.remove();
                }
            }
            usedColorList.add(paintColor);
        } else if (ivId == R.id.iv_exterior3_4) {
            addToUndo(ivId);
            ivWall4Red = colors[0];
            ivWall4Green = colors[1];
            ivWall4Blue = colors[2];

            if (ivWall4Red == 0 && ivWall4Green == 0 && ivWall4Blue == 0) {
                Iterator<PaintColor> iterator = usedColorList.iterator();
                while (iterator.hasNext()) {
                    PaintColor paintColor = iterator.next();
                    if ("Wall 4".equalsIgnoreCase(paintColor.getTypeWall())) {
                        iterator.remove();
                    }
                }
                return;
            }
            String wall4ColorHash = ImageUtils.RgbToHex(ivWall4Red, ivWall4Green, ivWall4Blue);
            String wall4ColorName = null;
            for (PaintColor paintColor : global.globalPaintColorList) {
                if (wall4ColorHash.equalsIgnoreCase(paintColor.getColorCode())) {
                    wall4ColorName = paintColor.getName();
                }
            }

            PaintColor paintColor = new PaintColor();
            paintColor.setTypeWall("Wall 4");
            paintColor.setColorCode(wall4ColorHash);
            paintColor.setName(wall4ColorName);

            Iterator<PaintColor> iterator = usedColorList.iterator();
            while (iterator.hasNext()) {
                PaintColor paintColor1 = iterator.next();
                if ("Wall 4".equalsIgnoreCase(paintColor1.getTypeWall())) {
                    iterator.remove();
                }
            }
            usedColorList.add(paintColor);
        } else if (ivId == R.id.iv_exterior3_5) {
            addToUndo(ivId);
            ivWall5Red = colors[0];
            ivWall5Green = colors[1];
            ivWall5Blue = colors[2];

            if (ivWall5Red == 0 && ivWall5Green == 0 && ivWall5Blue == 0) {
                Iterator<PaintColor> iterator = usedColorList.iterator();
                while (iterator.hasNext()) {
                    PaintColor paintColor = iterator.next();
                    if ("Wall 5".equalsIgnoreCase(paintColor.getTypeWall())) {
                        iterator.remove();
                    }
                }
                return;
            }
            String wall5ColorHash = ImageUtils.RgbToHex(ivWall5Red, ivWall5Green, ivWall5Blue);
            String wall5ColorName = null;
            for (PaintColor paintColor : global.globalPaintColorList) {
                if (wall5ColorHash.equalsIgnoreCase(paintColor.getColorCode())) {
                    wall5ColorName = paintColor.getName();
                }
            }

            PaintColor paintColor = new PaintColor();
            paintColor.setTypeWall("Wall 5");
            paintColor.setColorCode(wall5ColorHash);
            paintColor.setName(wall5ColorName);

            Iterator<PaintColor> iterator = usedColorList.iterator();
            while (iterator.hasNext()) {
                PaintColor paintColor1 = iterator.next();
                if ("Wall 5".equalsIgnoreCase(paintColor1.getTypeWall())) {
                    iterator.remove();
                }
            }
            usedColorList.add(paintColor);
        } else if (ivId == R.id.iv_exterior3_6) {
            addToUndo(ivId);
            ivWall6Red = colors[0];
            ivWall6Green = colors[1];
            ivWall6Blue = colors[2];

            if (ivWall6Red == 0 && ivWall6Green == 0 && ivWall6Blue == 0) {
                Iterator<PaintColor> iterator = usedColorList.iterator();
                while (iterator.hasNext()) {
                    PaintColor paintColor = iterator.next();
                    if ("Wall 6".equalsIgnoreCase(paintColor.getTypeWall())) {
                        iterator.remove();
                    }
                }
                return;
            }
            String wall6ColorHash = ImageUtils.RgbToHex(ivWall6Red, ivWall6Green, ivWall6Blue);
            String wall6ColorName = null;
            for (PaintColor paintColor : global.globalPaintColorList) {
                if (wall6ColorHash.equalsIgnoreCase(paintColor.getColorCode())) {
                    wall6ColorName = paintColor.getName();
                }
            }

            PaintColor paintColor = new PaintColor();
            paintColor.setTypeWall("Wall 6");
            paintColor.setColorCode(wall6ColorHash);
            paintColor.setName(wall6ColorName);

            Iterator<PaintColor> iterator = usedColorList.iterator();
            while (iterator.hasNext()) {
                PaintColor paintColor1 = iterator.next();
                if ("Wall 6".equalsIgnoreCase(paintColor1.getTypeWall())) {
                    iterator.remove();
                }
            }
            usedColorList.add(paintColor);
        } else if (ivId == R.id.iv_exterior3_7) {
            addToUndo(ivId);
            ivWall7Red = colors[0];
            ivWall7Green = colors[1];
            ivWall7Blue = colors[2];

            if (ivWall7Red == 0 && ivWall7Green == 0 && ivWall7Blue == 0) {
                Iterator<PaintColor> iterator = usedColorList.iterator();
                while (iterator.hasNext()) {
                    PaintColor paintColor = iterator.next();
                    if ("Wall 7".equalsIgnoreCase(paintColor.getTypeWall())) {
                        iterator.remove();
                    }
                }
                return;
            }
            String wall7ColorHash = ImageUtils.RgbToHex(ivWall7Red, ivWall7Green, ivWall7Blue);
            String wall7ColorName = null;
            for (PaintColor paintColor : global.globalPaintColorList) {
                if (wall7ColorHash.equalsIgnoreCase(paintColor.getColorCode())) {
                    wall7ColorName = paintColor.getName();
                }
            }

            PaintColor paintColor = new PaintColor();
            paintColor.setTypeWall("Wall 7");
            paintColor.setColorCode(wall7ColorHash);
            paintColor.setName(wall7ColorName);

            Iterator<PaintColor> iterator = usedColorList.iterator();
            while (iterator.hasNext()) {
                PaintColor paintColor1 = iterator.next();
                if ("Wall 7".equalsIgnoreCase(paintColor1.getTypeWall())) {
                    iterator.remove();
                }
            }
            usedColorList.add(paintColor);
        } else if (ivId == R.id.iv_exterior3_8) {
            addToUndo(ivId);
            ivWall8Red = colors[0];
            ivWall8Green = colors[1];
            ivWall8Blue = colors[2];

            if (ivWall8Red == 0 && ivWall8Green == 0 && ivWall8Blue == 0) {
                Iterator<PaintColor> iterator = usedColorList.iterator();
                while (iterator.hasNext()) {
                    PaintColor paintColor = iterator.next();
                    if ("Wall 8".equalsIgnoreCase(paintColor.getTypeWall())) {
                        iterator.remove();
                    }
                }
                return;
            }
            String wall8ColorHash = ImageUtils.RgbToHex(ivWall8Red, ivWall8Green, ivWall8Blue);
            String wall8ColorName = null;
            for (PaintColor paintColor : global.globalPaintColorList) {
                if (wall8ColorHash.equalsIgnoreCase(paintColor.getColorCode())) {
                    wall8ColorName = paintColor.getName();
                }
            }

            PaintColor paintColor = new PaintColor();
            paintColor.setTypeWall("Wall 8");
            paintColor.setColorCode(wall8ColorHash);
            paintColor.setName(wall8ColorName);

            Iterator<PaintColor> iterator = usedColorList.iterator();
            while (iterator.hasNext()) {
                PaintColor paintColor1 = iterator.next();
                if ("Wall 8".equalsIgnoreCase(paintColor1.getTypeWall())) {
                    iterator.remove();
                }
            }
            usedColorList.add(paintColor);
        } else if (ivId == R.id.iv_exterior3_9) {
            addToUndo(ivId);
            ivWall9Red = colors[0];
            ivWall9Green = colors[1];
            ivWall9Blue = colors[2];

            if (ivWall9Red == 0 && ivWall9Green == 0 && ivWall9Blue == 0) {
                Iterator<PaintColor> iterator = usedColorList.iterator();
                while (iterator.hasNext()) {
                    PaintColor paintColor = iterator.next();
                    if ("Wall 9".equalsIgnoreCase(paintColor.getTypeWall())) {
                        iterator.remove();
                    }
                }
                return;
            }
            String wall9ColorHash = ImageUtils.RgbToHex(ivWall9Red, ivWall9Green, ivWall9Blue);
            String wall9ColorName = null;
            for (PaintColor paintColor : global.globalPaintColorList) {
                if (wall9ColorHash.equalsIgnoreCase(paintColor.getColorCode())) {
                    wall9ColorName = paintColor.getName();
                }
            }

            PaintColor paintColor = new PaintColor();
            paintColor.setTypeWall("Wall 9");
            paintColor.setColorCode(wall9ColorHash);
            paintColor.setName(wall9ColorName);

            Iterator<PaintColor> iterator = usedColorList.iterator();
            while (iterator.hasNext()) {
                PaintColor paintColor1 = iterator.next();
                if ("Wall 9".equalsIgnoreCase(paintColor1.getTypeWall())) {
                    iterator.remove();
                }
            }
            usedColorList.add(paintColor);
        } else if (ivId == R.id.iv_exterior3_10) {
            addToUndo(ivId);
            ivWall10Red = colors[0];
            ivWall10Green = colors[1];
            ivWall10Blue = colors[2];

            if (ivWall10Red == 0 && ivWall10Green == 0 && ivWall10Blue == 0) {
                Iterator<PaintColor> iterator = usedColorList.iterator();
                while (iterator.hasNext()) {
                    PaintColor paintColor = iterator.next();
                    if ("Wall 10".equalsIgnoreCase(paintColor.getTypeWall())) {
                        iterator.remove();
                    }
                }
                return;
            }
            String wall10ColorHash = ImageUtils.RgbToHex(ivWall10Red, ivWall10Green, ivWall10Blue);
            String wall10ColorName = null;
            for (PaintColor paintColor : global.globalPaintColorList) {
                if (wall10ColorHash.equalsIgnoreCase(paintColor.getColorCode())) {
                    wall10ColorName = paintColor.getName();
                }
            }

            PaintColor paintColor = new PaintColor();
            paintColor.setTypeWall("Wall 10");
            paintColor.setColorCode(wall10ColorHash);
            paintColor.setName(wall10ColorName);

            Iterator<PaintColor> iterator = usedColorList.iterator();
            while (iterator.hasNext()) {
                PaintColor paintColor1 = iterator.next();
                if ("Wall 10".equalsIgnoreCase(paintColor1.getTypeWall())) {
                    iterator.remove();
                }
            }
            usedColorList.add(paintColor);
        } else if (ivId == R.id.iv_exterior3_11) {
            addToUndo(ivId);
            ivWall11Red = colors[0];
            ivWall11Green = colors[1];
            ivWall11Blue = colors[2];

            if (ivWall11Red == 0 && ivWall11Green == 0 && ivWall11Blue == 0) {
                Iterator<PaintColor> iterator = usedColorList.iterator();
                while (iterator.hasNext()) {
                    PaintColor paintColor = iterator.next();
                    if ("Wall 11".equalsIgnoreCase(paintColor.getTypeWall())) {
                        iterator.remove();
                    }
                }
                return;
            }
            String wall11ColorHash = ImageUtils.RgbToHex(ivWall11Red, ivWall11Green, ivWall11Blue);
            String wall11ColorName = null;
            for (PaintColor paintColor : global.globalPaintColorList) {
                if (wall11ColorHash.equalsIgnoreCase(paintColor.getColorCode())) {
                    wall11ColorName = paintColor.getName();
                }
            }

            PaintColor paintColor = new PaintColor();
            paintColor.setTypeWall("Wall 11");
            paintColor.setColorCode(wall11ColorHash);
            paintColor.setName(wall11ColorName);

            Iterator<PaintColor> iterator = usedColorList.iterator();
            while (iterator.hasNext()) {
                PaintColor paintColor1 = iterator.next();
                if ("Wall 11".equalsIgnoreCase(paintColor1.getTypeWall())) {
                    iterator.remove();
                }
            }
            usedColorList.add(paintColor);
        } else if (ivId == R.id.iv_exterior3_12) {
            addToUndo(ivId);
            ivWall12Red = colors[0];
            ivWall12Green = colors[1];
            ivWall12Blue = colors[2];

            if (ivWall12Red == 0 && ivWall12Green == 0 && ivWall12Blue == 0) {
                Iterator<PaintColor> iterator = usedColorList.iterator();
                while (iterator.hasNext()) {
                    PaintColor paintColor = iterator.next();
                    if ("Wall 12".equalsIgnoreCase(paintColor.getTypeWall())) {
                        iterator.remove();
                    }
                }
                return;
            }
            String wall12ColorHash = ImageUtils.RgbToHex(ivWall12Red, ivWall12Green, ivWall12Blue);
            String wall12ColorName = null;
            for (PaintColor paintColor : global.globalPaintColorList) {
                if (wall12ColorHash.equalsIgnoreCase(paintColor.getColorCode())) {
                    wall12ColorName = paintColor.getName();
                }
            }

            PaintColor paintColor = new PaintColor();
            paintColor.setTypeWall("Wall 12");
            paintColor.setColorCode(wall12ColorHash);
            paintColor.setName(wall12ColorName);

            Iterator<PaintColor> iterator = usedColorList.iterator();
            while (iterator.hasNext()) {
                PaintColor paintColor1 = iterator.next();
                if ("Wall 12".equalsIgnoreCase(paintColor1.getTypeWall())) {
                    iterator.remove();
                }
            }
            usedColorList.add(paintColor);
        }
    }

    private void addToUndo(int imageId) {
        if (isDonePainting) {
            boolean isFirstTimePainted = true;
            for (PaintColorHistory paintColorHistory : undoColorHistoryList) {
                if (paintColorHistory.getImageViewId() == imageId) {
                    isFirstTimePainted = false;
                    AppLog.showLog(CLASS_TAG, "is first time painted:" + false);
                    break;
                } else {
                    isFirstTimePainted = true;
                    AppLog.showLog(CLASS_TAG, "is first time painted:" + true);
                }
            }
            if (isFirstTimePainted) {
                PaintColorHistory defaultPaintColor = new PaintColorHistory();
                defaultPaintColor.setImageViewId(imageId);
                defaultPaintColor.setColorCode(new Float[]{0f, 0f, 0f});
                undoColorHistoryList.add(defaultPaintColor);

                PaintColorHistory paintColorHistory = new PaintColorHistory();
                paintColorHistory.setColorCode(new Float[]{redScale, greenScale, blueScale});
                paintColorHistory.setImageViewId(imageId);
                undoColorHistoryList.add(paintColorHistory);
                AppLog.showLog(CLASS_TAG, "first time painted: undo size " + undoColorHistoryList.size());
            } else {
                int maxIndex = undoColorHistoryList.size() - 1;
                PaintColorHistory paintColorHistory1 = undoColorHistoryList.get(maxIndex);
                AppLog.showLog(CLASS_TAG, "is id same:" + (paintColorHistory1.getImageViewId() != imageId));
                AppLog.showLog(CLASS_TAG, "is color same:" + (paintColorHistory1.getColorCode()[0] != redScale
                        && paintColorHistory1.getColorCode()[1] != greenScale
                        && paintColorHistory1.getColorCode()[2] != blueScale));
                if ((paintColorHistory1.getImageViewId() != imageId)
                        || (paintColorHistory1.getColorCode()[0] != redScale
                        || paintColorHistory1.getColorCode()[1] != greenScale
                        || paintColorHistory1.getColorCode()[2] != blueScale)) {
                    PaintColorHistory paintColorHistory = new PaintColorHistory();
                    paintColorHistory.setColorCode(new Float[]{redScale, greenScale, blueScale});
                    paintColorHistory.setImageViewId(imageId);
                    undoColorHistoryList.add(paintColorHistory);
                    AppLog.showLog(CLASS_TAG, "not first time painted: undo size " + undoColorHistoryList.size());
                }
            }
        }
    }

    @Override
    public void onViewTouch(PaintColor paintColor) {
        sendToParent(paintColor);
    }

    private class ChoiceDragListener implements View.OnDragListener {

        @Override
        public boolean onDrag(View v, DragEvent event) {
            switch (event.getAction()) {
                case DragEvent.ACTION_DRAG_STARTED:
                    //no action necessary
                    break;
                case DragEvent.ACTION_DRAG_ENTERED:
                    //no action necessary
                    break;
                case DragEvent.ACTION_DRAG_EXITED:
                    //no action necessary
                    break;
                case DragEvent.ACTION_DROP:
                    int[] colors = ImageUtils.startPainting(Exterior3V2Activity.this,
                            ImageUtils.EXTERIOR_TEMPLATE_3, (int) event.getX(),
                            (int) event.getY(), ivMainImage, parent,
                            new float[]{redScale, greenScale, blueScale},
                            ivWall1, ivWall2, ivWall3, ivWall4, ivWall5,
                            ivWall6, ivWall7, ivWall8, ivWall9,
                            ivWall10, ivWall11, ivWall12);
                    isDonePainting = true;
                    updatePaintStatus(colors);

                    //handle the dragged view being dropped over a drop view
                    View view = (View) event.getLocalState();

                    //stop displaying the view where it was before it was dragged
                    ////view.setVisibility(View.INVISIBLE);

                    //view dragged item is being dropped on
                    ImageView dropTarget = (ImageView) v;

                    //view being dragged and dropped
//                    RelativeLayout dropped = (RelativeLayout) view;


                    //if an item has already been dropped here, there will be a tag
                    Object tag = dropTarget.getTag();


                    //set the tag in the target view being dropped on - to the ID of the view being dropped
                    dropTarget.setTag(view.getId());

                    break;
                case DragEvent.ACTION_DRAG_ENDED:
                    //no action necessary
                    break;
                default:
                    break;
            }

            return true;
        }
    }

    private final class ChoiceTouchListener implements View.OnTouchListener {

        @Override
        public boolean onTouch(View v, MotionEvent event) {

            if (event.getAction() == MotionEvent.ACTION_DOWN) {
                GridView parent = (GridView) v;

                int x = (int) event.getX();
                int y = (int) event.getY();

                int position = parent.pointToPosition(x, y);
                if (position > AdapterView.INVALID_POSITION) {

                    int count = parent.getChildCount();

                    AppLog.showLog(CLASS_TAG, "x: " + x + " y:: " + y + " position:: "
                            + position + " count:: " + count);

                    PaintColor paintColor = (PaintColor) gridViewSelectedColors.getAdapter().getItem(position);
                    AppLog.showLog(CLASS_TAG, "grid selected item:: " + paintColor.getName());

                    sendToParent(paintColor);

                    for (int i = 0; i < count; i++) {
                        View view = parent.getChildAt(i);

                        ClipData data = ClipData.newPlainText("color", paintColor.getColorCode());
                        ColorDragShadowBuilder shadowBuilder = new ColorDragShadowBuilder(paintColor.getColorCode());
                        //start dragging the item touched
                        view.startDrag(data, shadowBuilder, view, 0);

                        return true;
                    }
                }
            }
            return false;
        }
    }
}
