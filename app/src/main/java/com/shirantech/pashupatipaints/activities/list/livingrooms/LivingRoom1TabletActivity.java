package com.shirantech.pashupatipaints.activities.list.livingrooms;

import android.annotation.TargetApi;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.os.Vibrator;
import android.support.v4.view.ViewPager;
import android.util.DisplayMetrics;
import android.view.DragEvent;
import android.view.MotionEvent;
import android.view.View;
import android.view.Window;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.actionbarsherlock.app.SherlockFragmentActivity;
import com.jess.ui.TwoWayAdapterView;
import com.jess.ui.TwoWayGridView;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.shirantech.pashupatipaints.R;
import com.shirantech.pashupatipaints.activities.list.GalleryActivity;
import com.shirantech.pashupatipaints.activities.list.ImagePreviewActivity;
import com.shirantech.pashupatipaints.adapter.TabletColorGridPagerAdapter;
import com.shirantech.pashupatipaints.adapter.TabletColorHorizontalListAdapter;
import com.shirantech.pashupatipaints.adapter.UsedColorAdapter;
import com.shirantech.pashupatipaints.customwidgets.CustomBoldTextView;
import com.shirantech.pashupatipaints.fragments.BeigeFragment;
import com.shirantech.pashupatipaints.fragments.BlackFragment;
import com.shirantech.pashupatipaints.fragments.BlueFragment;
import com.shirantech.pashupatipaints.fragments.BrownFragment;
import com.shirantech.pashupatipaints.fragments.GrayFragment;
import com.shirantech.pashupatipaints.fragments.GreenFragment;
import com.shirantech.pashupatipaints.fragments.IndigoFragment;
import com.shirantech.pashupatipaints.fragments.OrangeFragment;
import com.shirantech.pashupatipaints.fragments.RedFragment;
import com.shirantech.pashupatipaints.fragments.VioletFragment;
import com.shirantech.pashupatipaints.fragments.WhiteFragment;
import com.shirantech.pashupatipaints.fragments.YellowFragment;
import com.shirantech.pashupatipaints.interfaces.ViewOnTouchListener;
import com.shirantech.pashupatipaints.model.PaintColor;
import com.shirantech.pashupatipaints.model.PaintColorHistory;
import com.shirantech.pashupatipaints.util.AppLog;
import com.shirantech.pashupatipaints.util.AppText;
import com.shirantech.pashupatipaints.util.Global;
import com.shirantech.pashupatipaints.util.ImageUtils;
import com.shirantech.pashupatipaints.util.Utility;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;

/**
 * Drawing Room One Template Activity for tablet.
 * Created by susan on 12/22/14.
 */
public class LivingRoom1TabletActivity extends SherlockFragmentActivity
        implements
        VioletFragment.onPaintSwatchSelectedListener, IndigoFragment.onPaintSwatchSelectedListener,
        BlueFragment.onPaintSwatchSelectedListener, GreenFragment.onPaintSwatchSelectedListener,
        YellowFragment.onPaintSwatchSelectedListener, OrangeFragment.onPaintSwatchSelectedListener,
        RedFragment.onPaintSwatchSelectedListener, BrownFragment.onPaintSwatchSelectedListener,
        BeigeFragment.onPaintSwatchSelectedListener, GrayFragment.onPaintSwatchSelectedListener,
        WhiteFragment.onPaintSwatchSelectedListener, BlackFragment.onPaintSwatchSelectedListener,
        View.OnClickListener, ViewOnTouchListener {
    private static final String CLASS_TAG = LivingRoom1TabletActivity.class.getSimpleName();
    private ImageView ivMainImage, ivBorderCeiling, ivCeiling, ivRightWall;
    private Button buttonHideNShow;
    private TextView textViewColorName;
    private CustomBoldTextView imageButtonPreviousPage;
    private CustomBoldTextView imageButtonNextPage;
    private ImageLoader imageLoader = ImageLoader.getInstance();
    private ViewPager viewPagerColors;
    private TwoWayGridView horizontalListView;
    private ListView listViewUsedColors;
    private TabletColorGridPagerAdapter adapter;
    private int pagerPagePosition;
    private List<PaintColor> selectedColorsList;
    private Vibrator mVibrator;
    private int x, y;
    Global global;
    List<PaintColor> tempElements = null;
    private static final int VIBRATE_DURATION = 35;
    private TabletColorHorizontalListAdapter paintColorAdapter;

    private float redScale = 1f;
    private float greenScale = 0f;
    private float blueScale = 0f;
    private int borderCeilingRed = -1, borderCeilingGreen = -1, borderCeilingBlue = -1;
    private int ceilingRed = -1, ceilingGreen = -1, ceilingBlue = -1;
    private int rightRed = -1, rightGreen = -1, rightBlue = -1;

    private Boolean isColorClicked = false;
    private List<PaintColor> usedColorList;
    RelativeLayout parent;
    private boolean isDonePainting = false;
    private List<PaintColorHistory> undoColorHistoryList = new ArrayList<PaintColorHistory>();
    private List<PaintColorHistory> redoColorHistoryList = new ArrayList<PaintColorHistory>();
    private LinearLayout linearLayoutPagerTab;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        if (Build.VERSION.SDK_INT <= Build.VERSION_CODES.HONEYCOMB) {
            getSupportActionBar().hide();
        }
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);

        setContentView(R.layout.activity_living_room1);
        parent = (RelativeLayout) findViewById(R.id.roomLayout_living_1);

        listViewUsedColors = (ListView) findViewById(R.id.listview_used_colors);

        imageLoader.init(ImageLoaderConfiguration.createDefault(this));

        ivMainImage = (ImageView) findViewById(R.id.iv_living1_main_image);
        ivBorderCeiling = (ImageView) findViewById(R.id.iv_living1_border_ceiling);
        ivCeiling = (ImageView) findViewById(R.id.iv_living1_ceiling);
        ivRightWall = (ImageView) findViewById(R.id.iv_living1_right_wall);

        ImageUtils.loadImageUsingLoader(imageLoader, ivMainImage, "assets://" + AppText.LIVINGROOM1_LOCATION_MAIN);
        ImageUtils.loadImageUsingLoader(imageLoader, ivBorderCeiling, "assets://" + AppText.LIVINGROOM1_LOCATION_BORDER_CEILING);
        ImageUtils.loadImageUsingLoader(imageLoader, ivCeiling, "assets://" + AppText.LIVINGROOM1_LOCATION_CEILING);
        ImageUtils.loadImageUsingLoader(imageLoader, ivRightWall, "assets://" + AppText.LIVINGROOM1_LOCATION_RIGHT);

        textViewColorName = (TextView) findViewById(R.id.textview_color_name);

        linearLayoutPagerTab = (LinearLayout) findViewById(R.id.linear_layout_pager_tab);
        viewPagerColors = (ViewPager) findViewById(R.id.viewpager_colors);
        adapter = new TabletColorGridPagerAdapter(getSupportFragmentManager());
        textViewColorName.setText(adapter.getPageTitle(0));
        viewPagerColors.setAdapter(adapter);
        linearLayoutPagerTab.setBackground(Utility.getCustomShape(Color.parseColor("#7F00FF")));
        viewPagerColors.setOnPageChangeListener(new ViewPager.SimpleOnPageChangeListener() {
            @Override
            public void onPageSelected(int position) {
                pagerPagePosition = position;
                textViewColorName.setText(adapter.getPageTitle(position));
                switch (position) {
                    case 0:
                        linearLayoutPagerTab.setBackground(Utility.getCustomShape(Color.parseColor("#7F00FF")));
                        textViewColorName.setTextColor(Color.BLACK);
                        imageButtonNextPage.setTextColor(Color.BLACK);
                        imageButtonPreviousPage.setTextColor(Color.BLACK);
                        imageButtonPreviousPage.setVisibility(View.INVISIBLE);
                        imageButtonNextPage.setVisibility(View.VISIBLE);
                        break;
                    case 1:
                        linearLayoutPagerTab.setBackground(Utility.getCustomShape(Color.parseColor("#4b0082")));
                        textViewColorName.setTextColor(Color.BLACK);
                        imageButtonNextPage.setTextColor(Color.BLACK);
                        imageButtonPreviousPage.setTextColor(Color.BLACK);
                        imageButtonPreviousPage.setVisibility(View.VISIBLE);
                        imageButtonNextPage.setVisibility(View.VISIBLE);
                        break;
                    case 2:
                        linearLayoutPagerTab.setBackground(Utility.getCustomShape(Color.BLUE));
                        textViewColorName.setTextColor(Color.BLACK);
                        imageButtonNextPage.setTextColor(Color.BLACK);
                        imageButtonPreviousPage.setTextColor(Color.BLACK);
                        imageButtonPreviousPage.setVisibility(View.VISIBLE);
                        imageButtonNextPage.setVisibility(View.VISIBLE);
                        break;
                    case 3:
                        linearLayoutPagerTab.setBackground(Utility.getCustomShape(Color.GREEN));
                        textViewColorName.setTextColor(Color.BLACK);
                        imageButtonNextPage.setTextColor(Color.BLACK);
                        imageButtonPreviousPage.setTextColor(Color.BLACK);
                        imageButtonPreviousPage.setVisibility(View.VISIBLE);
                        imageButtonNextPage.setVisibility(View.VISIBLE);
                        break;
                    case 4:
                        linearLayoutPagerTab.setBackground(Utility.getCustomShape(Color.parseColor("#FFD700")));
                        textViewColorName.setTextColor(Color.BLACK);
                        imageButtonNextPage.setTextColor(Color.BLACK);
                        imageButtonPreviousPage.setTextColor(Color.BLACK);
                        imageButtonPreviousPage.setVisibility(View.VISIBLE);
                        imageButtonNextPage.setVisibility(View.VISIBLE);
                        break;
                    case 5:
                        linearLayoutPagerTab.setBackground(Utility.getCustomShape(Color.parseColor("#FFA500")));
                        textViewColorName.setTextColor(Color.BLACK);
                        imageButtonNextPage.setTextColor(Color.BLACK);
                        imageButtonPreviousPage.setTextColor(Color.BLACK);
                        imageButtonPreviousPage.setVisibility(View.VISIBLE);
                        imageButtonNextPage.setVisibility(View.VISIBLE);
                        break;
                    case 6:
                        linearLayoutPagerTab.setBackground(Utility.getCustomShape(Color.RED));
                        textViewColorName.setTextColor(Color.BLACK);
                        imageButtonNextPage.setTextColor(Color.BLACK);
                        imageButtonPreviousPage.setTextColor(Color.BLACK);
                        imageButtonPreviousPage.setVisibility(View.VISIBLE);
                        imageButtonNextPage.setVisibility(View.VISIBLE);
                        break;
                    case 7:
                        linearLayoutPagerTab.setBackground(Utility.getCustomShape(Color.parseColor("#f4a460")));
                        textViewColorName.setTextColor(Color.BLACK);
                        imageButtonNextPage.setTextColor(Color.BLACK);
                        imageButtonPreviousPage.setTextColor(Color.BLACK);
                        imageButtonPreviousPage.setVisibility(View.VISIBLE);
                        imageButtonNextPage.setVisibility(View.VISIBLE);
                        break;
                    case 8:
                        linearLayoutPagerTab.setBackground(Utility.getCustomShape(Color.parseColor("#F5F5DC")));
                        textViewColorName.setTextColor(Color.BLACK);
                        imageButtonNextPage.setTextColor(Color.BLACK);
                        imageButtonPreviousPage.setTextColor(Color.BLACK);
                        imageButtonPreviousPage.setVisibility(View.VISIBLE);
                        imageButtonNextPage.setVisibility(View.VISIBLE);
                        break;
                    case 9:
                        linearLayoutPagerTab.setBackground(Utility.getCustomShape(Color.parseColor("#d3d3d3")));
                        textViewColorName.setTextColor(Color.BLACK);
                        imageButtonNextPage.setTextColor(Color.BLACK);
                        imageButtonPreviousPage.setTextColor(Color.BLACK);
                        imageButtonPreviousPage.setVisibility(View.VISIBLE);
                        imageButtonNextPage.setVisibility(View.VISIBLE);
                        break;
                    case 10:
                        linearLayoutPagerTab.setBackground(Utility.getCustomShape(Color.WHITE));
                        textViewColorName.setTextColor(Color.BLACK);
                        imageButtonNextPage.setTextColor(Color.BLACK);
                        imageButtonPreviousPage.setTextColor(Color.BLACK);
                        imageButtonPreviousPage.setVisibility(View.VISIBLE);
                        imageButtonNextPage.setVisibility(View.VISIBLE);

                        break;
                    case 11:
                        linearLayoutPagerTab.setBackground(Utility.getCustomShape(Color.BLACK));
                        textViewColorName.setTextColor(Color.WHITE);
                        imageButtonNextPage.setTextColor(Color.WHITE);
                        imageButtonPreviousPage.setTextColor(Color.WHITE);
                        imageButtonPreviousPage.setVisibility(View.VISIBLE);
                        imageButtonNextPage.setVisibility(View.GONE);
                        break;
                }

            }
        });
        viewPagerColors.setCurrentItem(0);


        imageButtonPreviousPage = (CustomBoldTextView) findViewById(R.id.button_previous_page);
        imageButtonNextPage = (CustomBoldTextView) findViewById(R.id.button_next_page);
        buttonHideNShow = (Button) findViewById(R.id.button_hide_and_show_hlv);
        ImageButton imageButtonRefresh = (ImageButton) findViewById(R.id.imagebutton_paint_refresh);
        ImageButton imageButtonSave = (ImageButton) findViewById(R.id.imagebutton_paint_save);
        ImageButton imageButtonShare = (ImageButton) findViewById(R.id.imagebutton_paint_share);
        ImageButton imageButtonUndo = (ImageButton) findViewById(R.id.imagebutton_paint_undo);
        ImageButton imageButtonRedo = (ImageButton) findViewById(R.id.imagebutton_paint_redo);
        CustomBoldTextView imageButtonGoToGallery = (CustomBoldTextView) findViewById(R.id.imagebutton_go_to_gallery);
        ImageButton imageButtonBack = (ImageButton) findViewById(R.id.home);

        imageButtonBack.setOnClickListener(this);
        imageButtonGoToGallery.setOnClickListener(this);
        imageButtonRedo.setOnClickListener(this);
        imageButtonUndo.setOnClickListener(this);
        imageButtonShare.setOnClickListener(this);
        imageButtonSave.setOnClickListener(this);
        imageButtonRefresh.setOnClickListener(this);
        buttonHideNShow.setOnClickListener(this);
        imageButtonNextPage.setOnClickListener(this);
        imageButtonPreviousPage.setOnClickListener(this);
        imageButtonPreviousPage.setVisibility(View.INVISIBLE);

        horizontalListView = (TwoWayGridView) findViewById(R.id.hlv_selected_colors);
        selectedColorsList = new ArrayList<PaintColor>();
        usedColorList = new ArrayList<PaintColor>();
        global = (Global) getApplication();
        if (global.getGlobalPaintColorList() != null) {
            if (global.getGlobalPaintColorList().size() > 0) {
                global.getGlobalPaintColorList().clear();
            }
        }

        final RelativeLayout.LayoutParams params = (RelativeLayout.LayoutParams) buttonHideNShow.getLayoutParams();
        buttonHideNShow.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent event) {
                switch (event.getAction()) {
                    case MotionEvent.ACTION_DOWN:
                        AppLog.showLog(CLASS_TAG, "Action Down entered");
                        x = (int) event.getX();
                        y = (int) event.getY();
                        break;
                    case MotionEvent.ACTION_MOVE:
                        AppLog.showLog(CLASS_TAG, "Action Move entered:::");

                        if (y < event.getY() && horizontalListView.getVisibility() == View.VISIBLE) {
                            AppLog.showLog(CLASS_TAG, "Hide layout() old x: " + x + " new x: " + event.getX()
                                    + " old y: " + y + " new y: " + event.getY());

                            Animation slideDown = AnimationUtils.loadAnimation(LivingRoom1TabletActivity.this, R.anim.slide_down);
                            slideDown.setAnimationListener(new Animation.AnimationListener() {
                                @Override
                                public void onAnimationStart(Animation animation) {

                                }

                                @Override
                                public void onAnimationEnd(Animation animation) {
                                    params.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM);
                                    horizontalListView.setVisibility(View.GONE);
                                    buttonHideNShow.setText("^");
                                }

                                @Override
                                public void onAnimationRepeat(Animation animation) {

                                }
                            });
                            buttonHideNShow.startAnimation(slideDown);
                            horizontalListView.startAnimation(slideDown);
                        } else if (y > event.getY() && horizontalListView.getVisibility() != View.VISIBLE) {
                            AppLog.showLog(CLASS_TAG, "Show layout() old x: " + x + " new x: " + event.getX()
                                    + " old y: " + y + " new y: " + event.getY());
                            final Animation slideUp = AnimationUtils.loadAnimation(LivingRoom1TabletActivity.this, R.anim.slide_up);
                            slideUp.setAnimationListener(new Animation.AnimationListener() {
                                @TargetApi(Build.VERSION_CODES.JELLY_BEAN_MR1)
                                @Override
                                public void onAnimationStart(Animation animation) {
                                    if (Build.VERSION.SDK_INT > Build.VERSION_CODES.JELLY_BEAN_MR1) {
                                        params.removeRule(RelativeLayout.ALIGN_PARENT_BOTTOM);
                                    } else {
                                        params.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM, 0);
                                    }
                                    params.addRule(RelativeLayout.ABOVE, R.id.hlv_selected_colors);
                                }


                                @Override
                                public void onAnimationEnd(Animation animation) {
                                    horizontalListView.setVisibility(View.VISIBLE);
                                    buttonHideNShow.setText("v");
                                }

                                @Override
                                public void onAnimationRepeat(Animation animation) {

                                }
                            });
                            buttonHideNShow.startAnimation(slideUp);
                            horizontalListView.startAnimation(slideUp);
                        }
                        break;
                    case MotionEvent.ACTION_UP:
                        x = (int) event.getX();
                        y = (int) event.getY();
                        break;
                }
                return false;
            }
        });
        mVibrator = (Vibrator) this.getSystemService(Context.VIBRATOR_SERVICE);


        horizontalListView.setOnItemLongClickListener(new TwoWayAdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(TwoWayAdapterView<?> parent, View view, final int position, long id) {
                mVibrator.vibrate(VIBRATE_DURATION);
                TabletColorHorizontalListAdapter.ViewHolder holder = (TabletColorHorizontalListAdapter.ViewHolder) view.getTag();
                holder.imageButtonDelete.setVisibility(View.VISIBLE);
                holder.imageButtonDelete.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        final PaintColor pc = paintColorAdapter.getItem(position);
                        selectedColorsList.remove(pc);
                        tempElements.remove(pc);
                        paintColorAdapter = new TabletColorHorizontalListAdapter(LivingRoom1TabletActivity.this,
                                tempElements, LivingRoom1TabletActivity.this);
                        horizontalListView.setAdapter(paintColorAdapter);
                        paintColorAdapter.notifyDataSetChanged();
                    }
                });
                return true;
            }
        });
       /* horizontalListView.setOnItemLongClickListener(new TwoWayAdapterView.OnItemLongClickListener() {

            @Override
            public boolean onItemLongClick(TwoWayAdapterView<?> parent, View view, int position, long id) {
                mVibrator.vibrate(VIBRATE_DURATION);

                final PaintColor pc = paintColorAdapter.getItem(position);

                AlertDialog.Builder builder = new AlertDialog.Builder(LivingRoom1TabletActivity.this);
                builder.setMessage(
                        "Delete selected color?")
                        .setCancelable(false)
                                // Prevents user to use "back button"
                        .setPositiveButton("Delete",
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int id) {
                                        AppLog.showLog(CLASS_TAG, "OK clicked!! id: " + id);
                                        *//* delete *//*
                                        selectedColorsList.remove(pc);
                                        tempElements.remove(pc);
                                        paintColorAdapter = new TabletColorHorizontalListAdapter(LivingRoom1TabletActivity.this,
                                                tempElements, LivingRoom1TabletActivity.this);
                                        horizontalListView.setAdapter(paintColorAdapter);
                                        paintColorAdapter.notifyDataSetChanged();

                                    }
                                }
                        )
                        .setNegativeButton("Cancel",
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int id) {
                                        AppLog.showLog(CLASS_TAG, "cancel clicked!! id: " + id);
                                        dialog.cancel();
                                    }
                                }
                        );
                builder.show();
                return false;
            }
        });*/
//        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.HONEYCOMB) {
//            horizontalListView.setOnTouchListener(new ChoiceTouchListener());
//        }
        horizontalListView.setOnItemClickListener(new TwoWayAdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(TwoWayAdapterView<?> parent, View view, int position, long id) {
                if (((TabletColorHorizontalListAdapter.ViewHolder) view.getTag()).imageButtonDelete.getVisibility() == View.VISIBLE) {
                    ((TabletColorHorizontalListAdapter.ViewHolder) view.getTag()).imageButtonDelete.setVisibility(View.GONE);
                }
                PaintColor paintColor = (PaintColor) horizontalListView.getAdapter().getItem(position);
                AppLog.showLog(CLASS_TAG, "grid selected item:: " + paintColor.getName());
                sendToParent(paintColor);
            }
        });
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.HONEYCOMB) {
//            drag and drop!!
            ivRightWall.setOnDragListener(new ChoiceDragListener());
        }
        ivRightWall.setOnTouchListener(new View.OnTouchListener() {

            @Override
            public boolean onTouch(View v, MotionEvent event) {

                if (isColorClicked) {
                    int[] colors = ImageUtils.startPainting(LivingRoom1TabletActivity.this,
                            ImageUtils.LIVINGROOM_TEMPLATE_1, (int) event.getX(), (int) event.getY(), ivMainImage,
                            parent, new float[]{redScale, greenScale, blueScale},
                            ivBorderCeiling, ivCeiling, ivRightWall);
                    isDonePainting = true;
                    updatePaintStatus(colors);
                } else {
                    Toast.makeText(getApplicationContext(), "Please select the color below to paint the rooms", Toast.LENGTH_SHORT).show();
                }
                return false;
            }
        });

        //Determine density
        DisplayMetrics metrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(metrics);
        int density = metrics.densityDpi;

        if (density == DisplayMetrics.DENSITY_HIGH) {
            AppLog.showLog(CLASS_TAG, "DENSITY_HIGH... Density is " + String.valueOf(density));
        } else if (density == DisplayMetrics.DENSITY_MEDIUM) {
            AppLog.showLog(CLASS_TAG, "DENSITY_MEDIUM... Density is " + String.valueOf(density));
        } else if (density == DisplayMetrics.DENSITY_LOW) {
            AppLog.showLog(CLASS_TAG, "DENSITY_LOW... Density is " + String.valueOf(density));
        } else {
            AppLog.showLog(CLASS_TAG, "Density is neither HIGH, MEDIUM OR LOW.  Density is " + String.valueOf(density));
/*
            parent.setScaleX(2.0f);
            parent.setScaleY(2.0f);

            parent.post(new Runnable() {

                @Override
                public void run() {
                    int h, w;
                    h = parent.getHeight();
                    w = parent.getWidth();

                    RelativeLayout.LayoutParams tableLP = new RelativeLayout.LayoutParams(w * 2, h * 2);
                    tableLP.addRule(RelativeLayout.CENTER_IN_PARENT, 1);
                    tableLP.addRule(RelativeLayout.ALIGN_PARENT_TOP, 1);
                    parent.setLayoutParams(tableLP);

                }
            });*/
        }
    }

    private void sendToParent(PaintColor paintColor) {
        isColorClicked = true;
        Float[] rgb = ImageUtils.hexToRgb(paintColor.getColorCode());
        redScale = rgb[0];
        greenScale = rgb[1];
        blueScale = rgb[2];
        AppLog.showLog(CLASS_TAG, "  we get:::::: redScale:: " + redScale + " greenScale:: " + greenScale + " blueScale:: " + blueScale);
    }

    private void hideOrShowColorListView() {
        final RelativeLayout.LayoutParams params = (RelativeLayout.LayoutParams) buttonHideNShow.getLayoutParams();
        if (horizontalListView.getVisibility() == View.VISIBLE) {
            Animation slideDown = AnimationUtils.loadAnimation(this, R.anim.slide_down);
            slideDown.setAnimationListener(new Animation.AnimationListener() {
                @Override
                public void onAnimationStart(Animation animation) {

                }

                @Override
                public void onAnimationEnd(Animation animation) {
                    params.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM);
                    horizontalListView.setVisibility(View.GONE);
                    buttonHideNShow.setText("^");
                }

                @Override
                public void onAnimationRepeat(Animation animation) {

                }
            });
            horizontalListView.startAnimation(slideDown);

        } else {

            Animation slideUp = AnimationUtils.loadAnimation(this, R.anim.slide_up);
            slideUp.setAnimationListener(new Animation.AnimationListener() {
                @Override
                public void onAnimationStart(Animation animation) {
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) {
                        params.removeRule(RelativeLayout.ALIGN_PARENT_BOTTOM);
                    } else {
                        params.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM, 0);
                    }
                    params.addRule(RelativeLayout.ABOVE, R.id.hlv_selected_colors);
                }

                @Override
                public void onAnimationEnd(Animation animation) {
                    horizontalListView.setVisibility(View.VISIBLE);
                    buttonHideNShow.setText("v");
                }

                @Override
                public void onAnimationRepeat(Animation animation) {

                }
            });

            horizontalListView.startAnimation(slideUp);
        }
    }

    @Override
    public void onViewTouch(PaintColor paintColor) {
        findViewById(R.id.imageview_selected_color)
                .setBackground(Utility.getCustomShape(Color.parseColor(paintColor.getColorCode())));
        ((TextView) findViewById(R.id.textview_selected_color_name))
                .setText(paintColor.getName());
        ((TextView) findViewById(R.id.textview_selected_color_code))
                .setText(paintColor.getColorCode());
        sendToParent(paintColor);
    }

    private class ChoiceDragListener implements View.OnDragListener {

        @Override
        public boolean onDrag(View v, DragEvent event) {
            switch (event.getAction()) {
                case DragEvent.ACTION_DRAG_STARTED:
                    //no action necessary
                    break;
                case DragEvent.ACTION_DRAG_ENTERED:
                    //no action necessary
                    break;
                case DragEvent.ACTION_DRAG_EXITED:
                    //no action necessary
                    break;
                case DragEvent.ACTION_DROP:
                    int[] colors = ImageUtils.startPainting(LivingRoom1TabletActivity.this,
                            ImageUtils.LIVINGROOM_TEMPLATE_1, (int) event.getX(), (int) event.getY(), ivMainImage,
                            parent, new float[]{redScale, greenScale, blueScale},
                            ivBorderCeiling, ivCeiling, ivRightWall);
                    isDonePainting = true;
                    updatePaintStatus(colors);

                    //handle the dragged view being dropped over a drop view
                    View view = (View) event.getLocalState();

                    //stop displaying the view where it was before it was dragged
                    ////view.setVisibility(View.INVISIBLE);

                    //view dragged item is being dropped on
                    ImageView dropTarget = (ImageView) v;

                    //view being dragged and dropped
//                    RelativeLayout dropped = (RelativeLayout) view;


                    //if an item has already been dropped here, there will be a tag
                    Object tag = dropTarget.getTag();


                    //set the tag in the target view being dropped on - to the ID of the view being dropped
                    dropTarget.setTag(view.getId());

                    break;
                case DragEvent.ACTION_DRAG_ENDED:
                    //no action necessary
                    break;
                default:
                    break;
            }

            return true;
        }
    }

    private void updatePaintStatus(int[] colors) {
        if (colors[3] == R.id.iv_living1_border_ceiling) {
            if (isDonePainting) {
                boolean isFirstTimePainted = true;
                for (PaintColorHistory paintColorHistory : undoColorHistoryList) {
                    if (paintColorHistory.getImageViewId() == R.id.iv_living1_border_ceiling) {
                        isFirstTimePainted = false;
                        break;
                    } else {
                        isFirstTimePainted = true;
                    }
                }

                if (isFirstTimePainted) {
                    PaintColorHistory defaultPaintColor = new PaintColorHistory();
                    defaultPaintColor.setImageViewId(R.id.iv_living1_border_ceiling);
                    defaultPaintColor.setColorCode(new Float[]{0f, 0f, 0f});
                    undoColorHistoryList.add(defaultPaintColor);

                    PaintColorHistory paintColorHistory = new PaintColorHistory();
                    paintColorHistory.setColorCode(new Float[]{redScale, greenScale, blueScale});
                    paintColorHistory.setImageViewId(R.id.iv_living1_border_ceiling);
                    undoColorHistoryList.add(paintColorHistory);
                } else {
                    int maxIndex = undoColorHistoryList.size() - 1;
                    PaintColorHistory paintColorHistory1 = undoColorHistoryList.get(maxIndex);
                    if (paintColorHistory1.getImageViewId() != R.id.iv_living1_border_ceiling &&
                            (paintColorHistory1.getColorCode()[0] != redScale && paintColorHistory1.getColorCode()[1] != greenScale
                                    && paintColorHistory1.getColorCode()[2] != blueScale)) {
                        PaintColorHistory paintColorHistory = new PaintColorHistory();
                        paintColorHistory.setColorCode(new Float[]{redScale, greenScale, blueScale});
                        paintColorHistory.setImageViewId(R.id.iv_living1_border_ceiling);
                        undoColorHistoryList.add(paintColorHistory);
                    }
                }
            }
            borderCeilingRed = colors[0];
            borderCeilingGreen = colors[1];
            borderCeilingBlue = colors[2];

            if (borderCeilingRed == 0 && borderCeilingGreen == 0 && borderCeilingBlue == 0) {
                Iterator<PaintColor> iterator = usedColorList.iterator();
                while (iterator.hasNext()) {
                    PaintColor paintColor = iterator.next();
                    if ("Ceiling Border".equalsIgnoreCase(paintColor.getTypeWall())) {
                        iterator.remove();
                    }
                }
                listViewUsedColors.setAdapter(new UsedColorAdapter(LivingRoom1TabletActivity.this,
                        usedColorList));
                return;
            }

            //Ceiling border
            String borderCeilingColorHash = ImageUtils.RgbToHex(colors[0], colors[1], colors[2]);
            String borderCeilingColorName = null;
            for (PaintColor aGlobalPaintColorList : global.globalPaintColorList) {
                if (aGlobalPaintColorList.getColorCode().equalsIgnoreCase(borderCeilingColorHash)) {
                    borderCeilingColorName = aGlobalPaintColorList.getName();
                }
            }
            PaintColor paintColor = new PaintColor();
            paintColor.setColorCode(borderCeilingColorHash);
            paintColor.setName(borderCeilingColorName);
            paintColor.setTypeWall("Ceiling Border");

            AppLog.showLog(CLASS_TAG, "Used color list size: " + usedColorList.size());
            Iterator<PaintColor> iterator = usedColorList.iterator();
            while (iterator.hasNext()) {
                PaintColor paintColor1 = iterator.next();
                if ("Ceiling Border".equalsIgnoreCase(paintColor1.getTypeWall())) {
                    iterator.remove();
                }
            }

            usedColorList.add(paintColor);
            listViewUsedColors.setAdapter(new UsedColorAdapter(LivingRoom1TabletActivity.this, usedColorList));
        } else if (colors[3] == R.id.iv_living1_ceiling) {

            if (isDonePainting) {

                boolean isFirstTimePainted = true;
                for (PaintColorHistory paintColorHistory : undoColorHistoryList) {
                    if (paintColorHistory.getImageViewId() == R.id.iv_living1_ceiling) {
                        isFirstTimePainted = false;
                        break;
                    } else {
                        isFirstTimePainted = true;
                    }
                }

                if (isFirstTimePainted) {
                    PaintColorHistory defaultPaintColor = new PaintColorHistory();
                    defaultPaintColor.setImageViewId(R.id.iv_living1_ceiling);
                    defaultPaintColor.setColorCode(new Float[]{0f, 0f, 0f});
                    undoColorHistoryList.add(defaultPaintColor);

                    PaintColorHistory paintColorHistory = new PaintColorHistory();
                    paintColorHistory.setColorCode(new Float[]{redScale, greenScale, blueScale});
                    paintColorHistory.setImageViewId(R.id.iv_living1_ceiling);
                    undoColorHistoryList.add(paintColorHistory);
                } else {
                    int maxIndex = undoColorHistoryList.size() - 1;
                    PaintColorHistory paintColorHistory1 = undoColorHistoryList.get(maxIndex);
                    if (paintColorHistory1.getImageViewId() != R.id.iv_living1_ceiling &&
                            (paintColorHistory1.getColorCode()[0] != redScale && paintColorHistory1.getColorCode()[1] != greenScale
                                    && paintColorHistory1.getColorCode()[2] != blueScale)) {
                        PaintColorHistory paintColorHistory = new PaintColorHistory();
                        paintColorHistory.setColorCode(new Float[]{redScale, greenScale, blueScale});
                        paintColorHistory.setImageViewId(R.id.iv_living1_ceiling);
                        undoColorHistoryList.add(paintColorHistory);
                    }
                }
            }
            ceilingRed = colors[0];
            ceilingGreen = colors[1];
            ceilingBlue = colors[2];
            if (ceilingRed == 0 && ceilingGreen == 0 && ceilingBlue == 0) {
                Iterator<PaintColor> iterator = usedColorList.iterator();
                while (iterator.hasNext()) {
                    PaintColor paintColor = iterator.next();
                    if ("Ceiling".equalsIgnoreCase(paintColor.getTypeWall())) {
                        iterator.remove();
                    }
                }
                listViewUsedColors.setAdapter(new UsedColorAdapter(LivingRoom1TabletActivity.this,
                        usedColorList));
                return;
            }

            //Ceiling
            String ceilingColorHash = ImageUtils.RgbToHex(colors[0], colors[1], colors[2]);
            String ceilingColorName = null;
            for (PaintColor aGlobalPaintColorList : global.globalPaintColorList) {
                if (aGlobalPaintColorList.getColorCode().equalsIgnoreCase(ceilingColorHash)) {
                    ceilingColorName = aGlobalPaintColorList.getName();
                }
            }
            PaintColor paintColor = new PaintColor();
            paintColor.setColorCode(ceilingColorHash);
            paintColor.setName(ceilingColorName);
            paintColor.setTypeWall("Ceiling");

            AppLog.showLog(CLASS_TAG, "Used color list size: " + usedColorList.size());

            Iterator<PaintColor> iterator = usedColorList.iterator();
            while (iterator.hasNext()) {
                PaintColor paintColor1 = iterator.next();
                if ("Ceiling".equalsIgnoreCase(paintColor1.getTypeWall())) {
                    iterator.remove();
                }
            }
            usedColorList.add(paintColor);
            listViewUsedColors.setAdapter(new UsedColorAdapter(LivingRoom1TabletActivity.this, usedColorList));
        } else if (colors[3] == R.id.iv_living1_right_wall) {
            if (isDonePainting) {

                boolean isFirstTimePainted = true;
                for (PaintColorHistory paintColorHistory : undoColorHistoryList) {
                    if (paintColorHistory.getImageViewId() == R.id.iv_living1_right_wall) {
                        isFirstTimePainted = false;
                        break;
                    } else {
                        isFirstTimePainted = true;
                    }
                }

                if (isFirstTimePainted) {
                    PaintColorHistory defaultPaintColor = new PaintColorHistory();
                    defaultPaintColor.setImageViewId(R.id.iv_living1_right_wall);
                    defaultPaintColor.setColorCode(new Float[]{0f, 0f, 0f});
                    undoColorHistoryList.add(defaultPaintColor);

                    PaintColorHistory paintColorHistory = new PaintColorHistory();
                    paintColorHistory.setColorCode(new Float[]{redScale, greenScale, blueScale});
                    paintColorHistory.setImageViewId(R.id.iv_living1_right_wall);
                    undoColorHistoryList.add(paintColorHistory);
                } else {
                    int maxIndex = undoColorHistoryList.size() - 1;
                    PaintColorHistory paintColorHistory1 = undoColorHistoryList.get(maxIndex);
                    if (paintColorHistory1.getImageViewId() != R.id.iv_living1_right_wall ||
                            (paintColorHistory1.getColorCode()[0] != redScale
                                    || paintColorHistory1.getColorCode()[1] != greenScale
                                    || paintColorHistory1.getColorCode()[2] != blueScale)) {
                        PaintColorHistory paintColorHistory = new PaintColorHistory();
                        paintColorHistory.setColorCode(new Float[]{redScale, greenScale, blueScale});
                        paintColorHistory.setImageViewId(R.id.iv_living1_right_wall);
                        undoColorHistoryList.add(paintColorHistory);
                    }
                }
            }
            rightRed = colors[0];
            rightGreen = colors[1];
            rightBlue = colors[2];

            if (rightRed == 0 && rightGreen == 0 && rightBlue == 0) {
                Iterator<PaintColor> iterator = usedColorList.iterator();
                while (iterator.hasNext()) {
                    PaintColor paintColor = iterator.next();
                    if ("Right Wall".equalsIgnoreCase(paintColor.getTypeWall())) {
                        iterator.remove();
                    }
                }
                listViewUsedColors.setAdapter(new UsedColorAdapter(LivingRoom1TabletActivity.this,
                        usedColorList));
                return;
            }
            // right wall
            String rightColorHash = ImageUtils.RgbToHex(colors[0], colors[1], colors[2]);
            String rightColorName = null;
            for (PaintColor aGlobalPaintColorList : global.globalPaintColorList) {
                if (aGlobalPaintColorList.getColorCode().equalsIgnoreCase(rightColorHash)) {
                    rightColorName = aGlobalPaintColorList.getName();
                }
            }
            PaintColor paintColor = new PaintColor();
            paintColor.setColorCode(rightColorHash);
            paintColor.setName(rightColorName);
            paintColor.setTypeWall("Right Wall");
            AppLog.showLog(CLASS_TAG, "Used color list size: " + usedColorList.size());

            Iterator<PaintColor> iterator = usedColorList.iterator();
            while (iterator.hasNext()) {
                PaintColor paintColor1 = iterator.next();
                if ("Right Wall".equalsIgnoreCase(paintColor1.getTypeWall())) {
                    iterator.remove();
                }
            }
            usedColorList.add(paintColor);
            listViewUsedColors.setAdapter(new UsedColorAdapter(LivingRoom1TabletActivity.this, usedColorList));
        }
    }

    @Override
    public void onPaintSelected(PaintColor paintColor) {
        AppLog.showLog(CLASS_TAG, "onPaint sELECTED!! :: paintcolor id passed:: " + paintColor.getId());

        for (PaintColor p : selectedColorsList) {
            AppLog.showLog(CLASS_TAG, "in for loop:: paint-color id passed:: " + paintColor.getId());
            AppLog.showLog(CLASS_TAG, "selected color list in loop:: " + selectedColorsList.size() + " p id:: " + p.getId());
            if (p.getId().equals(paintColor.getId()) || p.getId().intValue() == paintColor.getId().intValue()) {
                Toast.makeText(getApplicationContext(), "The color is already selected.", Toast.LENGTH_SHORT).show();
                return;
            }
        }
        selectedColorsList.add(paintColor);
        AppLog.showLog(CLASS_TAG, "selectedColorsList size:: " + selectedColorsList.size());

        //global = (Global)getApplication();
        global.setGlobalPaintColorList(selectedColorsList);

        //AUTO SCROLL THE SELECTED COLORS!!
        tempElements = new ArrayList<PaintColor>(selectedColorsList);
        Collections.reverse(tempElements);

        paintColorAdapter = new TabletColorHorizontalListAdapter(getApplicationContext(), tempElements, this);
        horizontalListView.setAdapter(paintColorAdapter);
        paintColorAdapter.notifyDataSetChanged();
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.home:
                finish();
                break;
            case R.id.button_next_page:
                if (pagerPagePosition >= adapter.getCount()) {
                    return;
                }

                viewPagerColors.setCurrentItem(viewPagerColors.getCurrentItem() + 1, true);
                textViewColorName.setText(adapter.getPageTitle(viewPagerColors.getCurrentItem()));
                switch (pagerPagePosition) {
                    case 0:
                        linearLayoutPagerTab.setBackground(Utility.getCustomShape(Color.parseColor("#7F00FF")));
                        textViewColorName.setTextColor(Color.BLACK);
                        imageButtonNextPage.setTextColor(Color.BLACK);
                        imageButtonPreviousPage.setTextColor(Color.BLACK);
                        imageButtonPreviousPage.setVisibility(View.INVISIBLE);
                        imageButtonNextPage.setVisibility(View.VISIBLE);
                        break;
                    case 1:
                        linearLayoutPagerTab.setBackground(Utility.getCustomShape(Color.parseColor("#4b0082")));
                        textViewColorName.setTextColor(Color.BLACK);
                        imageButtonNextPage.setTextColor(Color.BLACK);
                        imageButtonPreviousPage.setTextColor(Color.BLACK);
                        imageButtonPreviousPage.setVisibility(View.VISIBLE);
                        imageButtonNextPage.setVisibility(View.VISIBLE);
                        break;
                    case 2:
                        linearLayoutPagerTab.setBackground(Utility.getCustomShape(Color.BLUE));
                        textViewColorName.setTextColor(Color.BLACK);
                        imageButtonNextPage.setTextColor(Color.BLACK);
                        imageButtonPreviousPage.setTextColor(Color.BLACK);
                        imageButtonPreviousPage.setVisibility(View.VISIBLE);
                        imageButtonNextPage.setVisibility(View.VISIBLE);
                        break;
                    case 3:
                        linearLayoutPagerTab.setBackground(Utility.getCustomShape(Color.GREEN));
                        textViewColorName.setTextColor(Color.BLACK);
                        imageButtonNextPage.setTextColor(Color.BLACK);
                        imageButtonPreviousPage.setTextColor(Color.BLACK);
                        imageButtonPreviousPage.setVisibility(View.VISIBLE);
                        imageButtonNextPage.setVisibility(View.VISIBLE);
                        break;
                    case 4:
                        linearLayoutPagerTab.setBackground(Utility.getCustomShape(Color.parseColor("#FFD700")));
                        textViewColorName.setTextColor(Color.BLACK);
                        imageButtonNextPage.setTextColor(Color.BLACK);
                        imageButtonPreviousPage.setTextColor(Color.BLACK);
                        imageButtonPreviousPage.setVisibility(View.VISIBLE);
                        imageButtonNextPage.setVisibility(View.VISIBLE);
                        break;
                    case 5:
                        linearLayoutPagerTab.setBackground(Utility.getCustomShape(Color.parseColor("#FFA500")));
                        textViewColorName.setTextColor(Color.BLACK);
                        imageButtonNextPage.setTextColor(Color.BLACK);
                        imageButtonPreviousPage.setTextColor(Color.BLACK);
                        imageButtonPreviousPage.setVisibility(View.VISIBLE);
                        imageButtonNextPage.setVisibility(View.VISIBLE);
                        break;
                    case 6:
                        linearLayoutPagerTab.setBackground(Utility.getCustomShape(Color.RED));
                        textViewColorName.setTextColor(Color.BLACK);
                        imageButtonNextPage.setTextColor(Color.BLACK);
                        imageButtonPreviousPage.setTextColor(Color.BLACK);
                        imageButtonPreviousPage.setVisibility(View.VISIBLE);
                        imageButtonNextPage.setVisibility(View.VISIBLE);
                        break;
                    case 7:
                        linearLayoutPagerTab.setBackground(Utility.getCustomShape(Color.parseColor("#f4a460")));
                        textViewColorName.setTextColor(Color.BLACK);
                        imageButtonNextPage.setTextColor(Color.BLACK);
                        imageButtonPreviousPage.setTextColor(Color.BLACK);
                        imageButtonPreviousPage.setVisibility(View.VISIBLE);
                        imageButtonNextPage.setVisibility(View.VISIBLE);
                        break;
                    case 8:
                        linearLayoutPagerTab.setBackground(Utility.getCustomShape(Color.parseColor("#F5F5DC")));
                        textViewColorName.setTextColor(Color.BLACK);
                        imageButtonNextPage.setTextColor(Color.BLACK);
                        imageButtonPreviousPage.setTextColor(Color.BLACK);
                        imageButtonPreviousPage.setVisibility(View.VISIBLE);
                        imageButtonNextPage.setVisibility(View.VISIBLE);
                        break;
                    case 9:
                        linearLayoutPagerTab.setBackground(Utility.getCustomShape(Color.parseColor("#d3d3d3")));
                        textViewColorName.setTextColor(Color.BLACK);
                        imageButtonNextPage.setTextColor(Color.BLACK);
                        imageButtonPreviousPage.setTextColor(Color.BLACK);
                        imageButtonPreviousPage.setVisibility(View.VISIBLE);
                        imageButtonNextPage.setVisibility(View.VISIBLE);
                        break;
                    case 10:
                        linearLayoutPagerTab.setBackground(Utility.getCustomShape(Color.WHITE));
                        textViewColorName.setTextColor(Color.BLACK);
                        imageButtonNextPage.setTextColor(Color.BLACK);
                        imageButtonPreviousPage.setTextColor(Color.BLACK);
                        imageButtonPreviousPage.setVisibility(View.VISIBLE);
                        imageButtonNextPage.setVisibility(View.VISIBLE);

                        break;
                    case 11:
                        linearLayoutPagerTab.setBackground(Utility.getCustomShape(Color.BLACK));
                        textViewColorName.setTextColor(Color.WHITE);
                        imageButtonNextPage.setTextColor(Color.WHITE);
                        imageButtonPreviousPage.setTextColor(Color.WHITE);
                        imageButtonPreviousPage.setVisibility(View.VISIBLE);
                        imageButtonNextPage.setVisibility(View.GONE);
                        break;
                }
                break;
            case R.id.button_previous_page:
                if (pagerPagePosition < 0) {
                    return;
                }
                viewPagerColors.setCurrentItem(viewPagerColors.getCurrentItem() - 1, true);
                textViewColorName.setText(adapter.getPageTitle(viewPagerColors.getCurrentItem()));
                switch (pagerPagePosition) {
                    case 0:
                        linearLayoutPagerTab.setBackground(Utility.getCustomShape(Color.parseColor("#7F00FF")));
                        textViewColorName.setTextColor(Color.BLACK);
                        imageButtonNextPage.setTextColor(Color.BLACK);
                        imageButtonPreviousPage.setTextColor(Color.BLACK);
                        imageButtonPreviousPage.setVisibility(View.INVISIBLE);
                        imageButtonNextPage.setVisibility(View.VISIBLE);
                        break;
                    case 1:
                        linearLayoutPagerTab.setBackground(Utility.getCustomShape(Color.parseColor("#4b0082")));
                        textViewColorName.setTextColor(Color.BLACK);
                        imageButtonNextPage.setTextColor(Color.BLACK);
                        imageButtonPreviousPage.setTextColor(Color.BLACK);
                        imageButtonPreviousPage.setVisibility(View.VISIBLE);
                        imageButtonNextPage.setVisibility(View.VISIBLE);
                        break;
                    case 2:
                        linearLayoutPagerTab.setBackground(Utility.getCustomShape(Color.BLUE));
                        textViewColorName.setTextColor(Color.BLACK);
                        imageButtonNextPage.setTextColor(Color.BLACK);
                        imageButtonPreviousPage.setTextColor(Color.BLACK);
                        imageButtonPreviousPage.setVisibility(View.VISIBLE);
                        imageButtonNextPage.setVisibility(View.VISIBLE);
                        break;
                    case 3:
                        linearLayoutPagerTab.setBackground(Utility.getCustomShape(Color.GREEN));
                        textViewColorName.setTextColor(Color.BLACK);
                        imageButtonNextPage.setTextColor(Color.BLACK);
                        imageButtonPreviousPage.setTextColor(Color.BLACK);
                        imageButtonPreviousPage.setVisibility(View.VISIBLE);
                        imageButtonNextPage.setVisibility(View.VISIBLE);
                        break;
                    case 4:
                        linearLayoutPagerTab.setBackground(Utility.getCustomShape(Color.parseColor("#FFD700")));
                        textViewColorName.setTextColor(Color.BLACK);
                        imageButtonNextPage.setTextColor(Color.BLACK);
                        imageButtonPreviousPage.setTextColor(Color.BLACK);
                        imageButtonPreviousPage.setVisibility(View.VISIBLE);
                        imageButtonNextPage.setVisibility(View.VISIBLE);
                        break;
                    case 5:
                        linearLayoutPagerTab.setBackground(Utility.getCustomShape(Color.parseColor("#FFA500")));
                        textViewColorName.setTextColor(Color.BLACK);
                        imageButtonNextPage.setTextColor(Color.BLACK);
                        imageButtonPreviousPage.setTextColor(Color.BLACK);
                        imageButtonPreviousPage.setVisibility(View.VISIBLE);
                        imageButtonNextPage.setVisibility(View.VISIBLE);
                        break;
                    case 6:
                        linearLayoutPagerTab.setBackground(Utility.getCustomShape(Color.RED));
                        textViewColorName.setTextColor(Color.BLACK);
                        imageButtonNextPage.setTextColor(Color.BLACK);
                        imageButtonPreviousPage.setTextColor(Color.BLACK);
                        imageButtonPreviousPage.setVisibility(View.VISIBLE);
                        imageButtonNextPage.setVisibility(View.VISIBLE);
                        break;
                    case 7:
                        linearLayoutPagerTab.setBackground(Utility.getCustomShape(Color.parseColor("#f4a460")));
                        textViewColorName.setTextColor(Color.BLACK);
                        imageButtonNextPage.setTextColor(Color.BLACK);
                        imageButtonPreviousPage.setTextColor(Color.BLACK);
                        imageButtonPreviousPage.setVisibility(View.VISIBLE);
                        imageButtonNextPage.setVisibility(View.VISIBLE);
                        break;
                    case 8:
                        linearLayoutPagerTab.setBackground(Utility.getCustomShape(Color.parseColor("#F5F5DC")));
                        textViewColorName.setTextColor(Color.BLACK);
                        imageButtonNextPage.setTextColor(Color.BLACK);
                        imageButtonPreviousPage.setTextColor(Color.BLACK);
                        imageButtonPreviousPage.setVisibility(View.VISIBLE);
                        imageButtonNextPage.setVisibility(View.VISIBLE);
                        break;
                    case 9:
                        linearLayoutPagerTab.setBackground(Utility.getCustomShape(Color.parseColor("#d3d3d3")));
                        textViewColorName.setTextColor(Color.BLACK);
                        imageButtonNextPage.setTextColor(Color.BLACK);
                        imageButtonPreviousPage.setTextColor(Color.BLACK);
                        imageButtonPreviousPage.setVisibility(View.VISIBLE);
                        imageButtonNextPage.setVisibility(View.VISIBLE);
                        break;
                    case 10:
                        linearLayoutPagerTab.setBackground(Utility.getCustomShape(Color.WHITE));
                        textViewColorName.setTextColor(Color.BLACK);
                        imageButtonNextPage.setTextColor(Color.BLACK);
                        imageButtonPreviousPage.setTextColor(Color.BLACK);
                        imageButtonPreviousPage.setVisibility(View.VISIBLE);
                        imageButtonNextPage.setVisibility(View.VISIBLE);

                        break;
                    case 11:
                        linearLayoutPagerTab.setBackground(Utility.getCustomShape(Color.BLACK));
                        textViewColorName.setTextColor(Color.WHITE);
                        imageButtonNextPage.setTextColor(Color.WHITE);
                        imageButtonPreviousPage.setTextColor(Color.WHITE);
                        imageButtonPreviousPage.setVisibility(View.VISIBLE);
                        imageButtonNextPage.setVisibility(View.GONE);
                        break;
                }
                break;
            case R.id.button_hide_and_show_hlv:
                hideOrShowColorListView();
                break;
            case R.id.imagebutton_paint_undo:
                undoPainting();
                break;
            case R.id.imagebutton_paint_refresh:
                AlertDialog.Builder builder = new AlertDialog.Builder(this);
                builder.setTitle("Reset Painting");
                builder.setMessage("Want to reset all the painting details?");
                builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        refreshPaintStatus();
                        dialogInterface.dismiss();
                    }
                });
                builder.setNegativeButton("No", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dialogInterface.dismiss();
                    }
                });
                builder.create().show();
                break;
            case R.id.imagebutton_paint_save:
                if (usedColorList.size() <= 0 || usedColorList.isEmpty()) {
                    Toast.makeText(this, "Please paint the image to save.", Toast.LENGTH_SHORT)
                            .show();
                    return;
                }
                AlertDialog.Builder builder1 = new AlertDialog.Builder(this);
                builder1.setTitle("Save Image");
                builder1.setMessage("Do you want to save image?");
                builder1.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        ImageUtils.saveTemplateImage(LivingRoom1TabletActivity.this, global, ImageUtils.LIVINGROOM_TEMPLATE_1,
                                parent, borderCeilingRed, borderCeilingGreen, borderCeilingBlue,
                                ceilingRed, ceilingGreen, ceilingBlue,
                                rightRed, rightGreen, rightBlue);
                        dialogInterface.dismiss();
                    }
                });
                builder1.setNegativeButton("No", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dialogInterface.dismiss();
                    }
                });
                builder1.create().show();
                break;
            case R.id.imagebutton_paint_share:

                Bitmap bm;
                parent.setDrawingCacheEnabled(true);
                parent.buildDrawingCache();
                bm = parent.getDrawingCache();
                Bitmap bmCopy = bm.copy(Bitmap.Config.ARGB_8888, false);
                parent.destroyDrawingCache();
                Intent intent = new Intent(this, ImagePreviewActivity.class);
                ((Global) getApplication()).setSnapAndPaintBitmap(bmCopy);
                startActivity(intent);
                /*AlertDialog.Builder imageDialog = new AlertDialog.Builder(this);
                LayoutInflater inflater = (LayoutInflater) this.getSystemService(LAYOUT_INFLATER_SERVICE);

                View layout = inflater.inflate(R.layout.layout_paint_preview,
                        (ViewGroup) findViewById(R.id.layout_root));
                ImageView image = (ImageView) layout.findViewById(R.id.fullimage);
                image.setImageBitmap(bmCopy);
                imageDialog.setView(layout);
                imageDialog.setPositiveButton("OK", new DialogInterface.OnClickListener() {

                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }

                });


                imageDialog.create();
                imageDialog.show();
                /*if (usedColorList.size() <= 0 || usedColorList.isEmpty()) {
                    Toast.makeText(this, "Please paint the image to share.", Toast.LENGTH_SHORT)
                            .show();
                    return;
                }
                AlertDialog.Builder builder2 = new AlertDialog.Builder(this);
                builder2.setTitle("Share Image");
                builder2.setMessage("Want to share image?");
                builder2.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        ImageUtils.shareTemplateImage(LivingRoom1TabletActivity.this, global, ImageUtils.LIVINGROOM_TEMPLATE_1,
                                parent, borderCeilingRed, borderCeilingGreen, borderCeilingBlue,
                                ceilingRed, ceilingGreen, ceilingBlue,
                                rightRed, rightGreen, rightBlue);
                        dialogInterface.dismiss();
                    }
                });
                builder2.setNegativeButton("No", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dialogInterface.dismiss();
                    }
                });
                builder2.create().show();*/

                break;
            case R.id.imagebutton_paint_redo:
                redoPainting();
                break;
            case R.id.imagebutton_go_to_gallery:
                Intent intentGallery = new Intent(LivingRoom1TabletActivity.this,
                        GalleryActivity.class);
                intentGallery.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intentGallery);
                break;
        }
    }

    private void redoPainting() {
        isDonePainting = false;
        if (redoColorHistoryList.size() - 1 < 0) {
            Toast.makeText(this, "redo unavailable.", Toast.LENGTH_SHORT)
                    .show();
        } else {
            int maxIndex = redoColorHistoryList.size() - 1;

            AppLog.showLog(CLASS_TAG,
                    "redo/undo undo color list size: before redo" + undoColorHistoryList.size());
            undoColorHistoryList.add(redoColorHistoryList.get(maxIndex));
            AppLog.showLog(CLASS_TAG,
                    "redo/undo undo color list size: after redo" + undoColorHistoryList.size());

            PaintColorHistory paintColorHistory = redoColorHistoryList.get(maxIndex);
            redoColorHistoryList.remove(maxIndex);
            Float[] colorScale = paintColorHistory.getColorCode();
            switch (paintColorHistory.getImageViewId()) {
                case R.id.iv_living1_border_ceiling:
                    if (colorScale[0] == 0 && colorScale[1] == 0 && colorScale[2] == 0) {
                        ImageUtils.loadImageUsingLoader(imageLoader, ivBorderCeiling,
                                "assets://" + AppText.LIVINGROOM1_LOCATION_BORDER_CEILING);
                        updatePaintStatus(new int[]{0, 0, 0, R.id.iv_living1_border_ceiling});
                    } else {
                        int[] colors = ImageUtils.undoPainting(LivingRoom1TabletActivity.this,
                                ivBorderCeiling, AppText.LIVINGROOM1_LOCATION_BORDER_CEILING,
                                colorScale);
                        updatePaintStatus(colors);
                    }
                    break;
                case R.id.iv_living1_ceiling:
                    if (colorScale[0] == 0 && colorScale[1] == 0 && colorScale[2] == 0) {
                        ImageUtils.loadImageUsingLoader(imageLoader, ivCeiling,
                                "assets://" + AppText.LIVINGROOM1_LOCATION_CEILING);
                        updatePaintStatus(new int[]{0, 0, 0, R.id.iv_living1_ceiling});
                    } else {
                        int[] colors = ImageUtils.undoPainting(LivingRoom1TabletActivity.this,
                                ivCeiling, AppText.LIVINGROOM1_LOCATION_CEILING,
                                colorScale);
                        updatePaintStatus(colors);
                    }
                    break;
                case R.id.iv_living1_right_wall:
                    if (colorScale[0] == 0 && colorScale[1] == 0 && colorScale[2] == 0) {
                        ImageUtils.loadImageUsingLoader(imageLoader, ivRightWall,
                                "assets://" + AppText.LIVINGROOM1_LOCATION_RIGHT);
                        updatePaintStatus(new int[]{0, 0, 0, R.id.iv_living1_right_wall});
                    } else {
                        int[] colors = ImageUtils.undoPainting(LivingRoom1TabletActivity.this,
                                ivRightWall, AppText.LIVINGROOM1_LOCATION_RIGHT,
                                colorScale);
                        updatePaintStatus(colors);
                    }
                    break;
            }
        }
    }

    private void refreshPaintStatus() {
        isColorClicked = false;
        redScale = 1f;
        greenScale = 0f;
        blueScale = 0f;
        usedColorList.clear();
        listViewUsedColors.setAdapter(new UsedColorAdapter(this, usedColorList));
        selectedColorsList.clear();
        undoColorHistoryList.clear();
        redoColorHistoryList.clear();
        horizontalListView.setAdapter(new TabletColorHorizontalListAdapter(this, selectedColorsList, this));
        findViewById(R.id.imageview_selected_color)
                .setBackgroundColor(android.R.color.transparent);
        ((TextView) findViewById(R.id.textview_selected_color_name))
                .setText("");
        ((TextView) findViewById(R.id.textview_selected_color_code))
                .setText("");
        ImageUtils.loadImageUsingLoader(imageLoader, ivMainImage, "assets://" + AppText.LIVINGROOM1_LOCATION_MAIN);
        ImageUtils.loadImageUsingLoader(imageLoader, ivBorderCeiling, "assets://" + AppText.LIVINGROOM1_LOCATION_BORDER_CEILING);
        ImageUtils.loadImageUsingLoader(imageLoader, ivCeiling, "assets://" + AppText.LIVINGROOM1_LOCATION_CEILING);
        ImageUtils.loadImageUsingLoader(imageLoader, ivRightWall, "assets://" + AppText.LIVINGROOM1_LOCATION_RIGHT);
    }

    private void undoPainting() {
        isDonePainting = false;
        if (undoColorHistoryList.size() <= 0) {
            Toast.makeText(this, "undo unavailable.", Toast.LENGTH_SHORT)
                    .show();
        } else {
            int maxIndex = undoColorHistoryList.size() - 1;
            redoColorHistoryList.add(undoColorHistoryList.get(maxIndex));
            undoColorHistoryList.remove(maxIndex);
            maxIndex = undoColorHistoryList.size() - 1;
            if (maxIndex < 0) {
                Toast.makeText(this, "undo unavailable.", Toast.LENGTH_SHORT)
                        .show();
                return;
            }
            PaintColorHistory paintColorHistory = undoColorHistoryList.get(maxIndex);
            Float[] colorScale = paintColorHistory.getColorCode();
            switch (paintColorHistory.getImageViewId()) {
                case R.id.iv_living1_border_ceiling:
                    if (colorScale[0] == 0 && colorScale[1] == 0 && colorScale[2] == 0) {
                        ImageUtils.loadImageUsingLoader(imageLoader, ivBorderCeiling,
                                "assets://" + AppText.LIVINGROOM1_LOCATION_BORDER_CEILING);
                        updatePaintStatus(new int[]{0, 0, 0, R.id.iv_living1_border_ceiling});
                    } else {
                        int[] colors = ImageUtils.undoPainting(LivingRoom1TabletActivity.this,
                                ivBorderCeiling, AppText.LIVINGROOM1_LOCATION_BORDER_CEILING,
                                colorScale);
                        updatePaintStatus(colors);
                    }
                    break;
                case R.id.iv_living1_ceiling:
                    if (colorScale[0] == 0 && colorScale[1] == 0 && colorScale[2] == 0) {
                        ImageUtils.loadImageUsingLoader(imageLoader, ivCeiling,
                                "assets://" + AppText.LIVINGROOM1_LOCATION_CEILING);
                        updatePaintStatus(new int[]{0, 0, 0, R.id.iv_living1_ceiling});
                    } else {
                        int[] colors = ImageUtils.undoPainting(LivingRoom1TabletActivity.this,
                                ivCeiling, AppText.LIVINGROOM1_LOCATION_CEILING,
                                colorScale);
                        updatePaintStatus(colors);
                    }
                    break;
                case R.id.iv_living1_right_wall:
                    if (colorScale[0] == 0 && colorScale[1] == 0 && colorScale[2] == 0) {
                        ImageUtils.loadImageUsingLoader(imageLoader, ivRightWall,
                                "assets://" + AppText.LIVINGROOM1_LOCATION_RIGHT);
                        updatePaintStatus(new int[]{0, 0, 0, R.id.iv_living1_right_wall});
                    } else {
                        int[] colors = ImageUtils.undoPainting(LivingRoom1TabletActivity.this,
                                ivRightWall, AppText.LIVINGROOM1_LOCATION_RIGHT,
                                colorScale);
                        updatePaintStatus(colors);
                    }
                    break;
            }
        }
    }
}
