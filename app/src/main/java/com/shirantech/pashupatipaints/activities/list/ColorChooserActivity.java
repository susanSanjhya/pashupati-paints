package com.shirantech.pashupatipaints.activities.list;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.os.Vibrator;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.text.TextUtils;
import android.util.DisplayMetrics;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.actionbarsherlock.app.SherlockFragmentActivity;
import com.actionbarsherlock.view.Menu;
import com.actionbarsherlock.view.MenuItem;
import com.actionbarsherlock.view.Window;
import com.shirantech.pashupatipaints.R;
import com.shirantech.pashupatipaints.activities.list.bedrooms.Bedroom2Activity;
import com.shirantech.pashupatipaints.activities.list.bedrooms.Bedroom3Activity;
import com.shirantech.pashupatipaints.activities.list.bedrooms.Bedroom4Activity;
import com.shirantech.pashupatipaints.activities.list.bedrooms.Bedroom5Activity;
import com.shirantech.pashupatipaints.activities.list.bedrooms.Bedroom6Activity;
import com.shirantech.pashupatipaints.activities.list.exteriors.ExteriorTemplate1Activity;
import com.shirantech.pashupatipaints.activities.list.exteriors.ExteriorTemplate2Activity;
import com.shirantech.pashupatipaints.activities.list.exteriors.ExteriorTemplate3Activity;
import com.shirantech.pashupatipaints.activities.list.kitchens.Kitchens1TemplateActivity;
import com.shirantech.pashupatipaints.activities.list.kitchens.Kitchens2TemplateActivity;
import com.shirantech.pashupatipaints.activities.list.kitchens.Kitchens3TemplateActivity;
import com.shirantech.pashupatipaints.activities.list.kitchens.Kitchens4TemplateActivity;
import com.shirantech.pashupatipaints.activities.list.kitchens.Kitchens5TemplateActivity;
import com.shirantech.pashupatipaints.activities.list.kitchens.Kitchens6TemplateActivity;
import com.shirantech.pashupatipaints.activities.list.kitchens.Kitchens7TemplateActivity;
import com.shirantech.pashupatipaints.activities.list.livingrooms.LivingRoom1Activity;
import com.shirantech.pashupatipaints.activities.list.livingrooms.LivingRoom2Activity;
import com.shirantech.pashupatipaints.activities.list.livingrooms.LivingRoom3Activity;
import com.shirantech.pashupatipaints.activities.list.livingrooms.LivingRoom4Activity;
import com.shirantech.pashupatipaints.adapter.ColorGridPagerAdapter;
import com.shirantech.pashupatipaints.adapter.ColorHorizontalListAdapter;
import com.shirantech.pashupatipaints.fragments.BeigeFragment;
import com.shirantech.pashupatipaints.fragments.BlackFragment;
import com.shirantech.pashupatipaints.fragments.BlueFragment;
import com.shirantech.pashupatipaints.fragments.BrownFragment;
import com.shirantech.pashupatipaints.fragments.GrayFragment;
import com.shirantech.pashupatipaints.fragments.GreenFragment;
import com.shirantech.pashupatipaints.fragments.IndigoFragment;
import com.shirantech.pashupatipaints.fragments.OrangeFragment;
import com.shirantech.pashupatipaints.fragments.RedFragment;
import com.shirantech.pashupatipaints.fragments.VioletFragment;
import com.shirantech.pashupatipaints.fragments.WhiteFragment;
import com.shirantech.pashupatipaints.fragments.YellowFragment;
import com.shirantech.pashupatipaints.model.PaintColor;
import com.shirantech.pashupatipaints.util.AppLog;
import com.shirantech.pashupatipaints.util.Global;
import com.shirantech.pashupatipaints.util.Utility;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

@SuppressWarnings("deprecation")
public class ColorChooserActivity extends SherlockFragmentActivity implements
        VioletFragment.onPaintSwatchSelectedListener,
        IndigoFragment.onPaintSwatchSelectedListener,
        BlueFragment.onPaintSwatchSelectedListener,
        GreenFragment.onPaintSwatchSelectedListener,
        YellowFragment.onPaintSwatchSelectedListener,
        OrangeFragment.onPaintSwatchSelectedListener,
        RedFragment.onPaintSwatchSelectedListener,
        BrownFragment.onPaintSwatchSelectedListener,
        BeigeFragment.onPaintSwatchSelectedListener,
        GrayFragment.onPaintSwatchSelectedListener,
        BlackFragment.onPaintSwatchSelectedListener,
        WhiteFragment.onPaintSwatchSelectedListener {

    public static final String CLASS_TAG = ColorChooserActivity.class.getSimpleName();
    private int position;
    private String intentType;
    private ColorHorizontalListAdapter paintColorAdapter;
//    private HorizontalListView horizontalListView;

    private List<PaintColor> selectedColorsList;

    Global global;

    private Vibrator mVibrator;
    private static final int VIBRATE_DURATION = 35;

    ArrayList<PaintColor> tempElements = null;

    private TextView textViewColorName, imageButtonPreviousPage, imageButtonNextPage;
    private LinearLayout linearLayoutPagerTab;
    private int pagerPagePosition;
    private FragmentPagerAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        if (Build.VERSION.SDK_INT <= Build.VERSION_CODES.HONEYCOMB) {
            getSupportActionBar().hide();
        }
        setContentView(R.layout.activity_color_chooser);
        LinearLayout linearLayoutContainer = (LinearLayout) findViewById(R.id.linear_layout_container);
        linearLayoutContainer.setMinimumWidth((int) (getScreenWidth() - (2 * getResources().getDimension(R.dimen.dialog_margin))));
        position = getIntent().getExtras().getInt("POSITION");
        intentType = getIntent().getExtras().getString("TYPE");

        adapter = new ColorGridPagerAdapter(getSupportFragmentManager());

        ViewPager pager = (ViewPager) findViewById(R.id.pager);
        pager.setAdapter(adapter);

        pager.setCurrentItem(0);
        findViewById(R.id.image_button_delete).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
               /* Intent bedroom1Intent = new Intent(ColorChooserActivity.this, Bedroom1V2Activity.class);
                startActivity(bedroom1Intent);*/
                finish();
            }
        });

        textViewColorName = (TextView) findViewById(R.id.textview_color_name);
        imageButtonPreviousPage = (TextView) findViewById(R.id.button_previous_page);
        imageButtonNextPage = (TextView) findViewById(R.id.button_next_page);
        linearLayoutPagerTab = (LinearLayout) findViewById(R.id.linear_layout_pager_tab);
        textViewColorName.setText(adapter.getPageTitle(0));
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
            linearLayoutPagerTab.setBackground(Utility.getCustomShape(Color.parseColor("#7F00FF")));
        } else {
            linearLayoutPagerTab.setBackgroundDrawable(Utility.getCustomShape(Color.parseColor("#7F00FF")));
        }
        pager.setOnPageChangeListener(new ViewPager.SimpleOnPageChangeListener() {
            @Override
            public void onPageSelected(int position) {
                pagerPagePosition = position;
                textViewColorName.setText(adapter.getPageTitle(position));
                switch (pagerPagePosition) {
                    case 0:
                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
                            linearLayoutPagerTab.setBackground(Utility.getCustomShape(Color.parseColor("#7F00FF")));
                        } else {
                            linearLayoutPagerTab.setBackgroundDrawable(Utility.getCustomShape(Color.parseColor("#7F00FF")));
                        }
                        textViewColorName.setTextColor(Color.BLACK);
                        imageButtonNextPage.setTextColor(Color.BLACK);
                        imageButtonPreviousPage.setTextColor(Color.BLACK);
                        imageButtonPreviousPage.setVisibility(View.INVISIBLE);
                        imageButtonNextPage.setVisibility(View.VISIBLE);
                        break;
                    case 1:
                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
                            linearLayoutPagerTab.setBackground(Utility.getCustomShape(Color.parseColor("#4b0082")));
                        } else {
                            linearLayoutPagerTab.setBackgroundDrawable(Utility.getCustomShape(Color.parseColor("#4b0082")));
                        }
                        textViewColorName.setTextColor(Color.BLACK);
                        imageButtonNextPage.setTextColor(Color.BLACK);
                        imageButtonPreviousPage.setTextColor(Color.BLACK);
                        imageButtonPreviousPage.setVisibility(View.VISIBLE);
                        imageButtonNextPage.setVisibility(View.VISIBLE);
                        break;
                    case 2:
                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
                            linearLayoutPagerTab.setBackground(Utility.getCustomShape(Color.BLUE));
                        } else {
                            linearLayoutPagerTab.setBackgroundDrawable(Utility.getCustomShape(Color.BLUE));
                        }
                        textViewColorName.setTextColor(Color.BLACK);
                        imageButtonNextPage.setTextColor(Color.BLACK);
                        imageButtonPreviousPage.setTextColor(Color.BLACK);
                        imageButtonPreviousPage.setVisibility(View.VISIBLE);
                        imageButtonNextPage.setVisibility(View.VISIBLE);
                        break;
                    case 3:
                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
                            linearLayoutPagerTab.setBackground(Utility.getCustomShape(Color.GREEN));
                        } else {
                            linearLayoutPagerTab.setBackgroundDrawable(Utility.getCustomShape(Color.GREEN));
                        }
                        textViewColorName.setTextColor(Color.BLACK);
                        imageButtonNextPage.setTextColor(Color.BLACK);
                        imageButtonPreviousPage.setTextColor(Color.BLACK);
                        imageButtonPreviousPage.setVisibility(View.VISIBLE);
                        imageButtonNextPage.setVisibility(View.VISIBLE);
                        break;
                    case 4:
                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
                            linearLayoutPagerTab.setBackground(Utility.getCustomShape(Color.parseColor("#FFD700")));
                        } else {
                            linearLayoutPagerTab.setBackgroundDrawable(Utility.getCustomShape(Color.parseColor("#FFD700")));
                        }
                        textViewColorName.setTextColor(Color.BLACK);
                        imageButtonNextPage.setTextColor(Color.BLACK);
                        imageButtonPreviousPage.setTextColor(Color.BLACK);
                        imageButtonPreviousPage.setVisibility(View.VISIBLE);
                        imageButtonNextPage.setVisibility(View.VISIBLE);
                        break;
                    case 5:
                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
                            linearLayoutPagerTab.setBackground(Utility.getCustomShape(Color.parseColor("#FFA500")));
                        } else {
                            linearLayoutPagerTab.setBackgroundDrawable(Utility.getCustomShape(Color.parseColor("#FFA500")));
                        }
                        textViewColorName.setTextColor(Color.BLACK);
                        imageButtonNextPage.setTextColor(Color.BLACK);
                        imageButtonPreviousPage.setTextColor(Color.BLACK);
                        imageButtonPreviousPage.setVisibility(View.VISIBLE);
                        imageButtonNextPage.setVisibility(View.VISIBLE);
                        break;
                    case 6:
                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
                            linearLayoutPagerTab.setBackground(Utility.getCustomShape(Color.RED));
                        } else {
                            linearLayoutPagerTab.setBackgroundDrawable(Utility.getCustomShape(Color.RED));
                        }
                        textViewColorName.setTextColor(Color.BLACK);
                        imageButtonNextPage.setTextColor(Color.BLACK);
                        imageButtonPreviousPage.setTextColor(Color.BLACK);
                        imageButtonPreviousPage.setVisibility(View.VISIBLE);
                        imageButtonNextPage.setVisibility(View.VISIBLE);
                        break;
                    case 7:
                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
                            linearLayoutPagerTab.setBackground(Utility.getCustomShape(Color.parseColor("#f4a460")));
                        } else {
                            linearLayoutPagerTab.setBackgroundDrawable(Utility.getCustomShape(Color.parseColor("#f4a460")));
                        }
                        textViewColorName.setTextColor(Color.BLACK);
                        imageButtonNextPage.setTextColor(Color.BLACK);
                        imageButtonPreviousPage.setTextColor(Color.BLACK);
                        imageButtonPreviousPage.setVisibility(View.VISIBLE);
                        imageButtonNextPage.setVisibility(View.VISIBLE);
                        break;
                    case 8:
                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
                            linearLayoutPagerTab.setBackground(Utility.getCustomShape(Color.parseColor("#F5F5DC")));
                        } else {
                            linearLayoutPagerTab.setBackgroundDrawable(Utility.getCustomShape(Color.parseColor("#F5F5DC")));
                        }
                        textViewColorName.setTextColor(Color.BLACK);
                        imageButtonNextPage.setTextColor(Color.BLACK);
                        imageButtonPreviousPage.setTextColor(Color.BLACK);
                        imageButtonPreviousPage.setVisibility(View.VISIBLE);
                        imageButtonNextPage.setVisibility(View.VISIBLE);
                        break;
                    case 9:
                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
                            linearLayoutPagerTab.setBackground(Utility.getCustomShape(Color.parseColor("#d3d3d3")));
                        } else {
                            linearLayoutPagerTab.setBackgroundDrawable(Utility.getCustomShape(Color.parseColor("#d3d3d3")));
                        }
                        textViewColorName.setTextColor(Color.BLACK);
                        imageButtonNextPage.setTextColor(Color.BLACK);
                        imageButtonPreviousPage.setTextColor(Color.BLACK);
                        imageButtonPreviousPage.setVisibility(View.VISIBLE);
                        imageButtonNextPage.setVisibility(View.VISIBLE);
                        break;
                    case 10:
                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
                            linearLayoutPagerTab.setBackground(Utility.getCustomShape(Color.WHITE));
                        } else {
                            linearLayoutPagerTab.setBackgroundDrawable(Utility.getCustomShape(Color.WHITE));
                        }
                        textViewColorName.setTextColor(Color.BLACK);
                        imageButtonNextPage.setTextColor(Color.BLACK);
                        imageButtonPreviousPage.setTextColor(Color.BLACK);
                        imageButtonPreviousPage.setVisibility(View.VISIBLE);
                        imageButtonNextPage.setVisibility(View.VISIBLE);

                        break;
                    case 11:
                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
                            linearLayoutPagerTab.setBackground(Utility.getCustomShape(Color.BLACK));
                        } else {
                            linearLayoutPagerTab.setBackgroundDrawable(Utility.getCustomShape(Color.BLACK));
                        }
                        textViewColorName.setTextColor(Color.WHITE);
                        imageButtonNextPage.setTextColor(Color.WHITE);
                        imageButtonPreviousPage.setTextColor(Color.WHITE);
                        imageButtonPreviousPage.setVisibility(View.VISIBLE);
                        imageButtonNextPage.setVisibility(View.GONE);
                        break;
                }
            }
        });

//        horizontalListView = (HorizontalListView) findViewById(R.id.hlv_selected_colors);
        selectedColorsList = new ArrayList<PaintColor>();

        global = (Global) getApplication();

        String colorChooserSource = getIntent().getExtras().getString("COLOR CHOOSER SOURCE");
        if (colorChooserSource != null) {
            if (colorChooserSource.equalsIgnoreCase("STARTER")) {
                if (global.getGlobalPaintColorList() != null) {
                    if (global.getGlobalPaintColorList().size() > 0) {
                        global.getGlobalPaintColorList().clear();
                    }
                }


            } else if (colorChooserSource.equalsIgnoreCase("TEMPLATE")) {
                //check if we already have global Selected Colors
                if (global.getGlobalPaintColorList() != null) {
                    if (global.getGlobalPaintColorList().size() > 0) { // this activity is called from ADD MORE COLORS
                        selectedColorsList.addAll(global.getGlobalPaintColorList());
                        AppLog.showLog(CLASS_TAG, "ColorChooser onCreate selected color list: " + selectedColorsList.size());
                        paintColorAdapter = new ColorHorizontalListAdapter(getApplicationContext(), selectedColorsList);
                        //notify horizontalListView about the new list-items!!
//                        horizontalListView.setAdapter(paintColorAdapter);
                        paintColorAdapter.notifyDataSetChanged();

                    }
                }
            }
        }

        // On a long press, we vibrate so the user gets some feedback.
        mVibrator = (Vibrator) this.getSystemService(Context.VIBRATOR_SERVICE);

        /*horizontalListView.setOnItemLongClickListener(new OnItemLongClickListener() {

            @Override
            public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {

                mVibrator.vibrate(VIBRATE_DURATION);

                final PaintColor pc = paintColorAdapter.getItem(position);

                AlertDialog.Builder builder = new AlertDialog.Builder(ColorChooserActivity.this);
                builder.setMessage(
                        "Delete selected color?")
                        .setCancelable(false)
                                // Prevents user to use "back button"
                        .setPositiveButton("Delete",
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int id) {
                                        AppLog.showLog(CLASS_TAG, "OK clicked!! id: " + id);
                                        *//* delete *//*
                                        selectedColorsList.remove(pc);
                                        tempElements.remove(pc);
                                        paintColorAdapter.notifyDataSetChanged();

                                    }
                                }
                        )
                        .setNegativeButton("Cancel",
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int id) {
                                        AppLog.showLog(CLASS_TAG, "cancel clicked!! id: " + id);
                                        dialog.cancel();
                                    }
                                }
                        );
                builder.show();
                return false;

            }

        });*/
    }

    private float getScreenWidth() {
        DisplayMetrics displaymetrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(displaymetrics);
        return displaymetrics.widthPixels;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getSupportMenuInflater().inflate(R.menu.activity_color_chooser, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        super.onOptionsItemSelected(item);

        switch (item.getItemId()) {
            case android.R.id.home:
                if (!TextUtils.isEmpty(intentType) && "snap".equalsIgnoreCase(intentType)) {
                    Intent intent = new Intent(this, PainingWaysActivity.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(intent);
                } else {
                    Intent intent = new Intent(this, RoomCategoryActivity.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(intent);
                }
                break;

            case R.id.menu_next:
                if (selectedColorsList.size() > 0) {
                    if (!TextUtils.isEmpty(intentType) && "exterior".equalsIgnoreCase(intentType)) {
                        switch (position) {
                            case 0:
                                Intent exterior1Intent = new Intent(ColorChooserActivity.this, ExteriorTemplate1Activity.class);
                                startActivity(exterior1Intent);
                                break;
                            case 1:
                                Intent exterior2Intent = new Intent(ColorChooserActivity.this, ExteriorTemplate2Activity.class);
                                startActivity(exterior2Intent);
                                break;
                            case 2:
                                Intent exterior3Intent = new Intent(ColorChooserActivity.this, ExteriorTemplate3Activity.class);
                                startActivity(exterior3Intent);
                                break;
                        }
                    } else if (!TextUtils.isEmpty(intentType) && "drawinghall".equalsIgnoreCase(intentType)) {
                        switch (position) {
                            case 0:
                                Intent drawingHall1Intent = new Intent(ColorChooserActivity.this, LivingRoom1Activity.class);
                                startActivity(drawingHall1Intent);
                                break;
                            case 1:
                                Intent drawingHall2Intent = new Intent(ColorChooserActivity.this, LivingRoom2Activity.class);
                                startActivity(drawingHall2Intent);
                                break;
                            case 2:
                                Intent drawingHall3Intent = new Intent(ColorChooserActivity.this, LivingRoom3Activity.class);
                                startActivity(drawingHall3Intent);
                                break;
                            case 3:
                                Intent drawingHall4Intent = new Intent(ColorChooserActivity.this, LivingRoom4Activity.class);
                                startActivity(drawingHall4Intent);
                                break;
                        }
                    } else if (!TextUtils.isEmpty(intentType) && "bedroom".equalsIgnoreCase(intentType)) {
                        switch (position) {
                            case 0:
                               /* Intent bedroom1Intent = new Intent(ColorChooserActivity.this, Bedroom1V2Activity.class);
                                startActivity(bedroom1Intent);*/
                                finish();
                                break;

                            case 1:
                                Intent bedroom2Intent = new Intent(ColorChooserActivity.this, Bedroom2Activity.class);
                                startActivity(bedroom2Intent);
                                break;

                            case 2:
                                Intent bedroom3Intent = new Intent(ColorChooserActivity.this, Bedroom3Activity.class);
                                startActivity(bedroom3Intent);
                                break;

                            case 3:
                                Intent bedroom4Intent = new Intent(ColorChooserActivity.this, Bedroom4Activity.class);
                                startActivity(bedroom4Intent);
                                break;

                            case 4:
                                Intent bedroom5Intent = new Intent(ColorChooserActivity.this, Bedroom5Activity.class);
                                startActivity(bedroom5Intent);
                                break;

                            case 5:
                                Intent bedroom6Intent = new Intent(ColorChooserActivity.this, Bedroom6Activity.class);
                                startActivity(bedroom6Intent);
                                break;

                            default:
                                break;
                        }
                    } else if (!TextUtils.isEmpty(intentType) && "kitchen".equalsIgnoreCase(intentType)) {
                        switch (position) {
                            case 0:
                                Intent kitchen1Intent = new Intent(ColorChooserActivity.this, Kitchens1TemplateActivity.class);
                                startActivity(kitchen1Intent);
                                break;

                            case 1:
                                Intent kitchen2Intent = new Intent(ColorChooserActivity.this, Kitchens2TemplateActivity.class);
                                startActivity(kitchen2Intent);
                                break;

                            case 2:
                                Intent kitchen3Intent = new Intent(ColorChooserActivity.this, Kitchens3TemplateActivity.class);
                                startActivity(kitchen3Intent);
                                break;

                            case 3:
                                Intent kitchen4Intent = new Intent(ColorChooserActivity.this, Kitchens4TemplateActivity.class);
                                startActivity(kitchen4Intent);
                                break;

                            case 4:
                                Intent kitchen5Intent = new Intent(ColorChooserActivity.this, Kitchens5TemplateActivity.class);
                                startActivity(kitchen5Intent);
                                break;

                            case 5:
                                Intent kitchen6Intent = new Intent(ColorChooserActivity.this, Kitchens6TemplateActivity.class);
                                startActivity(kitchen6Intent);
                                break;


                            case 6:
                                Intent kitchen7Intent = new Intent(ColorChooserActivity.this, Kitchens7TemplateActivity.class);
                                startActivity(kitchen7Intent);
                                break;

                            default:
                                break;
                        }
                    } else if (!TextUtils.isEmpty(intentType) && "snap".equalsIgnoreCase(intentType)) {
                        if (position == 0) {
                            startActivity(new Intent(ColorChooserActivity.this, SnapAndPaintActivity.class));
                            finish();
                        }
                    }

                } else {
                    Toast.makeText(getApplicationContext(), "Please select colors first.", Toast.LENGTH_SHORT).show();
                }
                break;
            default:
                break;
        }

        return true;
    }

    @Override
    public void onPaintSelected(PaintColor paintColor) {

        AppLog.showLog(CLASS_TAG, "onPaint sELECTED!! :: paintcolor id passed:: " + paintColor.getId());

        for (PaintColor p : selectedColorsList) {
            AppLog.showLog(CLASS_TAG, "in for loop:: paint-color id passed:: " + paintColor.getId());
            AppLog.showLog(CLASS_TAG, "selected color list in loop:: " + selectedColorsList.size() + " p id:: " + p.getId());
            if (p.getId().equals(paintColor.getId()) || p.getId().intValue() == paintColor.getId().intValue()) {
                Toast.makeText(getApplicationContext(), "The color is already selected.", Toast.LENGTH_SHORT).show();
                return;
            }
        }
        selectedColorsList.add(paintColor);
        AppLog.showLog(CLASS_TAG, "selectedColorsList size:: " + selectedColorsList.size());

        //global = (Global)getApplication();
        global.setGlobalPaintColorList(selectedColorsList);

        //AUTO SCROLL THE SELECTED COLORS!!
        tempElements = new ArrayList<PaintColor>(selectedColorsList);
        Collections.reverse(tempElements);

        paintColorAdapter = new ColorHorizontalListAdapter(getApplicationContext(), tempElements);
//        horizontalListView.setAdapter(paintColorAdapter);
        paintColorAdapter.notifyDataSetChanged();
    }

    @Override
    public void onBackPressed() {

    }
}
