package com.shirantech.pashupatipaints.activities.list;

import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.actionbarsherlock.app.SherlockActivity;
import com.shirantech.pashupatipaints.R;
import com.shirantech.pashupatipaints.controller.FromExcelToDbTask;
import com.shirantech.pashupatipaints.interfaces.DbLoadListener;
import com.shirantech.pashupatipaints.util.AppLog;
import com.shirantech.pashupatipaints.util.AppText;
import com.shirantech.pashupatipaints.util.Utility;

public class FirstLaunchActivity extends SherlockActivity implements OnClickListener, DbLoadListener {

    private static final String CLASS_TAG = FirstLaunchActivity.class.getSimpleName();
    private Button btnGoToApp;
    private ProgressBar pbFirstLaunch;
    private TextView tvWait;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (Utility.isTablet(this)) {
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
        } else {
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        }
        setContentView(R.layout.activity_first_launch);
        AppLog.showLog(CLASS_TAG, "First Launch started");
        /*final ActionBar actionBar = getSupportActionBar();
        actionBar.hide();*/

        btnGoToApp = (Button) findViewById(R.id.btn_go_to_app);
        tvWait = (TextView) findViewById(R.id.tv_wait);

        pbFirstLaunch = (ProgressBar) findViewById(R.id.progressBar1);

        btnGoToApp.setOnClickListener(this);
        new FromExcelToDbTask(this, this).execute();
    }

    @Override
    public void onTaskStarted() {
        pbFirstLaunch.setVisibility(View.VISIBLE);
        btnGoToApp.setEnabled(false);
    }

    @Override
    public void onTaskFinished() {
        pbFirstLaunch.setVisibility(View.GONE);
        btnGoToApp.setEnabled(true);
        tvWait.setVisibility(View.GONE);
        SharedPreferences settings = getSharedPreferences(AppText.PREF_KEY_HAS_RUN_BEFORE, 0);
        SharedPreferences.Editor edit = settings.edit();
        edit.putBoolean(AppText.KEY_HAS_RUN, true); //set to has run
        edit.commit(); //apply
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_go_to_app:
                Intent homeListIntent;
                if(Utility.isTablet(this)) {
                    homeListIntent = new Intent(FirstLaunchActivity.this, StarterTabletActivity.class);
                } else {
                    homeListIntent = new Intent(FirstLaunchActivity.this, StarterV2Activity.class);
                }
                startActivity(homeListIntent);
                finish();
                break;
        }
    }
}
