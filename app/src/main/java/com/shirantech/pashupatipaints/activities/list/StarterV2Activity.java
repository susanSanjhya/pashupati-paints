package com.shirantech.pashupatipaints.activities.list;

import android.os.Bundle;
import android.support.v4.view.ViewPager;

import com.actionbarsherlock.app.SherlockFragmentActivity;
import com.shirantech.pashupatipaints.R;
import com.shirantech.pashupatipaints.adapter.CarouselViewpagerAdapter;

/**
 * Created by susan on 7/8/15.
 */
public class StarterV2Activity extends SherlockFragmentActivity {
    public final static int PAGES = 4;
    public static final int LOOPS = 1000;
    public static final int FIRST_PAGE = PAGES * LOOPS / 2;
    public static final float BIG_SCALE = 1.0f;
    public static final float SMALL_SCALE = 0.7f;
    public static final float DIFF_SCALE = BIG_SCALE - SMALL_SCALE;

    public CarouselViewpagerAdapter adapter;
    public ViewPager pager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_starterv2_new);
        getSupportActionBar().hide();
        pager = (ViewPager) findViewById(R.id.view_pager_carousel);
        adapter = new CarouselViewpagerAdapter(this, this.getSupportFragmentManager());
        pager.setAdapter(adapter);
        pager.setOnPageChangeListener(adapter);
        pager.setCurrentItem(FIRST_PAGE);
        pager.setOffscreenPageLimit(3);
        pager.setPageMargin(-1 * (getResources().getInteger(R.integer.main_pager_margin)));
    }
}
