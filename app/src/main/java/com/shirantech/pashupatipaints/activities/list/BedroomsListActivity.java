package com.shirantech.pashupatipaints.activities.list;

import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.ProgressBar;

import com.actionbarsherlock.app.SherlockActivity;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.shirantech.pashupatipaints.R;
import com.shirantech.pashupatipaints.activities.list.bedrooms.Bedroom1TabletActivity;
import com.shirantech.pashupatipaints.activities.list.bedrooms.Bedroom1V2Activity;
import com.shirantech.pashupatipaints.activities.list.bedrooms.Bedroom2TabletActivity;
import com.shirantech.pashupatipaints.activities.list.bedrooms.Bedroom2V2Activity;
import com.shirantech.pashupatipaints.activities.list.bedrooms.Bedroom3TabletActivity;
import com.shirantech.pashupatipaints.activities.list.bedrooms.Bedroom3V2Activity;
import com.shirantech.pashupatipaints.activities.list.bedrooms.Bedroom4TabletActivity;
import com.shirantech.pashupatipaints.activities.list.bedrooms.Bedroom4V2Activity;
import com.shirantech.pashupatipaints.activities.list.bedrooms.Bedroom5TabletActivity;
import com.shirantech.pashupatipaints.activities.list.bedrooms.Bedroom5V2Activity;
import com.shirantech.pashupatipaints.activities.list.bedrooms.Bedroom6TabletActivity;
import com.shirantech.pashupatipaints.activities.list.bedrooms.Bedroom6V2Activity;
import com.shirantech.pashupatipaints.adapter.RoomCategoryListAdapter;
import com.shirantech.pashupatipaints.controller.GetRoomCategoriesTask;
import com.shirantech.pashupatipaints.interfaces.ModelListGetter;
import com.shirantech.pashupatipaints.model.RoomCategory;
import com.shirantech.pashupatipaints.util.Utility;

import java.util.List;

/**
 * Created by susan on 12/30/14.
 */
public class BedroomsListActivity extends SherlockActivity implements AdapterView.OnItemClickListener, ModelListGetter<RoomCategory> {
    private GridView gridViewBedroomList;
    private ProgressBar progressBarRoomLoading;
    private ImageLoader imageLoader = ImageLoader.getInstance();
    private String[] bedroomDrawableArray = {
            "Bedroom/bedroom1/bed1_main.png",
            "Bedroom/bedroom2/bedroom2_main.png",
            "Bedroom/bedroom3/bedroom3_main.png",
            "Bedroom/bedroom4/bedroom4_main.png",
            "Bedroom/bedroom5/bed5_main.png",
            "Bedroom/bedroom6/bedroom6_main.png"
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(android.view.Window.FEATURE_NO_TITLE);
        if (Build.VERSION.SDK_INT <= Build.VERSION_CODES.HONEYCOMB) {
            getSupportActionBar().hide();
        }
        if (Utility.isTablet(this)) {
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
        } else {
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        }

        setContentView(R.layout.activity_bedroom_list);
        imageLoader.init(ImageLoaderConfiguration.createDefault(this));
        progressBarRoomLoading = (ProgressBar) findViewById(R.id.progressbar_bedrooms_loading);

        gridViewBedroomList = (GridView) findViewById(R.id.gridview_bedrooms);
        gridViewBedroomList.setOnItemClickListener(this);
        findViewById(R.id.home).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(BedroomsListActivity.this, InteriorListActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);

                startActivity(intent);
            }
        });
        new GetRoomCategoriesTask(this, this, "Bed Rooms").execute(bedroomDrawableArray);
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        switch (position) {
            case 0:
                if (Utility.isTablet(this)) {
                    Intent intentBedroom1 = new Intent(this, Bedroom1TabletActivity.class);
                    intentBedroom1.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);

                    startActivity(intentBedroom1);
                } else {
                    Intent intentBedroom1 = new Intent(this, Bedroom1V2Activity.class);
                    intentBedroom1.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);

                    startActivity(intentBedroom1);
                }
                break;
            case 1:
                if (Utility.isTablet(this)) {
                    Intent intentBedroom2 = new Intent(this, Bedroom2TabletActivity.class);
                    intentBedroom2.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(intentBedroom2);
                } else {
                    Intent intentBedroom2 = new Intent(this, Bedroom2V2Activity.class);
                    intentBedroom2.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(intentBedroom2);
                }
                break;
            case 2:
                if (Utility.isTablet(this)) {
                    Intent intentBedroom3 = new Intent(this, Bedroom3TabletActivity.class);
                    intentBedroom3.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(intentBedroom3);
                } else {
                    Intent intentBedroom3 = new Intent(this, Bedroom3V2Activity.class);
                    intentBedroom3.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(intentBedroom3);
                }
                break;
            case 3:
                if (Utility.isTablet(this)) {
                    Intent intentBedroom4 = new Intent(this, Bedroom4TabletActivity.class);
                    intentBedroom4.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(intentBedroom4);
                } else {
                    Intent intentBedroom4 = new Intent(this, Bedroom4V2Activity.class);
                    intentBedroom4.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(intentBedroom4);
                }
                break;
            case 4:
                if(Utility.isTablet(this)) {
                    Intent intentBedroom5 = new Intent(this, Bedroom5TabletActivity.class);
                    intentBedroom5.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(intentBedroom5);
                } else {
                    Intent intentBedroom5 = new Intent(this, Bedroom5V2Activity.class);
                    intentBedroom5.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(intentBedroom5);
                }
                break;
            case 5:
                if(Utility.isTablet(this)) {
                    Intent intentBedroom6 = new Intent(this, Bedroom6TabletActivity.class);
                    intentBedroom6.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(intentBedroom6);
                } else {
                    Intent intentBedroom6 = new Intent(this, Bedroom6V2Activity.class);
                    intentBedroom6.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(intentBedroom6);
                }
                break;
        }
    }

    @Override
    public void onTaskStarted() {
        progressBarRoomLoading.setVisibility(View.VISIBLE);
    }

    @Override
    public void onTaskFinished(List<RoomCategory> modelList) {
        progressBarRoomLoading.setVisibility(View.GONE);
        gridViewBedroomList.setAdapter(new RoomCategoryListAdapter(this, imageLoader, modelList));
    }
}
