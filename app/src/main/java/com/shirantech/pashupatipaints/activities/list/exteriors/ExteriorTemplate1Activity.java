package com.shirantech.pashupatipaints.activities.list.exteriors;

import android.content.Intent;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.view.DragEvent;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnDragListener;
import android.view.View.OnTouchListener;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.actionbarsherlock.app.ActionBar;
import com.actionbarsherlock.app.SherlockFragmentActivity;
import com.actionbarsherlock.view.Menu;
import com.actionbarsherlock.view.MenuItem;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.shirantech.pashupatipaints.R;
import com.shirantech.pashupatipaints.activities.list.ColorChooserActivity;
import com.shirantech.pashupatipaints.fragments.SelectedColorsFragment;
import com.shirantech.pashupatipaints.model.PaintColor;
import com.shirantech.pashupatipaints.util.AppLog;
import com.shirantech.pashupatipaints.util.AppText;
import com.shirantech.pashupatipaints.util.Global;
import com.shirantech.pashupatipaints.util.ImageUtils;

import java.util.List;

public class ExteriorTemplate1Activity extends SherlockFragmentActivity
        implements SelectedColorsFragment.onSelectedColorSelectedListener {

    private static final String CLASS_TAG = ExteriorTemplate1Activity.class.getSimpleName();
    private ImageView ivMainImage, ivFront, ivBorderTop, ivBack, ivTop;
    private float redScale = 1f;
    private float greenScale = 0f;
    private float blueScale = 0f;
    private Boolean isColorClicked = false;

    private int front_r = -1, front_g = -1, front_b = -1;
    private int border_top_r = -1, border_top_g = -1, border_top_b = -1;
    private int back_r = -1, back_g = -1, back_b = -1;
    private int top_r = -1, top_g = -1, top_b = -1;

    RelativeLayout parent;
    RelativeLayout mainParent;

    Global global;
    private ImageLoader imageLoader = ImageLoader.getInstance();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_exterior1);

        imageLoader.init(ImageLoaderConfiguration.createDefault(this));

        final ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setTitle("");

        parent = (RelativeLayout) findViewById(R.id.roomLayout_exterior_1);

        global = (Global) getApplication();
        List<PaintColor> globalPaintColorList = global.getGlobalPaintColorList();

        AppLog.showLog(CLASS_TAG, "size of the global paint-color list :: "
                + globalPaintColorList.size());

        ivMainImage = (ImageView) findViewById(R.id.iv_exterior1_main_image);
        ivFront = (ImageView) findViewById(R.id.iv_exterior1_front);
        ivBorderTop = (ImageView) findViewById(R.id.iv_exterior1_border);
        ivBack = (ImageView) findViewById(R.id.iv_exterior1_back);
        ivTop = (ImageView) findViewById(R.id.iv_exterior1_top);

        /*ivMainImage.setImageBitmap(ImageUtils.getBitmapFromAsset(
                getApplicationContext(), AppText.EXTERIOR1_LOCATION_MAIN_IMAGE, 1));
        ivFront.setImageBitmap(ImageUtils.getBitmapFromAsset(
                getApplicationContext(), AppText.EXTERIOR1_LOCATION_FRONT, 1));
        ivBack.setImageBitmap(ImageUtils.getBitmapFromAsset(
                getApplicationContext(), AppText.EXTERIOR1_LOCATION_BACK, 1));
        ivBorderTop.setImageBitmap(ImageUtils.getBitmapFromAsset(
                getApplicationContext(), AppText.EXTERIOR1_LOCATION_BORDER_TOP, 1));
        ivTop.setImageBitmap(ImageUtils.getBitmapFromAsset(
                getApplicationContext(), AppText.EXTERIOR1_LOCATION_TOP, 1));*/

        ImageUtils.loadImageUsingLoader(imageLoader, ivMainImage, "assets://" + AppText.EXTERIOR1_LOCATION_MAIN_IMAGE);
        ImageUtils.loadImageUsingLoader(imageLoader, ivFront, "assets://" + AppText.EXTERIOR1_LOCATION_FRONT);
        ImageUtils.loadImageUsingLoader(imageLoader, ivBorderTop, "assets://" + AppText.EXTERIOR1_LOCATION_BORDER_TOP);
        ImageUtils.loadImageUsingLoader(imageLoader, ivBack, "assets://" + AppText.EXTERIOR1_LOCATION_BACK);
        ImageUtils.loadImageUsingLoader(imageLoader, ivTop, "assets://" + AppText.EXTERIOR1_LOCATION_TOP);

        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.HONEYCOMB) {
            ivTop.setOnDragListener(new ChoiceDragListener());
        }
        ivTop.setOnTouchListener(new OnTouchListener() {

            @Override
            public boolean onTouch(View v, MotionEvent event) {

                if (isColorClicked) {
                    AppLog.showLog(CLASS_TAG, "Action Touch: start Painting");
                    int x = (int) event.getX();
                    int y = (int) event.getY();
                    int[] colors = ImageUtils.startPainting(getApplicationContext(), ImageUtils.EXTERIOR_TEMPLATE_1,
                            x, y, ivMainImage, parent, new float[]{redScale, greenScale, blueScale},
                            ivFront, ivBorderTop, ivBack, ivTop);
                    int ivId = colors[3];
                    if (ivId == R.id.iv_exterior1_front) {
                        front_r = colors[0];
                        front_g = colors[1];
                        front_b = colors[2];
                    } else if (ivId == R.id.iv_exterior1_border) {
                        border_top_r = colors[0];
                        border_top_g = colors[1];
                        border_top_b = colors[2];
                    } else if (ivId == R.id.iv_exterior1_back) {
                        back_r = colors[0];
                        back_g = colors[1];
                        back_b = colors[2];
                    } else if (ivId == R.id.iv_exterior1_top) {
                        top_r = colors[0];
                        top_g = colors[1];
                        top_b = colors[2];
                    }
                } else {
                    Toast.makeText(getApplicationContext(), "Please select the color below to paint the rooms", Toast.LENGTH_SHORT).show();
                }

                return false;
            }
        });

        //Determine density
        DisplayMetrics metrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(metrics);
        int density = metrics.densityDpi;

        if (density == DisplayMetrics.DENSITY_HIGH) {
            AppLog.showLog(CLASS_TAG, "DENSITY_HIGH... Density is " + String.valueOf(density));
        } else if (density == DisplayMetrics.DENSITY_MEDIUM) {
            AppLog.showLog(CLASS_TAG, "DENSITY_MEDIUM... Density is " + String.valueOf(density));
        } else if (density == DisplayMetrics.DENSITY_LOW) {
            AppLog.showLog(CLASS_TAG, "DENSITY_LOW... Density is " + String.valueOf(density));
        } else {
            AppLog.showLog(CLASS_TAG, "Density is neither HIGH, MEDIUM OR LOW.  Density is " + String.valueOf(density));

            parent.setScaleX(2.0f);
            parent.setScaleY(2.0f);

            parent.post(new Runnable() {

                @Override
                public void run() {
                    int h, w;
                    h = parent.getHeight();
                    w = parent.getWidth();
                    //String size = String.format("TextView's width: %d, height: %d", parent.getWidth(), parent.getHeight());
                    // Toast.makeText(getApplicationContext(), size, Toast.LENGTH_SHORT).show();

                    RelativeLayout.LayoutParams tableLP = new RelativeLayout.LayoutParams(w * 2, h * 2);
                    tableLP.addRule(RelativeLayout.CENTER_IN_PARENT, 1);
                    tableLP.addRule(RelativeLayout.ALIGN_PARENT_TOP, 1);
                    parent.setLayoutParams(tableLP);

                }
            });
        }
    }

    @Override
    public void onColorSelected(PaintColor paintColor) {
        isColorClicked = true;
        Float[] rgb = ImageUtils.hexToRgb(paintColor.getColorCode());
        redScale = rgb[0];
        greenScale = rgb[1];
        blueScale = rgb[2];
        AppLog.showLog(CLASS_TAG, "RedScale::" + redScale + " GreenScale::" + greenScale
                + " BlueScale::" + blueScale);

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getSupportMenuInflater().inflate(R.menu.common_menu, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {

            case R.id.share:
                ImageUtils.shareTemplateImage(this, global, ImageUtils.EXTERIOR_TEMPLATE_1, parent, front_r, front_g, front_b,
                        border_top_r, border_top_g, border_top_b,
                        back_r, back_g, back_b, top_r, top_g, top_b);
                break;

            case R.id.menu_save:
                ImageUtils.saveTemplateImage(this, global, ImageUtils.EXTERIOR_TEMPLATE_1, parent, front_r, front_g, front_b,
                        border_top_r, border_top_g, border_top_b,
                        back_r, back_g, back_b, top_r, top_g, top_b);
                break;

            case android.R.id.home:
                Intent intent = new Intent(this, ColorChooserActivity.class);
                intent.putExtra("COLOR CHOOSER SOURCE", "TEMPLATE");
                intent.putExtra("POSITION", 0);
                intent.putExtra("TYPE", "exterior");
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
                break;

            case R.id.add_more_colors:
                Intent colorChooserIntent = new Intent(getApplicationContext(),
                        ColorChooserActivity.class);
                colorChooserIntent.putExtra("COLOR CHOOSER SOURCE", "TEMPLATE");
                colorChooserIntent.putExtra("POSITION", 0);
                colorChooserIntent.putExtra("TYPE", "exterior");
                colorChooserIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(colorChooserIntent);

                break;

            default:
                break;
        }
        return super.onOptionsItemSelected(item);
    }


    /**
     * DragListener will handle dragged views being dropped on the drop area -
     * only the drop action will have processing added to it as we are not -
     * amending the default behavior for other parts of the drag process
     */
    private class ChoiceDragListener implements OnDragListener {

        @Override
        public boolean onDrag(View v, DragEvent event) {
            switch (event.getAction()) {
                case DragEvent.ACTION_DRAG_STARTED:
                    // no action necessary
                    break;
                case DragEvent.ACTION_DRAG_ENTERED:
                    // no action necessary
                    break;
                case DragEvent.ACTION_DRAG_EXITED:
                    // no action necessary
                    break;
                case DragEvent.ACTION_DROP:
                    AppLog.showLog(CLASS_TAG, "Action Drop: start Painting");
                    int x = (int) event.getX();
                    int y = (int) event.getY();
                    int[] colors = ImageUtils.startPainting(getApplicationContext(), ImageUtils.EXTERIOR_TEMPLATE_1,
                            x, y, ivMainImage, parent, new float[]{redScale, blueScale, greenScale},
                            ivFront, ivBorderTop, ivBack, ivTop);
                    int ivId = colors[3];
                    if (ivId == R.id.iv_exterior1_front) {
                        front_r = colors[0];
                        front_g = colors[1];
                        front_b = colors[2];
                    } else if (ivId == R.id.iv_exterior1_border) {
                        border_top_r = colors[0];
                        border_top_g = colors[1];
                        border_top_b = colors[2];
                    } else if (ivId == R.id.iv_exterior1_back) {
                        back_r = colors[0];
                        back_g = colors[1];
                        back_b = colors[2];
                    } else if (ivId == R.id.iv_exterior1_top) {
                        top_r = colors[0];
                        top_g = colors[1];
                        top_b = colors[2];
                    }
                    // handle the dragged view being dropped over a drop view
                    View view = (View) event.getLocalState();


                    // view dragged item is being dropped on
                    ImageView dropTarget = (ImageView) v;

                    // view being dragged and dropped
                    LinearLayout dropped = (LinearLayout) view;

                    // set the tag in the target view being dropped on - to the ID
                    // of the view being dropped
                    dropTarget.setTag(dropped.getId());

                    break;
                case DragEvent.ACTION_DRAG_ENDED:
                    // no action necessary
                    break;
                default:
                    break;
            }
            return true;
        }
    }
}
