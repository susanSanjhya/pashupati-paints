package com.shirantech.pashupatipaints.activities.list.livingrooms;

import android.app.AlertDialog;
import android.content.ClipData;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Build;
import android.os.Bundle;
import android.os.Vibrator;
import android.util.DisplayMetrics;
import android.view.DragEvent;
import android.view.MotionEvent;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.GridView;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.actionbarsherlock.app.SherlockFragmentActivity;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.shirantech.pashupatipaints.R;
import com.shirantech.pashupatipaints.activities.list.ColorChooserActivity;
import com.shirantech.pashupatipaints.activities.list.GalleryActivity;
import com.shirantech.pashupatipaints.activities.list.GalleryV2Activity;
import com.shirantech.pashupatipaints.activities.list.ImagePreviewActivity;
import com.shirantech.pashupatipaints.adapter.TabletColorHorizontalListAdapter;
import com.shirantech.pashupatipaints.customwidgets.ColorDragShadowBuilder;
import com.shirantech.pashupatipaints.fragments.PaintInfoFragment;
import com.shirantech.pashupatipaints.fragments.SelectedColorsFragment;
import com.shirantech.pashupatipaints.interfaces.ViewOnTouchListener;
import com.shirantech.pashupatipaints.model.PaintColor;
import com.shirantech.pashupatipaints.model.PaintColorHistory;
import com.shirantech.pashupatipaints.util.AppLog;
import com.shirantech.pashupatipaints.util.AppText;
import com.shirantech.pashupatipaints.util.Global;
import com.shirantech.pashupatipaints.util.ImageUtils;
import com.shirantech.pashupatipaints.util.Utility;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 * Created by susan on 4/8/15.
 */
public class LivingRoom2V2Activity extends SherlockFragmentActivity
        implements SelectedColorsFragment.onSelectedColorSelectedListener, View.OnClickListener, ViewOnTouchListener {

    private static final String CLASS_TAG = LivingRoom2V2Activity.class.getSimpleName();
    private ImageLoader imageLoader = ImageLoader.getInstance();
    private RelativeLayout parent;
    private Global global;
    private List<PaintColor> globalPaintColorList;
    private ImageView ivMainImage, ivRightWall;
    private TextView textViewSelectedColorCode, textViewSelectedColorName;
    private View viewSelectedColor;
    private List<PaintColor> usedColorList;
    private boolean isColorClicked = false;
    private float redScale = 1f, greenScale = 0f, blueScale = 0f;
    private boolean isDonePainting = false;
    private GridView gridViewSelectedColors;
    private int rightRed, rightGreen, rightBlue;
    private List<PaintColorHistory> redoColorHisitoryList = new ArrayList<PaintColorHistory>();
    private List<PaintColorHistory> undoColorHistoryList = new ArrayList<PaintColorHistory>();
    private Vibrator mVibrator;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(android.view.Window.FEATURE_NO_TITLE);
        if (Build.VERSION.SDK_INT <= Build.VERSION_CODES.HONEYCOMB) {
            getSupportActionBar().hide();
        }
        setContentView(R.layout.activity_living_room2);
        imageLoader.init(ImageLoaderConfiguration.createDefault(this));
        parent = (RelativeLayout) findViewById(R.id.roomLayout_living_2);

        global = (Global) getApplication();
        globalPaintColorList = global.getGlobalPaintColorList();

        ivMainImage = (ImageView) findViewById(R.id.iv_living2_main_image);
        ivRightWall = (ImageView) findViewById(R.id.iv_living2_right_wall);

        ImageUtils.loadImageUsingLoader(imageLoader, ivMainImage, "assets://" + AppText.LIVINGROOM2_LOCATION_MAIN);
        ImageUtils.loadImageUsingLoader(imageLoader, ivRightWall, "assets://" + AppText.LIVINGROOM2_LOCATION_RIGHT);

        Button imageButtonAddMoreColor = (Button) findViewById(R.id.image_button_add_more_color);
        imageButtonAddMoreColor.setOnClickListener(this);

        ImageButton imageButtonRefresh = (ImageButton) findViewById(R.id.imagebutton_paint_refresh);
        ImageButton imageButtonSave = (ImageButton) findViewById(R.id.imagebutton_paint_save);
        ImageButton imageButtonShare = (ImageButton) findViewById(R.id.imagebutton_paint_share);
        ImageButton imageButtonUndo = (ImageButton) findViewById(R.id.imagebutton_paint_undo);
        ImageButton imageButtonRedo = (ImageButton) findViewById(R.id.imagebutton_paint_redo);
        Button imageButtonInfo = (Button) findViewById(R.id.imagebutton_info);
        TextView imageButtonGoToGallery = (TextView) findViewById(R.id.imagebutton_go_to_gallery);
        ImageButton imageButtonBack = (ImageButton) findViewById(R.id.home);

        imageButtonInfo.setOnClickListener(this);
        imageButtonBack.setOnClickListener(this);
        imageButtonGoToGallery.setOnClickListener(this);
        imageButtonRedo.setOnClickListener(this);
        imageButtonUndo.setOnClickListener(this);
        imageButtonShare.setOnClickListener(this);
        imageButtonSave.setOnClickListener(this);
        imageButtonRefresh.setOnClickListener(this);

        textViewSelectedColorCode = (TextView) findViewById(R.id.text_view_selected_color_code);
        textViewSelectedColorName = (TextView) findViewById(R.id.text_view_selected_color_name);
        viewSelectedColor = findViewById(R.id.view_selected_color);

        usedColorList = new ArrayList<PaintColor>();

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
            ivRightWall.setOnDragListener(new ChoiceDragListener());
        }
        ivRightWall.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (isColorClicked) {
                    int[] colors = ImageUtils.startPainting(LivingRoom2V2Activity.this, ImageUtils.LIVINGROOM_TEMPLATE_2,
                            (int) event.getX(), (int) event.getY(), ivMainImage, parent,
                            new float[]{redScale, greenScale, blueScale},
                            ivRightWall);
                    isDonePainting = true;
                    updatePaintStatus(colors);

                } else {
                    Toast.makeText(getApplicationContext(), "Please select the color below to paint the rooms", Toast.LENGTH_SHORT).show();
                }
                return false;
            }
        });
        //Determine density
        DisplayMetrics metrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(metrics);
        int density = metrics.densityDpi;

        if (density == DisplayMetrics.DENSITY_HIGH) {
            AppLog.showLog(CLASS_TAG, "DENSITY_HIGH... Density is " + String.valueOf(density));
        } else if (density == DisplayMetrics.DENSITY_MEDIUM) {
            AppLog.showLog(CLASS_TAG, "DENSITY_MEDIUM... Density is " + String.valueOf(density));
        } else if (density == DisplayMetrics.DENSITY_LOW) {
            AppLog.showLog(CLASS_TAG, "DENSITY_LOW... Density is " + String.valueOf(density));
        } else {
            AppLog.showLog(CLASS_TAG, "Density is neither HIGH, MEDIUM OR LOW.  Density is " + String.valueOf(density));
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        globalPaintColorList = ((Global) getApplication()).getGlobalPaintColorList();

        gridViewSelectedColors = (GridView) findViewById(R.id.selected_color_fragment);/*.findViewById(R.id.gridViewSelectedColors));*/
        gridViewSelectedColors.setAdapter(new TabletColorHorizontalListAdapter(this, globalPaintColorList, this));
        /*if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.HONEYCOMB) {
            gridViewSelectedColors.setOnTouchListener(new ChoiceTouchListener());
        }*/


        gridViewSelectedColors.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                PaintColor paintColor = (PaintColor) gridViewSelectedColors.getAdapter().getItem(position);
                AppLog.showLog(CLASS_TAG, "grid selected item:: " + paintColor.getName());
                sendToParent(paintColor);
            }
        });

        mVibrator = (Vibrator) this.getSystemService(Context.VIBRATOR_SERVICE);
        gridViewSelectedColors.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(final AdapterView<?> parent, View view, final int position, long id) {
                mVibrator.vibrate(30);
                TabletColorHorizontalListAdapter.ViewHolder holder = (TabletColorHorizontalListAdapter.ViewHolder) view.getTag();
                holder.imageButtonDelete.setVisibility(View.VISIBLE);
                holder.imageButtonDelete.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        final PaintColor pc = (PaintColor) parent.getAdapter().getItem(position);
                        globalPaintColorList.remove(pc);
                        gridViewSelectedColors.setAdapter(new TabletColorHorizontalListAdapter(LivingRoom2V2Activity.this,
                                globalPaintColorList, LivingRoom2V2Activity.this));
                        ((TabletColorHorizontalListAdapter)parent.getAdapter()).notifyDataSetChanged();
                    }
                });
                return true;
            }
        });
    }

    private void sendToParent(PaintColor paintColor) {
        isColorClicked = true;

        Float[] rgb = ImageUtils.hexToRgb(paintColor.getColorCode());

        redScale = rgb[0];
        greenScale = rgb[1];
        blueScale = rgb[2];
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
            viewSelectedColor.setBackground(Utility.getCustomShape(Color.parseColor(paintColor.getColorCode())));
        } else {
            viewSelectedColor.setBackgroundDrawable(Utility.getCustomShape(Color.parseColor(paintColor.getColorCode())));
        }
        textViewSelectedColorName.setText(paintColor.getName());
        textViewSelectedColorCode.setText(paintColor.getColorCode());
    }

    @Override
    public void onColorSelected(PaintColor paintColor) {
        isColorClicked = true;

        Float[] rgb = ImageUtils.hexToRgb(paintColor.getColorCode());

        redScale = rgb[0];
        greenScale = rgb[1];
        blueScale = rgb[2];
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
            viewSelectedColor.setBackground(Utility.getCustomShape(Color.parseColor(paintColor.getColorCode())));
        } else {
            viewSelectedColor.setBackgroundDrawable(Utility.getCustomShape(Color.parseColor(paintColor.getColorCode())));
        }
        textViewSelectedColorName.setText(paintColor.getName());
        textViewSelectedColorCode.setText(paintColor.getColorCode());
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.image_button_add_more_color:
                Intent colorChooserIntent = new Intent(this, ColorChooserActivity.class);
//                colorChooserIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                if (globalPaintColorList.size() <= 0) {
                    colorChooserIntent.putExtra("COLOR CHOOSER SOURCE", "STARTER");
                    colorChooserIntent.putExtra("POSITION", 0);
                    colorChooserIntent.putExtra("TYPE", "bedroom");
                } else {
                    colorChooserIntent.putExtra("COLOR CHOOSER SOURCE", "TEMPLATE");
                    colorChooserIntent.putExtra("POSITION", 0);
                    colorChooserIntent.putExtra("TYPE", "bedroom");
                }
                startActivity(colorChooserIntent);
                break;

            case R.id.home:
                global.getGlobalPaintColorList().clear();
                finish();
                break;
            case R.id.imagebutton_paint_undo:
                undoPainting();
                break;
            case R.id.imagebutton_paint_refresh:
                AlertDialog.Builder builder = new AlertDialog.Builder(this);
                builder.setTitle("Reset painting");
                builder.setMessage("Want to reset all the painting details?");
                builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        refreshPaintStatus();
                        dialogInterface.dismiss();
                    }
                });
                builder.setNegativeButton("No", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dialogInterface.dismiss();
                    }
                });
                builder.create().show();
                break;
            case R.id.imagebutton_paint_save:
                if (usedColorList.size() <= 0 || usedColorList.isEmpty()) {
                    Toast.makeText(this, "Please Paint the image to save", Toast.LENGTH_SHORT)
                            .show();
                    return;
                }
                AlertDialog.Builder builder1 = new AlertDialog.Builder(this);
                builder1.setTitle("Save Image");
                builder1.setMessage("Do you want to save image?");
                builder1.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        ImageUtils.saveTemplateImage(LivingRoom2V2Activity.this, global, ImageUtils.LIVINGROOM_TEMPLATE_2,
                                parent, rightRed, rightGreen, rightBlue
                        );
                        dialogInterface.dismiss();
                    }
                });
                builder1.setNegativeButton("No", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dialogInterface.dismiss();
                    }
                });
                builder1.create().show();
                break;
            case R.id.imagebutton_paint_share:
                Bitmap bm;
                parent.setDrawingCacheEnabled(true);
                parent.buildDrawingCache();
                bm = parent.getDrawingCache();
                Bitmap bmCopy = bm.copy(Bitmap.Config.ARGB_8888, false);
                parent.destroyDrawingCache();
                Intent intent = new Intent(this, ImagePreviewActivity.class);
                ((Global) getApplication()).setSnapAndPaintBitmap(bmCopy);
                startActivity(intent);
                break;
            case R.id.imagebutton_paint_redo:
                redoPainting();
                break;
            case R.id.imagebutton_info:
//                showInfoDialog();
                showInfoActivity();
                break;
            case R.id.imagebutton_go_to_gallery:
                Intent intentGallery = new Intent(LivingRoom2V2Activity.this,
                        GalleryV2Activity.class);
                intentGallery.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intentGallery);
                break;
        }
    }

    @Override
    public void finish() {
        ((Global) getApplication()).getGlobalPaintColorList().clear();
        super.finish();
    }

    private void showInfoActivity() {
        AppLog.showLog(CLASS_TAG, "size of used color : " + usedColorList.size());
        if (usedColorList == null || usedColorList.size() <= 0) {
            Toast.makeText(this, "Info unavailable. Paint first.", Toast.LENGTH_SHORT)
                    .show();
            return;
        }
//        Intent infoActivity = new Intent(this, PaintInfoActivity.class);
        PaintInfoFragment fragment = new PaintInfoFragment();
        Bundle bundle = new Bundle();
        bundle.putParcelableArrayList("USED_COLOR_LIST", (ArrayList<PaintColor>) usedColorList);
        fragment.setArguments(bundle);
        fragment.show(getSupportFragmentManager(), "Paint Info");
//        infoActivity.putExtras(bundle);
//        startActivity(infoActivity);
    }

    private void redoPainting() {
        isDonePainting = false;
        if (redoColorHisitoryList.size() - 1 < 0) {
            Toast.makeText(this, "redo unavailable.", Toast.LENGTH_SHORT)
                    .show();
        } else {
            int maxIndex = redoColorHisitoryList.size() - 1;
            undoColorHistoryList.add(redoColorHisitoryList.get(maxIndex));
            PaintColorHistory paintColorHistory = redoColorHisitoryList.get(maxIndex);
            redoColorHisitoryList.remove(maxIndex);
            Float[] colorScale = paintColorHistory.getColorCode();
            if (paintColorHistory.getImageViewId() == R.id.iv_living2_right_wall) {
                if (colorScale[0] == 0 && colorScale[1] == 0 && colorScale[2] == 0) {
                    ImageUtils.loadImageUsingLoader(imageLoader, ivRightWall,
                            "assets://" + AppText.LIVINGROOM2_LOCATION_RIGHT);
                } else {
                    int[] colors = ImageUtils.undoPainting(LivingRoom2V2Activity.this,
                            ivRightWall, AppText.LIVINGROOM2_LOCATION_RIGHT, colorScale);
                    updatePaintStatus(colors);
                }
            }
        }
    }

    private void refreshPaintStatus() {
        isColorClicked = false;
        redScale = 1f;
        greenScale = 0f;
        blueScale = 0f;
        usedColorList.clear();
        undoColorHistoryList.clear();
        redoColorHisitoryList.clear();

        viewSelectedColor.setBackgroundDrawable(new ColorDrawable(android.R.color.transparent));
        textViewSelectedColorCode.setText("");
        textViewSelectedColorName.setText("");
        ImageUtils.loadImageUsingLoader(imageLoader, ivMainImage, "assets://" + AppText.LIVINGROOM2_LOCATION_MAIN);
        ImageUtils.loadImageUsingLoader(imageLoader, ivRightWall, "assets://" + AppText.LIVINGROOM2_LOCATION_RIGHT);
        globalPaintColorList.clear();
        ((TabletColorHorizontalListAdapter)gridViewSelectedColors.getAdapter()).notifyDataSetChanged();
    }

    private void undoPainting() {
        isDonePainting = false;
        if (undoColorHistoryList.size() <= 0) {
            Toast.makeText(this, "undo unavailable.", Toast.LENGTH_SHORT)
                    .show();
        } else {
            int maxIndex = undoColorHistoryList.size() - 1;
            redoColorHisitoryList.add(undoColorHistoryList.get(maxIndex));
            undoColorHistoryList.remove(maxIndex);
            maxIndex = undoColorHistoryList.size() - 1;
            if (maxIndex < 0) {
                Toast.makeText(this, "undo unavailable.", Toast.LENGTH_SHORT)
                        .show();
                return;
            }
            PaintColorHistory paintColorHistory = undoColorHistoryList.get(maxIndex);
            Float[] colorScale = paintColorHistory.getColorCode();
            if (paintColorHistory.getImageViewId() == R.id.iv_living2_right_wall) {
                if (colorScale[0] == 0 && colorScale[1] == 0 && colorScale[2] == 0) {
                    ImageUtils.loadImageUsingLoader(imageLoader, ivRightWall,
                            "assets://" + AppText.LIVINGROOM2_LOCATION_RIGHT);
                    updatePaintStatus(new int[]{0, 0, 0, R.id.iv_living2_right_wall});
                } else {
                    int[] colors = ImageUtils.undoPainting(this, ivRightWall, AppText.LIVINGROOM2_LOCATION_RIGHT, colorScale);
                    updatePaintStatus(colors);
                }
            }
        }
    }

    private void updatePaintStatus(int[] colors) {
        if (colors[3] == R.id.iv_living2_right_wall) {

            if (isDonePainting) {
                boolean isFirstTimePainted = true;
                for (PaintColorHistory paintColorHistory : undoColorHistoryList) {
                    if (paintColorHistory.getImageViewId() == R.id.iv_living2_right_wall) {
                        isFirstTimePainted = false;
                        AppLog.showLog(CLASS_TAG, "is first time painted:" + false);
                        break;
                    } else {
                        isFirstTimePainted = true;
                        AppLog.showLog(CLASS_TAG, "is first time painted:" + true);
                    }
                }
                if (isFirstTimePainted) {
                    PaintColorHistory defaultPaintColor = new PaintColorHistory();
                    defaultPaintColor.setImageViewId(R.id.iv_living2_right_wall);
                    defaultPaintColor.setColorCode(new Float[]{0f, 0f, 0f});
                    undoColorHistoryList.add(defaultPaintColor);

                    PaintColorHistory paintColorHistory = new PaintColorHistory();
                    paintColorHistory.setColorCode(new Float[]{redScale, greenScale, blueScale});
                    paintColorHistory.setImageViewId(R.id.iv_living2_right_wall);
                    undoColorHistoryList.add(paintColorHistory);
                    AppLog.showLog(CLASS_TAG, "first time painted: undo size " + undoColorHistoryList.size());
                } else {
                    int maxIndex = undoColorHistoryList.size() - 1;
                    PaintColorHistory paintColorHistory1 = undoColorHistoryList.get(maxIndex);
                    AppLog.showLog(CLASS_TAG, "is id same:" + (paintColorHistory1.getImageViewId() != R.id.iv_living2_right_wall));
                    AppLog.showLog(CLASS_TAG, "is color same:" + (paintColorHistory1.getColorCode()[0] != redScale
                            && paintColorHistory1.getColorCode()[1] != greenScale
                            && paintColorHistory1.getColorCode()[2] != blueScale));
                    if ((paintColorHistory1.getImageViewId() != R.id.iv_living2_right_wall)
                            || (paintColorHistory1.getColorCode()[0] != redScale
                            || paintColorHistory1.getColorCode()[1] != greenScale
                            || paintColorHistory1.getColorCode()[2] != blueScale)) {
                        PaintColorHistory paintColorHistory = new PaintColorHistory();
                        paintColorHistory.setColorCode(new Float[]{redScale, greenScale, blueScale});
                        paintColorHistory.setImageViewId(R.id.iv_living2_right_wall);
                        undoColorHistoryList.add(paintColorHistory);
                        AppLog.showLog(CLASS_TAG, "not first time painted: undo size " + undoColorHistoryList.size());
                    }
                }
            }
            rightRed = colors[0];
            rightGreen = colors[1];
            rightBlue = colors[2];

            if (rightRed == 0 && rightGreen == 0 && rightBlue == 0) {
                Iterator<PaintColor> iterator = usedColorList.iterator();
                while (iterator.hasNext()) {
                    PaintColor paintColor = iterator.next();
                    if ("Right Wall".equalsIgnoreCase(paintColor.getTypeWall())) {
                        iterator.remove();
                    }
                }
                return;
            }

            String rightColorHash = ImageUtils.RgbToHex(rightRed, rightGreen, rightBlue);
            String rightColorName = null;
            for (PaintColor paintColor : global.globalPaintColorList) {
                if (paintColor.getColorCode().equalsIgnoreCase(rightColorHash)) {
                    rightColorName = paintColor.getName();
                }
            }

            PaintColor paintColor = new PaintColor();
            paintColor.setColorCode(rightColorHash);
            paintColor.setName(rightColorName);
            paintColor.setTypeWall("Right Wall");

            Iterator<PaintColor> iterator = usedColorList.iterator();
            while (iterator.hasNext()) {
                PaintColor paintColor1 = iterator.next();
                if ("Right Wall".equalsIgnoreCase(paintColor1.getTypeWall())) {
                    iterator.remove();
                }
            }
            usedColorList.add(paintColor);
        }
    }

    @Override
    public void onViewTouch(PaintColor paintColor) {
        sendToParent(paintColor);
    }

    private class ChoiceDragListener implements View.OnDragListener {
        @Override
        public boolean onDrag(View v, DragEvent event) {
            switch (event.getAction()) {
                case DragEvent.ACTION_DRAG_STARTED:
                    //no action necessary
                    break;
                case DragEvent.ACTION_DRAG_ENTERED:
                    //no action necessary
                    break;
                case DragEvent.ACTION_DRAG_EXITED:
                    //no action necessary
                    break;
                case DragEvent.ACTION_DROP:
                    int[] colors = ImageUtils.startPainting(LivingRoom2V2Activity.this, ImageUtils.LIVINGROOM_TEMPLATE_2,
                            (int) event.getX(), (int) event.getY(), ivMainImage, parent,
                            new float[]{redScale, greenScale, blueScale},
                            ivRightWall);
                    isDonePainting = true;
                    updatePaintStatus(colors);

                    //handle the dragged view being dropped over a drop view
                    View view = (View) event.getLocalState();

                    //stop displaying the view where it was before it was dragged
                    ////view.setVisibility(View.INVISIBLE);

                    //view dragged item is being dropped on
                    ImageView dropTarget = (ImageView) v;

                    //view being dragged and dropped
//                    RelativeLayout dropped = (RelativeLayout) view;


                    //if an item has already been dropped here, there will be a tag
                    Object tag = dropTarget.getTag();


                    //set the tag in the target view being dropped on - to the ID of the view being dropped
                    dropTarget.setTag(view.getId());

                    break;
                case DragEvent.ACTION_DRAG_ENDED:
                    //no action necessary
                    break;
                default:
                    break;
            }

            return true;
        }
    }

    private final class ChoiceTouchListener implements View.OnTouchListener {

        @Override
        public boolean onTouch(View v, MotionEvent event) {

            if (event.getAction() == MotionEvent.ACTION_DOWN) {
                GridView parent = (GridView) v;

                int x = (int) event.getX();
                int y = (int) event.getY();

                int position = parent.pointToPosition(x, y);
                if (position > AdapterView.INVALID_POSITION) {

                    int count = parent.getChildCount();

                    AppLog.showLog(CLASS_TAG, "x: " + x + " y:: " + y + " position:: "
                            + position + " count:: " + count);

                    PaintColor paintColor = (PaintColor) gridViewSelectedColors.getAdapter().getItem(position);
                    AppLog.showLog(CLASS_TAG, "grid selected item:: " + paintColor.getName());

                    sendToParent(paintColor);

                    for (int i = 0; i < count; i++) {
                        View view = parent.getChildAt(i);

                        ClipData data = ClipData.newPlainText("color", paintColor.getColorCode());
                        ColorDragShadowBuilder shadowBuilder = new ColorDragShadowBuilder(paintColor.getColorCode());
                        //start dragging the item touched
                        view.startDrag(data, shadowBuilder, view, 0);

                        return true;
                    }
                }
            }
            return false;
        }
    }
}
