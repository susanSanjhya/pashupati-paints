package com.shirantech.pashupatipaints.activities.list;

import android.content.Context;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.graphics.Rect;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.view.ViewPager;
import android.text.Html;
import android.text.InputType;
import android.util.DisplayMetrics;
import android.util.Log;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.view.Window;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import com.actionbarsherlock.app.SherlockFragmentActivity;
import com.shirantech.pashupatipaints.R;
import com.shirantech.pashupatipaints.adapter.PaintTypeAdapter;
import com.shirantech.pashupatipaints.customwidgets.CustomBoldTextView;
import com.shirantech.pashupatipaints.customwidgets.CustomThinTextView;
import com.shirantech.pashupatipaints.model.PaintCalculatorField;
import com.shirantech.pashupatipaints.model.PaintCalculatorType;
import com.shirantech.pashupatipaints.util.AppLog;
import com.shirantech.pashupatipaints.util.Utility;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by susan on 1/7/15.
 */
@SuppressWarnings("deprecation")
public class PaintCalculatorTabletActivity extends SherlockFragmentActivity implements View.OnClickListener, View.OnTouchListener, TextView.OnEditorActionListener {

    private static final String CLASS_TAG = PaintCalculatorTabletActivity.class.getSimpleName();
    private LinearLayout linearLayoutWallFields, linearLayoutWdFields;
    private int wallFieldCounter, wdFieldCounter;
    private List<PaintCalculatorField> wallPaintCalculatorFieldList, wdPaintCalculatorFieldList;
    private TextView textViewliter, textViewNotification, textViewResult;
    private EditText editTextWallHeight1, editTextWallHeight2, editTextWallHeight3, editTextWallHeight4,
            editTextWdHeight1, editTextWdHeight2, editTextWdHeight3, editTextWdHeight4,
            editTextWallWidth1, editTextWallWidth2, editTextWallWidth3, editTextWallWidth4,
            editTextWdWidth1, editTextWdWidth2, editTextWdWidth3, editTextWdWidth4;
    //    private ImageView imageViewColorType;
    private ImageButton buttonUp, buttonDown;

    private List<PaintCalculatorType> paintCalculatorTypeList;
    private int paintCalculatorTypeCounter = 0;
    Runnable mRunnable;
    private ScrollView scrollView;
    Handler mHandler = new Handler();
    private ViewPager pager;
    private PaintTypeAdapter adapter;
    private LinearLayout root;
    private int layoutHeight;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        AppLog.showLog(CLASS_TAG, "which device " + getResources().getString(R.string.whichDevice));
        requestWindowFeature(android.view.Window.FEATURE_NO_TITLE);
        if (Build.VERSION.SDK_INT <= Build.VERSION_CODES.HONEYCOMB) {
            getSupportActionBar().hide();
        }
        if (Utility.isTablet(this)) {
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
        } else {
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        }
        setContentView(R.layout.activity_paint_calculator);

        if (!Utility.isTablet(this)) {
            setLayoutHeight();
        }

        scrollView = (ScrollView) findViewById(R.id.scroll_view);
        AppLog.showLog(CLASS_TAG, "PaintCalculatorTabletActivity started");

        makeMyScrollViewSmart();
        pager = (ViewPager) findViewById(R.id.pager);
        adapter = new PaintTypeAdapter(getSupportFragmentManager());
        pager.setAdapter(adapter);
        pager.setOnTouchListener(this);
        pager.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int i, float v, int i2) {

            }

            @Override
            public void onPageSelected(int i) {
                paintCalculatorTypeCounter = i;
                displayPaintCalculatorType(paintCalculatorTypeCounter);
            }

            @Override
            public void onPageScrollStateChanged(int i) {

            }
        });

//        imageViewColorType = (ImageView) findViewById(R.id.image_view_color_type);
//        imageViewColorType.setOnTouchListener(this);
        if ("xhdpi".equals(getString(R.string.whichDevice))||"xxxhdpi".equals(getString(R.string.whichDevice))||"xxhdpi".equals(getString(R.string.whichDevice)) || "600dp".equals(getString(R.string.whichDevice))) {
            wallFieldCounter = 2;
            wdFieldCounter = 2;
        } else if ("normal values".equals(getString(R.string.whichDevice)) || "mdpi".equals(getString(R.string.whichDevice)) || "hdpi".equals(getString(R.string.whichDevice))) {

            wallFieldCounter = 1;
            wdFieldCounter = 1;
        } else {
            wdFieldCounter = 4;
            wallFieldCounter = 4;
        }

        buttonUp = (ImageButton) findViewById(R.id.button_up);
        buttonUp.setOnClickListener(this);
        buttonUp.setVisibility(View.INVISIBLE);
        buttonDown = (ImageButton) findViewById(R.id.button_down);
        buttonDown.setOnClickListener(this);

        mRunnable = new Runnable() {

            @Override
            public void run() {
                buttonDown.setVisibility(View.GONE);
                buttonUp.setVisibility(View.INVISIBLE);
            }
        };

        mHandler.postDelayed(mRunnable, 5 * 1000);

        findViewById(R.id.home).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (Utility.isTablet(PaintCalculatorTabletActivity.this)) {
                    Intent intent = new Intent(PaintCalculatorTabletActivity.this, StarterTabletActivity.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(intent);
                } else {
                    Intent intent = new Intent(PaintCalculatorTabletActivity.this, StarterV2Activity.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(intent);
                }
            }
        });
        linearLayoutWallFields = (LinearLayout) findViewById(R.id.linearlayout_wall_fields);
        linearLayoutWdFields = (LinearLayout) findViewById(R.id.linearlayout_wd_fields);

        textViewResult = (TextView) findViewById(R.id.text_view_result);
        textViewliter = (TextView) findViewById(R.id.textview_result);
        textViewNotification = (TextView) findViewById(R.id.text_view_paint_calculator_notification);

        editTextWallHeight1 = (EditText) findViewById(R.id.edittext_wall_height_1);
        editTextWallHeight1.setOnEditorActionListener(this);
        editTextWdHeight1 = (EditText) findViewById(R.id.edittext_wd_height_1);
        editTextWdHeight1.setOnEditorActionListener(this);
        editTextWallWidth1 = (EditText) findViewById(R.id.edittext_wall_width_1);
        editTextWallWidth1.setOnEditorActionListener(this);
        editTextWdWidth1 = (EditText) findViewById(R.id.edittext_wd_width_1);
        editTextWdWidth1.setOnEditorActionListener(this);

        if ("720dp".equals(getString(R.string.whichDevice)) || "1024dp".equals(getString(R.string.whichDevice))) {
            editTextWallHeight2 = (EditText) findViewById(R.id.edittext_wall_height_2);
            editTextWallHeight2.setOnEditorActionListener(this);
            editTextWdHeight2 = (EditText) findViewById(R.id.edittext_wd_height_2);
            editTextWdHeight2.setOnEditorActionListener(this);
            editTextWallWidth2 = (EditText) findViewById(R.id.edittext_wall_width_2);
            editTextWallWidth2.setOnEditorActionListener(this);
            editTextWdWidth2 = (EditText) findViewById(R.id.edittext_wd_width_2);
            editTextWdWidth2.setOnEditorActionListener(this);
//            editTextWallHeight3 = (EditText) findViewById(R.id.edittext_wall_height_3);
//            editTextWallHeight3.setOnEditorActionListener(this);
//            editTextWallHeight4 = (EditText) findViewById(R.id.edittext_wall_height_4);
//            editTextWallHeight4.setOnEditorActionListener(this);
//            editTextWdHeight3 = (EditText) findViewById(R.id.edittext_wd_height_3);
//            editTextWdHeight3.setOnEditorActionListener(this);
//            editTextWdHeight4 = (EditText) findViewById(R.id.edittext_wd_height_4);
//            editTextWdHeight4.setOnEditorActionListener(this);
//            editTextWallWidth3 = (EditText) findViewById(R.id.edittext_wall_width_3);
//            editTextWallWidth3.setOnEditorActionListener(this);
//            editTextWallWidth4 = (EditText) findViewById(R.id.edittext_wall_width_4);
//            editTextWallWidth4.setOnEditorActionListener(this);
//            editTextWdWidth3 = (EditText) findViewById(R.id.edittext_wd_width_3);
//            editTextWdWidth3.setOnEditorActionListener(this);
//            editTextWdWidth4 = (EditText) findViewById(R.id.edittext_wd_width_4);
//            editTextWdWidth4.setOnEditorActionListener(this);
        } else if ("xhdpi".equals(getString(R.string.whichDevice)) || "600dp".equals(getString(R.string.whichDevice))) {
            editTextWallHeight2 = (EditText) findViewById(R.id.edittext_wall_height_2);
            editTextWallHeight2.setOnEditorActionListener(this);
            editTextWdHeight2 = (EditText) findViewById(R.id.edittext_wd_height_2);
            editTextWdHeight2.setOnEditorActionListener(this);
            editTextWallWidth2 = (EditText) findViewById(R.id.edittext_wall_width_2);
            editTextWallWidth2.setOnEditorActionListener(this);
            editTextWdWidth2 = (EditText) findViewById(R.id.edittext_wd_width_2);
            editTextWdWidth2.setOnEditorActionListener(this);
        }

        TextView buttonAddWallField = (TextView) findViewById(R.id.button_add_wall_field);
        TextView buttonAddWdField = (TextView) findViewById(R.id.button_add_wd_field);
        Button buttonCalculate = (Button) findViewById(R.id.button_calculate);
        buttonCalculate.setOnClickListener(this);
        buttonAddWallField.setOnClickListener(this);
        buttonAddWdField.setOnClickListener(this);

        wallPaintCalculatorFieldList = new ArrayList<PaintCalculatorField>();
        wdPaintCalculatorFieldList = new ArrayList<PaintCalculatorField>();

        insertPaintCalculatorTypeDetails();
//        imageViewColorType.setImageDrawable(getResources().getDrawable(paintCalculatorTypeList.get(paintCalculatorTypeCounter).getProductsDrawable()));
    }

    private void makeMyScrollViewSmart() {
        scrollView.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (event.getAction() == MotionEvent.ACTION_DOWN) {
                    //  Disallow the touch request for parent scroll on touch of child view
                    requestDisallowParentInterceptTouchEvent(v, true);
                } else if (event.getAction() == MotionEvent.ACTION_UP || event.getAction() == MotionEvent.ACTION_CANCEL) {
                    // Re-allows parent events
                    requestDisallowParentInterceptTouchEvent(v, false);
                }
                return false;
            }
        });
    }

    private void requestDisallowParentInterceptTouchEvent(View v, Boolean disallowIntercept) {
        while (v.getParent() != null && v.getParent() instanceof View) {
            if (v.getParent() instanceof ScrollView) {
                v.getParent().requestDisallowInterceptTouchEvent(disallowIntercept);
            }
            v = (View) v.getParent();
        }
    }

    private void setLayoutHeight() {
        root = (LinearLayout) findViewById(R.id.mainroot);
        root.post(new Runnable() {
            public void run() {
                Rect rect = new Rect();
                Window win = getWindow();  // Get the Window
                win.getDecorView().getWindowVisibleDisplayFrame(rect);
                // Get the height of Status Bar
                int statusBarHeight = rect.top;
                // Get the height occupied by the decoration contents
                int contentViewTop = win.findViewById(Window.ID_ANDROID_CONTENT).getTop();
                // Calculate titleBarHeight by deducting statusBarHeight from contentViewTop
                int titleBarHeight = contentViewTop - statusBarHeight;
                Log.i("MY", "titleHeight = " + titleBarHeight + " statusHeight = " + statusBarHeight + " contentViewTop = " + contentViewTop);

                // By now we got the height of titleBar & statusBar
                // Now lets get the screen size
                DisplayMetrics metrics = new DisplayMetrics();
                getWindowManager().getDefaultDisplay().getMetrics(metrics);
                int screenHeight = metrics.heightPixels;
                int screenWidth = metrics.widthPixels;
                Log.i("MY", "Actual Screen Height = " + screenHeight + " Width = " + screenWidth);

                // Now calculate the height that our layout can be set
                // If you know that your application doesn't have statusBar added, then don't add here also. Same applies to application bar also

                layoutHeight = screenHeight - (titleBarHeight + statusBarHeight);
                Log.i("MY", "Layout Height = " + layoutHeight);

                // Lastly, set the height of the layout
                FrameLayout.LayoutParams rootParams = (FrameLayout.LayoutParams) root.getLayoutParams();
                rootParams.height = layoutHeight;
                root.setLayoutParams(rootParams);
            }
        });

        final RelativeLayout layoutActionBar = (RelativeLayout) findViewById(R.id.layout_action_bar);
        layoutActionBar.post(new Runnable() {
            @Override
            public void run() {
                LinearLayout.LayoutParams params = (LinearLayout.LayoutParams) layoutActionBar.getLayoutParams();
                params.height = (int) (layoutHeight * .10);
                Log.i(CLASS_TAG, "action bar height " + (int) (layoutHeight * .15));
                layoutActionBar.setLayoutParams(params);
            }
        });

        final CustomThinTextView textViewTitle = (CustomThinTextView) findViewById(R.id.text_view_title);
        textViewTitle.post(new Runnable() {
            @Override
            public void run() {
                LinearLayout.LayoutParams params = (LinearLayout.LayoutParams) textViewTitle.getLayoutParams();
                params.height = (int) (layoutHeight * .10);
                Log.i(CLASS_TAG, "title height " + (int) (layoutHeight * .10));
                textViewTitle.setLayoutParams(params);
            }
        });

        final LinearLayout linearLayoutContainer = (LinearLayout) findViewById(R.id.linear_layout_container);
        linearLayoutContainer.post(new Runnable() {
            @Override
            public void run() {
                LinearLayout.LayoutParams params = (LinearLayout.LayoutParams) linearLayoutContainer.getLayoutParams();
                params.height = (int) (layoutHeight * .78);
                Log.i(CLASS_TAG, "Container height " + (int) (layoutHeight * .73));
                linearLayoutContainer.setLayoutParams(params);
            }
        });

        final LinearLayout layoutBottomPattern = (LinearLayout) findViewById(R.id.layout_bottom_pattern);
        layoutBottomPattern.post(new Runnable() {
            @Override
            public void run() {
                LinearLayout.LayoutParams params = (LinearLayout.LayoutParams) layoutBottomPattern.getLayoutParams();
                params.height = (int) (layoutHeight * .02);
                Log.i(CLASS_TAG, "bottom pattern height " + (int) (layoutHeight * .02));
                layoutBottomPattern.setLayoutParams(params);
            }
        });
    }

    private void insertPaintCalculatorTypeDetails() {
        paintCalculatorTypeList = new ArrayList<PaintCalculatorType>();
        paintCalculatorTypeList.add(new PaintCalculatorType(R.drawable.excoat_plus, 70));
        paintCalculatorTypeList.add(new PaintCalculatorType(R.drawable.excoat, 65));
        paintCalculatorTypeList.add(new PaintCalculatorType(R.drawable.eco_safe, 90));
        paintCalculatorTypeList.add(new PaintCalculatorType(R.drawable.nepolin_exterior, 55));
        paintCalculatorTypeList.add(new PaintCalculatorType(R.drawable.luxuria_interior, 80));
        paintCalculatorTypeList.add(new PaintCalculatorType(R.drawable.oneway_interior, 75));
        paintCalculatorTypeList.add(new PaintCalculatorType(R.drawable.nepolin_interior, 60));
        paintCalculatorTypeList.add(new PaintCalculatorType(R.drawable.nepolin_iacrylic_distemper, 45));
        paintCalculatorTypeList.add(new PaintCalculatorType(R.drawable.super_synthetic_enamel, 65));
        paintCalculatorTypeList.add(new PaintCalculatorType(R.drawable.danfe_gp_enamel, 60));
    }

    private void addWallFields() {
        LinearLayout linearLayoutFieldContainer = new LinearLayout(this);
        linearLayoutFieldContainer.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT));
        linearLayoutFieldContainer.setOrientation(LinearLayout.HORIZONTAL);
        linearLayoutFieldContainer.setWeightSum(100);
        ((LinearLayout.LayoutParams) linearLayoutFieldContainer.getLayoutParams()).bottomMargin = 5;

        CustomBoldTextView textViewCounter = new CustomBoldTextView(this);
        textViewCounter.setText(String.valueOf(++wallFieldCounter));
        textViewCounter.setGravity(Gravity.CENTER);
        textViewCounter.setLayoutParams(new LinearLayout.LayoutParams(0,
                LinearLayout.LayoutParams.WRAP_CONTENT, 16f));
        linearLayoutFieldContainer.addView(textViewCounter);

        CustomBoldTextView textViewHeight = new CustomBoldTextView(this);
        textViewHeight.setLayoutParams(new LinearLayout.LayoutParams(0, LinearLayout.LayoutParams.WRAP_CONTENT, 22f));
        textViewHeight.setText("Height");
        textViewHeight.setGravity(Gravity.CENTER);
        textViewHeight.setTextColor(getResources().getColor(R.color.secondary_text));
        linearLayoutFieldContainer.addView(textViewHeight);

        EditText editTextHeight = new EditText(this);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
            editTextHeight.setBackground(getResources().getDrawable(R.drawable.edit_text_height_width));
        } else {
            editTextHeight.setBackgroundDrawable(getResources().getDrawable(R.drawable.edit_text_height_width));
        }
        editTextHeight.setImeOptions(EditorInfo.IME_FLAG_NO_EXTRACT_UI);
        editTextHeight.setInputType(InputType.TYPE_CLASS_NUMBER | InputType.TYPE_NUMBER_FLAG_DECIMAL);
        editTextHeight.setLayoutParams(new LinearLayout.LayoutParams(0,
                LinearLayout.LayoutParams.WRAP_CONTENT, 20f));
        editTextHeight.setTextSize(TypedValue.COMPLEX_UNIT_SP, getResources().getInteger(R.integer.paint_calculator_paint_field_text_size));
        editTextHeight.setImeActionLabel("Calculate", getResources().getInteger(R.integer.calculate));
        editTextHeight.setOnEditorActionListener(this);
        linearLayoutFieldContainer.addView(editTextHeight);

        CustomBoldTextView textViewWidth = new CustomBoldTextView(this);
        textViewWidth.setLayoutParams(new LinearLayout.LayoutParams(0, LinearLayout.LayoutParams.WRAP_CONTENT, 22f));
        textViewWidth.setText("Width");
        textViewWidth.setGravity(Gravity.CENTER);
        textViewWidth.setTextColor(getResources().getColor(R.color.secondary_text));
        ((LinearLayout.LayoutParams) textViewWidth.getLayoutParams()).leftMargin = 5;
        linearLayoutFieldContainer.addView(textViewWidth);

        EditText editTextWidth = new EditText(this);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
            editTextWidth.setBackground(getResources().getDrawable(R.drawable.edit_text_height_width));
        } else {
            editTextWidth.setBackgroundDrawable(getResources().getDrawable(R.drawable.edit_text_height_width));
        }
        editTextWidth.setImeOptions(EditorInfo.IME_FLAG_NO_EXTRACT_UI);
        editTextWidth.setInputType(InputType.TYPE_CLASS_NUMBER | InputType.TYPE_NUMBER_FLAG_DECIMAL);
        editTextWidth.setLayoutParams(new LinearLayout.LayoutParams(0,
                LinearLayout.LayoutParams.WRAP_CONTENT, 20f));
        editTextWidth.setTextSize(TypedValue.COMPLEX_UNIT_SP, getResources().getInteger(R.integer.paint_calculator_paint_field_text_size));
        editTextWidth.setImeActionLabel("Calculate", getResources().getInteger(R.integer.calculate));
        editTextWidth.setOnEditorActionListener(this);
        linearLayoutFieldContainer.addView(editTextWidth);

        linearLayoutWallFields.addView(linearLayoutFieldContainer);
        scrollView.post(new Runnable() {
            @Override
            public void run() {
                scrollView.scrollTo(0, findViewById(R.id.button_add_wall_field).getBottom());
            }
        });

        wallPaintCalculatorFieldList.add(new PaintCalculatorField(editTextHeight, editTextWidth));
    }

    private void addWdFields() {
        LinearLayout linearLayoutFieldContainer = new LinearLayout(this);
        linearLayoutFieldContainer.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT));
        linearLayoutFieldContainer.setOrientation(LinearLayout.HORIZONTAL);
        linearLayoutFieldContainer.setWeightSum(100);
        ((LinearLayout.LayoutParams) linearLayoutFieldContainer.getLayoutParams()).bottomMargin = 5;

        CustomBoldTextView textViewCounter = new CustomBoldTextView(this);
        textViewCounter.setText(String.valueOf(++wdFieldCounter));
        textViewCounter.setGravity(Gravity.CENTER);
        textViewCounter.setLayoutParams(new LinearLayout.LayoutParams(0,
                LinearLayout.LayoutParams.WRAP_CONTENT, 16f));
        linearLayoutFieldContainer.addView(textViewCounter);

        CustomBoldTextView textViewHeight = new CustomBoldTextView(this);
        textViewHeight.setLayoutParams(new LinearLayout.LayoutParams(0, LinearLayout.LayoutParams.WRAP_CONTENT, 22f));
        textViewHeight.setText("Height");
        textViewHeight.setGravity(Gravity.CENTER);
        textViewHeight.setTextColor(getResources().getColor(R.color.secondary_text));
        linearLayoutFieldContainer.addView(textViewHeight);

        EditText editTextHeight = new EditText(this);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
            editTextHeight.setBackground(getResources().getDrawable(R.drawable.edit_text_height_width));
        } else {
            editTextHeight.setBackgroundDrawable(getResources().getDrawable(R.drawable.edit_text_height_width));
        }
        editTextHeight.setImeOptions(EditorInfo.IME_FLAG_NO_EXTRACT_UI);
        editTextHeight.setInputType(InputType.TYPE_CLASS_NUMBER | InputType.TYPE_NUMBER_FLAG_DECIMAL);
        editTextHeight.setLayoutParams(new LinearLayout.LayoutParams(0,
                LinearLayout.LayoutParams.WRAP_CONTENT, 20f));
        editTextHeight.setTextSize(TypedValue.COMPLEX_UNIT_SP, getResources().getInteger(R.integer.paint_calculator_paint_field_text_size));
        editTextHeight.setImeActionLabel("Calculate", getResources().getInteger(R.integer.calculate));
        editTextHeight.setOnEditorActionListener(this);
        linearLayoutFieldContainer.addView(editTextHeight);

        CustomBoldTextView textViewWidth = new CustomBoldTextView(this);
        textViewWidth.setLayoutParams(new LinearLayout.LayoutParams(0, LinearLayout.LayoutParams.WRAP_CONTENT, 22f));
        textViewWidth.setText("Width");
        textViewWidth.setGravity(Gravity.CENTER);
        textViewWidth.setTextColor(getResources().getColor(R.color.secondary_text));
        ((LinearLayout.LayoutParams) textViewWidth.getLayoutParams()).leftMargin = 5;
        linearLayoutFieldContainer.addView(textViewWidth);

        EditText editTextWidth = new EditText(this);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
            editTextWidth.setBackground(getResources().getDrawable(R.drawable.edit_text_height_width));
        } else {
            editTextWidth.setBackgroundDrawable(getResources().getDrawable(R.drawable.edit_text_height_width));
        }
        editTextWidth.setImeOptions(EditorInfo.IME_FLAG_NO_EXTRACT_UI);
        editTextWidth.setInputType(InputType.TYPE_CLASS_NUMBER | InputType.TYPE_NUMBER_FLAG_DECIMAL);
        editTextWidth.setLayoutParams(new LinearLayout.LayoutParams(0,
                LinearLayout.LayoutParams.WRAP_CONTENT, 20f));
        editTextWidth.setTextSize(TypedValue.COMPLEX_UNIT_SP, getResources().getInteger(R.integer.paint_calculator_paint_field_text_size));
        editTextWidth.setImeActionLabel("Calculate", getResources().getInteger(R.integer.calculate));
        editTextWidth.setOnEditorActionListener(this);
        linearLayoutFieldContainer.addView(editTextWidth);

        linearLayoutWdFields.addView(linearLayoutFieldContainer);

        scrollView.post(new Runnable() {
            @Override
            public void run() {
                scrollView.fullScroll(View.FOCUS_DOWN);
            }
        });
        wdPaintCalculatorFieldList.add(new PaintCalculatorField(editTextHeight, editTextWidth));
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.button_add_wall_field:
                if (!isDefaultWallFieldEmpty()) {
                    addWallFields();
                } else {
                    Toast.makeText(this, "Fill all the fields to add new field", Toast.LENGTH_SHORT)
                            .show();
                }
                break;
            case R.id.button_add_wd_field:
                if (!isDefaultWdFieldEmpty()) {
                    addWdFields();
                } else {
                    Toast.makeText(this, "Fill all the fields to add new field", Toast.LENGTH_SHORT)
                            .show();
                }
                break;
            case R.id.button_calculate:
                hideKeyboard();
                calculate();
                break;
            case R.id.button_up:
                if (paintCalculatorTypeCounter > 0) {
                    paintCalculatorTypeCounter--;
                    displayPaintCalculatorType(paintCalculatorTypeCounter);
                    mHandler.removeCallbacks(mRunnable);
                    mHandler.postDelayed(mRunnable, 5000);
                }
                break;
            case R.id.button_down:
                if (paintCalculatorTypeCounter < paintCalculatorTypeList.size() - 1) {
                    paintCalculatorTypeCounter++;
                    displayPaintCalculatorType(paintCalculatorTypeCounter);
                    mHandler.removeCallbacks(mRunnable);
                    mHandler.postDelayed(mRunnable, 5000);
                }
                break;
        }
    }

    private void hideKeyboard() {
        InputMethodManager imm = (InputMethodManager) getSystemService(
                Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(editTextWdWidth1.getWindowToken(), 0);
    }

    private void displayPaintCalculatorType(int paintCalculatorTypeCounter) {
//        imageViewColorType.setImageDrawable(getResources().getDrawable(paintCalculatorTypeList.get(paintCalculatorTypeCounter).getProductsDrawable()));
        pager.setCurrentItem(paintCalculatorTypeCounter);
        switch (paintCalculatorTypeCounter) {
            case 0:
                buttonUp.setVisibility(View.INVISIBLE);
                buttonDown.setVisibility(View.VISIBLE);
                break;
            case 1:
                buttonUp.setVisibility(View.VISIBLE);
                buttonDown.setVisibility(View.VISIBLE);
                break;
            case 2:
                buttonUp.setVisibility(View.VISIBLE);
                buttonDown.setVisibility(View.VISIBLE);
                break;
            case 3:
                buttonUp.setVisibility(View.VISIBLE);
                buttonDown.setVisibility(View.VISIBLE);
                break;
            case 4:
                buttonUp.setVisibility(View.VISIBLE);
                buttonDown.setVisibility(View.VISIBLE);
                break;
            case 5:
                buttonUp.setVisibility(View.VISIBLE);
                buttonDown.setVisibility(View.VISIBLE);
                break;
            case 6:
                buttonUp.setVisibility(View.VISIBLE);
                buttonDown.setVisibility(View.VISIBLE);
                break;
            case 7:
                buttonUp.setVisibility(View.VISIBLE);
                buttonDown.setVisibility(View.VISIBLE);
                break;
            case 8:
                buttonUp.setVisibility(View.VISIBLE);
                buttonDown.setVisibility(View.VISIBLE);
                break;
            case 9:
                buttonUp.setVisibility(View.VISIBLE);
                buttonDown.setVisibility(View.GONE);
                break;
        }
    }

    private boolean isDefaultWdFieldEmpty() {
        if ("720dp".equals(getString(R.string.whichDevice)) || "1024dp".equals(getString(R.string.whichDevice)) || "xhdpi".equals(getString(R.string.whichDevice)) || "600dp".equals(getString(R.string.whichDevice))) {
            return editTextWdHeight1.getText().toString().isEmpty() || editTextWdWidth1.getText().toString().isEmpty()
                    || editTextWdHeight2.getText().toString().isEmpty() || editTextWdWidth2.getText().toString().isEmpty();
        } else {
            return editTextWdHeight1.getText().toString().isEmpty() || editTextWdWidth1.getText().toString().isEmpty();
        }
    }

    private boolean isDefaultWallFieldEmpty() {
        if ("xhdpi".equals(getString(R.string.whichDevice)) || "600dp".equals(getString(R.string.whichDevice)) || "720dp".equals(getString(R.string.whichDevice)) || "1024dp".equals(getString(R.string.whichDevice))) {
            return editTextWallHeight1.getText().toString().isEmpty() || editTextWallWidth1.getText().toString().isEmpty()
                    || editTextWallHeight2.getText().toString().isEmpty() || editTextWallWidth2.getText().toString().isEmpty();
        } else {
            return editTextWallHeight1.getText().toString().isEmpty() || editTextWallWidth1.getText().toString().isEmpty();
        }
    }

    private void calculate() {
        float wallHeight1, wallWidth1;
        try {
            wallHeight1 = Float.parseFloat(editTextWallHeight1.getText().toString());
            wallWidth1 = Float.parseFloat(editTextWallWidth1.getText().toString());
        } catch (Exception e) {
            AppLog.showLog(CLASS_TAG, "Exception is: " + e.getLocalizedMessage());
            wallHeight1 = 0;
            wallWidth1 = 0;
        }

        float wallArea1 = wallHeight1 * wallWidth1;

        float wallHeight2, wallWidth2;
        try {
            wallHeight2 = Float.parseFloat(editTextWallHeight2.getText().toString());
            wallWidth2 = Float.parseFloat(editTextWallWidth2.getText().toString());
        } catch (Exception e) {
            wallHeight2 = 0;
            wallWidth2 = 0;
        }

        float wallArea2 = wallHeight2 * wallWidth2;

        float wallHeight3, wallWidth3;
        try {
            wallHeight3 = Float.parseFloat(editTextWallHeight3.getText().toString());
            wallWidth3 = Float.parseFloat(editTextWallWidth3.getText().toString());
        } catch (Exception e) {
            wallHeight3 = 0;
            wallWidth3 = 0;
        }

        float wallArea3 = wallHeight3 * wallWidth3;

        float wallHeight4, wallWidth4;
        try {
            wallHeight4 = Float.parseFloat(editTextWallHeight4.getText().toString());
            wallWidth4 = Float.parseFloat(editTextWallWidth4.getText().toString());
        } catch (Exception e) {
            wallHeight4 = 0;
            wallWidth4 = 0;
        }

        float wallArea4 = wallHeight4 * wallWidth4;

        float extraWallArea = 0;
        if (wallPaintCalculatorFieldList.size() > 0) {
            for (PaintCalculatorField paintCalculatorField : wallPaintCalculatorFieldList) {
                EditText editTextHeight = paintCalculatorField.getEditTextHeight();
                EditText editTextWidth = paintCalculatorField.getEditTextWidth();
                float height, width;
                try {
                    height = Float.parseFloat(editTextHeight.getText().toString());
                    width = Float.parseFloat(editTextWidth.getText().toString());
                } catch (Exception e) {
                    height = 0;
                    width = 0;
                }
                extraWallArea = extraWallArea + (height * width);
            }
        }

        float totalWallArea = wallArea1 + wallArea2 + wallArea3 + wallArea4 + extraWallArea;


        float wdHeight1, wdWidth1;
        try {
            wdHeight1 = Float.parseFloat(editTextWdHeight1.getText().toString());
            wdWidth1 = Float.parseFloat(editTextWdWidth1.getText().toString());
        } catch (Exception e) {
            wdHeight1 = 0;
            wdWidth1 = 0;
        }

        float wdArea1 = wdHeight1 * wdWidth1;

        float wdHeight2, wdWidth2;
        try {
            wdHeight2 = Float.parseFloat(editTextWdHeight2.getText().toString());
            wdWidth2 = Float.parseFloat(editTextWdWidth2.getText().toString());
        } catch (Exception e) {
            wdHeight2 = 0;
            wdWidth2 = 0;
        }

        float wdArea2 = wdHeight2 * wdWidth2;

        float wdHeight3, wdWidth3;
        try {
            wdHeight3 = Float.parseFloat(editTextWdHeight3.getText().toString());
            wdWidth3 = Float.parseFloat(editTextWdWidth3.getText().toString());
        } catch (Exception e) {
            wdHeight3 = 0;
            wdWidth3 = 0;
        }

        float wdArea3 = wdHeight3 * wdWidth3;

        float wdHeight4, wdWidth4;
        try {
            wdHeight4 = Float.parseFloat(editTextWdHeight4.getText().toString());
            wdWidth4 = Float.parseFloat(editTextWdWidth4.getText().toString());
        } catch (Exception e) {
            wdHeight4 = 0;
            wdWidth4 = 0;
        }

        float wdArea4 = wdHeight4 * wdWidth4;

        float extraWdArea = 0;
        if (wdPaintCalculatorFieldList.size() > 0) {
            for (PaintCalculatorField paintCalculatorField : wdPaintCalculatorFieldList) {
                EditText editTextHeight = paintCalculatorField.getEditTextHeight();
                EditText editTextWidth = paintCalculatorField.getEditTextWidth();
                float height, width;
                try {
                    height = Float.parseFloat(editTextHeight.getText().toString());
                    width = Float.parseFloat(editTextWidth.getText().toString());
                } catch (Exception e) {
                    height = 0;
                    width = 0;
                }
                extraWdArea = extraWdArea + (height * width);
            }
        }

        float totalWdArea = wdArea1 + wdArea2 + wdArea3 + wdArea4 + extraWdArea;

        float applicableArea = totalWallArea - totalWdArea;
        AppLog.showLog(CLASS_TAG, "Applicable Area: " + applicableArea);
        AppLog.showLog(CLASS_TAG, "Counter: " + paintCalculatorTypeCounter);
        AppLog.showLog(CLASS_TAG, "Coverage area: " + paintCalculatorTypeList.get(paintCalculatorTypeCounter).getCoveragePerSqFt());
        String quantity = String.valueOf(roundMyData(applicableArea / paintCalculatorTypeList.get(paintCalculatorTypeCounter).getCoveragePerSqFt()));
        setResultInTextView(quantity);
    }

    public static double roundMyData(double value) {
        double p = (float) Math.pow(10, 1);
        value = value * p;
        double tmp = Math.floor(value);
        return tmp / p;
    }

    private void setResultInTextView(String quantity) {
        textViewNotification.setVisibility(View.GONE);
        textViewResult.setVisibility(View.VISIBLE);
        textViewliter.setVisibility(View.VISIBLE);
        textViewliter.setText(Html.fromHtml("l<sup><small>*</small></sup>"));
        textViewliter.setTextSize(TypedValue.COMPLEX_UNIT_SP, getResources().getInteger(R.integer.paint_calculator_result_text_size));
        textViewResult.setText(quantity);
        textViewResult.setTextSize(TypedValue.COMPLEX_UNIT_SP, getResources().getInteger(R.integer.paint_calculator_result_text_size));
    }

    private boolean allFieldEmpty() {
        return editTextWallHeight1.getText().toString().isEmpty()
                && editTextWallHeight2.getText().toString().isEmpty()
                && editTextWallHeight3.getText().toString().isEmpty()
                && editTextWallHeight4.getText().toString().isEmpty()
                && editTextWallWidth1.getText().toString().isEmpty()
                && editTextWallWidth2.getText().toString().isEmpty()
                && editTextWallWidth3.getText().toString().isEmpty()
                && editTextWallWidth4.getText().toString().isEmpty()
                && editTextWdHeight1.getText().toString().isEmpty()
                && editTextWdHeight2.getText().toString().isEmpty()
                && editTextWdHeight3.getText().toString().isEmpty()
                && editTextWdHeight4.getText().toString().isEmpty()
                && editTextWdWidth1.getText().toString().isEmpty()
                && editTextWdWidth2.getText().toString().isEmpty()
                && editTextWdWidth3.getText().toString().isEmpty()
                && editTextWdWidth4.getText().toString().isEmpty();
    }

    @Override
    public boolean onTouch(View v, MotionEvent event) {
        if (paintCalculatorTypeCounter == 0) {
            buttonUp.setVisibility(View.INVISIBLE);
            buttonDown.setVisibility(View.VISIBLE);
        } else if (paintCalculatorTypeCounter >= paintCalculatorTypeList.size() - 1) {
            buttonUp.setVisibility(View.VISIBLE);
            buttonDown.setVisibility(View.GONE);
        } else {
            buttonUp.setVisibility(View.VISIBLE);
            buttonDown.setVisibility(View.VISIBLE);
        }
        mHandler.removeCallbacks(mRunnable);
        mHandler.postDelayed(mRunnable, 5000);
        return false;
    }

    @Override
    public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
        if (actionId == getResources().getInteger(R.integer.calculate)) {
            hideKeyboard();
            calculate();
            return true;
        }
        return false;
    }
}
