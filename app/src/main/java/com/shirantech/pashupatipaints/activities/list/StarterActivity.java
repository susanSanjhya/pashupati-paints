package com.shirantech.pashupatipaints.activities.list;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;

import com.actionbarsherlock.app.SherlockActivity;
import com.shirantech.pashupatipaints.R;
import com.shirantech.pashupatipaints.util.AppLog;

public class StarterActivity extends SherlockActivity implements OnClickListener {

    //GET THE NAME AND EMAIL IF HAVE GOOGLE LOGGED IN ACCOUNT else set NULL
    public static String name;
    private static final String CLASS_TAG = StarterActivity.class.getSimpleName();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_starter);
        AppLog.showLog(CLASS_TAG, "StarterActivity started.");
        getSupportActionBar().setTitle("");
        Button buttonTemplatePaint = (Button) findViewById(R.id.btn_template_paint);
        buttonTemplatePaint.setOnClickListener(this);

        Button buttonGallery = (Button) findViewById(R.id.btn_gallery);
        buttonGallery.setOnClickListener(this);

        Button buttonDealersDetail = (Button) findViewById(R.id.button_dealers_detail);
        buttonDealersDetail.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_template_paint:
                Intent paintingWaysIntent = new Intent(StarterActivity.this, PainingWaysActivity.class);

                startActivity(paintingWaysIntent);
                break;

            case R.id.btn_gallery:
                Intent galleryIntent = new Intent(getApplicationContext(), GalleryActivity.class);

                startActivity(galleryIntent);
                break;

            case R.id.button_dealers_detail:
                Intent dealersDetailIntent = new Intent(getApplicationContext(), DealersDetailActivity.class);

                startActivity(dealersDetailIntent);
                break;
            default:
                break;
        }

    }
}
