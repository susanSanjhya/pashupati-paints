package com.shirantech.pashupatipaints.activities.list;

import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.widget.ListView;
import android.widget.TextView;

import com.actionbarsherlock.app.SherlockActivity;
import com.shirantech.pashupatipaints.R;
import com.shirantech.pashupatipaints.adapter.UsedColorAdapter;
import com.shirantech.pashupatipaints.model.PaintColor;
import com.shirantech.pashupatipaints.util.AppLog;
import com.shirantech.pashupatipaints.util.Utility;

import java.util.List;

/**
 * Created by susan on 4/2/15.
 */
@SuppressWarnings("deprecation")
public class PaintInfoActivity extends SherlockActivity {

    private static final String CLASS_TAG = PaintInfoActivity.class.getSimpleName();
    private ListView listViewUsedColors;
    private View viewColor;
    private TextView textViewColorName, textViewColorCode;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(android.view.Window.FEATURE_NO_TITLE);
        if (Build.VERSION.SDK_INT <= Build.VERSION_CODES.HONEYCOMB) {
            getSupportActionBar().hide();
        }
        setContentView(R.layout.activity_paint_info);
        Bundle bundle = getIntent().getExtras();
        List<PaintColor> paintColorList;
        paintColorList = bundle.getParcelableArrayList("USED_COLOR_LIST");

        PaintColor selectedColor = bundle.getParcelable("SELECTED_COLOR");

        listViewUsedColors = (ListView) findViewById(R.id.list_view_used_colors);
        viewColor = findViewById(R.id.view_selected_color);
        textViewColorName = (TextView) findViewById(R.id.text_view_selected_color_name);
        textViewColorCode = (TextView) findViewById(R.id.text_view_selected_color_code);

        for (PaintColor paintColor : paintColorList) {
            AppLog.showLog(CLASS_TAG, "paint color name : " + paintColor.getName());
        }
        listViewUsedColors.setAdapter(new UsedColorAdapter(this, paintColorList));

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
            viewColor
                    .setBackground(Utility.getCustomShape(Color.parseColor(selectedColor.getColorCode())));
        } else {
            viewColor
                    .setBackgroundDrawable(Utility.getCustomShape(Color.parseColor(selectedColor.getColorCode())));
        }
        textViewColorName
                .setText(selectedColor.getName());
        textViewColorCode
                .setText(selectedColor.getColorCode());
       /* findViewById(R.id.home).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });*/

    }
}
