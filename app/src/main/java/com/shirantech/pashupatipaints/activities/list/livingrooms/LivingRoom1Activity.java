package com.shirantech.pashupatipaints.activities.list.livingrooms;

import android.content.Intent;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.view.DragEvent;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnDragListener;
import android.view.View.OnTouchListener;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.actionbarsherlock.app.ActionBar;
import com.actionbarsherlock.app.SherlockFragmentActivity;
import com.actionbarsherlock.view.Menu;
import com.actionbarsherlock.view.MenuItem;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.shirantech.pashupatipaints.R;
import com.shirantech.pashupatipaints.activities.list.ColorChooserActivity;
import com.shirantech.pashupatipaints.fragments.SelectedColorsFragment;
import com.shirantech.pashupatipaints.model.PaintColor;
import com.shirantech.pashupatipaints.util.AppLog;
import com.shirantech.pashupatipaints.util.AppText;
import com.shirantech.pashupatipaints.util.Global;
import com.shirantech.pashupatipaints.util.ImageUtils;

import java.util.List;

public class LivingRoom1Activity extends SherlockFragmentActivity implements SelectedColorsFragment.onSelectedColorSelectedListener {

    private static final String CLASS_TAG = LivingRoom1Activity.class.getSimpleName();
    private ImageView iv_mainimage, iv_border_ceiling, iv_ceiling, iv_rightwall;
    private float redScale = 1f;
    private float greenScale = 0f;
    private float blueScale = 0f;
    private Boolean isColorClicked = false;

    private int borderceiling_r = -1, borderceiling_g = -1, borderceiling_b = -1;
    private int ceiling_r = -1, ceiling_g = -1, ceiling_b = -1;
    private int rightwall_r = -1, rightwall_g = -1, rightwall_b = -1;

    RelativeLayout parent;

    Global global;

    private ImageLoader imageLoader = ImageLoader.getInstance();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_living_room1);

        imageLoader.init(ImageLoaderConfiguration.createDefault(this));

        final ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setTitle("");

        parent = (RelativeLayout) findViewById(R.id.roomLayout_living_1);

        global = (Global) getApplication();
        List<PaintColor> globalPaintColorList = global.getGlobalPaintColorList();

        AppLog.showLog(CLASS_TAG, "size of the global paintcolor list at Drawingroom2 :: " + globalPaintColorList.size());

        iv_mainimage = (ImageView) findViewById(R.id.iv_living1_main_image);
        iv_border_ceiling = (ImageView) findViewById(R.id.iv_living1_border_ceiling);
        iv_ceiling = (ImageView) findViewById(R.id.iv_living1_ceiling);
        iv_rightwall = (ImageView) findViewById(R.id.iv_living1_right_wall);

        /*iv_mainimage.setImageBitmap(ImageUtils.getBitmapFromAsset(getApplicationContext(), AppText.LIVINGROOM1_LOCATION_MAIN, 1));
        iv_border_ceiling.setImageBitmap(ImageUtils.getBitmapFromAsset(getApplicationContext(), AppText.LIVINGROOM1_LOCATION_BORDER_CEILING, 1));
        iv_ceiling.setImageBitmap(ImageUtils.getBitmapFromAsset(getApplicationContext(), AppText.LIVINGROOM1_LOCATION_CEILING, 1));
        iv_rightwall.setImageBitmap(ImageUtils.getBitmapFromAsset(getApplicationContext(), AppText.LIVINGROOM1_LOCATION_RIGHT, 1));*/

        ImageUtils.loadImageUsingLoader(imageLoader, iv_mainimage, "assets://" + AppText.LIVINGROOM1_LOCATION_MAIN);
        ImageUtils.loadImageUsingLoader(imageLoader, iv_border_ceiling, "assets://" + AppText.LIVINGROOM1_LOCATION_BORDER_CEILING);
        ImageUtils.loadImageUsingLoader(imageLoader, iv_ceiling, "assets://" + AppText.LIVINGROOM1_LOCATION_CEILING);
        ImageUtils.loadImageUsingLoader(imageLoader, iv_rightwall, "assets://" + AppText.LIVINGROOM1_LOCATION_RIGHT);

        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.HONEYCOMB) {
            //drag and drop!!
            iv_rightwall.setOnDragListener(new ChoiceDragListener());
        }
        iv_rightwall.setOnTouchListener(new OnTouchListener() {

            @Override
            public boolean onTouch(View v, MotionEvent event) {

                if (isColorClicked) {
                    int[] colors = ImageUtils.startPainting(LivingRoom1Activity.this,
                            ImageUtils.LIVINGROOM_TEMPLATE_1, (int) event.getX(), (int) event.getY(), iv_mainimage,
                            parent, new float[]{redScale, greenScale, blueScale},
                            iv_border_ceiling, iv_ceiling, iv_rightwall);
                    if (colors[3] == R.id.iv_living1_border_ceiling) {
                        borderceiling_r = colors[0];
                        borderceiling_g = colors[1];
                        borderceiling_b = colors[2];
                    } else if (colors[3] == R.id.iv_living1_ceiling) {
                        ceiling_r = colors[0];
                        ceiling_g = colors[1];
                        ceiling_b = colors[2];
                    } else if (colors[3] == R.id.iv_living1_right_wall) {
                        rightwall_r = colors[0];
                        rightwall_g = colors[1];
                        rightwall_b = colors[2];
                    }
                } else {
                    Toast.makeText(getApplicationContext(), "Please select the color below to paint the rooms", Toast.LENGTH_SHORT).show();
                }


                return false;
            }
        });

        //Determine density
        DisplayMetrics metrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(metrics);
        int density = metrics.densityDpi;

        if (density == DisplayMetrics.DENSITY_HIGH) {
            AppLog.showLog(CLASS_TAG, "DENSITY_HIGH... Density is " + String.valueOf(density));
        } else if (density == DisplayMetrics.DENSITY_MEDIUM) {
            AppLog.showLog(CLASS_TAG, "DENSITY_MEDIUM... Density is " + String.valueOf(density));
        } else if (density == DisplayMetrics.DENSITY_LOW) {
            AppLog.showLog(CLASS_TAG, "DENSITY_LOW... Density is " + String.valueOf(density));
        } else {
            AppLog.showLog(CLASS_TAG, "Density is neither HIGH, MEDIUM OR LOW.  Density is " + String.valueOf(density));

            parent.setScaleX(2.0f);
            parent.setScaleY(2.0f);

            parent.post(new Runnable() {

                @Override
                public void run() {
                    int h, w;
                    h = parent.getHeight();
                    w = parent.getWidth();

                    RelativeLayout.LayoutParams tableLP = new RelativeLayout.LayoutParams(w * 2, h * 2);
                    tableLP.addRule(RelativeLayout.CENTER_IN_PARENT, 1);
                    tableLP.addRule(RelativeLayout.ALIGN_PARENT_TOP, 1);
                    parent.setLayoutParams(tableLP);

                }
            });
        }

    }


    @Override
    public void onColorSelected(PaintColor paintColor) {
        isColorClicked = true;

        Float[] rgb = ImageUtils.hexToRgb(paintColor.getColorCode());

        redScale = rgb[0];
        greenScale = rgb[1];
        blueScale = rgb[2];

        AppLog.showLog(CLASS_TAG, "  we get:::::: redScale:: " + redScale + " greenScale:: " + greenScale + " blueScale:: " + blueScale);


    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getSupportMenuInflater().inflate(R.menu.common_menu, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {

            case R.id.share:
                ImageUtils.shareTemplateImage(this, global, ImageUtils.LIVINGROOM_TEMPLATE_1,
                        parent, borderceiling_r, borderceiling_g, borderceiling_b,
                        ceiling_r, ceiling_g, ceiling_b,
                        rightwall_r, rightwall_g, rightwall_b);
                break;

            case R.id.menu_save:
                ImageUtils.saveTemplateImage(this, global, ImageUtils.LIVINGROOM_TEMPLATE_1,
                        parent, borderceiling_r, borderceiling_g, borderceiling_b,
                        ceiling_r, ceiling_g, ceiling_b,
                        rightwall_r, rightwall_g, rightwall_b);
                break;

            case android.R.id.home:
                Intent colorChooserIntenta = new Intent(getApplicationContext(), ColorChooserActivity.class);
                colorChooserIntenta.putExtra("COLOR CHOOSER SOURCE", "TEMPLATE");
                colorChooserIntenta.putExtra("POSITION", 0);
                colorChooserIntenta.putExtra("TYPE", "drawinghall");
                colorChooserIntenta.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(colorChooserIntenta);
                break;

            case R.id.add_more_colors:
                Intent colorChooserIntent = new Intent(getApplicationContext(), ColorChooserActivity.class);
                colorChooserIntent.putExtra("COLOR CHOOSER SOURCE", "TEMPLATE");
                colorChooserIntent.putExtra("POSITION", 0);
                colorChooserIntent.putExtra("TYPE", "drawinghall");
                colorChooserIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(colorChooserIntent);

                break;

            default:
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    /**
     * DragListener will handle dragged views being dropped on the drop area
     * - only the drop action will have processing added to it as we are not
     * - amending the default behavior for other parts of the drag process
     */
    private class ChoiceDragListener implements OnDragListener {

        @Override
        public boolean onDrag(View v, DragEvent event) {
            switch (event.getAction()) {
                case DragEvent.ACTION_DRAG_STARTED:
                    //no action necessary
                    break;
                case DragEvent.ACTION_DRAG_ENTERED:
                    //no action necessary
                    break;
                case DragEvent.ACTION_DRAG_EXITED:
                    //no action necessary
                    break;
                case DragEvent.ACTION_DROP:
                    int[] colors = ImageUtils.startPainting(LivingRoom1Activity.this,
                            ImageUtils.LIVINGROOM_TEMPLATE_1, (int) event.getX(), (int) event.getY(), iv_mainimage,
                            parent, new float[]{redScale, greenScale, blueScale},
                            iv_border_ceiling, iv_border_ceiling, iv_border_ceiling);
                    if (colors[3] == R.id.iv_living1_border_ceiling) {
                        borderceiling_r = colors[0];
                        borderceiling_g = colors[1];
                        borderceiling_b = colors[2];
                    } else if (colors[3] == R.id.iv_living1_ceiling) {
                        ceiling_r = colors[0];
                        ceiling_g = colors[1];
                        ceiling_b = colors[2];
                    } else if (colors[3] == R.id.iv_living1_right_wall) {
                        rightwall_r = colors[0];
                        rightwall_g = colors[1];
                        rightwall_b = colors[2];
                    }
                    //handle the dragged view being dropped over a drop view
                    View view = (View) event.getLocalState();

                    //stop displaying the view where it was before it was dragged
                    ////view.setVisibility(View.INVISIBLE);

                    //view dragged item is being dropped on
                    ImageView dropTarget = (ImageView) v;

                    //view being dragged and dropped
                    LinearLayout dropped = (LinearLayout) view;


                    //if an item has already been dropped here, there will be a tag
                    Object tag = dropTarget.getTag();


                    //set the tag in the target view being dropped on - to the ID of the view being dropped
                    dropTarget.setTag(dropped.getId());

                    break;
                case DragEvent.ACTION_DRAG_ENDED:
                    //no action necessary
                    break;
                default:
                    break;
            }
            return true;
        }
    }

}


