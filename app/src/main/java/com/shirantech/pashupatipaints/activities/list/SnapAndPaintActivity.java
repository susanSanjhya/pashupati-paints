package com.shirantech.pashupatipaints.activities.list;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffXfermode;
import android.graphics.drawable.BitmapDrawable;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewConfiguration;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.actionbarsherlock.app.ActionBar;
import com.actionbarsherlock.view.Menu;
import com.actionbarsherlock.view.MenuItem;
import com.shirantech.pashupatipaints.R;
import com.shirantech.pashupatipaints.customwidgets.ColorPickerDialog;
import com.shirantech.pashupatipaints.fragments.SelectedColorsFragment;
import com.shirantech.pashupatipaints.model.PaintColor;
import com.shirantech.pashupatipaints.util.AppLog;
import com.shirantech.pashupatipaints.util.Global;
import com.shirantech.pashupatipaints.util.ImageUtils;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;

public class SnapAndPaintActivity extends GraphicsActivity
        implements SelectedColorsFragment.onSelectedColorSelectedListener, ColorPickerDialog.OnColorChangedListener {

    private static final String CLASS_TAG = SnapAndPaintActivity.class.getSimpleName();
    private RelativeLayout rlRoom;

    Global global;
    private List<PaintColor> globalPaintColorList;
    private int usedColorId;

    private Boolean isColorSelected = false;

    int screenWidth = 0;
    int x_adjustment = 0;

    Bitmap snappedBitmap;

    SnapAndPaintView mv;

    private List<PaintColor> usedColorList;
    private Paint mPaint;

    @SuppressLint("NewApi")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_snap_and_paint);


        final ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setTitle("Snap and Paint");

        try {
            ViewConfiguration config = ViewConfiguration.get(this);
            Field menuKeyField = ViewConfiguration.class.getDeclaredField("sHasPermanentMenuKey");
            if (menuKeyField != null) {
                menuKeyField.setAccessible(true);
                menuKeyField.setBoolean(config, false);
            }
        } catch (Exception ex) { // Ignore }
        }

        usedColorList = new ArrayList<PaintColor>();


        global = (Global) getApplication();
        globalPaintColorList = global.getGlobalPaintColorList();
        snappedBitmap = global.getSnapAndPaintBitmap();

        AppLog.showLog(CLASS_TAG, "Global bitmap is :" + snappedBitmap);
        rlRoom = (RelativeLayout) findViewById(R.id.rl_snap_and_paint);
        mv = new SnapAndPaintView(this, snappedBitmap);
        rlRoom.addView(mv);

        if (globalPaintColorList == null || globalPaintColorList.size() <= 0) {
            AppLog.showLog(CLASS_TAG, "global paint color list is empty");
            Toast.makeText(this, "Color list is empty. Select Color first.", Toast.LENGTH_SHORT).show();
            Intent colorChooserIntent = new Intent(this, ColorChooserActivity.class);
            colorChooserIntent.putExtra("COLOR CHOOSER SOURCE", "STARTER");
            colorChooserIntent.putExtra("TYPE", "snap");
            colorChooserIntent.putExtra("POSITION", 0);
            startActivity(colorChooserIntent);
            finish();
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        for (int i = 0; i < global.getGlobalPaintColorList().size(); i++) {
            global.getGlobalPaintColorList().remove(i);
        }
        global.setSnapAndPaintBitmap(null);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getSupportMenuInflater().inflate(R.menu.snap_and_paint, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {

            case R.id.share:

                ImageUtils.share(this, rlRoom, usedColorList);
                break;

            case android.R.id.home:
                Intent intent = new Intent(this, PainingWaysActivity.class);
                for (int i = 0; i < global.getGlobalPaintColorList().size(); i++) {
                    global.getGlobalPaintColorList().remove(i);
                }
                global.setSnapAndPaintBitmap(null);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
                break;
            case R.id.menu_save:
                ImageUtils.save(this, rlRoom, usedColorList);
                break;

            case R.id.add_more_colors:
                Intent colorChooserIntent = new Intent(getApplicationContext(), ColorChooserActivity.class);
                colorChooserIntent.putExtra("COLOR CHOOSER SOURCE", "TEMPLATE");
                colorChooserIntent.putExtra("TYPE", "snap");
                colorChooserIntent.putExtra("POSITION", 0);
                startActivity(colorChooserIntent);

                break;

            case R.id.menu_erase:
                try {
                    if (mPaint != null) {
                        mPaint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.CLEAR));
                    } else {
                        Toast.makeText(getApplicationContext(), "Nothing to erase!", Toast.LENGTH_SHORT).show();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }

                return true;

            default:
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onColorSelected(PaintColor paintColor) {

        isColorSelected = true;

        mPaint = new Paint();
        mPaint.setAntiAlias(true);
        mPaint.setDither(true);
        mPaint.setAlpha(0x80);
        mPaint.setColor(-1);
        mPaint.setStyle(Paint.Style.STROKE);
        mPaint.setStrokeJoin(Paint.Join.ROUND);
        mPaint.setStrokeCap(Paint.Cap.ROUND);
        mPaint.setStrokeWidth(12);

        //set the paint color to paint
        mPaint.setColor(Color.parseColor(paintColor.getColorCode()));
        mPaint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.SRC));
        mPaint.setAlpha(0x80);


        Float[] rgb = ImageUtils.hexToRgb(paintColor.getColorCode());

        float redScale = rgb[0];
        float greenScale = rgb[1];
        float blueScale = rgb[2];

        AppLog.showLog(CLASS_TAG,
                "we get:::::: redScale:: " + redScale + " greenScale:: " + greenScale + " blueScale:: " + blueScale);

    }


    public void colorChanged(int color) {
        mPaint.setColor(color);
    }


    @SuppressWarnings("deprecation")
    public class SnapAndPaintView extends View {

        private Bitmap mBitmap, bmp, bm;
        private Canvas mCanvas;
        private Path mPath;
        private Paint mBitmapPaint;

        public SnapAndPaintView(Context c, Bitmap mbmp) {
            super(c);
            mPath = new Path();
            mBitmapPaint = new Paint(Paint.DITHER_FLAG);
            bmp = mbmp;

            x_adjustment = (screenWidth - bmp.getWidth()) / 2;
        }

        @Override
        protected void onSizeChanged(int w, int h, int oldw, int oldh) {
            super.onSizeChanged(w, h, oldw, oldh);

            mBitmap = Bitmap.createBitmap(bmp.copy(Bitmap.Config.ARGB_8888, true));
            mv.setBackgroundDrawable(new BitmapDrawable(mBitmap));//set background here

            bm = Bitmap.createBitmap(w, h, Bitmap.Config.ARGB_8888);

            AppLog.showLog(CLASS_TAG, "original bitmap width:: " + mBitmap.getWidth() + " height:: " + mBitmap.getHeight());
            mCanvas = new Canvas(bm);
        }

        @Override
        protected void onDraw(Canvas canvas) {
            super.onDraw(canvas);
            canvas.drawBitmap(bm, 0, 0, mBitmapPaint);

            if (isColorSelected) {
                usedColorId = mPaint.getColor();
            }

        }

        private float mX, mY;
        private static final float TOUCH_TOLERANCE = 4;

        private void touch_start(float x, float y) {
            mPath.reset();
            mPath.moveTo(x, y);
            mX = x;
            mY = y;

            try {
                mCanvas.drawPath(mPath, mPaint);
            } catch (Exception e) {
                AppLog.showLog(CLASS_TAG, "touch start error: " + e.getLocalizedMessage());
            }
        }

        private void touch_move(float x, float y) {
            float dx = Math.abs(x - mX);
            float dy = Math.abs(y - mY);
            if (dx >= TOUCH_TOLERANCE || dy >= TOUCH_TOLERANCE) {
                mPath.quadTo(mX, mY, (x + mX) / 2, (y + mY) / 2);
                mX = x;
                mY = y;
            }
            try {
                mCanvas.drawPath(mPath, mPaint);
            } catch (Exception e) {
                AppLog.showLog(CLASS_TAG, "touch move error: " + e.getLocalizedMessage());
            }

        }

        private void touch_up() {

            //GET THE COLOT AND SAVE TO DATABASE!!!
            Boolean isNewSelectedColor = true;

            //CONVERT COLOR INT TO HASH
            String colorHash = String.format("#%06X", 0xFFFFFF & usedColorId);
            AppLog.showLog(CLASS_TAG, "color Hash value:: " + colorHash);

            for (PaintColor p : usedColorList)
                try {
                    if (p.getColorCode().equalsIgnoreCase(colorHash)) {
                        isNewSelectedColor = false;
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                }

            if (isNewSelectedColor) {
                PaintColor pc = new PaintColor();
                pc.setColorCode(colorHash);

                String colorName = null;

                //set corresponding color name for colorHash!!
                for (PaintColor aGlobalPaintColorList : globalPaintColorList) {
                    if (aGlobalPaintColorList.getColorCode().equalsIgnoreCase(colorHash)) {
                        colorName = aGlobalPaintColorList.getName();
                    }
                }
                pc.setName(colorName);

                usedColorList.add(pc);
            }

            try {
                mPath.lineTo(mX, mY);
                // commit the path to our offscreen
                mCanvas.drawPath(mPath, mPaint);
                // kill this so we don't double draw
                mPath.reset();

                mPaint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.SRC));
                mPaint.setAlpha(0x80);
                mPaint.setMaskFilter(null);

            } catch (Exception e) {
                AppLog.showLog(CLASS_TAG, "touch up error: " + e.getLocalizedMessage());
            }

        }

        @Override
        public boolean onTouchEvent(@NonNull MotionEvent event) {
            float x = event.getX();
            float y = event.getY();

            switch (event.getAction()) {
                case MotionEvent.ACTION_DOWN:
                    touch_start(x, y);
                    invalidate();
                    break;
                case MotionEvent.ACTION_MOVE:
                    touch_move(x, y);
                    invalidate();
                    break;
                case MotionEvent.ACTION_UP:
                    touch_up();
                    invalidate();
                    break;
            }
            return true;
        }
    }

}
