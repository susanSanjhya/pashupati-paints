package com.shirantech.pashupatipaints.activities.list;

import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.view.Window;

import com.actionbarsherlock.app.SherlockActivity;
import com.shirantech.pashupatipaints.R;
import com.shirantech.pashupatipaints.util.ImageUtils;
import com.shirantech.pashupatipaints.customwidgets.TouchImageView;

public class FullImageActivity extends SherlockActivity {

    private Bitmap bitmap;
    private TouchImageView imageview;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_full_image);

        Intent i = getIntent();

        String imageName = i.getExtras().getString("imageName");
        System.out.println("imageName:: " + imageName);

        imageview = (TouchImageView) findViewById(R.id.full_image_view);
        try {

            bitmap = ImageUtils.getBitmapFromSdFromName(getApplicationContext(), imageName);

            imageview.setImageBitmap(bitmap);
            imageview.setMaxZoom(4f);
        } catch (IndexOutOfBoundsException e) {
            e.printStackTrace();
        }

    }


}
