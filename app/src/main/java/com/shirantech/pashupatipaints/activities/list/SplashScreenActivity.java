package com.shirantech.pashupatipaints.activities.list;

import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.view.MotionEvent;

import com.actionbarsherlock.app.SherlockActivity;
import com.shirantech.pashupatipaints.R;
import com.shirantech.pashupatipaints.util.AppLog;
import com.shirantech.pashupatipaints.util.AppText;
import com.shirantech.pashupatipaints.util.Utility;

/**
 * Splash Screen Activity.
 */
public class SplashScreenActivity extends SherlockActivity {

    private static final String CLASS_TAG = SplashScreenActivity.class.getSimpleName();
    private Thread splashThread;
    boolean isThreadRunning = false;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        AppLog.showLog(CLASS_TAG, "This devices uses values folder? " + getResources().getString(R.string.whichDevice));
        AppLog.showLog(CLASS_TAG, "This devices dimension? " + getResources().getString(R.string.dimension));
        AppLog.showLog(CLASS_TAG, "This is tablet? " + Utility.isTablet(this));
        if (Utility.isTablet(this)) {
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
        } else {
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        }
        setContentView(R.layout.splash_screen);

        if (savedInstanceState != null) {
            isThreadRunning = savedInstanceState.getBoolean("isThreadRunning");
            AppLog.showLog(CLASS_TAG, "saved. isThreadRunning: " + isThreadRunning);
        }
        if (!isThreadRunning) {
            AppLog.showLog(CLASS_TAG, "Thread defining:" + false);
            splashThread = new Thread() {
                @Override
                public void run() {
                    isThreadRunning = true;
                    AppLog.showLog(CLASS_TAG, "Thread run:" + splashThread.isAlive());
                    try {
                        synchronized (this) {
                            // Wait given period of time or exit on touch
                            wait(3000);
                        }
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    } finally {
                        finish();
                        SharedPreferences runCheck = getSharedPreferences(AppText.PREF_KEY_HAS_RUN_BEFORE, 0); //load the prefereces
                        Boolean hasRun = runCheck.getBoolean(AppText.KEY_HAS_RUN, false); //see if its run before. The default is NO.

                        if (!hasRun) {
                            AppLog.showLog(CLASS_TAG, "RUN for the first time");

                            //the starter activity to show USUALLY!!
                            Intent firstLaunchIntent;
//                            if (Utility.isTablet(SplashScreenActivity.this)) {
//                                firstLaunchIntent = new Intent(SplashScreenActivity.this, FirstLaunchTabletActivity.class);
//                            } else {
                            firstLaunchIntent = new Intent(SplashScreenActivity.this, FirstLaunchActivity.class);
//                            }
                            firstLaunchIntent.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
                            firstLaunchIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                            startActivity(firstLaunchIntent);
                        } else {
                            AppLog.showLog(CLASS_TAG, "Launch Starter Activity");
                            //the starter activity to show USUALLY!!
                            Intent homeListIntent;
                            if (Utility.isTablet(SplashScreenActivity.this)) {
                                homeListIntent = new Intent(SplashScreenActivity.this, StarterTabletActivity.class);
                            } else {
                                homeListIntent = new Intent(SplashScreenActivity.this, StarterV2Activity.class);
                            }
                            homeListIntent.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
                            homeListIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                            homeListIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                            startActivity(homeListIntent);
                        }

                    }
                }
            };
            splashThread.start();
        }

    }

    /**
     * Processes splash screen touch events
     */
    @Override
    public boolean onTouchEvent(MotionEvent evt) {
        if (evt.getAction() == MotionEvent.ACTION_DOWN) {
            synchronized (splashThread) {
                splashThread.notifyAll();
            }
        }
        return true;
    }

}
