package com.shirantech.pashupatipaints.activities.list;

import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.ImageView;

import com.actionbarsherlock.app.SherlockActivity;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.shirantech.pashupatipaints.R;
import com.shirantech.pashupatipaints.util.AppLog;
import com.shirantech.pashupatipaints.util.AppText;
import com.shirantech.pashupatipaints.util.ImageUtils;
import com.shirantech.pashupatipaints.util.Utility;

/**
 * Created by susan on 12/22/14.
 */
public class InteriorListActivity extends SherlockActivity implements View.OnClickListener {

    private static final String CLASS_TAG = InteriorListActivity.class.getSimpleName();
    private ImageView imageViewDrawingRoom, imageViewBedroom, imageViewKitchen;
    private ImageLoader imageLoader = ImageLoader.getInstance();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        if (Build.VERSION.SDK_INT <= Build.VERSION_CODES.HONEYCOMB) {
            getSupportActionBar().hide();
        }
        if (Utility.isTablet(this)) {
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
        } else {
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        }

        setContentView(R.layout.activity_room_category);
        AppLog.showLog(CLASS_TAG, "InteriorListActivity started");
        imageLoader.init(ImageLoaderConfiguration.createDefault(this));
//        if (Utility.isTablet(this)) {
        imageViewDrawingRoom = (ImageView) findViewById(R.id.imageview_drawingrooms);
        imageViewBedroom = (ImageView) findViewById(R.id.imageview_bedrooms);
        imageViewKitchen = (ImageView) findViewById(R.id.imageview_kitchens);

        ImageUtils.loadImageUsingLoader(imageLoader, imageViewDrawingRoom, "assets://" + AppText.LIVINGROOM1_LOCATION_MAIN);
        ImageUtils.loadImageUsingLoader(imageLoader, imageViewBedroom, "assets://" + AppText.BEDROOM1_LOCATION_MAIN);
        ImageUtils.loadImageUsingLoader(imageLoader, imageViewKitchen, "assets://" + AppText.KITCHEN1_LOCATION_MAIN);
        imageViewDrawingRoom.setOnClickListener(this);
        imageViewBedroom.setOnClickListener(this);
        imageViewKitchen.setOnClickListener(this);
        findViewById(R.id.home).setOnClickListener(this);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        imageLoader.destroy();
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.home:
                Intent homeIntent = new Intent(this, PainingWaysActivity.class);
                homeIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);

                startActivity(homeIntent);
                break;
            case R.id.imageview_drawingrooms:
                Intent drawingListIntent = new Intent(this, DrawingRoomListActivity.class);

                startActivity(drawingListIntent);
                break;
            case R.id.imageview_kitchens:
                Intent kitchenListIntent = new Intent(this, KitchensListActivity.class);
                kitchenListIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);

                startActivity(kitchenListIntent);
                break;
            case R.id.imageview_bedrooms:
                Intent bedroomListIntent = new Intent(this, BedroomsListActivity.class);
                bedroomListIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);

                startActivity(bedroomListIntent);
                break;
        }
    }
}
