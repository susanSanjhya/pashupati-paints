package com.shirantech.pashupatipaints.activities.list;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffXfermode;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.ColorDrawable;
import android.os.Build;
import android.os.Bundle;
import android.os.Vibrator;
import android.support.annotation.NonNull;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewConfiguration;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.GridView;
import android.widget.ImageButton;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.actionbarsherlock.app.SherlockFragmentActivity;
import com.shirantech.pashupatipaints.R;
import com.shirantech.pashupatipaints.adapter.TabletColorHorizontalListAdapter;
import com.shirantech.pashupatipaints.fragments.PaintInfoFragment;
import com.shirantech.pashupatipaints.fragments.SelectedColorsFragment;
import com.shirantech.pashupatipaints.interfaces.ViewOnTouchListener;
import com.shirantech.pashupatipaints.model.PaintColor;
import com.shirantech.pashupatipaints.util.AppLog;
import com.shirantech.pashupatipaints.util.Global;
import com.shirantech.pashupatipaints.util.ImageUtils;
import com.shirantech.pashupatipaints.util.Utility;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by susan on 4/19/15.
 */
@SuppressWarnings("deprecation")
public class SnapAndPaintV2Activity extends SherlockFragmentActivity implements
        SelectedColorsFragment.onSelectedColorSelectedListener,
        View.OnClickListener, ViewOnTouchListener {

    private static final String CLASS_TAG = SnapAndPaintV2Activity.class.getSimpleName();
    private int x_adjustment = 0, screenWidth = 0;
    private Global global;
    private Bitmap snappedBitmap;
    private RelativeLayout rlRoom;
    private SnapAndPaintView mv;
    private boolean isColorSelected = false;
    private int usedColorId;
    private Paint mPaint;
    private List<PaintColor> usedColorList;
    private List<PaintColor> globalPaintColorList;
    private View viewSelectedColor;
    private TextView textViewSelectedColorName, textViewSelectedColorCode;
    private GridView gridViewSelectedColors;
    private Vibrator mVibrator;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        if (Build.VERSION.SDK_INT <= Build.VERSION_CODES.HONEYCOMB) {
            getSupportActionBar().hide();
        }
        setContentView(R.layout.activity_snap_and_paint);
        try {
            ViewConfiguration config = ViewConfiguration.get(this);
            Field menuKeyField = ViewConfiguration.class.getDeclaredField("sHasPermanentMenuKey");
            if (menuKeyField != null) {
                menuKeyField.setAccessible(true);
                menuKeyField.setBoolean(config, false);
            }
        } catch (Exception ex) { // Ignore }
        }

        Button imageButtonAddMoreColor = (Button) findViewById(R.id.image_button_add_more_color);
        imageButtonAddMoreColor.setOnClickListener(this);

        ImageButton imageButtonRefresh = (ImageButton) findViewById(R.id.imagebutton_paint_refresh);
        ImageButton imageButtonSave = (ImageButton) findViewById(R.id.imagebutton_paint_save);
        ImageButton imageButtonShare = (ImageButton) findViewById(R.id.imagebutton_paint_share);
        ImageButton imageButtonUndo = (ImageButton) findViewById(R.id.imagebutton_paint_undo);
        Button imageButtonInfo = (Button) findViewById(R.id.imagebutton_info);
        TextView imageButtonGoToGallery = (TextView) findViewById(R.id.imagebutton_go_to_gallery);
        ImageButton imageButtonBack = (ImageButton) findViewById(R.id.home);

        imageButtonInfo.setOnClickListener(this);
        imageButtonBack.setOnClickListener(this);
        imageButtonGoToGallery.setOnClickListener(this);
        imageButtonUndo.setOnClickListener(this);
        imageButtonShare.setOnClickListener(this);
        imageButtonSave.setOnClickListener(this);
        imageButtonRefresh.setOnClickListener(this);

        textViewSelectedColorCode = (TextView) findViewById(R.id.text_view_selected_color_code);
        textViewSelectedColorName = (TextView) findViewById(R.id.text_view_selected_color_name);
        viewSelectedColor = findViewById(R.id.view_selected_color);
        usedColorList = new ArrayList<PaintColor>();
        global = (Global) getApplication();
        globalPaintColorList = global.getGlobalPaintColorList();
        snappedBitmap = global.getSnapAndPaintBitmap();
        rlRoom = (RelativeLayout) findViewById(R.id.rl_snap_and_paint);
        mv = new SnapAndPaintView(this, snappedBitmap);
        rlRoom.addView(mv);

    }

    @Override
    public void onColorSelected(PaintColor paintColor) {
        isColorSelected = true;

        mPaint = new Paint();
        mPaint.setAntiAlias(true);
        mPaint.setDither(true);
        mPaint.setAlpha(0x80);
        mPaint.setColor(-1);
        mPaint.setStyle(Paint.Style.STROKE);
        mPaint.setStrokeJoin(Paint.Join.ROUND);
        mPaint.setStrokeCap(Paint.Cap.ROUND);
        mPaint.setStrokeWidth(12);

        //set the paint color to paint
        mPaint.setColor(Color.parseColor(paintColor.getColorCode()));
        mPaint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.SRC));
        mPaint.setAlpha(0x80);


        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
            viewSelectedColor.setBackground(Utility
                    .getCustomShape(Color.parseColor(paintColor.getColorCode())));
        } else {
            viewSelectedColor.setBackgroundDrawable(Utility
                    .getCustomShape(Color.parseColor(paintColor.getColorCode())));
        }
        textViewSelectedColorName.setText(paintColor.getName());
        textViewSelectedColorCode.setText(paintColor.getColorCode());
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.image_button_add_more_color:
                Intent colorChooserIntent = new Intent(this, ColorChooserActivity.class);
//                colorChooserIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                if (globalPaintColorList.size() <= 0) {
                    colorChooserIntent.putExtra("COLOR CHOOSER SOURCE", "STARTER");
                    colorChooserIntent.putExtra("POSITION", 0);
                    colorChooserIntent.putExtra("TYPE", "bedroom");
                } else {
                    colorChooserIntent.putExtra("COLOR CHOOSER SOURCE", "TEMPLATE");
                    colorChooserIntent.putExtra("POSITION", 0);
                    colorChooserIntent.putExtra("TYPE", "bedroom");
                }
                startActivity(colorChooserIntent);
                break;
            case R.id.home:
                global.getGlobalPaintColorList().clear();
                finish();
                break;
            case R.id.imagebutton_paint_refresh:
                AlertDialog.Builder builder = new AlertDialog.Builder(this);
                builder.setTitle("Reset Paintings");
                builder.setMessage("Want to reset all the painting details?");
                builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        refreshPaintStatus();
                        dialogInterface.dismiss();
                    }
                });
                builder.setNegativeButton("No", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dialogInterface.dismiss();
                    }
                });
                builder.create().show();
                break;
            case R.id.imagebutton_paint_save:
                if (usedColorList.size() <= 0 || usedColorList.isEmpty()) {
                    Toast.makeText(this, "Please Paint the image to save", Toast.LENGTH_SHORT)
                            .show();
                    return;
                }
                AlertDialog.Builder builder1 = new AlertDialog.Builder(this);
                builder1.setTitle("Save Image");
                builder1.setMessage("Do you want to save image?");
                builder1.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        ImageUtils.save(SnapAndPaintV2Activity.this, rlRoom, usedColorList);
                        dialogInterface.dismiss();
                    }
                });
                builder1.setNegativeButton("No", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dialogInterface.dismiss();
                    }
                });
                builder1.create().show();
                break;
            case R.id.imagebutton_paint_share:
                Bitmap bm;
                rlRoom.setDrawingCacheEnabled(true);
                rlRoom.buildDrawingCache();
                bm = rlRoom.getDrawingCache();
                Bitmap bmCopy = bm.copy(Bitmap.Config.ARGB_8888, false);
                rlRoom.destroyDrawingCache();

                ((Global) getApplication()).setSnapAndPaintBitmap(bmCopy);
                startActivity(new Intent(this, ImagePreviewActivity.class));
                break;
            case R.id.imagebutton_info:
//                showInfoDialog();
                showInfoActivity();
                break;
            case R.id.imagebutton_go_to_gallery:
                Intent intentGallery = new Intent(SnapAndPaintV2Activity.this,
                        GalleryV2Activity.class);
                intentGallery.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intentGallery);
                break;
            case R.id.imagebutton_paint_undo:
                try {
                    if (mPaint != null) {
                        mPaint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.CLEAR));
                    } else {
                        Toast.makeText(getApplicationContext(), "Nothing to erase!", Toast.LENGTH_SHORT).show();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
                break;
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        globalPaintColorList = ((Global) getApplication()).getGlobalPaintColorList();

        gridViewSelectedColors = (GridView) findViewById(R.id.selected_color_fragment);/*.findViewById(R.id.gridViewSelectedColors));*/
        gridViewSelectedColors.setAdapter(new TabletColorHorizontalListAdapter(this, globalPaintColorList, this));
        gridViewSelectedColors.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                PaintColor paintColor = (PaintColor) gridViewSelectedColors.getAdapter().getItem(position);
                AppLog.showLog(CLASS_TAG, "grid selected item:: " + paintColor.getName());
                sendToParent(paintColor);
            }
        });
        mVibrator = (Vibrator) this.getSystemService(Context.VIBRATOR_SERVICE);
        gridViewSelectedColors.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(final AdapterView<?> parent, View view, final int position, long id) {
                mVibrator.vibrate(30);
                TabletColorHorizontalListAdapter.ViewHolder holder = (TabletColorHorizontalListAdapter.ViewHolder) view.getTag();
                holder.imageButtonDelete.setVisibility(View.VISIBLE);
                holder.imageButtonDelete.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        final PaintColor pc = (PaintColor) parent.getAdapter().getItem(position);
                        globalPaintColorList.remove(pc);
                        gridViewSelectedColors.setAdapter(new TabletColorHorizontalListAdapter(SnapAndPaintV2Activity.this,
                                globalPaintColorList, SnapAndPaintV2Activity.this));
                        ((TabletColorHorizontalListAdapter)parent.getAdapter()).notifyDataSetChanged();
                    }
                });
                return true;
            }
        });
    }

    private void sendToParent(PaintColor paintColor) {
        isColorSelected = true;

        mPaint = new Paint();
        mPaint.setAntiAlias(true);
        mPaint.setDither(true);
        mPaint.setAlpha(0x80);
        mPaint.setColor(-1);
        mPaint.setStyle(Paint.Style.STROKE);
        mPaint.setStrokeJoin(Paint.Join.ROUND);
        mPaint.setStrokeCap(Paint.Cap.ROUND);
        mPaint.setStrokeWidth(12);

        //set the paint color to paint
        mPaint.setColor(Color.parseColor(paintColor.getColorCode()));
        mPaint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.SRC));
        mPaint.setAlpha(0x80);
    }

    private void showInfoActivity() {
        AppLog.showLog(CLASS_TAG, "size of used color : " + usedColorList.size());
        if (usedColorList == null || usedColorList.size() <= 0) {
            Toast.makeText(this, "Info unavailable. Paint first.", Toast.LENGTH_SHORT)
                    .show();
            return;
        }
//        Intent infoActivity = new Intent(this, PaintInfoActivity.class);
        PaintInfoFragment fragment = new PaintInfoFragment();
        Bundle bundle = new Bundle();
        bundle.putParcelableArrayList("USED_COLOR_LIST", (ArrayList<PaintColor>) usedColorList);
        fragment.setArguments(bundle);
        fragment.show(getSupportFragmentManager(), "Paint Info");
//        infoActivity.putExtras(bundle);
//        startActivity(infoActivity);
    }

    private void refreshPaintStatus() {
        rlRoom.removeAllViews();
        mv = new SnapAndPaintView(this, snappedBitmap);
        usedColorList.clear();

        globalPaintColorList.clear();
        ((TabletColorHorizontalListAdapter)gridViewSelectedColors.getAdapter()).notifyDataSetChanged();
        viewSelectedColor.setBackgroundDrawable(new ColorDrawable(getResources().getColor(android.R.color.transparent)));
        textViewSelectedColorCode.setText("");
        textViewSelectedColorName.setText("");
        usedColorList.clear();
        mPaint = new Paint();
        mPaint.setAntiAlias(true);
        mPaint.setDither(true);
        mPaint.setAlpha(0x80);
        mPaint.setColor(getResources().getColor(android.R.color.transparent));
        mPaint.setStyle(Paint.Style.STROKE);
        mPaint.setStrokeJoin(Paint.Join.ROUND);
        mPaint.setStrokeCap(Paint.Cap.ROUND);
        mPaint.setStrokeWidth(12);
        rlRoom.addView(mv);
    }

    @Override
    public void onViewTouch(PaintColor paintColor) {
        sendToParent(paintColor);
    }

    @SuppressWarnings("deprecation")
    public class SnapAndPaintView extends View {

        private Bitmap mBitmap, bmp, bm;
        private Canvas mCanvas;
        private Path mPath;
        private Paint mBitmapPaint;

        public SnapAndPaintView(Context c, Bitmap mbmp) {
            super(c);
            mPath = new Path();
            mBitmapPaint = new Paint(Paint.DITHER_FLAG);
            bmp = mbmp;

            x_adjustment = (screenWidth - bmp.getWidth()) / 2;
        }

        @Override
        protected void onSizeChanged(int w, int h, int oldw, int oldh) {
            super.onSizeChanged(w, h, oldw, oldh);

            mBitmap = Bitmap.createBitmap(bmp.copy(Bitmap.Config.ARGB_8888, true));
            mv.setBackgroundDrawable(new BitmapDrawable(mBitmap));//set background here

            bm = Bitmap.createBitmap(w, h, Bitmap.Config.ARGB_8888);

            AppLog.showLog(CLASS_TAG, "original bitmap width:: " + mBitmap.getWidth() + " height:: " + mBitmap.getHeight());
            mCanvas = new Canvas(bm);
        }

        @Override
        protected void onDraw(Canvas canvas) {
            super.onDraw(canvas);
            canvas.drawBitmap(bm, 0, 0, mBitmapPaint);

            if (isColorSelected) {
                usedColorId = mPaint.getColor();
            }

        }

        private float mX, mY;
        private static final float TOUCH_TOLERANCE = 4;

        private void touch_start(float x, float y) {
            mPath.reset();
            mPath.moveTo(x, y);
            mX = x;
            mY = y;

            try {
                mCanvas.drawPath(mPath, mPaint);
            } catch (Exception e) {
                AppLog.showLog(CLASS_TAG, "touch start error: " + e.getLocalizedMessage());
            }
        }

        private void touch_move(float x, float y) {
            float dx = Math.abs(x - mX);
            float dy = Math.abs(y - mY);
            if (dx >= TOUCH_TOLERANCE || dy >= TOUCH_TOLERANCE) {
                mPath.quadTo(mX, mY, (x + mX) / 2, (y + mY) / 2);
                mX = x;
                mY = y;
            }
            try {
                mCanvas.drawPath(mPath, mPaint);
            } catch (Exception e) {
                AppLog.showLog(CLASS_TAG, "touch move error: " + e.getLocalizedMessage());
            }

        }

        private void touch_up() {

            //GET THE COLOR AND SAVE TO DATABASE!!!
            Boolean isNewSelectedColor = true;

            //CONVERT COLOR INT TO HASH
            String colorHash = String.format("#%06X", 0xFFFFFF & usedColorId);
            AppLog.showLog(CLASS_TAG, "color Hash value:: " + colorHash);

            for (PaintColor p : usedColorList)
                try {
                    if (p.getColorCode().equalsIgnoreCase(colorHash)) {
                        isNewSelectedColor = false;
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                }

            if (isNewSelectedColor) {
                PaintColor pc = new PaintColor();
                pc.setColorCode(colorHash);

                String colorName = null;

                //set corresponding color name for colorHash!!
                for (PaintColor aGlobalPaintColorList : globalPaintColorList) {
                    if (aGlobalPaintColorList.getColorCode().equalsIgnoreCase(colorHash)) {
                        colorName = aGlobalPaintColorList.getName();
                    }
                }
                pc.setName(colorName);

                usedColorList.add(pc);
            }

            try {
                mPath.lineTo(mX, mY);
                // commit the path to our off-screen
                mCanvas.drawPath(mPath, mPaint);
                // kill this so we don't double draw
                mPath.reset();

                mPaint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.SRC));
                mPaint.setAlpha(0x80);
                mPaint.setMaskFilter(null);

            } catch (Exception e) {
                AppLog.showLog(CLASS_TAG, "touch up error: " + e.getLocalizedMessage());
            }

        }

        @Override
        public boolean onTouchEvent(@NonNull MotionEvent event) {
            float x = event.getX();
            float y = event.getY();

            switch (event.getAction()) {
                case MotionEvent.ACTION_DOWN:
                    touch_start(x, y);
                    invalidate();
                    break;
                case MotionEvent.ACTION_MOVE:
                    touch_move(x, y);
                    invalidate();
                    break;
                case MotionEvent.ACTION_UP:
                    touch_up();
                    invalidate();
                    break;
            }
            return true;
        }
    }
}
