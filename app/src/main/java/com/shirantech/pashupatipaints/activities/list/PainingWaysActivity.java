package com.shirantech.pashupatipaints.activities.list;

import android.app.AlertDialog;
import android.content.ActivityNotFoundException;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.ArrayAdapter;
import android.widget.ImageButton;
import android.widget.ImageView;

import com.actionbarsherlock.app.SherlockActivity;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.shirantech.pashupatipaints.R;
import com.shirantech.pashupatipaints.util.AppLog;
import com.shirantech.pashupatipaints.util.AppText;
import com.shirantech.pashupatipaints.util.Global;
import com.shirantech.pashupatipaints.util.ImageUtils;
import com.shirantech.pashupatipaints.util.Utility;

import java.io.File;
import java.io.FileOutputStream;

public class PainingWaysActivity extends SherlockActivity implements OnClickListener {

    private static final String CLASS_TAG = PainingWaysActivity.class.getSimpleName();
    private static Uri mImageCaptureUri;
    private ImageLoader imageLoader = ImageLoader.getInstance();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        if (Build.VERSION.SDK_INT <= Build.VERSION_CODES.HONEYCOMB) {
            getSupportActionBar().hide();
        }
        if (Utility.isTablet(this)) {
            AppLog.showLog(CLASS_TAG, "is tablet");
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
        /*getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);*/
        } else {
            AppLog.showLog(CLASS_TAG, "is not tablet");
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        }
        setContentView(R.layout.activity_paining_ways);
        imageLoader.init(ImageLoaderConfiguration.createDefault(this));
        AppLog.showLog(CLASS_TAG, "PaintingWaysActivity started.");
        ImageButton buttonHome = (ImageButton) findViewById(R.id.home);
        ImageView buttonFromTemplate = (ImageView) findViewById(R.id.button_from_template);
        ImageUtils.loadImageUsingLoader(imageLoader, buttonFromTemplate, "assets://" + AppText.LIVINGROOM1_LOCATION_MAIN);

        ImageView buttonExterior = (ImageView) findViewById(R.id.imageview_exterior);
        ImageUtils.loadImageUsingLoader(imageLoader, buttonExterior, "assets://" + AppText.EXTERIOR1_LOCATION_MAIN_IMAGE);

        ImageButton buttonFromPhoto = (ImageButton) findViewById(R.id.button_from_photo);
        buttonHome.setOnClickListener(this);
        buttonFromTemplate.setOnClickListener(this);
        buttonExterior.setOnClickListener(this);
        buttonFromPhoto.setOnClickListener(this);
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.home:
                if (Utility.isTablet(this)) {
                    Intent intent = new Intent(this, StarterTabletActivity.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(intent);
                } else {
                    Intent intent = new Intent(this, StarterV2Activity.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(intent);
                }
                break;
            case R.id.button_from_template:
//                if (Utility.isTablet(this)) {
                Intent interiorListIntent = new Intent(getApplicationContext(), InteriorListActivity.class);
                startActivity(interiorListIntent);
               /* } else {
                    Intent roomCategoriesIntent = new Intent(getApplicationContext(), RoomCategoryActivity.class);

                    startActivity(roomCategoriesIntent);
                }*/
                break;
            case R.id.imageview_exterior:
                Intent exteriorListIntent = new Intent(getApplicationContext(), ExteriorListActivity.class);
                startActivity(exteriorListIntent);
                break;
            case R.id.button_from_photo:
                AppLog.showLog(CLASS_TAG, "button from photo");
                final String[] items = new String[]{"Take from camera", "Select from gallery"};
                ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, android.R.layout.select_dialog_item, items);
                AlertDialog.Builder builder = new AlertDialog.Builder(this);
                builder.setTitle("Select Image");
                builder.setAdapter(adapter, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int item) {
                        if (item == 0) { // pick from camera
                            AppLog.showLog(CLASS_TAG, "pick from camera");
                            Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                            mImageCaptureUri = Uri.fromFile(new File(Environment.getExternalStorageDirectory(), "Pashupatipaints_" + String.valueOf(System.currentTimeMillis()) + ".jpg"));
                            intent.putExtra(android.provider.MediaStore.EXTRA_OUTPUT, mImageCaptureUri);

                            try {
                                intent.putExtra("return-data", true);
                                startActivityForResult(intent, ImageUtils.PICk_FROM_CAMERA);
                            } catch (ActivityNotFoundException e) {
                                e.printStackTrace();
                            }
                        } else { // pick from file
                            AppLog.showLog(CLASS_TAG, "pick from file");
                            Intent intent = new Intent();
                            intent.setType("image/*");
                            intent.setAction(Intent.ACTION_GET_CONTENT);
                            startActivityForResult(Intent.createChooser(intent, "Complete action using"), ImageUtils.PICK_FROM_FILE);
                        }
                    }
                });
                builder.create().show();

                break;

            default:
                break;
        }

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        AppLog.showLog(CLASS_TAG, "on activity result:" +
                " request code: " + ((requestCode == ImageUtils.PICk_FROM_CAMERA) ? "Pick from camera "
                : (requestCode == ImageUtils.PICK_FROM_FILE) ? "Pick from file" : "crop from camera" +
                ":: result code:" + resultCode));
        if (resultCode != RESULT_OK)
            return;

        switch (requestCode) {
            case ImageUtils.PICk_FROM_CAMERA:
                AppLog.showLog(CLASS_TAG, "do crop from camera");
                ImageUtils.doCrop(this, mImageCaptureUri);
                break;

            case ImageUtils.PICK_FROM_FILE:
                AppLog.showLog(CLASS_TAG, "do crop from file");
                mImageCaptureUri = data.getData();
                ImageUtils.doCrop(this, mImageCaptureUri);
                break;

            case ImageUtils.CROP_FROM_CAMERA:
                AppLog.showLog(CLASS_TAG, "cropped from camera");
                Bundle extras = data.getExtras();
                Bitmap photo = null;


                if (extras != null) {
                    photo = extras.getParcelable("data");
                    AppLog.showLog(CLASS_TAG, "Bitmap returned after cropping! Now should be calling SnapAndCropActivity!" + photo);
                } else {
                    AppLog.showLog(CLASS_TAG, "Nothing returned after cropping!");
                }

                if (mImageCaptureUri == null) {
                    AppLog.showLog(CLASS_TAG, "mImageCaptureUri NULL !");
                } else {
                    AppLog.showLog(CLASS_TAG, "mImageCaptureUri NOT NULL !");
                }

                File f = new File(mImageCaptureUri.getPath());

                Bitmap bitmap;

                if (f.exists()) {
                    f.delete();

                    bitmap = (Bitmap) data.getExtras().get("data");
                    try {
                        FileOutputStream out = new FileOutputStream(f);
                        bitmap.compress(Bitmap.CompressFormat.PNG, 100, out);
                        out.flush();
                        out.close();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }

                Intent snapAndPaintIntent;
                if (Utility.isTablet(this)) {
                    snapAndPaintIntent = new Intent(getApplicationContext(), SnapAndPaintTabletActivity.class);
                } else {
                    snapAndPaintIntent = new Intent(getApplicationContext(), SnapAndPaintV2Activity.class);
                }
                ((Global) getApplication()).setSnapAndPaintBitmap(photo);

                startActivity(snapAndPaintIntent);

                break;

        }
    }


}
