package com.shirantech.pashupatipaints.activities.list;

import android.content.pm.ActivityInfo;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;

import com.actionbarsherlock.app.SherlockActivity;
import com.shirantech.pashupatipaints.R;
import com.shirantech.pashupatipaints.util.Global;
import com.shirantech.pashupatipaints.util.Utility;

/**
 * Created by susan on 3/18/15.
 */
public class ImagePreviewActivity extends SherlockActivity implements View.OnClickListener {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(android.view.Window.FEATURE_NO_TITLE);
        if (Build.VERSION.SDK_INT <= Build.VERSION_CODES.HONEYCOMB) {
            getSupportActionBar().hide();
        }
        if (Utility.isTablet(this)) {
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
        } else {
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        }
        setContentView(R.layout.activity_image_preview);
        findViewById(R.id.home).setOnClickListener(this);
        ((ImageView) findViewById(R.id.full_image_view)).setImageBitmap(((Global) getApplication()).getSnapAndPaintBitmap());
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.home:
                finish();
                break;
        }
    }
}
