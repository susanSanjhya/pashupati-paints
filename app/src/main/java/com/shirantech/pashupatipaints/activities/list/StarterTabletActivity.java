package com.shirantech.pashupatipaints.activities.list;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;

import com.actionbarsherlock.app.SherlockFragmentActivity;
import com.shirantech.pashupatipaints.R;
import com.shirantech.pashupatipaints.customwidgets.carouseltablet.CarouselViewAdapter;
import com.shirantech.pashupatipaints.customwidgets.carouseltablet.CarouselDataItem;
import com.shirantech.pashupatipaints.customwidgets.carouseltablet.CarouselView;
import com.shirantech.pashupatipaints.util.Global;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by susan on 2/19/15.
 */
public class StarterTabletActivity extends SherlockFragmentActivity implements AdapterView.OnItemClickListener {
    Global global = Global.getInstance();
    CarouselViewAdapter carouselAdapter = null;
    private List<CarouselDataItem> dataItemList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        global.InitGUIFrame(this);
        setContentView(R.layout.activity_starter);
        getSupportActionBar().hide();

        dataItemList = new ArrayList<CarouselDataItem>();
        for (int i = 0; i < 1000; i++) {
            CarouselDataItem dataItem;
            if (i % 4 == 0) {
                dataItem = new CarouselDataItem(R.drawable.start_painting, 0, "START PAINTING");
            } else if (i % 4 == 1) {
                dataItem = new CarouselDataItem(R.drawable.paint_calculator, 0, "PAINT CALCULATOR");
            } else if (i % 4 == 2) {
                dataItem = new CarouselDataItem(R.drawable.authorized_dealers, 0, "AUTHORIZED DEALERS");
            } else {
                dataItem = new CarouselDataItem(R.drawable.ic_gallery, 0, "GALLERY");
            }
            dataItemList.add(dataItem);
        }
        CarouselView coverFlow = (CarouselView) findViewById(R.id.carousel_view);
        carouselAdapter = new CarouselViewAdapter(this, dataItemList, global.Scale(600), global.Scale(300));
        coverFlow.setAdapter(carouselAdapter);
        coverFlow.setSpacing(-1 * global.Scale(250));
        coverFlow.setSelection(1000 / 2, true);
        coverFlow.setAnimationDuration(1000);
        coverFlow.setUnselectedAlpha(0.3f);
        coverFlow.setMaxRotationAngle(30);
        coverFlow.setOnItemClickListener(this);
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        Log.e(StarterTabletActivity.class.getSimpleName(), "Clicked on " + position);
        if (position >= 4) {
            position = position % 4;
        }
        Log.e(StarterTabletActivity.class.getSimpleName(), "Clicked on " + position);
        switch (position) {
            case 0:
                Intent paintingWaysIntent = new Intent(getApplicationContext(), PainingWaysActivity.class);

                startActivity(paintingWaysIntent);
                break;
            case 1:
                Intent paintCalculatorIntent = new Intent(getApplicationContext(), PaintCalculatorTabletActivity.class);
                startActivity(paintCalculatorIntent);
                break;
            case 2:
                Intent dealersDetailIntent = new Intent(getApplicationContext(), DealersDetailActivity.class);

                startActivity(dealersDetailIntent);
                break;
            case 3:
                Intent galleryIntent = new Intent(getApplicationContext(), GalleryActivity.class);

                startActivity(galleryIntent);
                break;
        }
    }
}