package com.shirantech.pashupatipaints.activities.list;

import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.ProgressBar;

import com.actionbarsherlock.app.SherlockActivity;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.shirantech.pashupatipaints.R;
import com.shirantech.pashupatipaints.activities.list.livingrooms.LivingRoom1TabletActivity;
import com.shirantech.pashupatipaints.activities.list.livingrooms.LivingRoom1V2Activity;
import com.shirantech.pashupatipaints.activities.list.livingrooms.LivingRoom2TabletActivity;
import com.shirantech.pashupatipaints.activities.list.livingrooms.LivingRoom2V2Activity;
import com.shirantech.pashupatipaints.activities.list.livingrooms.LivingRoom3TabletActivity;
import com.shirantech.pashupatipaints.activities.list.livingrooms.LivingRoom3V2Activity;
import com.shirantech.pashupatipaints.activities.list.livingrooms.LivingRoom4TabletActivity;
import com.shirantech.pashupatipaints.activities.list.livingrooms.LivingRoom4V2Activity;
import com.shirantech.pashupatipaints.adapter.RoomCategoryListAdapter;
import com.shirantech.pashupatipaints.controller.GetRoomCategoriesTask;
import com.shirantech.pashupatipaints.interfaces.ModelListGetter;
import com.shirantech.pashupatipaints.model.RoomCategory;
import com.shirantech.pashupatipaints.util.Utility;

import java.util.List;

/**
 * Created by susan on 12/22/14.
 */
public class DrawingRoomListActivity extends SherlockActivity implements AdapterView.OnItemClickListener, ModelListGetter<RoomCategory> {
    private GridView gridViewDrawingRoomList;
    private ProgressBar progressBarRoomLoading;
    private ImageLoader imageLoader = ImageLoader.getInstance();
    private String[] drawingRoomDrawableArray = {
            "LivingRoom/livingroom1/livingroom1_main.png",
            "LivingRoom/livingroom2/livingroom2_main.png",
            "LivingRoom/livingroom3/livingroom3_main.png",
            "LivingRoom/livingroom4/livingroom4_main.png"
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        if (Build.VERSION.SDK_INT <= Build.VERSION_CODES.HONEYCOMB) {
            getSupportActionBar().hide();
        }
        if (Utility.isTablet(this)) {
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
        } else {
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        }

        setContentView(R.layout.activity_drawingroom_list);

        imageLoader.init(ImageLoaderConfiguration.createDefault(this));
        progressBarRoomLoading = (ProgressBar) findViewById(R.id.progressbar_drawingrooms_loading);

        gridViewDrawingRoomList = (GridView) findViewById(R.id.gridview_drawingrooms);
        gridViewDrawingRoomList.setOnItemClickListener(this);
        findViewById(R.id.home).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(DrawingRoomListActivity.this, InteriorListActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
            }
        });
        new GetRoomCategoriesTask(this, this, "Drawing Hall 2").execute(drawingRoomDrawableArray);
    }

    @Override
    public void onItemClick(AdapterView<?> adapterView, View view, int position, long l) {
        switch (position) {
            case 0:
                if (Utility.isTablet(this)) {
                    Intent drawingRoom1ActivityIntent = new Intent(DrawingRoomListActivity.this, LivingRoom1TabletActivity.class);
                    drawingRoom1ActivityIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(drawingRoom1ActivityIntent);
                } else {
                    Intent drawingRoom1ActivityIntent = new Intent(DrawingRoomListActivity.this, LivingRoom1V2Activity.class);
                    drawingRoom1ActivityIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(drawingRoom1ActivityIntent);
                }
                break;
            case 1:
                if (Utility.isTablet(this)) {
                    Intent livingRoom2ActivityIntent = new Intent(DrawingRoomListActivity.this, LivingRoom2TabletActivity.class);
                    livingRoom2ActivityIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(livingRoom2ActivityIntent);
                }else{
                    Intent livingRoom2ActivityIntent = new Intent(DrawingRoomListActivity.this, LivingRoom2V2Activity.class);
                    livingRoom2ActivityIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(livingRoom2ActivityIntent);
                }
                break;
            case 2:
                if (Utility.isTablet(this)) {
                    Intent livingRoom3ActivityIntent = new Intent(DrawingRoomListActivity.this, LivingRoom3TabletActivity.class);
                    livingRoom3ActivityIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(livingRoom3ActivityIntent);
                } else {
                    Intent livingRoom3ActivityIntent = new Intent(DrawingRoomListActivity.this, LivingRoom3V2Activity.class);
                    livingRoom3ActivityIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(livingRoom3ActivityIntent);
                }
                break;
            case 3:
                if (Utility.isTablet(this)) {
                    Intent livingRoom4ActivityIntent = new Intent(DrawingRoomListActivity.this, LivingRoom4TabletActivity.class);
                    livingRoom4ActivityIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(livingRoom4ActivityIntent);
                }else{
                    Intent livingRoom4ActivityIntent = new Intent(DrawingRoomListActivity.this, LivingRoom4V2Activity.class);
                    livingRoom4ActivityIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(livingRoom4ActivityIntent);
                }
                break;
        }
    }

    @Override
    public void onTaskStarted() {
        progressBarRoomLoading.setVisibility(View.VISIBLE);
    }

    @Override
    public void onTaskFinished(List<RoomCategory> modelList) {
        gridViewDrawingRoomList.setAdapter(new RoomCategoryListAdapter(this, imageLoader, modelList));
        progressBarRoomLoading.setVisibility(View.GONE);
    }

}
