package com.shirantech.pashupatipaints.activities.list.exteriors;

import android.app.AlertDialog;
import android.content.ClipData;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Build;
import android.os.Bundle;
import android.os.Vibrator;
import android.util.DisplayMetrics;
import android.view.DragEvent;
import android.view.MotionEvent;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.GridView;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.actionbarsherlock.app.SherlockFragmentActivity;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.shirantech.pashupatipaints.R;
import com.shirantech.pashupatipaints.activities.list.ColorChooserActivity;
import com.shirantech.pashupatipaints.activities.list.GalleryActivity;
import com.shirantech.pashupatipaints.activities.list.GalleryV2Activity;
import com.shirantech.pashupatipaints.activities.list.ImagePreviewActivity;
import com.shirantech.pashupatipaints.adapter.TabletColorHorizontalListAdapter;
import com.shirantech.pashupatipaints.customwidgets.ColorDragShadowBuilder;
import com.shirantech.pashupatipaints.fragments.PaintInfoFragment;
import com.shirantech.pashupatipaints.fragments.SelectedColorsFragment;
import com.shirantech.pashupatipaints.interfaces.ViewOnTouchListener;
import com.shirantech.pashupatipaints.model.PaintColor;
import com.shirantech.pashupatipaints.model.PaintColorHistory;
import com.shirantech.pashupatipaints.util.AppLog;
import com.shirantech.pashupatipaints.util.AppText;
import com.shirantech.pashupatipaints.util.Global;
import com.shirantech.pashupatipaints.util.ImageUtils;
import com.shirantech.pashupatipaints.util.Utility;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 * Created by susan on 4/6/15.
 */
@SuppressWarnings("deprecation")
public class Exterior2V2Activity extends SherlockFragmentActivity implements SelectedColorsFragment.onSelectedColorSelectedListener, View.OnClickListener, ViewOnTouchListener {

    private static final String CLASS_TAG = Exterior2V2Activity.class.getSimpleName();
    private ImageLoader imageLoader = ImageLoader.getInstance();
    private RelativeLayout parent;
    private Global global;
    private List<PaintColor> globalPaintColorList;
    private ImageView imageViewMain, imageViewTop, imageViewBottom;
    private TextView textViewSelectedColorCode, textViewSelectedColorName;
    private View viewSelectedColor;
    private List<PaintColor> usedColorList;
    private boolean isColorClicked = false;
    private float redScale = 1f, greenScale = 0f, blueScale = 0f;
    private boolean isDonePainting = false;
    private GridView gridViewSelectedColors;
    private int topRed, topGreen, topBlue;
    private int bottomRed, bottomGreen, bottomBlue;
    private List<PaintColorHistory> redoColorHistoryList = new ArrayList<PaintColorHistory>();
    private List<PaintColorHistory> undoColorHistoryList = new ArrayList<PaintColorHistory>();
    private Vibrator mVibrator;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(android.view.Window.FEATURE_NO_TITLE);
        if (Build.VERSION.SDK_INT <= Build.VERSION_CODES.HONEYCOMB) {
            getSupportActionBar().hide();
        }
        setContentView(R.layout.activity_exteriors2);
        imageLoader.init(ImageLoaderConfiguration.createDefault(this));
        parent = (RelativeLayout) findViewById(R.id.roomLayout_exterior2);

        global = (Global) getApplication();
        globalPaintColorList = global.getGlobalPaintColorList();
        imageViewMain = (ImageView) findViewById(R.id.iv_exterior2_main_image);
        imageViewTop = (ImageView) findViewById(R.id.iv_exterior2_top);
        imageViewBottom = (ImageView) findViewById(R.id.iv_exterior2_bottom);

        ImageUtils.loadImageUsingLoader(imageLoader, imageViewMain, "assets://" + AppText.EXTERIOR2_LOCATION_MAIN_IMAGE);
        ImageUtils.loadImageUsingLoader(imageLoader, imageViewTop, "assets://" + AppText.EXTERIOR2_LOCATION_TOP);
        ImageUtils.loadImageUsingLoader(imageLoader, imageViewBottom, "assets://" + AppText.EXTERIOR2_LOCATION_BOTTOM);

        Button imageButtonAddMoreColor = (Button) findViewById(R.id.image_button_add_more_color);
        imageButtonAddMoreColor.setOnClickListener(this);

        ImageButton imageButtonRefresh = (ImageButton) findViewById(R.id.imagebutton_paint_refresh);
        ImageButton imageButtonSave = (ImageButton) findViewById(R.id.imagebutton_paint_save);
        ImageButton imageButtonShare = (ImageButton) findViewById(R.id.imagebutton_paint_share);
        ImageButton imageButtonUndo = (ImageButton) findViewById(R.id.imagebutton_paint_undo);
        ImageButton imageButtonRedo = (ImageButton) findViewById(R.id.imagebutton_paint_redo);
        Button imageButtonInfo = (Button) findViewById(R.id.imagebutton_info);
        TextView imageButtonGoToGallery = (TextView) findViewById(R.id.imagebutton_go_to_gallery);
        ImageButton imageButtonBack = (ImageButton) findViewById(R.id.home);

        imageButtonInfo.setOnClickListener(this);
        imageButtonBack.setOnClickListener(this);
        imageButtonGoToGallery.setOnClickListener(this);
        imageButtonRedo.setOnClickListener(this);
        imageButtonUndo.setOnClickListener(this);
        imageButtonShare.setOnClickListener(this);
        imageButtonSave.setOnClickListener(this);
        imageButtonRefresh.setOnClickListener(this);

        textViewSelectedColorCode = (TextView) findViewById(R.id.text_view_selected_color_code);
        textViewSelectedColorName = (TextView) findViewById(R.id.text_view_selected_color_name);
        viewSelectedColor = findViewById(R.id.view_selected_color);

        usedColorList = new ArrayList<PaintColor>();

        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.HONEYCOMB) {
            //drag and drop!!
            imageViewBottom.setOnDragListener(new ChoiceDragListener());
        }
        imageViewBottom.setOnTouchListener(new View.OnTouchListener() {

            @Override
            public boolean onTouch(View v, MotionEvent event) {

                if (isColorClicked) {
                    int[] colors = ImageUtils.startPainting(getApplicationContext(), ImageUtils.EXTERIOR_TEMPLATE_2,
                            (int) event.getX(), (int) event.getY(), imageViewMain, parent,
                            new float[]{redScale, greenScale, blueScale}, imageViewTop, imageViewBottom);
                    isDonePainting = true;
                    updatePaintStatus(colors);

                } else {
                    Toast.makeText(getApplicationContext(), "Please select the color below to paint the rooms", Toast.LENGTH_SHORT).show();
                }
                return false;
            }
        });

        //Determine density
        DisplayMetrics metrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(metrics);
        int density = metrics.densityDpi;

        if (density == DisplayMetrics.DENSITY_HIGH) {
            AppLog.showLog(CLASS_TAG, "DENSITY_HIGH... Density is " + String.valueOf(density));
        } else if (density == DisplayMetrics.DENSITY_MEDIUM) {
            AppLog.showLog(CLASS_TAG, "DENSITY_MEDIUM... Density is " + String.valueOf(density));
        } else if (density == DisplayMetrics.DENSITY_LOW) {
            AppLog.showLog(CLASS_TAG, "DENSITY_LOW... Density is " + String.valueOf(density));
        } else {
            AppLog.showLog(CLASS_TAG, "Density is neither HIGH, MEDIUM OR LOW.  Density is " + String.valueOf(density));
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        globalPaintColorList = ((Global) getApplication()).getGlobalPaintColorList();

        gridViewSelectedColors = (GridView) findViewById(R.id.selected_color_fragment);/*.findViewById(R.id.gridViewSelectedColors));*/
        gridViewSelectedColors.setAdapter(new TabletColorHorizontalListAdapter(this, globalPaintColorList, this));
        /*if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.HONEYCOMB) {
            gridViewSelectedColors.setOnTouchListener(new ChoiceTouchListener());
        }*/

        gridViewSelectedColors.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                PaintColor paintColor = (PaintColor) gridViewSelectedColors.getAdapter().getItem(position);
                AppLog.showLog(CLASS_TAG, "grid selected item:: " + paintColor.getName());
                sendToParent(paintColor);
            }
        });
        mVibrator = (Vibrator) this.getSystemService(Context.VIBRATOR_SERVICE);
        gridViewSelectedColors.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(final AdapterView<?> parent, View view, final int position, long id) {
                mVibrator.vibrate(30);
                TabletColorHorizontalListAdapter.ViewHolder holder = (TabletColorHorizontalListAdapter.ViewHolder) view.getTag();
                holder.imageButtonDelete.setVisibility(View.VISIBLE);
                holder.imageButtonDelete.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        final PaintColor pc = (PaintColor) parent.getAdapter().getItem(position);
                        globalPaintColorList.remove(pc);
                        gridViewSelectedColors.setAdapter(new TabletColorHorizontalListAdapter(Exterior2V2Activity.this,
                                globalPaintColorList, Exterior2V2Activity.this));
                        ((TabletColorHorizontalListAdapter)parent.getAdapter()).notifyDataSetChanged();
                    }
                });
                return true;
            }
        });
    }
    private void sendToParent(PaintColor paintColor) {
        isColorClicked = true;

        Float[] rgb = ImageUtils.hexToRgb(paintColor.getColorCode());

        redScale = rgb[0];
        greenScale = rgb[1];
        blueScale = rgb[2];
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
            viewSelectedColor.setBackground(Utility.getCustomShape(Color.parseColor(paintColor.getColorCode())));
        } else {
            viewSelectedColor.setBackgroundDrawable(Utility.getCustomShape(Color.parseColor(paintColor.getColorCode())));
        }
        textViewSelectedColorName.setText(paintColor.getName());
        textViewSelectedColorCode.setText(paintColor.getColorCode());
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.image_button_add_more_color:
                Intent colorChooserIntent = new Intent(this, ColorChooserActivity.class);
//                colorChooserIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                if (globalPaintColorList.size() <= 0) {
                    colorChooserIntent.putExtra("COLOR CHOOSER SOURCE", "STARTER");
                    colorChooserIntent.putExtra("POSITION", 0);
                    colorChooserIntent.putExtra("TYPE", "bedroom");
                } else {
                    colorChooserIntent.putExtra("COLOR CHOOSER SOURCE", "TEMPLATE");
                    colorChooserIntent.putExtra("POSITION", 0);
                    colorChooserIntent.putExtra("TYPE", "bedroom");
                }
                startActivity(colorChooserIntent);
                break;
            case R.id.home:
                global.getGlobalPaintColorList().clear();
                finish();
                break;
            case R.id.imagebutton_paint_undo:
                undoPainting();
                break;
            case R.id.imagebutton_paint_refresh:
                AlertDialog.Builder builder = new AlertDialog.Builder(this);
                builder.setTitle("Reset painting");
                builder.setMessage("Want to reset all the painting details?");
                builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        refreshPaintStatus();
                        dialogInterface.dismiss();
                    }
                });
                builder.setNegativeButton("No", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dialogInterface.dismiss();
                    }
                });
                builder.create().show();
                break;
            case R.id.imagebutton_paint_save:
                if (usedColorList.size() <= 0 || usedColorList.isEmpty()) {
                    Toast.makeText(this, "Please Paint the image to save", Toast.LENGTH_SHORT)
                            .show();
                    return;
                }
                AlertDialog.Builder builder1 = new AlertDialog.Builder(this);
                builder1.setTitle("Save Image");
                builder1.setMessage("Do you want to save image?");
                builder1.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        ImageUtils.saveTemplateImage(Exterior2V2Activity.this,
                                global, ImageUtils.EXTERIOR_TEMPLATE_2, parent,
                                topRed, topGreen, topBlue,
                                bottomRed, bottomGreen, bottomBlue);
                        dialogInterface.dismiss();
                    }
                });
                builder1.setNegativeButton("No", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dialogInterface.dismiss();
                    }
                });
                builder1.create().show();
                break;
            case R.id.imagebutton_paint_share:
                Bitmap bm;
                parent.setDrawingCacheEnabled(true);
                parent.buildDrawingCache();
                bm = parent.getDrawingCache();
                Bitmap bmCopy = bm.copy(Bitmap.Config.ARGB_8888, false);
                parent.destroyDrawingCache();
                Intent intent = new Intent(this, ImagePreviewActivity.class);
                ((Global) getApplication()).setSnapAndPaintBitmap(bmCopy);
                startActivity(intent);
                break;
            case R.id.imagebutton_paint_redo:
                redoPainting();
                break;
            case R.id.imagebutton_info:
//                showInfoDialog();
                showInfoActivity();
                break;
            case R.id.imagebutton_go_to_gallery:
                Intent intentGallery = new Intent(Exterior2V2Activity.this,
                        GalleryV2Activity.class);
                intentGallery.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intentGallery);
                break;
        }
    }

    @Override
    public void onColorSelected(PaintColor paintColor) {
        isColorClicked = true;

        Float[] rgb = ImageUtils.hexToRgb(paintColor.getColorCode());

        redScale = rgb[0];
        greenScale = rgb[1];
        blueScale = rgb[2];
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
            viewSelectedColor.setBackground(Utility.getCustomShape(Color.parseColor(paintColor.getColorCode())));
        } else {
            viewSelectedColor.setBackgroundDrawable(Utility.getCustomShape(Color.parseColor(paintColor.getColorCode())));
        }
        textViewSelectedColorName.setText(paintColor.getName());
        textViewSelectedColorCode.setText(paintColor.getColorCode());
    }

    @Override
    public void finish() {
        ((Global) getApplication()).getGlobalPaintColorList().clear();
        super.finish();
    }

    private void showInfoActivity() {
        AppLog.showLog(CLASS_TAG, "size of used color : " + usedColorList.size());
        if (usedColorList == null || usedColorList.size() <= 0) {
            Toast.makeText(this, "Info unavailable. Paint first.", Toast.LENGTH_SHORT)
                    .show();
            return;
        }
//        Intent infoActivity = new Intent(this, PaintInfoActivity.class);
        PaintInfoFragment fragment = new PaintInfoFragment();
        Bundle bundle = new Bundle();
        bundle.putParcelableArrayList("USED_COLOR_LIST", (ArrayList<PaintColor>) usedColorList);
        fragment.setArguments(bundle);
        fragment.show(getSupportFragmentManager(), "Paint Info");
//        infoActivity.putExtras(bundle);
//        startActivity(infoActivity);
    }
    private void redoPainting() {
        isDonePainting = false;
        if (redoColorHistoryList.size() - 1 < 0) {
            Toast.makeText(this, "redo unavailable.", Toast.LENGTH_SHORT)
                    .show();
        } else {
            int maxIndex = redoColorHistoryList.size() - 1;
            undoColorHistoryList.add(redoColorHistoryList.get(maxIndex));
            PaintColorHistory paintColorHistory = redoColorHistoryList.get(maxIndex);
            redoColorHistoryList.remove(maxIndex);
            Float[] colorScale = paintColorHistory.getColorCode();
            int imageId = paintColorHistory.getImageViewId();
            switch (imageId) {
                case R.id.iv_exterior2_top:
                    if (colorScale[0] == 0 && colorScale[1] == 0 && colorScale[2] == 0) {
                        ImageUtils.loadImageUsingLoader(imageLoader, imageViewTop,
                                "assets://" + AppText.EXTERIOR2_LOCATION_TOP);
                        updatePaintStatus(new int[]{0, 0, 0, R.id.iv_exterior2_top});
                    } else {
                        int[] colors = ImageUtils.undoPainting(this, imageViewTop, AppText.EXTERIOR2_LOCATION_TOP,
                                colorScale);
                        updatePaintStatus(colors);
                    }
                    break;
                case R.id.iv_exterior2_bottom:
                    if (colorScale[0] == 0 && colorScale[1] == 0 && colorScale[2] == 0) {
                        ImageUtils.loadImageUsingLoader(imageLoader, imageViewBottom,
                                "assets://" + AppText.EXTERIOR2_LOCATION_BOTTOM);
                        updatePaintStatus(new int[]{0, 0, 0, R.id.iv_exterior2_bottom});
                    } else {
                        int[] colors = ImageUtils.undoPainting(this, imageViewBottom, AppText.EXTERIOR2_LOCATION_BOTTOM,
                                colorScale);
                        updatePaintStatus(colors);
                    }
                    break;
            }
        }
    }

    private void refreshPaintStatus() {
        isColorClicked = false;
        redScale = 1f;
        greenScale = 0f;
        blueScale = 0f;
        usedColorList.clear();
        undoColorHistoryList.clear();
        redoColorHistoryList.clear();

        viewSelectedColor.setBackgroundDrawable(new ColorDrawable(android.R.color.transparent));
        textViewSelectedColorCode.setText("");
        textViewSelectedColorName.setText("");
        ImageUtils.loadImageUsingLoader(imageLoader, imageViewMain, "assets://" + AppText.EXTERIOR2_LOCATION_MAIN_IMAGE);
        ImageUtils.loadImageUsingLoader(imageLoader, imageViewTop, "assets://" + AppText.EXTERIOR2_LOCATION_TOP);
        ImageUtils.loadImageUsingLoader(imageLoader, imageViewBottom, "assets://" + AppText.EXTERIOR2_LOCATION_BOTTOM);
        globalPaintColorList.clear();
        ((TabletColorHorizontalListAdapter)gridViewSelectedColors.getAdapter()).notifyDataSetChanged();
    }

    private void undoPainting() {
        isDonePainting = false;
        if (undoColorHistoryList.size() <= 0) {
            Toast.makeText(this, "undo unavailable.", Toast.LENGTH_SHORT)
                    .show();
        } else {
            int maxIndex = undoColorHistoryList.size() - 1;
            redoColorHistoryList.add(undoColorHistoryList.get(maxIndex));
            undoColorHistoryList.remove(maxIndex);
            maxIndex = undoColorHistoryList.size() - 1;
            if (maxIndex < 0) {
                Toast.makeText(this, "undo unavailable.", Toast.LENGTH_SHORT)
                        .show();
                return;
            }
            PaintColorHistory paintColorHistory = undoColorHistoryList.get(maxIndex);
            Float[] colorScale = paintColorHistory.getColorCode();
            switch (paintColorHistory.getImageViewId()) {
                case R.id.iv_exterior2_top:
                    if (colorScale[0] == 0 && colorScale[1] == 0 && colorScale[2] == 0) {
                        ImageUtils.loadImageUsingLoader(imageLoader, imageViewTop,
                                "assets://" + AppText.EXTERIOR2_LOCATION_TOP);
                        updatePaintStatus(new int[]{0, 0, 0, R.id.iv_exterior2_top});
                    } else {
                        int[] colors = ImageUtils.undoPainting(this, imageViewTop, AppText.EXTERIOR2_LOCATION_TOP,
                                colorScale);
                        updatePaintStatus(colors);
                    }
                    break;
                case R.id.iv_exterior2_bottom:
                    if (colorScale[0] == 0 && colorScale[1] == 0 && colorScale[2] == 0) {
                        ImageUtils.loadImageUsingLoader(imageLoader, imageViewBottom,
                                "assets://" + AppText.EXTERIOR2_LOCATION_BOTTOM);
                        updatePaintStatus(new int[]{0, 0, 0, R.id.iv_exterior2_bottom});
                    } else {
                        int[] colors = ImageUtils.undoPainting(this, imageViewBottom, AppText.EXTERIOR2_LOCATION_BOTTOM,
                                colorScale);
                        updatePaintStatus(colors);
                    }
                    break;
            }
        }
    }
    private void updatePaintStatus(int[] colors) {
        int ivId = colors[3];
        if (ivId == R.id.iv_exterior2_top) {
            addToUndo(ivId);
            topRed = colors[0];
            topGreen = colors[1];
            topBlue = colors[2];

            if (topRed == 0 && topGreen == 0 && topBlue == 0) {
                Iterator<PaintColor> iterator = usedColorList.iterator();
                while (iterator.hasNext()) {
                    PaintColor paintColor = iterator.next();
                    if ("Top Wall".equalsIgnoreCase(paintColor.getTypeWall())) {
                        iterator.remove();
                    }
                }
                return;
            }

            String topColorHash = ImageUtils.RgbToHex(topRed, topGreen, topBlue);
            String topColorName = null;
            for (PaintColor paintColor : global.globalPaintColorList) {
                if (topColorHash.equalsIgnoreCase(paintColor.getColorCode())) {
                    topColorName = paintColor.getName();
                }
            }

            PaintColor paintColor = new PaintColor();
            paintColor.setTypeWall("Top Wall");
            paintColor.setColorCode(topColorHash);
            paintColor.setName(topColorName);

            Iterator<PaintColor> iterator = usedColorList.iterator();
            while (iterator.hasNext()) {
                PaintColor paintColor1 = iterator.next();
                if ("Top Wall".equalsIgnoreCase(paintColor1.getTypeWall())) {
                    iterator.remove();
                }
            }
            usedColorList.add(paintColor);
        } else if (ivId == R.id.iv_exterior2_bottom) {
            addToUndo(ivId);
            bottomRed = colors[0];
            bottomGreen = colors[1];
            bottomBlue = colors[2];

            if (bottomRed == 0 && bottomGreen == 0 && bottomBlue == 0) {
                Iterator<PaintColor> iterator = usedColorList.iterator();
                while (iterator.hasNext()) {
                    PaintColor paintColor = iterator.next();
                    if ("Bottom Wall".equalsIgnoreCase(paintColor.getTypeWall())) {
                        iterator.remove();
                    }
                }
                return;
            }

            String bottomColorHash = ImageUtils.RgbToHex(bottomRed, bottomGreen, bottomBlue);
            String bottomColorName = null;
            for (PaintColor paintColor : global.globalPaintColorList) {
                if (bottomColorHash.equalsIgnoreCase(paintColor.getColorCode())) {
                    bottomColorName = paintColor.getName();
                }
            }

            PaintColor paintColor = new PaintColor();
            paintColor.setTypeWall("Bottom Wall");
            paintColor.setColorCode(bottomColorHash);
            paintColor.setName(bottomColorName);

            Iterator<PaintColor> iterator = usedColorList.iterator();
            while (iterator.hasNext()) {
                PaintColor paintColor1 = iterator.next();
                if ("Bottom Wall".equalsIgnoreCase(paintColor1.getTypeWall())) {
                    iterator.remove();
                }
            }
            usedColorList.add(paintColor);
        }
    }
    private void addToUndo(int imageId) {
        if (isDonePainting) {
            boolean isFirstTimePainted = true;
            for (PaintColorHistory paintColorHistory : undoColorHistoryList) {
                if (paintColorHistory.getImageViewId() == imageId) {
                    isFirstTimePainted = false;
                    AppLog.showLog(CLASS_TAG, "is first time painted:" + false);
                    break;
                } else {
                    isFirstTimePainted = true;
                    AppLog.showLog(CLASS_TAG, "is first time painted:" + true);
                }
            }
            if (isFirstTimePainted) {
                PaintColorHistory defaultPaintColor = new PaintColorHistory();
                defaultPaintColor.setImageViewId(imageId);
                defaultPaintColor.setColorCode(new Float[]{0f, 0f, 0f});
                undoColorHistoryList.add(defaultPaintColor);

                PaintColorHistory paintColorHistory = new PaintColorHistory();
                paintColorHistory.setColorCode(new Float[]{redScale, greenScale, blueScale});
                paintColorHistory.setImageViewId(imageId);
                undoColorHistoryList.add(paintColorHistory);
                AppLog.showLog(CLASS_TAG, "first time painted: undo size " + undoColorHistoryList.size());
            } else {
                int maxIndex = undoColorHistoryList.size() - 1;
                PaintColorHistory paintColorHistory1 = undoColorHistoryList.get(maxIndex);
                AppLog.showLog(CLASS_TAG, "is id same:" + (paintColorHistory1.getImageViewId() != imageId));
                AppLog.showLog(CLASS_TAG, "is color same:" + (paintColorHistory1.getColorCode()[0] != redScale
                        && paintColorHistory1.getColorCode()[1] != greenScale
                        && paintColorHistory1.getColorCode()[2] != blueScale));
                if ((paintColorHistory1.getImageViewId() != imageId)
                        || (paintColorHistory1.getColorCode()[0] != redScale
                        || paintColorHistory1.getColorCode()[1] != greenScale
                        || paintColorHistory1.getColorCode()[2] != blueScale)) {
                    PaintColorHistory paintColorHistory = new PaintColorHistory();
                    paintColorHistory.setColorCode(new Float[]{redScale, greenScale, blueScale});
                    paintColorHistory.setImageViewId(imageId);
                    undoColorHistoryList.add(paintColorHistory);
                    AppLog.showLog(CLASS_TAG, "not first time painted: undo size " + undoColorHistoryList.size());
                }
            }
        }
    }

    @Override
    public void onViewTouch(PaintColor paintColor) {
        sendToParent(paintColor);
    }

    private class ChoiceDragListener implements View.OnDragListener {

        @Override
        public boolean onDrag(View v, DragEvent event) {
            switch (event.getAction()) {
                case DragEvent.ACTION_DRAG_STARTED:
                    //no action necessary
                    break;
                case DragEvent.ACTION_DRAG_ENTERED:
                    //no action necessary
                    break;
                case DragEvent.ACTION_DRAG_EXITED:
                    //no action necessary
                    break;
                case DragEvent.ACTION_DROP:
                    int[] colors = ImageUtils.startPainting(getApplicationContext(), ImageUtils.EXTERIOR_TEMPLATE_2,
                            (int) event.getX(), (int) event.getY(), imageViewMain, parent,
                            new float[]{redScale, greenScale, blueScale}, imageViewTop, imageViewBottom);
                    isDonePainting = true;
                    updatePaintStatus(colors);

                    //handle the dragged view being dropped over a drop view
                    View view = (View) event.getLocalState();

                    //stop displaying the view where it was before it was dragged
                    ////view.setVisibility(View.INVISIBLE);

                    //view dragged item is being dropped on
                    ImageView dropTarget = (ImageView) v;

                    //view being dragged and dropped
//                    RelativeLayout dropped = (RelativeLayout) view;


                    //if an item has already been dropped here, there will be a tag
                    Object tag = dropTarget.getTag();


                    //set the tag in the target view being dropped on - to the ID of the view being dropped
                    dropTarget.setTag(view.getId());

                    break;
                case DragEvent.ACTION_DRAG_ENDED:
                    //no action necessary
                    break;
                default:
                    break;
            }

            return true;
        }
    }
    private final class ChoiceTouchListener implements View.OnTouchListener {

        @Override
        public boolean onTouch(View v, MotionEvent event) {

            if (event.getAction() == MotionEvent.ACTION_DOWN) {
                GridView parent = (GridView) v;

                int x = (int) event.getX();
                int y = (int) event.getY();

                int position = parent.pointToPosition(x, y);
                if (position > AdapterView.INVALID_POSITION) {

                    int count = parent.getChildCount();

                    AppLog.showLog(CLASS_TAG, "x: " + x + " y:: " + y + " position:: "
                            + position + " count:: " + count);

                    PaintColor paintColor = (PaintColor) gridViewSelectedColors.getAdapter().getItem(position);
                    AppLog.showLog(CLASS_TAG, "grid selected item:: " + paintColor.getName());

                    sendToParent(paintColor);

                    for (int i = 0; i < count; i++) {
                        View view = parent.getChildAt(i);

                        ClipData data = ClipData.newPlainText("color", paintColor.getColorCode());
                        ColorDragShadowBuilder shadowBuilder = new ColorDragShadowBuilder(paintColor.getColorCode());
                        //start dragging the item touched
                        view.startDrag(data, shadowBuilder, view, 0);

                        return true;
                    }
                }
            }
            return false;
        }
    }
}
