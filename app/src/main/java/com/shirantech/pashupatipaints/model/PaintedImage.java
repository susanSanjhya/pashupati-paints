package com.shirantech.pashupatipaints.model;

import java.util.List;

public class PaintedImage {

    private Long id;
    private String name;
    private String location;
    private String date;
    private List<WallDescription> wallDescriptionList;

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public List<WallDescription> getWallDescriptionList() {
        return wallDescriptionList;
    }

    public void setWallDescriptionList(List<WallDescription> wallDescriptionList) {
        this.wallDescriptionList = wallDescriptionList;
    }
}
