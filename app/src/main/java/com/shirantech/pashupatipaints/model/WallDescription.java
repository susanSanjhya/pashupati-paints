package com.shirantech.pashupatipaints.model;

/**
 * Created by susan on 12/26/14.
 */
public class WallDescription {

    private String wallType;
    private String colorName;
    private String colorCode;

    public WallDescription(String wallType, String colorName, String colorCode) {
        this.wallType = wallType;
        this.colorName = colorName;
        this.colorCode = colorCode;
    }

    public WallDescription() {

    }

    public String getWallType() {
        return wallType;
    }

    public void setWallType(String wallType) {
        this.wallType = wallType;
    }

    public String getColorCode() {
        return colorCode;
    }

    public void setColorCode(String colorCode) {
        this.colorCode = colorCode;
    }

    public String getColorName() {
        return colorName;
    }

    public void setColorName(String colorName) {
        this.colorName = colorName;
    }
}
