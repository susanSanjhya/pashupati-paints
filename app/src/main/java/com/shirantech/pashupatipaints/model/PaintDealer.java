package com.shirantech.pashupatipaints.model;

import android.os.Parcel;
import android.os.Parcelable;

public class PaintDealer implements Parcelable {

    private Long id;
    private String dealerName;
    private String city;
    private String district;
    private String telephone;
    private String dealerCategory;
    private String searchValue;

    public PaintDealer() {
    }

    public PaintDealer(Long id, String dealerName, String city, String district, String telephone, String dealerCategory, String searchValue) {
        this.id = id;
        this.dealerName = dealerName;
        this.city = city;
        this.district = district;
        this.telephone = telephone;
        this.dealerCategory = dealerCategory;
        this.searchValue = searchValue;
    }

    public PaintDealer(Parcel in) {
        this.dealerName = in.readString();
        this.telephone = in.readString();
        this.city = in.readString();
        this.district = in.readString();
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getDealerName() {
        return dealerName;
    }

    public void setDealerName(String dealerName) {
        this.dealerName = dealerName;
    }

    public String getDistrict() {
        return district;
    }

    public void setDistrict(String district) {
        this.district = district;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getTelephone() {
        return telephone;
    }

    public void setTelephone(String telephone) {
        this.telephone = telephone;
    }

    public String getDealerCategory() {
        return dealerCategory;
    }

    public void setDealerCategory(String dealerCategory) {
        this.dealerCategory = dealerCategory;
    }

    public String getSearchValue() {
        return searchValue;
    }

    public void setSearchValue(String searchValue) {
        this.searchValue = searchValue;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(dealerName);
        dest.writeString(telephone);
        dest.writeString(city);
        dest.writeString(district);
    }

    public static final Creator<PaintDealer> CREATOR = new Creator<PaintDealer>() {
        @Override
        public PaintDealer createFromParcel(Parcel source) {
            return new PaintDealer(source);
        }

        @Override
        public PaintDealer[] newArray(int size) {
            return new PaintDealer[size];
        }
    };
}
