package com.shirantech.pashupatipaints.model;

/**
 * Created by susan on 12/24/14.
 */
public class PaintColorHistory {
    int imageViewId;
    Float[] colorCode;

    public int getImageViewId() {
        return imageViewId;
    }

    public void setImageViewId(int imageViewId) {
        this.imageViewId = imageViewId;
    }

    public Float[] getColorCode() {
        return colorCode;
    }

    public void setColorCode(Float[] colorCode) {
        this.colorCode = colorCode;
    }
}
