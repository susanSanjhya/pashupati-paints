package com.shirantech.pashupatipaints.model;

import android.widget.EditText;

/**
 * Created by susan on 1/8/15.
 */
public class PaintCalculatorField {
    private EditText editTextHeight, editTextWidth;

    public PaintCalculatorField(EditText editTextHeight, EditText editTextWidth) {
        this.editTextHeight = editTextHeight;
        this.editTextWidth = editTextWidth;
    }

    public EditText getEditTextHeight() {
        return editTextHeight;
    }

    public void setEditTextHeight(EditText editTextHeight) {
        this.editTextHeight = editTextHeight;
    }

    public EditText getEditTextWidth() {
        return editTextWidth;
    }

    public void setEditTextWidth(EditText editTextWidth) {
        this.editTextWidth = editTextWidth;
    }
}
