package com.shirantech.pashupatipaints.model;

/**
 * Created by susan on 2/13/15.
 */
public class PaintCalculatorType {
    private int productsDrawable;
    private int coveragePerSqFt;

    public PaintCalculatorType(int productsDrawable, int coveragePerSqFt) {
        this.productsDrawable = productsDrawable;
        this.coveragePerSqFt = coveragePerSqFt;
    }

    public int getProductsDrawable() {
        return productsDrawable;
    }

    public void setProductsDrawable(int productsDrawable) {
        this.productsDrawable = productsDrawable;
    }

    public int getCoveragePerSqFt() {
        return coveragePerSqFt;
    }

    public void setCoveragePerSqFt(int coveragePerSqFt) {
        this.coveragePerSqFt = coveragePerSqFt;
    }
}
