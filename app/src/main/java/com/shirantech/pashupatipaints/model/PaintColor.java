package com.shirantech.pashupatipaints.model;

import android.os.Parcel;
import android.os.Parcelable;

public class PaintColor implements Parcelable {

    private Long id;
    private String categoryId;
    private String name;
    private String colorCode;
    private String typeWall;
    private String paintCode;

    public PaintColor() {

    }

    public PaintColor(Long id, String categoryId, String name, String colorCode, String typeWall) {
        this.id = id;
        this.categoryId = categoryId;
        this.name = name;
        this.colorCode = colorCode;
        this.typeWall = typeWall;
    }

    public PaintColor(Long id, String categoryId, String name, String colorCode, String typeWall, String paintCode) {
        this.id = id;
        this.categoryId = categoryId;
        this.name = name;
        this.colorCode = colorCode;
        this.typeWall = typeWall;
        this.paintCode = paintCode;
    }

    public PaintColor(Parcel in) {
        this.name = in.readString();
        this.colorCode = in.readString();
        this.typeWall = in.readString();
    }

    public String getPaintCode() {
        return paintCode;
    }

    public void setPaintCode(String paintCode) {
        this.paintCode = paintCode;
    }

    public String getTypeWall() {
        return typeWall;
    }

    public void setTypeWall(String typeWall) {
        this.typeWall = typeWall;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(String categoryId) {
        this.categoryId = categoryId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getColorCode() {
        return colorCode;
    }

    public void setColorCode(String colorCode) {
        this.colorCode = colorCode;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                            dest.writeString(name);
        dest.writeString(colorCode);
        dest.writeString(typeWall);
    }

    public static final Creator<PaintColor> CREATOR = new Creator<PaintColor>() {
        @Override
        public PaintColor createFromParcel(Parcel source) {
            return new PaintColor(source);
        }

        @Override
        public PaintColor[] newArray(int size) {
            return new PaintColor[size];
        }
    };
}
