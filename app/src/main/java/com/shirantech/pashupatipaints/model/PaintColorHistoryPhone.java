package com.shirantech.pashupatipaints.model;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by susan on 4/2/15.
 */
public class PaintColorHistoryPhone implements Parcelable {
    private int imageViewId;
    private float[] colorCode;

    public PaintColorHistoryPhone() {

    }

    public PaintColorHistoryPhone(int imageViewId, float[] colorCode) {
        this.imageViewId = imageViewId;
        this.colorCode = colorCode;
    }

    public PaintColorHistoryPhone(Parcel in) {
        this.imageViewId = in.readInt();
        this.colorCode = in.createFloatArray();
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(imageViewId);
        dest.writeFloatArray(colorCode);
    }

    public static final Creator<PaintColorHistoryPhone> CREATOR = new Creator<PaintColorHistoryPhone>() {
        @Override
        public PaintColorHistoryPhone createFromParcel(Parcel source) {
            return new PaintColorHistoryPhone(source);
        }

        @Override
        public PaintColorHistoryPhone[] newArray(int size) {
            return new PaintColorHistoryPhone[size];
        }
    };
}
