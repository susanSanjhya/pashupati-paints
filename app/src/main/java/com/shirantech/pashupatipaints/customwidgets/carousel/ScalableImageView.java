package com.shirantech.pashupatipaints.customwidgets.carousel;
/*
 * 3D carousel View
 * http://www.pocketmagic.net 
 *
 * Copyright (c) 2013 by Radu Motisan , radu.motisan@gmail.com
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 * For more information on the GPL, please go to:
 * http://www.gnu.org/copyleft/gpl.html
 *
 */

import android.content.Context;
import android.util.AttributeSet;
import android.widget.ImageView;

public class ScalableImageView extends ImageView {
    int m_w,
            m_h;

    public ScalableImageView(Context context) {
        super(context);
    }

    public ScalableImageView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public ScalableImageView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
    }

    public ScalableImageView(Context context, int w, int h) {
        super(context);
        m_w = w;
        m_h = h;
    }

    public int getM_w() {
        return m_w;
    }

    public void setM_w(int m_w) {
        this.m_w = m_w;
    }

    public int getM_h() {
        return m_h;
    }

    public void setM_h(int m_h) {
        this.m_h = m_h;
    }

    public int getMaxW() {
        return m_w;
    }

    public int getMaxH() {
        return m_h;
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);
        this.setMeasuredDimension(m_w, m_h);
    }
}
