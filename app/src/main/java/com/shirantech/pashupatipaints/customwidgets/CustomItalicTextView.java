package com.shirantech.pashupatipaints.customwidgets;

import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.widget.TextView;

/**
 * Created by susan on 1/8/15.
 */
public class CustomItalicTextView extends TextView {
    public CustomItalicTextView(Context context) {
        super(context);
        init();
    }

    public CustomItalicTextView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public CustomItalicTextView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init();
    }

    private void init() {
        Typeface typeface = Typeface.createFromAsset(getResources().getAssets(), "fonts/MyriadPro-It.otf");
        setTypeface(typeface);
    }

}
