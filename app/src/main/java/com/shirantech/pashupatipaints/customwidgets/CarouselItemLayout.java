package com.shirantech.pashupatipaints.customwidgets;

import android.content.Context;
import android.graphics.Canvas;
import android.util.AttributeSet;
import android.widget.LinearLayout;

import com.shirantech.pashupatipaints.activities.list.StarterV2Activity;

/**
 * Created by susan on 7/8/15.
 */
public class CarouselItemLayout extends LinearLayout{
    private float scale = StarterV2Activity.BIG_SCALE;

    public CarouselItemLayout(Context context, AttributeSet attrs){
        super(context, attrs);
    }

    public CarouselItemLayout(Context context){
        super(context);
    }

    public void setScaleBoth(float scale){
        this.scale = scale;
        this.invalidate();
    }

    @Override
    protected void onDraw(Canvas canvas) {
        int w = this.getWidth();
        int h = this.getHeight();
        canvas.scale(scale, scale, w/2 , h/2);
        super.onDraw(canvas);
    }
}
