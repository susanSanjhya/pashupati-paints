package com.shirantech.pashupatipaints.customwidgets;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Point;
import android.support.annotation.NonNull;
import android.view.View;

import com.shirantech.pashupatipaints.R;

public class ColorDragShadowBuilder extends View.DragShadowBuilder {

    //private Bitmap image; // image to draw as a drag shadow
    private String paintColor;
    private Context context;

    public ColorDragShadowBuilder(String _paintColor) {
        super();
        paintColor = _paintColor;
    }

    public ColorDragShadowBuilder(Context context, String _paintColor) {
        super();
        this.context = context;
        paintColor = _paintColor;
    }

    public void onDrawShadow(@NonNull Canvas canvas) {
        //canvas.drawBitmap(image, 0, 0, null);
        canvas.drawColor(Color.parseColor(paintColor));
    }

    public void onProvideShadowMetrics(@NonNull Point shadowSize, @NonNull Point shadowTouchPoint) {
        shadowSize.x = context.getResources().getInteger(R.integer.shadow_size);
        shadowSize.y = context.getResources().getInteger(R.integer.shadow_size);
        shadowTouchPoint.x = shadowSize.x / 2;
        shadowTouchPoint.y = shadowSize.y / 2;
    }
}
