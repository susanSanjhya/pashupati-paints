package com.shirantech.pashupatipaints.customwidgets;

import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.widget.Button;

/**
 * Created by susan on 1/8/15.
 */
public class CustomBoldButton extends Button {
    public CustomBoldButton(Context context) {
        super(context);
        init();
    }

    public CustomBoldButton(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public CustomBoldButton(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init();
    }

    private void init() {
        Typeface typeface = Typeface.createFromAsset(getResources().getAssets(), "fonts/Roboto-Medium.ttf");
        setTypeface(typeface);
    }
}
