package com.shirantech.pashupatipaints.customwidgets;

import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.widget.TextView;

/**
 * Created by susan on 12/12/14.
 */
public class CustomThinTextView extends TextView {
    public CustomThinTextView(Context context) {
        super(context);
        init();
    }

    public CustomThinTextView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public CustomThinTextView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init();
    }

    private void init() {
        Typeface typeface = Typeface.createFromAsset(getContext().getAssets(), "fonts/Roboto-Thin.ttf");
        setTypeface(typeface);
    }
}
