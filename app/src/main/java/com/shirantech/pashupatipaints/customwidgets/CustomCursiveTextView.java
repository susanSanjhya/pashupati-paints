package com.shirantech.pashupatipaints.customwidgets;

import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.widget.TextView;

/**
 * Created by susan on 2/26/15.
 */
public class CustomCursiveTextView extends TextView {
    public CustomCursiveTextView(Context context) {
        super(context);
        init();
    }

    public CustomCursiveTextView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public CustomCursiveTextView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init();
    }

    private void init() {
        Typeface typeFace = Typeface.createFromAsset(getContext().getAssets(), "fonts/DancingScript-Bold.otf");
        setTypeface(typeFace);
    }


}
