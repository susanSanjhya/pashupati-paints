package com.shirantech.pashupatipaints.fragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;

import com.actionbarsherlock.app.SherlockListFragment;
import com.shirantech.pashupatipaints.R;
import com.shirantech.pashupatipaints.adapter.DealersListAdapter;
import com.shirantech.pashupatipaints.controller.GetDealerTask;
import com.shirantech.pashupatipaints.interfaces.ModelListGetter;
import com.shirantech.pashupatipaints.model.PaintDealer;
import com.shirantech.pashupatipaints.util.AppLog;

import java.util.List;

public class DealersKathmanduFragment extends SherlockListFragment implements ModelListGetter<PaintDealer> {

    private static final String CLASS_TAG = DealersKathmanduFragment.class.getSimpleName();
    private ProgressBar pb;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.list_dealers, container, false);

        pb = (ProgressBar) view.findViewById(R.id.progressBar);

//        new Async().execute();
        new GetDealerTask(getActivity(), this).execute("Kathmandu");
        return view;
    }

    public void showView(List<PaintDealer> dealerList) {

        DealersListAdapter dealersListAdapter = new DealersListAdapter(getActivity(), dealerList);
        setListAdapter(dealersListAdapter);
    }

    @Override
    public void onTaskStarted() {
        pb.setVisibility(View.VISIBLE);
    }

    @Override
    public void onTaskFinished(List<PaintDealer> dealerList) {
        pb.setVisibility(View.GONE);
        AppLog.showLog(CLASS_TAG, "KTM dealers list size:: " + dealerList.size());
        if (dealerList.size() > 0) {
            showView(dealerList);
        } else {
            AppLog.showLog(CLASS_TAG, "dealerList for Category1 is null.");
        }
    }

    /*public class Async extends AsyncTask<Void, Void, Void> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            pb.setVisibility(View.VISIBLE);
        }

        @Override
        protected Void doInBackground(Void... params) {
            DBAdapter dbAdapter = new DBAdapter(getActivity());

            //DEALERS FROM KTM LIST GET
            dealerList = dbAdapter.getAllDealers("KTM");
            System.out.println("Dealers List ktm size:: " + dealerList.size());

            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            super.onPostExecute(result);
            pb.setVisibility(View.GONE);
            System.out.println("KTM dealers list size:: " + dealerList.size());
            if (dealerList != null || dealerList.size() > 0) {
                showView();
            } else {
                Log.e(this.getClass().getColorName(), "dealerList for Category1 is null.");
            }

        }

    }*/
}
