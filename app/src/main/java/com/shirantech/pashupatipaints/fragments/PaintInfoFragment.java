package com.shirantech.pashupatipaints.fragments;

import android.app.Dialog;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.LinearLayout;
import android.widget.ListView;

import com.shirantech.pashupatipaints.R;
import com.shirantech.pashupatipaints.adapter.UsedColorAdapter;
import com.shirantech.pashupatipaints.model.PaintColor;
import com.shirantech.pashupatipaints.util.AppLog;

import java.util.List;

/**
 * Created by susan on 4/2/15.
 */
@SuppressWarnings("deprecation")
public class PaintInfoFragment extends DialogFragment {

    private static final String CLASS_TAG = PaintInfoFragment.class.getSimpleName();
    private List<PaintColor> usedColorList;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Bundle bundle = getArguments();
        usedColorList = bundle.getParcelableArrayList("USED_COLOR_LIST");
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        Dialog dialog = super.onCreateDialog(savedInstanceState);
        dialog.getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        final Drawable d = new ColorDrawable(Color.WHITE);
//        d.setAlpha(130);
        dialog.getWindow().setBackgroundDrawable(d);
        dialog.setCanceledOnTouchOutside(false);
        return dialog;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.activity_paint_info, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        LinearLayout linearLayoutContainer = (LinearLayout) view.findViewById(R.id.linear_layout_container);
        linearLayoutContainer.setMinimumWidth((int) (getScreenWidth() - (2 * getResources().getDimension(R.dimen.dialog_margin))));
        ListView listViewUsedColors = (ListView) view.findViewById(R.id.list_view_used_colors);

        for (PaintColor paintColor : usedColorList) {
            AppLog.showLog(CLASS_TAG, "paint color name : " + paintColor.getName());
        }
        listViewUsedColors.setAdapter(new UsedColorAdapter(getActivity(), usedColorList));
        view.findViewById(R.id.image_button_delete).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getDialog().dismiss();
            }
        });
    }

    private float getScreenWidth() {
        DisplayMetrics displaymetrics = new DisplayMetrics();
        getActivity().getWindowManager().getDefaultDisplay().getMetrics(displaymetrics);
        return displaymetrics.widthPixels;
    }
}
