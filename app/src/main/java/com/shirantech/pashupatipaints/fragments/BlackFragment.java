package com.shirantech.pashupatipaints.fragments;

import android.app.Activity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.ProgressBar;

import com.actionbarsherlock.app.SherlockFragment;
import com.shirantech.pashupatipaints.R;
import com.shirantech.pashupatipaints.adapter.ColorPickerAdapter;
import com.shirantech.pashupatipaints.adapter.GetColorListTask;
import com.shirantech.pashupatipaints.interfaces.ModelListGetter;
import com.shirantech.pashupatipaints.model.PaintColor;
import com.shirantech.pashupatipaints.util.AppLog;

import java.util.List;

/**
 * Created by susan on 2/24/15.
 */
public class BlackFragment extends SherlockFragment implements ModelListGetter<PaintColor> {

    private static final String CLASS_TAG = BlackFragment.class.getSimpleName();
    private GridView gridViewColors;
    private ProgressBar pb_color;
    private onPaintSwatchSelectedListener listener;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_yellow, container, false);
        gridViewColors = (GridView) view.findViewById(R.id.gridViewColors_yellow);
        pb_color = (ProgressBar) view.findViewById(R.id.progressBar_color_yellow);
        new GetColorListTask(getActivity(), this).execute("12.0");
        return view;
    }

    public void showView(List<PaintColor> paintColorList) {

        gridViewColors.setAdapter(new ColorPickerAdapter(getActivity(), paintColorList));
        gridViewColors.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                PaintColor paintColor = (PaintColor) gridViewColors.getAdapter().getItem(position);
                AppLog.showLog(CLASS_TAG, "grid selected item:: " + paintColor.getName());
                sendToParent(paintColor);

            }
        });
    }

    @Override
    public void onTaskStarted() {
        pb_color.setVisibility(View.VISIBLE);
    }

    @Override
    public void onTaskFinished(List<PaintColor> paintColorList) {
        AppLog.showLog(CLASS_TAG, "paint color size:: yellow Color-chooser:: " + paintColorList.size());
        if (paintColorList.size() > 0) {
            pb_color.setVisibility(View.GONE);
            showView(paintColorList);
        } else {
            pb_color.setVisibility(View.GONE);
            AppLog.showLog(CLASS_TAG, "Color-list for yellow is null.");
        }
    }

    public interface onPaintSwatchSelectedListener {
        public void onPaintSelected(PaintColor paintColor);
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        if (activity instanceof onPaintSwatchSelectedListener) {
            listener = (onPaintSwatchSelectedListener) activity;
        } else {
            throw new ClassCastException(activity.toString() + " must implement YellowFragment.onPaintSwatchSelectedListener");
        }

    }

    public void sendToParent(PaintColor paintColor) {
        //interface being triggered from activity also
        listener.onPaintSelected(paintColor);
    }

}
