package com.shirantech.pashupatipaints.fragments;

import android.app.Activity;
import android.content.ClipData;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnTouchListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.GridView;
import android.widget.ProgressBar;

import com.actionbarsherlock.app.SherlockFragment;
import com.shirantech.pashupatipaints.R;
import com.shirantech.pashupatipaints.adapter.ColorPickerAdapter;
import com.shirantech.pashupatipaints.controller.GetGlobalColorListTask;
import com.shirantech.pashupatipaints.customwidgets.ColorDragShadowBuilder;
import com.shirantech.pashupatipaints.interfaces.ModelListGetter;
import com.shirantech.pashupatipaints.model.PaintColor;
import com.shirantech.pashupatipaints.util.AppLog;
import com.shirantech.pashupatipaints.util.Global;

import java.util.List;

public class SelectedColorsFragment extends SherlockFragment implements ModelListGetter<PaintColor> {

    private static final String CLASS_TAG = SelectedColorsFragment.class.getSimpleName();
    private GridView gridViewSelectedColors;
    private ProgressBar pb_color;

    private onSelectedColorSelectedListener listener;

    Global global;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_selected_colors, container, false);
        gridViewSelectedColors = (GridView) view.findViewById(R.id.gridViewSelectedColors);
        pb_color = (ProgressBar) view.findViewById(R.id.progressBar_selected_colors);
        global = (Global) getActivity().getApplication();
        new GetGlobalColorListTask(global, this).execute();
        return view;
    }

    public void showView(List<PaintColor> globalColorList) {
        gridViewSelectedColors.setAdapter(new ColorPickerAdapter(getActivity(), globalColorList));
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.HONEYCOMB) {
            gridViewSelectedColors.setOnTouchListener(new ChoiceTouchListener());
        }
        gridViewSelectedColors.setOnItemClickListener(new OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                PaintColor paintColor = (PaintColor) gridViewSelectedColors.getAdapter().getItem(position);
                AppLog.showLog(CLASS_TAG, "grid selected item:: " + paintColor.getName());
                sendToParent(paintColor);
            }
        });

    }

    @Override
    public void onTaskStarted() {
        pb_color.setVisibility(View.VISIBLE);
    }

    @Override
    public void onTaskFinished(List<PaintColor> globalColorList) {
        if (globalColorList != null && globalColorList.size() > 0) {
            pb_color.setVisibility(View.GONE);
            showView(globalColorList);
        } else {
            AppLog.showLog(CLASS_TAG, "Global color list is null.");
            pb_color.setVisibility(View.GONE);
        }
    }

    /**
     * ChoiceTouchListener will handle touch events on draggable views
     */
    private final class ChoiceTouchListener implements OnTouchListener {

        @Override
        public boolean onTouch(View v, MotionEvent event) {

            if (event.getAction() == MotionEvent.ACTION_DOWN) {
                GridView parent = (GridView) v;

                int x = (int) event.getX();
                int y = (int) event.getY();

                int position = parent.pointToPosition(x, y);
                if (position > AdapterView.INVALID_POSITION) {

                    int count = parent.getChildCount();

                    AppLog.showLog(CLASS_TAG, "x: " + x + " y:: " + y + " position:: "
                            + position + " count:: " + count);

                    PaintColor paintColor = (PaintColor) gridViewSelectedColors.getAdapter().getItem(position);
                    AppLog.showLog(CLASS_TAG, "grid selected item:: " + paintColor.getName());

                    sendToParent(paintColor);

                    for (int i = 0; i < count; i++) {
                        View view = parent.getChildAt(i);

                        ClipData data = ClipData.newPlainText("color", paintColor.getColorCode());
                        ColorDragShadowBuilder shadowBuilder = new ColorDragShadowBuilder(getActivity(), paintColor.getColorCode());
                        //start dragging the item touched
                        view.startDrag(data, shadowBuilder, view, 0);

                        return true;
                    }
                }
            }
            return false;
        }
    }

    public interface onSelectedColorSelectedListener {
        public void onColorSelected(PaintColor paintColor);
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        if (activity instanceof onSelectedColorSelectedListener) {
            listener = (onSelectedColorSelectedListener) activity;
        } else {
            throw new ClassCastException(activity.toString() + " must implement SelectedColorsFragment.onSelectedColorSelectedListener");
        }

    }

    public void sendToParent(PaintColor paintColor) {
        //interface being triggered from activity also
        AppLog.showLog(CLASS_TAG, "selected paint color code:" + paintColor.getColorCode());
        listener.onColorSelected(paintColor);
    }

}
