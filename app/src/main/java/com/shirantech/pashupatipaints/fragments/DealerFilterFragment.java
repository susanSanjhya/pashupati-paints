package com.shirantech.pashupatipaints.fragments;

import android.app.Activity;
import android.app.Dialog;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.shirantech.pashupatipaints.R;
import com.shirantech.pashupatipaints.interfaces.OnFilterChangeListener;

/**
 * Created by susan on 12/14/14.
 */
public class DealerFilterFragment extends DialogFragment implements View.OnClickListener {
    private static final String CLASS_TAG = DealerFilterFragment.class.getSimpleName();
    private OnFilterChangeListener listener;
    private String toSearch;
    private EditText editTextSearch;

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        getDialog().getWindow().setTitle("Filter By");
        return inflater.inflate(R.layout.fragment_dealer_filter, container, false);
    }


    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        Dialog dialog = super.onCreateDialog(savedInstanceState);
        dialog.getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        ColorDrawable drawable = new ColorDrawable(Color.WHITE);
        dialog.getWindow().setBackgroundDrawable(drawable);
        dialog.setCanceledOnTouchOutside(false);
        return dialog;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        LinearLayout linearLayoutContainer = (LinearLayout) view.findViewById(R.id.linear_layout_container);
        linearLayoutContainer.setMinimumWidth((int) (getScreenWidth() - getResources().getDimension(R.dimen.filter_dialog_margin)));
        Button buttonFilter = (Button) view.findViewById(R.id.button_filter);
        buttonFilter.setOnClickListener(this);
        editTextSearch = (EditText) view.findViewById(R.id.edit_text_search);
        ImageButton buttonCancel = (ImageButton) view.findViewById(R.id.image_button_delete);
        buttonCancel.setOnClickListener(this);
    }

    private float getScreenWidth() {
        DisplayMetrics displaymetrics = new DisplayMetrics();
        getActivity().getWindowManager().getDefaultDisplay().getMetrics(displaymetrics);
        return displaymetrics.widthPixels;
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        if (activity instanceof OnFilterChangeListener) {
            listener = (OnFilterChangeListener) activity;
        } else {
            throw new ClassCastException(activity.toString() + " must implement OnFilterChangeListener");
        }
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.button_filter:
                if (editTextSearch.getText().toString().equals("")) {
                    Toast.makeText(getActivity(), "Search text is empty", Toast.LENGTH_SHORT)
                            .show();
                    return;
                }
                toSearch = editTextSearch.getText().toString();
                listener.onFilterFinished(toSearch);
                getDialog().dismiss();
                break;
            case R.id.image_button_delete:
                getDialog().dismiss();
                break;
        }
    }
}
