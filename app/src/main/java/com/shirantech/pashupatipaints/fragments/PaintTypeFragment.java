package com.shirantech.pashupatipaints.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.shirantech.pashupatipaints.R;

/**
 * Created by susan on 3/22/15.
 */
public class PaintTypeFragment extends Fragment {
    private int position;
    private int imageResource;
    private ImageView imageViewPaint;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        position = getArguments().getInt("POSITION");
        imageResource = getArguments().getInt("IMAGE_RESOURCE");
    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_paint_type, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        imageViewPaint = (ImageView) view.findViewById(R.id.image_view_color_type);
        imageViewPaint.setImageResource(imageResource);
    }
}
