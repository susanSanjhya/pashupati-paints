package com.shirantech.pashupatipaints.fragments;

import android.app.Dialog;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.shirantech.pashupatipaints.R;
import com.shirantech.pashupatipaints.model.PaintDealer;
import com.shirantech.pashupatipaints.util.AppLog;

/**
 * Created by susan on 4/3/15.
 */
public class DealerInfoFragment extends DialogFragment {

    private static final String CLASS_TAG = DealerInfoFragment.class.getSimpleName();
    private PaintDealer paintDealer;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        paintDealer = getArguments().getParcelable("DEALER_DETAIL");
        AppLog.showLog(CLASS_TAG, "ID: " + paintDealer.getId()
                + "\nName: " + paintDealer.getDealerName());
    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.dealer_info, container, false);
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        Dialog dialog = super.onCreateDialog(savedInstanceState);
        dialog.getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        final Drawable d = new ColorDrawable(Color.WHITE);
//        d.setAlpha(130);
        dialog.getWindow().setBackgroundDrawable(d);
        dialog.setCanceledOnTouchOutside(false);
        return dialog;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        LinearLayout linearLayoutContainer = (LinearLayout) view.findViewById(R.id.linear_layout_container);
        linearLayoutContainer.setMinimumWidth((int) (getScreenWidth() - (2 * getResources().getDimension(R.dimen.dialog_margin))));
        TextView textViewName = (TextView) view.findViewById(R.id.tv_name);
        TextView textViewPhone1 = (TextView) view.findViewById(R.id.tv_phone_1);
        TextView textViewPhone2 = (TextView) view.findViewById(R.id.tv_phone_2);
        TextView textViewCity = (TextView) view.findViewById(R.id.tv_city);
        TextView textViewDistrict = (TextView) view.findViewById(R.id.tv_district);
        AppLog.showLog(CLASS_TAG, "onViewCreated:Name:" + paintDealer.getDealerName());
        textViewName.setText(String.format("%s",paintDealer.getDealerName()));
//        textViewName.setText("BARAL SUPPLIERS (AAMOD HARDWARE)");
//        textViewName.setText("BAL BINAYAK HARDWARE CENTER");
        textViewCity.setText(paintDealer.getCity());
        textViewDistrict.setText(paintDealer.getDistrict());
        String[] telephones = paintDealer.getTelephone().split("/");
        for (String telephone : telephones) {
            AppLog.showLog(CLASS_TAG, telephone);
        }
        textViewPhone1.setText(telephones[0]);
        if (telephones.length == 1) {
            textViewPhone2.setVisibility(View.GONE);
        } else {
            textViewPhone2.setText(telephones[1]);
        }

        view.findViewById(R.id.image_button_delete).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getDialog().dismiss();
            }
        });
    }

    private float getScreenWidth() {
        DisplayMetrics displaymetrics = new DisplayMetrics();
        getActivity().getWindowManager().getDefaultDisplay().getMetrics(displaymetrics);
        return displaymetrics.widthPixels;
    }
}
