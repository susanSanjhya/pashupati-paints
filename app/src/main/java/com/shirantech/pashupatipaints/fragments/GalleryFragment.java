package com.shirantech.pashupatipaints.fragments;

import android.app.Activity;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.shirantech.pashupatipaints.R;
import com.shirantech.pashupatipaints.customwidgets.CarouselItemLayout;
import com.shirantech.pashupatipaints.util.AppLog;
import com.shirantech.pashupatipaints.util.ImageUtils;
import com.shirantech.pashupatipaints.util.Utility;

/**
 * Created by susan on 7/9/15.
 */
public class GalleryFragment extends Fragment implements View.OnClickListener, View.OnLongClickListener {

    private static final java.lang.String CLASS_TAG = GalleryFragment.class.getCanonicalName();
    private String paintName;
    private String paintDate;
    private int position;
    private float scale;
    private ImageLoader imageLoader = ImageLoader.getInstance();
    private int rlContainerHeight, rlContainerWidth;
    private OnGalleryClickListener listener;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        paintName = getArguments().getString("paint_name");
        AppLog.showLog(CLASS_TAG, "Name: " + paintName);
        paintDate = getArguments().getString("paint_date");
        AppLog.showLog(CLASS_TAG, "Date: " + paintDate);
        position = getArguments().getInt("position");
        scale = getArguments().getFloat("scale");
        imageLoader.init(ImageLoaderConfiguration.createDefault(getActivity()));
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        if (container == null) {
            return null;
        }

        return inflater.inflate(R.layout.carousel_gallery_item_new, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
//        final RelativeLayout rlContainer = (RelativeLayout) getActivity().findViewById(R.id.rl_gallery_view_pager_container);
//        rlContainer.post(new Runnable() {
//            @Override
//            public void run() {
//                rlContainerHeight = rlContainer.getHeight();
//                rlContainerWidth = rlContainer.getWidth();
//            }
//        });

        TextView tv = (TextView) view.findViewById(R.id.text);
        tv.setText(paintDate);
//        ((ImageView) l.findViewById(R.id.image_view)).setImageResource(drawables[position]);
        ImageView imageViewThumbNail = (ImageView) view.findViewById(R.id.image_view);

        RelativeLayout rlImageContainer = (RelativeLayout) view.findViewById(R.id.rl_gallery_image_container);
        CarouselItemLayout.LayoutParams  rllp = (CarouselItemLayout.LayoutParams) rlImageContainer.getLayoutParams();
        rllp.width = rlContainerWidth - 200;
        rllp.height = rlContainerHeight - 200;

        RelativeLayout.LayoutParams ivlp = (RelativeLayout.LayoutParams) imageViewThumbNail.getLayoutParams();
        AppLog.showLog(CLASS_TAG, "Rl container width: " + rlContainerWidth);
        AppLog.showLog(CLASS_TAG, "Rl container height: " + rlContainerHeight);
        ivlp.width = Utility.getWidthOfScreen(getActivity()) - getActivity().getResources().getInteger(R.integer.gallery_image_difference);
//        ivlp.height = rlContainerHeight - 200;

        ImageUtils.loadImageUsingLoader(imageLoader,
                imageViewThumbNail, "file:///mnt/sdcard/PashupatiPaints/"
                        + paintName + ".png");
        CarouselItemLayout root = (CarouselItemLayout) view.findViewById(R.id.root);
        root.setScaleBoth(scale);
        root.setOnClickListener(this);
        root.setOnLongClickListener(this);
    }



    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        if(activity instanceof OnGalleryClickListener){
            listener = (OnGalleryClickListener) activity;
        } else {
            throw new ClassCastException(activity.getClass().getSimpleName() + " must implement " + OnGalleryClickListener.class.getSimpleName());
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.root:
                listener.onGalleryClicked(position);
                break;
        }
    }

    @Override
    public boolean onLongClick(View v) {
        switch (v.getId()){
            case R.id.root:
                listener.onGalleryLongClicked(position);
                break;
        }
        return false;
    }

    public interface OnGalleryClickListener{
        void onGalleryClicked(int position);
        void onGalleryLongClicked(int position);
    }
}
