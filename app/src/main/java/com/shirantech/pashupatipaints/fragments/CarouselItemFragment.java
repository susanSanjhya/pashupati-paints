package com.shirantech.pashupatipaints.fragments;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.shirantech.pashupatipaints.R;
import com.shirantech.pashupatipaints.activities.list.DealersDetailActivity;
import com.shirantech.pashupatipaints.activities.list.GalleryV2Activity;
import com.shirantech.pashupatipaints.activities.list.PainingWaysActivity;
import com.shirantech.pashupatipaints.activities.list.PaintCalculatorTabletActivity;
import com.shirantech.pashupatipaints.activities.list.StarterV2Activity;
import com.shirantech.pashupatipaints.customwidgets.CarouselItemLayout;

/**
 * Created by susan on 7/8/15.
 */
public class CarouselItemFragment extends Fragment implements View.OnClickListener {
    private int position;
    private float scale;
    private int pagerPosition;
    private int[] drawables = new int[]{
            R.drawable.start_painting,
            R.drawable.paint_calculator,
            R.drawable.ic_gallery,
            R.drawable.authorized_dealers
    };

    private String[] menu = new String[]{
            "START\nPAINTING",
            "PAINT\nCALCULATOR",
            "GALLERY",
            "AUTHORIZED\nDEALERS"
    };

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        position = getArguments().getInt("position");
        scale = getArguments().getFloat("scale");
        pagerPosition = getArguments().getInt("pager_position");
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        if (container == null) {
            return null;
        }

        LinearLayout l = (LinearLayout) inflater.inflate(R.layout.carousel_item_new, container, false);
        TextView tv = (TextView) l.findViewById(R.id.text);
        tv.setText(menu[position]);
        ((ImageView) l.findViewById(R.id.image_view)).setImageResource(drawables[position]);
        CarouselItemLayout root = (CarouselItemLayout) l.findViewById(R.id.root);

        root.setScaleBoth(scale);
        root.setOnClickListener(this);
        return l;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.root:
                switch (position) {
                    case 0:
                        ((StarterV2Activity) getActivity()).pager.setCurrentItem(pagerPosition, true);
                        /*Toast.makeText(getActivity(), "start painting", Toast.LENGTH_SHORT)
                                .show();*/
                        Intent paintingWaysIntent = new Intent(getActivity(), PainingWaysActivity.class);

                        startActivity(paintingWaysIntent);
                        break;
                    case 1:
                        ((StarterV2Activity) getActivity()).pager.setCurrentItem(pagerPosition, true);
                       /* Toast.makeText(getActivity(), "paint calculator", Toast.LENGTH_SHORT)
                                .show();*/
                        Intent paintCalculatorIntent = new Intent(getActivity(), PaintCalculatorTabletActivity.class);
                        startActivity(paintCalculatorIntent);
                        break;
                    case 2:
                        ((StarterV2Activity) getActivity()).pager.setCurrentItem(pagerPosition, true);
                        /*Toast.makeText(getActivity(), "gallery", Toast.LENGTH_SHORT)
                                .show();*/
                        Intent galleryIntent = new Intent(getActivity(), GalleryV2Activity.class);

                        startActivity(galleryIntent);
                        break;
                    case 3:
                        ((StarterV2Activity) getActivity()).pager.setCurrentItem(pagerPosition, true);
                       /* Toast.makeText(getActivity(), "Authorized Dealer", Toast.LENGTH_SHORT)
                                .show();*/
                        Intent dealersDetailIntent = new Intent(getActivity(), DealersDetailActivity.class);

                        startActivity(dealersDetailIntent);
                        break;
                }
                break;
        }
    }
}
