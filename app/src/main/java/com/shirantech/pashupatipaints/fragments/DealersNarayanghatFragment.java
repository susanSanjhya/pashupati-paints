package com.shirantech.pashupatipaints.fragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;

import com.actionbarsherlock.app.SherlockListFragment;
import com.shirantech.pashupatipaints.R;
import com.shirantech.pashupatipaints.adapter.DealersListAdapter;
import com.shirantech.pashupatipaints.controller.GetDealerTask;
import com.shirantech.pashupatipaints.interfaces.ModelListGetter;
import com.shirantech.pashupatipaints.model.PaintDealer;
import com.shirantech.pashupatipaints.util.AppLog;

import java.util.List;

public class DealersNarayanghatFragment extends SherlockListFragment implements ModelListGetter<PaintDealer> {

    private static final String CLASS_TAG = DealersNarayanghatFragment.class.getSimpleName();
    private ProgressBar pb;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.list_dealers, container, false);

        pb = (ProgressBar) view.findViewById(R.id.progressBar);

        new GetDealerTask(getActivity(), this).execute("Narayanghat");
        return view;
    }

    public void showView(List<PaintDealer> dealerList) {

        DealersListAdapter dealersListAdapter = new DealersListAdapter(getActivity(), dealerList);
        setListAdapter(dealersListAdapter);

    }

    @Override
    public void onTaskStarted() {
        pb.setVisibility(View.VISIBLE);
    }

    @Override
    public void onTaskFinished(List<PaintDealer> dealerList) {
        pb.setVisibility(View.GONE);
        AppLog.showLog(CLASS_TAG, "Narayanghat dealers list size:: " + dealerList.size());
        if (dealerList.size() > 0) {
            showView(dealerList);
        } else {
            AppLog.showLog(CLASS_TAG, "dealerList for Narayanghat is null.");
        }
    }
}
