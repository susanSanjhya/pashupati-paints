package com.shirantech.pashupatipaints.fragments;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.ProgressBar;

import com.actionbarsherlock.app.SherlockListFragment;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.shirantech.pashupatipaints.R;
import com.shirantech.pashupatipaints.activities.list.ColorChooserActivity;
import com.shirantech.pashupatipaints.adapter.RoomCategoryListAdapter;
import com.shirantech.pashupatipaints.controller.GetRoomCategoriesTask;
import com.shirantech.pashupatipaints.interfaces.ModelListGetter;
import com.shirantech.pashupatipaints.model.RoomCategory;
import com.shirantech.pashupatipaints.util.AppLog;

import java.util.List;

public class KitchensFragment extends SherlockListFragment implements ModelListGetter<RoomCategory> {

    private static final String CLASS_TAG = KitchensFragment.class.getSimpleName();
    private ProgressBar pb_color;
    private String[] drawableArray = new String[]{
            "Kitchen/kitchen1/kitchen1_main.png",
            "Kitchen/kitchen2/kitchen2_main.png",
            "Kitchen/kitchen3/kitchen3_main.png",
            "Kitchen/kitchen4/kitchen4_main.png",
            "Kitchen/kitchen5/kitchen5_main.png",
            "Kitchen/kitchen6/kitchen6_main.png",
            "Dining/dining1/dining1_main.png"
    };
    private ImageLoader imageLoader = ImageLoader.getInstance();

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_kitchens, container, false);
        imageLoader.init(ImageLoaderConfiguration.createDefault(getActivity()));
        pb_color = (ProgressBar) view.findViewById(R.id.progressBar_kitchens);
        new GetRoomCategoriesTask(getActivity(), this, "kitchens").execute(drawableArray);
        return view;
    }

    public void showView(List<RoomCategory> roomCategoryList) {
        RoomCategoryListAdapter adapter = new RoomCategoryListAdapter(getActivity(), imageLoader, roomCategoryList);
        setListAdapter(adapter);
    }

    @Override
    public void onTaskStarted() {
        pb_color.setVisibility(View.VISIBLE);
    }

    @Override
    public void onTaskFinished(List<RoomCategory> roomCategoryList) {
        AppLog.showLog(CLASS_TAG, "Room color size:: Rooms:: " + roomCategoryList.size());
        if (roomCategoryList.size() > 0) {
            pb_color.setVisibility(View.GONE);
            showView(roomCategoryList);
        } else {
            pb_color.setVisibility(View.GONE);
            AppLog.showLog(CLASS_TAG, "Rooms for Category1 is null.");
        }
    }

    @Override
    public void onListItemClick(ListView parent, View view, int position, long id) {
        Intent colorChooserIntent = new Intent(getActivity(), ColorChooserActivity.class);
        colorChooserIntent.putExtra("COLOR CHOOSER SOURCE", "STARTER");
        colorChooserIntent.putExtra("TYPE", "kitchen");
        switch (position) {
            case 0:
                AppLog.showLog(CLASS_TAG, "kitchen position 0 clicked");
                colorChooserIntent.putExtra("POSITION", position);
                break;

            case 1:
                AppLog.showLog(CLASS_TAG, "kitchen position 1 clicked");
                colorChooserIntent.putExtra("POSITION", position);
                break;

            case 2:
                AppLog.showLog(CLASS_TAG, "kitchen position 2 clicked");
                colorChooserIntent.putExtra("POSITION", position);
                break;

            case 3:
                AppLog.showLog(CLASS_TAG, "kitchen position 3 clicked");
                colorChooserIntent.putExtra("POSITION", position);
                break;
            case 4:
                AppLog.showLog(CLASS_TAG, "kitchen position 4 clicked");
                colorChooserIntent.putExtra("POSITION", position);
                break;
            case 5:
                AppLog.showLog(CLASS_TAG, "kitchen position 5 clicked");
                colorChooserIntent.putExtra("POSITION", position);
                break;
            case 6:
                AppLog.showLog(CLASS_TAG, "kitchen position 6 clicked");
                colorChooserIntent.putExtra("POSITION", position);
                break;
            default:
                break;
        }
        startActivity(colorChooserIntent);
    }
}
