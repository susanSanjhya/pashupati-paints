package com.shirantech.pashupatipaints.animatedViewPager;

import android.content.res.Resources;
import android.util.TypedValue;

public class EpaperUtil {
    private static final String TAG = EpaperUtil.class.getName();

	public static int dpToPx(Resources res, int dp) {
		return (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, dp, res.getDisplayMetrics());
	}
	
	
	public static class Config {
		public static final boolean DEVELOPER_MODE = false;
	}

}
