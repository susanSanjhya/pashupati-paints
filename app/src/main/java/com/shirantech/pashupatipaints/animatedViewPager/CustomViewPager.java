package com.shirantech.pashupatipaints.animatedViewPager;

import android.content.Context;
import android.support.v4.view.ViewPager;
import android.util.AttributeSet;
import android.view.MotionEvent;

import com.shirantech.pashupatipaints.util.AppLog;


public class CustomViewPager extends ViewPager {
	private boolean enabled;   // = true;

		
	
		public CustomViewPager(Context context) {
			super(context);
		}	
	    
	
		public CustomViewPager(Context context, AttributeSet attrs) {
	        super(context, attrs);
	        this.enabled = true;
	    }
	    
	    public void setPagingEnabled(boolean enabled) {
	        this.enabled = enabled;
	    }

	    
	    @Override
	    public boolean onInterceptTouchEvent(MotionEvent event) {
	    	AppLog.showLog("CustomViewPager ::onInterceptTouchEvent isPagingEnabled::" + enabled);
	    	
	    	if (enabled) {
	            try {
	                return super.onInterceptTouchEvent(event);
	            } catch (Exception e) { // sometimes happens
	            	e.printStackTrace();
	                return true;
	            }
	        } else {
	        	return false;
	        }
	    }
	    
	    @Override
	    public boolean onTouchEvent(MotionEvent event) {
	    	AppLog.showLog("CustomViewPager ::onTouchEvent isPagingEnabled::"+enabled);
	    	
	    	if (enabled) {
	            try {
	                return super.onTouchEvent(event);
	            } catch (Exception e) { // sometimes happens
	            	e.printStackTrace();
	                return true;
	            }
	        } else {
	        return false;
	    }
	    }
		
}
