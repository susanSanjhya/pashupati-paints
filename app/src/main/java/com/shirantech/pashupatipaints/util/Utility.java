package com.shirantech.pashupatipaints.util;

import android.content.Context;
import android.graphics.Point;
import android.graphics.drawable.ShapeDrawable;
import android.graphics.drawable.shapes.RoundRectShape;
import android.os.Build;
import android.os.Environment;
import android.util.DisplayMetrics;
import android.view.Display;
import android.view.WindowManager;

import java.text.SimpleDateFormat;
import java.util.Date;

@SuppressWarnings("deprecation")
public class Utility {

    private static final String CLASS_TAG = Utility.class.getSimpleName();

    public static boolean isExternalStorageReadOnly() {
        String extStorageState = Environment.getExternalStorageState();
        if (Environment.MEDIA_MOUNTED_READ_ONLY.equals(extStorageState)) {
            return true;
        }
        return false;
    }

    public static boolean isExternalStorageAvailable() {
        String extStorageState = Environment.getExternalStorageState();
        if (Environment.MEDIA_MOUNTED.equals(extStorageState)) {
            return true;
        }
        return false;
    }

    public static ShapeDrawable getCustomShape(int color) {
        ShapeDrawable shapeDrawable = new ShapeDrawable();
        float[] radii = new float[8];
        radii[0] = 10.0f;
        radii[1] = 10.0f;
        radii[2] = 10.0f;
        radii[3] = 10.0f;
        radii[4] = 10.0f;
        radii[5] = 10.0f;
        radii[6] = 10.0f;
        radii[7] = 10.0f;

        shapeDrawable.setShape(new RoundRectShape(radii, null, null));
        shapeDrawable.getPaint().setColor(color);
        return shapeDrawable;
    }

    public static String getCurrentDate() {
        return new SimpleDateFormat("MMMM d, yyyy").format(new Date());
    }

    public static boolean isTablet(Context context) {
        try {
            // Compute screen size
            DisplayMetrics dm =
                    context.getResources().getDisplayMetrics();
            float screenWidth = dm.widthPixels / dm.xdpi;
            float screenHeight = dm.heightPixels / dm.ydpi;
            double size = Math.sqrt(Math.pow(screenWidth, 2) +
                    Math.pow(screenHeight, 2));
            // Tablet devices should have a screen size greater than 6 inches
            return size >= 6;
        } catch (Throwable t) {
            AppLog.showLog(CLASS_TAG, "Failed to compute screen size " + t);
            return false;
        }
    }

    public static int getWidthOfScreen(Context context){
        WindowManager wm = (WindowManager) context.getSystemService(Context.WINDOW_SERVICE);
        Display display = wm.getDefaultDisplay();

        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB_MR2){
            Point point = new Point();
            display.getSize(point);
            return point.x;
        } else {
            return display.getWidth();
        }
    }



}
