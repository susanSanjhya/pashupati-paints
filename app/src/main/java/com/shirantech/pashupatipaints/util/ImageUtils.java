package com.shirantech.pashupatipaints.util;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.ComponentName;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.ResolveInfo;
import android.content.res.AssetManager;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.ColorMatrix;
import android.graphics.ColorMatrixColorFilter;
import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffXfermode;
import android.graphics.drawable.BitmapDrawable;
import android.net.Uri;
import android.os.Environment;
import android.provider.MediaStore;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.assist.FailReason;
import com.nostra13.universalimageloader.core.listener.ImageLoadingListener;
import com.shirantech.pashupatipaints.R;
import com.shirantech.pashupatipaints.adapter.CropOptionAdapter;
import com.shirantech.pashupatipaints.database.dao.PaintedImageDao;
import com.shirantech.pashupatipaints.model.CropOption;
import com.shirantech.pashupatipaints.model.PaintColor;
import com.shirantech.pashupatipaints.model.PaintedImage;
import com.shirantech.pashupatipaints.model.WallDescription;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.List;

/**
 * Collection Class for image processing functions.
 * Created by susan on 12/2/14.
 */
public class ImageUtils {
    private static final String CLASS_TAG = ImageUtils.class.getSimpleName();
    public static final int PICk_FROM_CAMERA = 1;
    public static final int CROP_FROM_CAMERA = 2;
    public static final int PICK_FROM_FILE = 3;
    private static float[] colorTransform;
    //Template Class Types
    public static final int EXTERIOR_TEMPLATE_1 = 0;
    public static final int EXTERIOR_TEMPLATE_2 = 1;
    public static final int EXTERIOR_TEMPLATE_3 = 2;
    public static final int LIVINGROOM_TEMPLATE_1 = 3;
    public static final int LIVINGROOM_TEMPLATE_2 = 4;
    public static final int LIVINGROOM_TEMPLATE_3 = 5;
    public static final int LIVINGROOM_TEMPLATE_4 = 6;
    public static final int BEDROOM_TEMPLATE_1 = 7;
    public static final int BEDROOM_TEMPLATE_2 = 8;
    public static final int BEDROOM_TEMPLATE_3 = 9;
    public static final int BEDROOM_TEMPLATE_4 = 10;
    public static final int BEDROOM_TEMPLATE_5 = 11;
    public static final int BEDROOM_TEMPLATE_6 = 12;
    public static final int KITCHEN_TEMPLATE_1 = 13;
    public static final int KITCHEN_TEMPLATE_2 = 14;
    public static final int KITCHEN_TEMPLATE_3 = 15;
    public static final int KITCHEN_TEMPLATE_4 = 16;
    public static final int KITCHEN_TEMPLATE_5 = 17;
    public static final int KITCHEN_TEMPLATE_6 = 18;
    public static final int KITCHEN_TEMPLATE_7 = 19;

    public static void doCrop(final Context context, final Uri mImageCaptureUri) {
        final List<CropOption> cropOptions = new ArrayList<CropOption>();
        Intent intent = new Intent("com.android.camera.action.CROP");
        intent.setType("image/*");
        List<ResolveInfo> resolveInfoList = context.getPackageManager()
                .queryIntentActivities(intent, 0);
        int size = resolveInfoList.size();
        if (size == 0) {
            Toast.makeText(context, "Can not find image crop app", Toast.LENGTH_SHORT).show();
        } else {
            intent.setData(mImageCaptureUri);
            intent.putExtra("outputX", 350);
            intent.putExtra("outputY", 350);
            intent.putExtra("aspectX", 4);
            intent.putExtra("aspectY", 3);
            intent.putExtra("return-data", true);
            intent.putExtra("scaleUpIfNeeded", true);
            if (size == 1) {
                Intent i = new Intent(intent);
                ResolveInfo resolveInfo = resolveInfoList.get(0);
                i.setComponent(new ComponentName(resolveInfo.activityInfo.packageName, resolveInfo.activityInfo.name));
                ((Activity) context).startActivityForResult(i, CROP_FROM_CAMERA);
            } else {
                for (ResolveInfo res : resolveInfoList) {
                    CropOption cropOption = new CropOption();
                    cropOption.title = context.getPackageManager()
                            .getApplicationLabel(res.activityInfo.applicationInfo);
                    cropOption.icon = context.getPackageManager()
                            .getApplicationIcon(res.activityInfo.applicationInfo);
                    cropOption.appIntent = new Intent(intent);
                    cropOption.appIntent.setComponent(new ComponentName(res.activityInfo.packageName, res.activityInfo.name));
                    cropOptions.add(cropOption);
                }
                CropOptionAdapter adapter = new CropOptionAdapter(context, cropOptions);
                AlertDialog.Builder builder = new AlertDialog.Builder(context);
                builder.setTitle("Choose Crop App");
                builder.setAdapter(adapter, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int position) {
                        ((Activity) context).startActivityForResult(cropOptions.get(position).appIntent, CROP_FROM_CAMERA);
                    }
                });
                builder.setOnCancelListener(new DialogInterface.OnCancelListener() {
                    @Override
                    public void onCancel(DialogInterface dialogInterface) {
                        if (mImageCaptureUri != null) {
                            context.getContentResolver().delete(mImageCaptureUri, null, null);
                        }
                    }
                });
                builder.create();
                builder.show();
            }
        }

    }

    public static void shareTemplateImage(Context context, Global global,
                                          int classType, View viewToBeShared, int... colors) {
        Bitmap bm1;
        viewToBeShared.setDrawingCacheEnabled(true);
        viewToBeShared.buildDrawingCache();
        bm1 = viewToBeShared.getDrawingCache();

        try {
            String path = Environment.getExternalStorageDirectory()
                    .toString();
            File imgDirectory = new File("/sdcard/PashupatiPaints/");
            imgDirectory.mkdirs();
            OutputStream fOut;
            File file;
            String fileId = System.currentTimeMillis() + "";
            String fileLocation = "/PashupatiPaints/" + fileId + ".png";
            file = new File(path, fileLocation);


            saveTemplateImageDetailsToDb(context, global, classType, fileId, fileLocation, colors);

            String path1 = MediaStore.Images.Media.insertImage(context.getContentResolver(),
                    bm1, "title", null);
            Uri screenShotUri = Uri.parse(path1);

            fOut = new FileOutputStream(file);
            bm1.compress(Bitmap.CompressFormat.PNG, 100, fOut);
            fOut.flush();
            fOut.close();

            final Intent shareIntent = new Intent(
                    Intent.ACTION_SEND);
            shareIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            shareIntent.putExtra(Intent.EXTRA_STREAM, screenShotUri);
            shareIntent.setType("image/png");

            context.startActivity(Intent.createChooser(shareIntent,
                    context.getResources().getString(R.string.text_share_heading)));

        } catch (Exception e) {
            e.printStackTrace();
            Toast.makeText(context, "can't be saved",
                    Toast.LENGTH_SHORT).show();
        }

    }

    public static void saveTemplateImage(Context context, Global global, int classType, View viewToBeSaved,
                                         int... colors) {
        Bitmap bm;
        viewToBeSaved.setDrawingCacheEnabled(true);
        viewToBeSaved.buildDrawingCache();
        bm = viewToBeSaved.getDrawingCache();

        try {
            String path = Environment.getExternalStorageDirectory()
                    .toString();
            File imgDirectory = new File("/sdcard/PashupatiPaints/");
            imgDirectory.mkdirs();
            OutputStream fOut;
            File file;
            String fileId = System.currentTimeMillis() + "";
            String fileLocation = "/PashupatiPaints/" + fileId + ".png";
            file = new File(path, fileLocation);


            fOut = new FileOutputStream(file);
            bm.compress(Bitmap.CompressFormat.PNG, 100, fOut);
            fOut.flush();
            fOut.close();
            saveTemplateImageDetailsToDb(context, global, classType, fileId, fileLocation, colors);
            Toast.makeText(context, "Image Saved",
                    Toast.LENGTH_SHORT).show();

        } catch (Exception e) {
            e.printStackTrace();
            Toast.makeText(context, "can't be saved",
                    Toast.LENGTH_SHORT).show();
        }
    }

    private static void saveTemplateImageDetailsToDb(Context context, Global global, int classType, String fileId, String fileLocation,
                                                     int... colors) {
        AppLog.showLog(CLASS_TAG, "saving image details to database!");
        PaintedImage paintImage = new PaintedImage();
        switch (classType) {
            // front
            case EXTERIOR_TEMPLATE_1:
                paintImage = saveTemplateExterior1ToDb(global, fileId, fileLocation, colors);
                break;
            case EXTERIOR_TEMPLATE_2:
                paintImage = saveTemplateExterior2ToDb(global, fileId, fileLocation, colors);
                break;
            case EXTERIOR_TEMPLATE_3:
                paintImage = saveTemplateExterior3ToDb(context, global, fileId, fileLocation, colors);
                break;
            case LIVINGROOM_TEMPLATE_1:
                paintImage = saveTemplateLivingroom1ToDb(global, fileId, fileLocation, colors);
                break;
            case LIVINGROOM_TEMPLATE_2:
                paintImage = saveTemplateLivingroom2ToDb(global, fileId, fileLocation, colors);
                break;
            case LIVINGROOM_TEMPLATE_3:
                paintImage = saveTemplateLivingroom3ToDb(global, fileId, fileLocation, colors);
                break;
            case LIVINGROOM_TEMPLATE_4:
                paintImage = saveTemplateLivingroom4ToDb(global, fileId, fileLocation, colors);
                break;
            case BEDROOM_TEMPLATE_1:
                paintImage = saveTemplateBedRoom1ToDb(global, fileId, fileLocation, colors);
                break;
            case BEDROOM_TEMPLATE_2:
                paintImage = saveTemplateBedRoom2ToDb(global, fileId, fileLocation, colors);
                break;
            case BEDROOM_TEMPLATE_3:
                paintImage = saveTemplateBedRoom3ToDb(global, fileId, fileLocation, colors);
                break;
            case BEDROOM_TEMPLATE_4:
                paintImage = saveTemplateBedRoom4ToDb(global, fileId, fileLocation, colors);
                break;
            case BEDROOM_TEMPLATE_5:
                paintImage = saveTemplateBedRoom5ToDb(global, fileId, fileLocation, colors);
                break;
            case BEDROOM_TEMPLATE_6:
                paintImage = saveTemplateBedRoom6ToDb(global, fileId, fileLocation, colors);
                break;
            case KITCHEN_TEMPLATE_1:
                paintImage = saveTemplateKitchen1ToDb(global, fileId, fileLocation, colors);
                break;
            case KITCHEN_TEMPLATE_2:
                paintImage = saveTemplateKitchen2ToDb(global, fileId, fileLocation, colors);
                break;
            case KITCHEN_TEMPLATE_3:
                paintImage = saveTemplateKitchen3ToDb(global, fileId, fileLocation, colors);
                break;
            case KITCHEN_TEMPLATE_4:
                paintImage = saveTemplateKitchen4ToDb(global, fileId, fileLocation, colors);
                break;
            case KITCHEN_TEMPLATE_5:
                paintImage = saveTemplateKitchen5ToDb(global, fileId, fileLocation, colors);
                break;
            case KITCHEN_TEMPLATE_6:
                paintImage = saveTemplateKitchen6ToDb(global, fileId, fileLocation, colors);
                break;
            case KITCHEN_TEMPLATE_7:
                paintImage = saveTemplateKitchen7ToDb(global, fileId, fileLocation, colors);
                break;
        }

        PaintedImageDao paintedImageDao = new PaintedImageDao(context);
        try {
            if (paintedImageDao.insertRecord(paintImage) > 0) {
                AppLog.showLog(CLASS_TAG, "inserted successfully!");
            } else {
                AppLog.showLog(CLASS_TAG, "cannot be inserted");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private static PaintedImage saveTemplateKitchen7ToDb(Global global, String fileId, String fileLocation, int[] colors) {
        List<PaintColor> globalPaintColorList = global.globalPaintColorList;

        int leftRed = colors[0];
        int leftGreen = colors[1];
        int leftBlue = colors[2];

        int ceilingRed = colors[3];
        int ceilingGreen = colors[4];
        int ceilingBlue = colors[5];

        int rightRed = colors[6];
        int rightGreen = colors[7];
        int rightBlue = colors[8];

        int bottomRed = colors[9];
        int bottomGreen = colors[10];
        int bottomBlue = colors[11];

        int cornerRed = colors[12];
        int cornerGreen = colors[13];
        int cornerBlue = colors[14];
        // leftwall
        String leftColorHash = RgbToHex(leftRed,
                leftGreen, leftBlue);
        String leftColorName = null;
        for (PaintColor aGlobalPaintColorList : globalPaintColorList) {
            if (aGlobalPaintColorList.getColorCode().equalsIgnoreCase(leftColorHash)) {
                leftColorName = aGlobalPaintColorList.getName();
            }
        }

        // Ceiling
        String ceilingColorHash = RgbToHex(ceilingRed, ceilingGreen,
                ceilingBlue);
        String ceilingColorName = null;
        for (PaintColor aGlobalPaintColorList : globalPaintColorList) {
            if (aGlobalPaintColorList.getColorCode().equalsIgnoreCase(ceilingColorHash)) {
                ceilingColorName = aGlobalPaintColorList.getName();
            }
        }

        // rightwall
        String rightColorHash = RgbToHex(rightRed, rightGreen, rightBlue);
        String rightColorName = null;
        for (PaintColor aGlobalPaintColorList : globalPaintColorList) {
            if (aGlobalPaintColorList.getColorCode().equalsIgnoreCase(rightColorHash)) {
                rightColorName = aGlobalPaintColorList.getName();
            }
        }

        //bottom
        String bottomColorHash = RgbToHex(bottomRed, bottomGreen, bottomBlue);
        String bottomColorName = null;
        for (PaintColor aGlobalPaintColorList : globalPaintColorList) {
            if (aGlobalPaintColorList.getColorCode().equalsIgnoreCase(bottomColorHash)) {
                bottomColorName = aGlobalPaintColorList.getName();
            }
        }


        //corner
        String cornerColorHash = RgbToHex(cornerRed, cornerGreen, cornerBlue);
        String cornerColorName = null;
        for (PaintColor aGlobalPaintColorList : globalPaintColorList) {
            if (aGlobalPaintColorList.getColorCode()
                    .equalsIgnoreCase(cornerColorHash)) {
                cornerColorName = aGlobalPaintColorList.getName();
            }
        }


        PaintedImage paintImage = new PaintedImage();
        paintImage.setName(fileId);
        paintImage.setLocation(fileLocation);
        List<WallDescription> wallDescriptionList = new ArrayList<WallDescription>();
        if (leftColorName != null) {
            wallDescriptionList.add(new WallDescription("Left Wall",
                    leftColorName,
                    leftColorHash
            ));
        }

        if (ceilingColorName != null) {
            wallDescriptionList.add(new WallDescription("Ceiling",
                    ceilingColorName,
                    ceilingColorHash
            ));
        }

        if (rightColorName != null) {
            wallDescriptionList.add(new WallDescription("Right Wall",
                    rightColorName,
                    rightColorHash
            ));
        }

        if (bottomColorName != null) {
            wallDescriptionList.add(new WallDescription("Bottom Wall",
                    bottomColorName,
                    bottomColorHash
            ));
        }
        if (cornerColorName != null) {
            wallDescriptionList.add(new WallDescription("Corner",
                    cornerColorName,
                    cornerColorHash
            ));
        }
        paintImage.setWallDescriptionList(wallDescriptionList);
        paintImage.setDate(Utility.getCurrentDate());
        return paintImage;
    }

    private static PaintedImage saveTemplateKitchen6ToDb(Global global, String fileId, String fileLocation, int[] colors) {
        List<PaintColor> globalPaintColorList = global.getGlobalPaintColorList();
        int leftwall_r = colors[0];
        int leftwall_g = colors[1];
        int leftwall_b = colors[2];
        int ceiling_r = colors[3];
        int ceiling_g = colors[4];
        int ceiling_b = colors[5];
        int rightwall_r = colors[6];
        int rightwall_g = colors[7];
        int rightwall_b = colors[8];

        //leftwall
        String leftwallColorHash = RgbToHex(leftwall_r, leftwall_g, leftwall_b);
        String leftwallColorName = null;
        for (PaintColor aGlobalPaintColorList : globalPaintColorList) {

            if (aGlobalPaintColorList.getColorCode().equalsIgnoreCase(leftwallColorHash)) {
                leftwallColorName = aGlobalPaintColorList.getName();
            }
        }

        //Ceiling
        String ceilingColorHash = RgbToHex(ceiling_r, ceiling_g, ceiling_b);
        String ceilingColorName = null;
        for (PaintColor aGlobalPaintColorList : globalPaintColorList) {
            if (aGlobalPaintColorList.getColorCode().equalsIgnoreCase(ceilingColorHash)) {
                ceilingColorName = aGlobalPaintColorList.getName();
            }
        }

        // rightwall
        String righttwallColorHash = RgbToHex(rightwall_r, rightwall_g, rightwall_b);
        String righttwallColorName = null;
        for (PaintColor aGlobalPaintColorList : globalPaintColorList) {
            if (aGlobalPaintColorList.getColorCode().equalsIgnoreCase(righttwallColorHash)) {
                righttwallColorName = aGlobalPaintColorList.getName();
            }
        }

        PaintedImage paintImage = new PaintedImage();
        paintImage.setName(fileId);
        paintImage.setLocation(fileLocation);
        List<WallDescription> wallDescriptionList = new ArrayList<WallDescription>();
        if (leftwallColorName != null) {
            wallDescriptionList.add(new WallDescription("Left Wall",
                    (leftwallColorName),
                    leftwallColorHash));
        }
        if (ceilingColorName != null) {
            wallDescriptionList.add(new WallDescription("Ceiling",
                    (ceilingColorName),
                    ceilingColorHash));
        }
        if (righttwallColorName != null) {
            wallDescriptionList.add(new WallDescription("Right Wall",
                    (righttwallColorName),
                    righttwallColorHash));
        }
        paintImage.setWallDescriptionList(wallDescriptionList);
        paintImage.setDate(Utility.getCurrentDate());
        return paintImage;
    }

    private static PaintedImage saveTemplateKitchen5ToDb(Global global, String fileId, String fileLocation, int[] colors) {

        List<PaintColor> globalPaintColorList = global.getGlobalPaintColorList();
        int leftwall_r = colors[0];
        int leftwall_g = colors[1];
        int leftwall_b = colors[2];
        int ceiling_r = colors[3];
        int ceiling_g = colors[4];
        int ceiling_b = colors[5];
        int rightwall_r = colors[6];
        int rightwall_g = colors[7];
        int rightwall_b = colors[8];
        //leftwall
        String leftwallColorHash = RgbToHex(leftwall_r, leftwall_g, leftwall_b);
        String leftwallColorName = null;
        for (PaintColor aGlobalPaintColorList : globalPaintColorList) {
            if (aGlobalPaintColorList.getColorCode().equalsIgnoreCase(leftwallColorHash)) {
                leftwallColorName = aGlobalPaintColorList.getName();
            }
        }

        //Ceiling
        String ceilingColorHash = RgbToHex(ceiling_r, ceiling_g, ceiling_b);
        String ceilingColorName = null;
        for (PaintColor aGlobalPaintColorList : globalPaintColorList) {
            if (aGlobalPaintColorList.getColorCode().equalsIgnoreCase(ceilingColorHash)) {
                ceilingColorName = aGlobalPaintColorList.getName();
            }
        }

        // rightwall
        String righttwallColorHash = RgbToHex(rightwall_r, rightwall_g, rightwall_b);
        String righttwallColorName = null;
        for (PaintColor aGlobalPaintColorList : globalPaintColorList) {
            if (aGlobalPaintColorList.getColorCode().equalsIgnoreCase(righttwallColorHash)) {
                righttwallColorName = aGlobalPaintColorList.getName();
            }
        }

        PaintedImage paintImage = new PaintedImage();
        paintImage.setName(fileId);
        paintImage.setLocation(fileLocation);
        List<WallDescription> wallDescriptionList = new ArrayList<WallDescription>();
        if (leftwallColorName != null) {
            wallDescriptionList.add(new WallDescription("Left Wall",
                    (leftwallColorName),
                    leftwallColorHash));
        }
        if (ceilingColorName != null) {
            wallDescriptionList.add(new WallDescription("Ceiling",
                    (ceilingColorName),
                    ceilingColorHash));
        }
        if (righttwallColorName != null) {
            wallDescriptionList.add(new WallDescription("Right Wall",
                    (righttwallColorName),
                    righttwallColorHash));
        }
        paintImage.setWallDescriptionList(wallDescriptionList);
        paintImage.setDate(Utility.getCurrentDate());
        return paintImage;
    }

    private static PaintedImage saveTemplateKitchen4ToDb(Global global, String fileId, String fileLocation, int[] colors) {
        List<PaintColor> globalPaintColorList = global.getGlobalPaintColorList();

        int ceiling_r = colors[0];
        int ceiling_g = colors[1];
        int ceiling_b = colors[2];
        int rightwall_r = colors[3];
        int rightwall_g = colors[4];
        int rightwall_b = colors[5];
        //ceiling
        String ceilingColorHash = RgbToHex(ceiling_r, ceiling_g, ceiling_b);
        String ceilingColorName = null;
        for (PaintColor aGlobalPaintColorList : globalPaintColorList) {
            if (aGlobalPaintColorList.getColorCode().equalsIgnoreCase(ceilingColorHash)) {
                ceilingColorName = aGlobalPaintColorList.getName();
            }
        }

        // rightwall
        String righttwallColorHash = RgbToHex(rightwall_r, rightwall_g, rightwall_b);
        String righttwallColorName = null;
        for (PaintColor aGlobalPaintColorList : globalPaintColorList) {
            if (aGlobalPaintColorList.getColorCode().equalsIgnoreCase(righttwallColorHash)) {
                righttwallColorName = aGlobalPaintColorList.getName();
            }
        }

        PaintedImage paintImage = new PaintedImage();
        paintImage.setName(fileId);
        paintImage.setLocation(fileLocation);
        List<WallDescription> wallDescriptionList = new ArrayList<WallDescription>();
        if (ceilingColorName != null) {
            wallDescriptionList.add(new WallDescription("Ceiling",
                    (ceilingColorName),
                    ceilingColorHash));
        }
        if (righttwallColorName != null) {
            wallDescriptionList.add(new WallDescription("Right Wall",
                    (righttwallColorName),
                    righttwallColorHash));
        }
        paintImage.setWallDescriptionList(wallDescriptionList);
        paintImage.setDate(Utility.getCurrentDate());
        return paintImage;
    }

    private static PaintedImage saveTemplateKitchen3ToDb(Global global, String fileId, String fileLocation, int[] colors) {
        List<PaintColor> globalPaintColorList = global.getGlobalPaintColorList();

        int leftwall_r = colors[0];
        int leftwall_g = colors[1];
        int leftwall_b = colors[2];
        int midwall_r = colors[3];
        int midwall_g = colors[4];
        int midwall_b = colors[5];
        int border_ceiling_r = colors[6];
        int border_ceiling_g = colors[7];
        int border_ceiling_b = colors[8];
        int ceiling_r = colors[9];
        int ceiling_g = colors[10];
        int ceiling_b = colors[11];
        int rightwall_r = colors[12];
        int rightwall_g = colors[13];
        int rightwall_b = colors[14];

        //leftwall
        String leftwallColorHash = RgbToHex(leftwall_r, leftwall_g, leftwall_b);
        String leftwallColorName = null;
        for (PaintColor aGlobalPaintColorList : globalPaintColorList) {
            if (aGlobalPaintColorList.getColorCode().equalsIgnoreCase(leftwallColorHash)) {
                leftwallColorName = aGlobalPaintColorList.getName();
            }
        }

        //Midwall
        String midwallColorHash = RgbToHex(midwall_r, midwall_g, midwall_b);
        String midwallColorName = null;
        for (PaintColor aGlobalPaintColorList : globalPaintColorList) {
            if (aGlobalPaintColorList.getColorCode().equalsIgnoreCase(midwallColorHash)) {
                midwallColorName = aGlobalPaintColorList.getName();
            }
        }

        //Ceiling border
        String borderceilingColorHash = RgbToHex(border_ceiling_r, border_ceiling_g, border_ceiling_b);
        String borderceilingColorName = null;
        for (PaintColor aGlobalPaintColorList : globalPaintColorList) {
            if (aGlobalPaintColorList.getColorCode().equalsIgnoreCase(borderceilingColorHash)) {
                borderceilingColorName = aGlobalPaintColorList.getName();
            }
        }

        //Ceiling
        String ceilingColorHash = RgbToHex(ceiling_r, ceiling_g, ceiling_b);
        String ceilingColorName = null;
        for (PaintColor aGlobalPaintColorList : globalPaintColorList) {
            if (aGlobalPaintColorList.getColorCode().equalsIgnoreCase(ceilingColorHash)) {
                ceilingColorName = aGlobalPaintColorList.getName();
            }
        }

        // rightwall
        String righttwallColorHash = RgbToHex(rightwall_r, rightwall_g, rightwall_b);
        String righttwallColorName = null;
        for (PaintColor aGlobalPaintColorList : globalPaintColorList) {
            if (aGlobalPaintColorList.getColorCode().equalsIgnoreCase(righttwallColorHash)) {
                righttwallColorName = aGlobalPaintColorList.getName();
            }
        }

        PaintedImage paintImage = new PaintedImage();
        paintImage.setName(fileId);
        paintImage.setLocation(fileLocation);
        List<WallDescription> wallDescriptionList = new ArrayList<WallDescription>();
        if (leftwallColorName != null) {
            wallDescriptionList.add(new WallDescription("Left Wall",
                    (leftwallColorName),
                    leftwallColorHash));
        }
        if (midwallColorName != null) {
            wallDescriptionList.add(new WallDescription("Mid Wall",
                    (midwallColorName),
                    midwallColorHash));
        }
        if (borderceilingColorName != null) {
            wallDescriptionList.add(new WallDescription("Ceiling Border",
                    (borderceilingColorName),
                    borderceilingColorHash));
        }
        if (ceilingColorName != null) {
            wallDescriptionList.add(new WallDescription("Ceiling",
                    (ceilingColorName),
                    ceilingColorHash));
        }
        if (righttwallColorName != null) {
            wallDescriptionList.add(new WallDescription("Right Wall",
                    (righttwallColorName),
                    righttwallColorHash));
        }
        paintImage.setWallDescriptionList(wallDescriptionList);
        paintImage.setDate(Utility.getCurrentDate());
        return paintImage;
    }

    private static PaintedImage saveTemplateKitchen2ToDb(Global global, String fileId, String fileLocation, int[] colors) {

        List<PaintColor> globalPaintColorList = global.getGlobalPaintColorList();
        int leftwall_r = colors[0];
        int leftwall_g = colors[1];
        int leftwall_b = colors[2];
        int midwall_r = colors[3];
        int midwall_g = colors[4];
        int midwall_b = colors[5];
        int ceiling_r = colors[6];
        int ceiling_g = colors[7];
        int ceiling_b = colors[8];
        int rightwall_r = colors[9];
        int rightwall_g = colors[10];
        int rightwall_b = colors[11];

        //leftwall
        String leftwallColorHash = RgbToHex(leftwall_r, leftwall_g, leftwall_b);
        String leftwallColorName = null;
        for (PaintColor aGlobalPaintColorList : globalPaintColorList) {
            if (aGlobalPaintColorList.getColorCode().equalsIgnoreCase(leftwallColorHash)) {
                leftwallColorName = aGlobalPaintColorList.getName();
            }
        }

        //Midwall
        String midwallColorHash = RgbToHex(midwall_r, midwall_g, midwall_b);
        String midwallColorName = null;
        for (PaintColor aGlobalPaintColorList : globalPaintColorList) {
            if (aGlobalPaintColorList.getColorCode().equalsIgnoreCase(midwallColorHash)) {
                midwallColorName = aGlobalPaintColorList.getName();
            }
        }

        //Ceiling
        String ceilingColorHash = RgbToHex(ceiling_r, ceiling_g, ceiling_b);
        String ceilingColorName = null;
        for (PaintColor aGlobalPaintColorList : globalPaintColorList) {
            if (aGlobalPaintColorList.getColorCode().equalsIgnoreCase(ceilingColorHash)) {
                ceilingColorName = aGlobalPaintColorList.getName();
            }
        }

        // rightwall
        String righttwallColorHash = RgbToHex(rightwall_r, rightwall_g, rightwall_b);
        String righttwallColorName = null;
        for (PaintColor aGlobalPaintColorList : globalPaintColorList) {
            if (aGlobalPaintColorList.getColorCode().equalsIgnoreCase(righttwallColorHash)) {
                righttwallColorName = aGlobalPaintColorList.getName();
            }
        }

        PaintedImage paintImage = new PaintedImage();
        paintImage.setName(fileId);
        paintImage.setLocation(fileLocation);
        List<WallDescription> wallDescriptionList = new ArrayList<WallDescription>();
        if (leftwallColorName != null) {
            wallDescriptionList.add(new WallDescription("Left Wall",
                    (leftwallColorName),
                    leftwallColorHash));
        }
        if (midwallColorName != null) {
            wallDescriptionList.add(new WallDescription("Mid Wall",
                    (midwallColorName),
                    midwallColorHash));
        }
        if (ceilingColorName != null) {
            wallDescriptionList.add(new WallDescription("Ceiling",
                    (ceilingColorName),
                    ceilingColorHash));
        }
        if (righttwallColorName != null) {
            wallDescriptionList.add(new WallDescription("Right Wall",
                    (righttwallColorName),
                    righttwallColorHash));
        }
        paintImage.setWallDescriptionList(wallDescriptionList);
        paintImage.setDate(Utility.getCurrentDate());
        return paintImage;
    }

    private static PaintedImage saveTemplateKitchen1ToDb(Global global, String fileId, String fileLocation, int[] colors) {
        List<PaintColor> globalPaintColorList = global.getGlobalPaintColorList();
        int leftwall_r = colors[0];
        int leftwall_g = colors[1];
        int leftwall_b = colors[2];
        int midwall_r = colors[3];
        int midwall_g = colors[4];
        int midwall_b = colors[5];

        //leftwall
        String leftwallColorHash = RgbToHex(leftwall_r, leftwall_g, leftwall_b);
        String leftwallColorName = null;
        for (PaintColor aGlobalPaintColorList : globalPaintColorList) {
            if (aGlobalPaintColorList.getColorCode().equalsIgnoreCase(leftwallColorHash)) {
                leftwallColorName = aGlobalPaintColorList.getName();
            }
        }

        // rightwall
        String righttwallColorHash = RgbToHex(midwall_r, midwall_g, midwall_b);
        String righttwallColorName = null;
        for (PaintColor aGlobalPaintColorList : globalPaintColorList) {
            if (aGlobalPaintColorList.getColorCode().equalsIgnoreCase(righttwallColorHash)) {
                righttwallColorName = aGlobalPaintColorList.getName();
            }
        }

        PaintedImage paintImage = new PaintedImage();

        paintImage.setName(fileId);
        paintImage.setLocation(fileLocation);
        List<WallDescription> wallDescriptionList = new ArrayList<WallDescription>();
        if (leftwallColorName != null) {
            wallDescriptionList.add(new WallDescription("Left Wall",
                    (leftwallColorName),
                    leftwallColorHash));
        }
        if (righttwallColorName != null) {
            wallDescriptionList.add(new WallDescription("Right Wall",
                    (righttwallColorName),
                    righttwallColorHash));
        }
        paintImage.setWallDescriptionList(wallDescriptionList);
        paintImage.setDate(Utility.getCurrentDate());
        return paintImage;
    }

    private static PaintedImage saveTemplateBedRoom6ToDb(Global global, String fileId, String fileLocation, int[] colors) {
        List<PaintColor> globalPaintColorList = global.getGlobalPaintColorList();
        int iv_leftwall_r = colors[0];
        int iv_leftwall_g = colors[1];
        int iv_leftwall_b = colors[2];
        int iv_corner_r = colors[3];
        int iv_corner_g = colors[4];
        int iv_corner_b = colors[5];
        int iv_front_wall_r = colors[6];
        int iv_front_wall_g = colors[7];
        int iv_front_wall_b = colors[8];
        //leftwall
        String leftwallColorHash = RgbToHex(iv_leftwall_r, iv_leftwall_g, iv_leftwall_b);
        String leftwallColorName = null;
        for (PaintColor aGlobalPaintColorList : globalPaintColorList) {
            if (aGlobalPaintColorList.getColorCode().equalsIgnoreCase(leftwallColorHash)) {
                leftwallColorName = aGlobalPaintColorList.getName();
            }
        }

        //corner
        String cornerColorHash = RgbToHex(iv_corner_r, iv_corner_g, iv_corner_b);
        String cornerColorName = null;
        for (PaintColor aGlobalPaintColorList : globalPaintColorList) {
            if (aGlobalPaintColorList.getColorCode().equalsIgnoreCase(cornerColorHash)) {
                cornerColorName = aGlobalPaintColorList.getName();
            }
        }

        // front wall
        String frontwallColorHash = RgbToHex(iv_front_wall_r, iv_front_wall_g, iv_front_wall_b);
        String frontwallColorName = null;
        for (PaintColor aGlobalPaintColorList : globalPaintColorList) {
            if (aGlobalPaintColorList.getColorCode().equalsIgnoreCase(frontwallColorHash)) {
                frontwallColorName = aGlobalPaintColorList.getName();
            }
        }

        PaintedImage paintImage = new PaintedImage();
        paintImage.setName(fileId);
        paintImage.setLocation(fileLocation);
        List<WallDescription> wallDescriptionList = new ArrayList<WallDescription>();
        if (leftwallColorName != null) {
            wallDescriptionList.add(new WallDescription("Left Wall",
                    (leftwallColorName),
                    leftwallColorHash));
        }
        if (frontwallColorName != null) {
            wallDescriptionList.add(new WallDescription("Front Wall",
                    (frontwallColorName),
                    frontwallColorHash));
        }
        if (cornerColorName != null) {
            wallDescriptionList.add(new WallDescription("Corner",
                    (cornerColorName),
                    cornerColorHash));
        }
        paintImage.setWallDescriptionList(wallDescriptionList);
        paintImage.setDate(Utility.getCurrentDate());
        return paintImage;
    }

    private static PaintedImage saveTemplateBedRoom5ToDb(Global global, String fileId, String fileLocation, int[] colors) {
        List<PaintColor> globalPaintColorList = global.getGlobalPaintColorList();
        int leftwall_r = colors[0];
        int leftwall_g = colors[1];
        int leftwall_b = colors[2];
        int iv_midwall_left_r = colors[3];
        int iv_midwall_left_g = colors[4];
        int iv_midwall_left_b = colors[5];
        int iv_midwall_right_r = colors[6];
        int iv_midwall_right_g = colors[7];
        int iv_midwall_right_b = colors[8];
        int rightwall_r = colors[9];
        int rightwall_g = colors[10];
        int rightwall_b = colors[11];
        //leftwall
        String leftwallColorHash = RgbToHex(leftwall_r, leftwall_g, leftwall_b);
        String leftwallColorName = null;
        for (PaintColor aGlobalPaintColorList : globalPaintColorList) {
            if (aGlobalPaintColorList.getColorCode().equalsIgnoreCase(leftwallColorHash)) {
                leftwallColorName = aGlobalPaintColorList.getName();
            }
        }

        //Midwall LEFT
        String midwallLeftColorHash = RgbToHex(iv_midwall_left_r, iv_midwall_left_g, iv_midwall_left_b);
        String midwallLeftColorName = null;
        for (PaintColor aGlobalPaintColorList : globalPaintColorList) {
            if (aGlobalPaintColorList.getColorCode().equalsIgnoreCase(midwallLeftColorHash)) {
                midwallLeftColorName = aGlobalPaintColorList.getName();
            }
        }

        //Midwall RIGHT
        String midwallRightColorHash = RgbToHex(iv_midwall_right_r, iv_midwall_right_g, iv_midwall_right_b);
        String midwallRightColorName = null;
        for (PaintColor aGlobalPaintColorList : globalPaintColorList) {
            if (aGlobalPaintColorList.getColorCode().equalsIgnoreCase(midwallRightColorHash)) {
                midwallRightColorName = aGlobalPaintColorList.getName();
            }
        }

        // rightwall
        String righttwallColorHash = RgbToHex(rightwall_r, rightwall_g, rightwall_b);
        String righttwallColorName = null;
        for (PaintColor aGlobalPaintColorList : globalPaintColorList) {
            if (aGlobalPaintColorList.getColorCode().equalsIgnoreCase(righttwallColorHash)) {
                righttwallColorName = aGlobalPaintColorList.getName();
            }
        }

        PaintedImage paintImage = new PaintedImage();
        paintImage.setName(fileId);
        paintImage.setLocation(fileLocation);
        List<WallDescription> wallDescriptionList = new ArrayList<WallDescription>();
        if (leftwallColorName != null) {
            wallDescriptionList.add(new WallDescription("Left Wall",
                    (leftwallColorName),
                    leftwallColorHash));
        }
        if (midwallLeftColorName != null) {
            wallDescriptionList.add(new WallDescription("Mid Left Wall",
                    (midwallLeftColorName),
                    midwallLeftColorHash));
        }
        if (midwallRightColorName != null) {
            wallDescriptionList.add(new WallDescription("Mid Right Wall",
                    (midwallRightColorName),
                    midwallRightColorHash));
        }
        if (righttwallColorName != null) {
            wallDescriptionList.add(new WallDescription("Right Wall",
                    (righttwallColorName),
                    righttwallColorHash));
        }
        paintImage.setWallDescriptionList(wallDescriptionList);
        paintImage.setDate(Utility.getCurrentDate());
        return paintImage;
    }

    private static PaintedImage saveTemplateBedRoom4ToDb(Global global, String fileId, String fileLocation, int[] colors) {
        List<PaintColor> globalPaintColorList = global.getGlobalPaintColorList();
        int leftwall_r = colors[0];
        int leftwall_g = colors[1];
        int leftwall_b = colors[2];
        int ceiling_r = colors[3];
        int ceiling_g = colors[4];
        int ceiling_b = colors[5];
        int rightwall_r = colors[6];
        int rightwall_g = colors[7];
        int rightwall_b = colors[8];
        //leftwall
        String leftwallColorHash = RgbToHex(leftwall_r, leftwall_g, leftwall_b);
        String leftwallColorName = null;
        for (PaintColor aGlobalPaintColorList : globalPaintColorList) {
            if (aGlobalPaintColorList.getColorCode().equalsIgnoreCase(leftwallColorHash)) {
                leftwallColorName = aGlobalPaintColorList.getName();
            }
        }

        //Ceiling
        String ceilingColorHash = RgbToHex(ceiling_r, ceiling_g, ceiling_b);
        String ceilingColorName = null;
        for (PaintColor aGlobalPaintColorList : globalPaintColorList) {
            if (aGlobalPaintColorList.getColorCode().equalsIgnoreCase(ceilingColorHash)) {
                ceilingColorName = aGlobalPaintColorList.getName();
            }
        }

        // rightwall
        String righttwallColorHash = RgbToHex(rightwall_r, rightwall_g, rightwall_b);
        String righttwallColorName = null;
        for (PaintColor aGlobalPaintColorList : globalPaintColorList) {
            if (aGlobalPaintColorList.getColorCode().equalsIgnoreCase(righttwallColorHash)) {
                righttwallColorName = aGlobalPaintColorList.getName();
            }
        }

        PaintedImage paintImage = new PaintedImage();
        paintImage.setName(fileId);
        paintImage.setLocation(fileLocation);
        List<WallDescription> wallDescriptionList = new ArrayList<WallDescription>();
        if (leftwallColorName != null) {
            wallDescriptionList.add(new WallDescription("Left Wall",
                    (leftwallColorName),
                    leftwallColorHash));
        }
        if (ceilingColorName != null) {
            wallDescriptionList.add(new WallDescription("Ceiling",
                    (ceilingColorName),
                    ceilingColorHash));
        }
        if (righttwallColorName != null) {
            wallDescriptionList.add(new WallDescription("Right Wall",
                    (righttwallColorName),
                    righttwallColorHash));
        }
        paintImage.setWallDescriptionList(wallDescriptionList);
        paintImage.setDate(Utility.getCurrentDate());
        return paintImage;
    }

    private static PaintedImage saveTemplateBedRoom3ToDb(Global global, String fileId, String fileLocation, int[] colors) {
        List<PaintColor> globalPaintColorList = global.getGlobalPaintColorList();
        int leftwall_r = colors[0];
        int leftwall_g = colors[1];
        int leftwall_b = colors[2];
        int rightwall_r = colors[3];
        int rightwall_g = colors[4];
        int rightwall_b = colors[5];
        //leftwall
        String leftwallColorHash = RgbToHex(leftwall_r, leftwall_g, leftwall_b);
        String leftwallColorName = null;
        for (PaintColor aGlobalPaintColorList : globalPaintColorList) {
            if (aGlobalPaintColorList.getColorCode().equalsIgnoreCase(leftwallColorHash)) {
                leftwallColorName = aGlobalPaintColorList.getName();
            }
        }
        // rightwall
        String righttwallColorHash = RgbToHex(rightwall_r, rightwall_g, rightwall_b);
        String righttwallColorName = null;
        for (PaintColor aGlobalPaintColorList : globalPaintColorList) {
            if (aGlobalPaintColorList.getColorCode().equalsIgnoreCase(righttwallColorHash)) {
                righttwallColorName = aGlobalPaintColorList.getName();
            }
        }

        PaintedImage paintImage = new PaintedImage();
        paintImage.setName(fileId);
        paintImage.setLocation(fileLocation);
        List<WallDescription> wallDescriptionList = new ArrayList<WallDescription>();
        if (leftwallColorName != null) {
            wallDescriptionList.add(new WallDescription("Left Wall",
                    leftwallColorName,
                    leftwallColorHash));
        }
        if (righttwallColorName != null) {
            wallDescriptionList.add(new WallDescription("Right Wall",
                    righttwallColorName,
                    righttwallColorHash));
        }
        paintImage.setWallDescriptionList(wallDescriptionList);
        paintImage.setDate(Utility.getCurrentDate());
        return paintImage;
    }

    private static PaintedImage saveTemplateBedRoom2ToDb(Global global, String fileId, String fileLocation, int[] colors) {
        List<PaintColor> globalPaintColorList = global.getGlobalPaintColorList();

        // rightwall
        String righttwallColorHash = RgbToHex(colors[0], colors[1], colors[2]);
        String righttwallColorName = null;
        for (PaintColor aGlobalPaintColorList : globalPaintColorList) {
            if (aGlobalPaintColorList.getColorCode().equalsIgnoreCase(righttwallColorHash)) {
                righttwallColorName = aGlobalPaintColorList.getName();
            }
        }

        PaintedImage paintImage = new PaintedImage();
        paintImage.setName(fileId);
        paintImage.setLocation(fileLocation);
        paintImage.setDate(Utility.getCurrentDate());
        List<WallDescription> wallDescriptionList = new ArrayList<WallDescription>();
        if (righttwallColorName != null) {
            wallDescriptionList.add(new WallDescription("Right Wall",
                    righttwallColorName,
                    righttwallColorHash));
        }
        paintImage.setWallDescriptionList(wallDescriptionList);
        return paintImage;
    }

    private static PaintedImage saveTemplateBedRoom1ToDb(Global global, String fileId, String fileLocation, int[] colors) {
        List<PaintColor> globalPaintColorList = global.getGlobalPaintColorList();
        //left wall
        String leftwallColorHash = RgbToHex(colors[0], colors[1], colors[2]);
        String leftwallColorName = null;
        for (PaintColor aGlobalPaintColorList : globalPaintColorList) {
            if (aGlobalPaintColorList.getColorCode().equalsIgnoreCase(leftwallColorHash)) {
                leftwallColorName = aGlobalPaintColorList.getName();
            }
        }

        //Ceiling
        String ceilingColorHash = RgbToHex(colors[3], colors[4], colors[5]);
        String ceilingColorName = null;
        for (PaintColor aGlobalPaintColorList : globalPaintColorList) {
            if (aGlobalPaintColorList.getColorCode().equalsIgnoreCase(ceilingColorHash)) {
                ceilingColorName = aGlobalPaintColorList.getName();
            }
        }

        // rightwall
        String righttwallColorHash = RgbToHex(colors[6], colors[7], colors[8]);
        String righttwallColorName = null;
        for (PaintColor aGlobalPaintColorList : globalPaintColorList) {
            if (aGlobalPaintColorList.getColorCode().equalsIgnoreCase(righttwallColorHash)) {
                righttwallColorName = aGlobalPaintColorList.getName();
            }
        }

        PaintedImage paintImage = new PaintedImage();
        paintImage.setName(fileId);
        paintImage.setLocation(fileLocation);
        paintImage.setDate(Utility.getCurrentDate());
        List<WallDescription> wallDescriptionList = new ArrayList<WallDescription>();
        if (leftwallColorName != null) {
            wallDescriptionList.add(new WallDescription("Left Wall",
                    leftwallColorName,
                    leftwallColorHash));
        }
        if (ceilingColorName != null) {
            wallDescriptionList.add(new WallDescription("Ceiling",
                    ceilingColorName,
                    ceilingColorHash));
        }

        if (righttwallColorName != null) {
            wallDescriptionList.add(new WallDescription("Right Wall",
                    righttwallColorName,
                    righttwallColorHash));
        }
        paintImage.setWallDescriptionList(wallDescriptionList);
        return paintImage;
    }

    private static PaintedImage saveTemplateLivingroom4ToDb(Global global, String fileId, String fileLocation, int[] colors) {
        List<PaintColor> globalPaintColorList = global.getGlobalPaintColorList();
        //leftwall
        String leftwallColorHash = RgbToHex(colors[0], colors[1], colors[2]);
        String leftwallColorName = null;
        for (PaintColor aGlobalPaintColorList4 : globalPaintColorList) {
            if (aGlobalPaintColorList4.getColorCode().equalsIgnoreCase(leftwallColorHash)) {
                leftwallColorName = aGlobalPaintColorList4.getName();
            }
        }

        //Midwall
        String midwallColorHash = RgbToHex(colors[3], colors[4], colors[5]);
        String midwallColorName = null;
        for (PaintColor aGlobalPaintColorList3 : globalPaintColorList) {
            if (aGlobalPaintColorList3.getColorCode().equalsIgnoreCase(midwallColorHash)) {
                midwallColorName = aGlobalPaintColorList3.getName();
            }
        }

        //Ceiling border
        String borderceilingColorHash = RgbToHex(colors[6], colors[7], colors[8]);
        String borderceilingColorName = null;
        for (PaintColor aGlobalPaintColorList2 : globalPaintColorList) {
            if (aGlobalPaintColorList2.getColorCode().equalsIgnoreCase(borderceilingColorHash)) {
                borderceilingColorName = aGlobalPaintColorList2.getName();
            }
        }

        //Ceiling
        String ceilingColorHash = RgbToHex(colors[9], colors[10], colors[11]);
        String ceilingColorName = null;
        for (PaintColor aGlobalPaintColorList1 : globalPaintColorList) {
            if (aGlobalPaintColorList1.getColorCode().equalsIgnoreCase(ceilingColorHash)) {
                ceilingColorName = aGlobalPaintColorList1.getName();
            }
        }

        // rightwall
        String righttwallColorHash = RgbToHex(colors[12], colors[13], colors[14]);
        String righttwallColorName = null;
        for (PaintColor aGlobalPaintColorList : globalPaintColorList) {
            if (aGlobalPaintColorList.getColorCode().equalsIgnoreCase(righttwallColorHash)) {
                righttwallColorName = aGlobalPaintColorList.getName();
            }
        }

        PaintedImage paintImage = new PaintedImage();
        paintImage.setName(fileId);
        paintImage.setLocation(fileLocation);
        List<WallDescription> wallDescriptionList = new ArrayList<WallDescription>();
        if (leftwallColorName != null) {
            wallDescriptionList.add(new WallDescription("Left Wall",
                    leftwallColorName,
                    leftwallColorHash));
        }

        if (midwallColorName != null) {
            wallDescriptionList.add(new WallDescription("Mid Wall",
                    midwallColorName,
                    midwallColorHash));
        }

        if (borderceilingColorName != null) {
            wallDescriptionList.add(new WallDescription("Ceiling Border",
                    borderceilingColorName,
                    borderceilingColorHash));
        }

        if (ceilingColorName != null) {
            wallDescriptionList.add(new WallDescription("Ceiling",
                    ceilingColorName,
                    ceilingColorHash));
        }
        if (righttwallColorName != null) {
            wallDescriptionList.add(new WallDescription("Right Wall",
                    righttwallColorName,
                    righttwallColorHash));
        }
        paintImage.setWallDescriptionList(wallDescriptionList);
        paintImage.setDate(Utility.getCurrentDate());
        return paintImage;
    }

    private static PaintedImage saveTemplateLivingroom3ToDb(Global global, String fileId, String fileLocation, int[] colors) {
        List<PaintColor> globalPaintColorList = global.getGlobalPaintColorList();
        // rightwall
        String righttwallColorHash = RgbToHex(colors[0], colors[1], colors[2]);
        String righttwallColorName = null;
        for (PaintColor aGlobalPaintColorList : globalPaintColorList) {
            if (aGlobalPaintColorList.getColorCode().equalsIgnoreCase(righttwallColorHash)) {
                righttwallColorName = aGlobalPaintColorList.getName();
            }
        }

        PaintedImage paintImage = new PaintedImage();
        paintImage.setName(fileId);
        paintImage.setLocation(fileLocation);
        List<WallDescription> wallDescriptionList = new ArrayList<WallDescription>();
        if (righttwallColorName != null) {
            wallDescriptionList.add(new WallDescription("Right Wall",
                    righttwallColorName,
                    righttwallColorHash));
        }
        paintImage.setWallDescriptionList(wallDescriptionList);
        paintImage.setDate(Utility.getCurrentDate());
        return paintImage;
    }

    private static PaintedImage saveTemplateLivingroom2ToDb(Global global, String fileId, String fileLocation, int[] colors) {
        // rightwall
        List<PaintColor> globalPaintColorList = global.getGlobalPaintColorList();

        String righttwallColorHash = RgbToHex(colors[0], colors[1], colors[2]);

        String righttwallColorName = null;
        for (PaintColor aGlobalPaintColorList : globalPaintColorList) {
            if (aGlobalPaintColorList.getColorCode().equalsIgnoreCase(righttwallColorHash)) {
                righttwallColorName = aGlobalPaintColorList.getName();
            }
        }

        PaintedImage paintImage = new PaintedImage();
        paintImage.setName(fileId);
        paintImage.setLocation(fileLocation);
        List<WallDescription> wallDescriptionList = new ArrayList<WallDescription>();
        if (righttwallColorName != null) {
            wallDescriptionList.add(new WallDescription("Right Wall",
                    righttwallColorName,
                    righttwallColorHash));
        }
        paintImage.setWallDescriptionList(wallDescriptionList);
        paintImage.setDate(Utility.getCurrentDate());
        return paintImage;
    }

    private static PaintedImage saveTemplateLivingroom1ToDb(Global global, String fileId, String fileLocation, int[] colors) {
        //Ceiling border
        String borderceilingColorHash = RgbToHex(colors[0], colors[1], colors[2]);
        String borderceilingColorName = null;
        List<PaintColor> globalPaintColorList = global.getGlobalPaintColorList();
        for (PaintColor aGlobalPaintColorList : globalPaintColorList) {
            if (aGlobalPaintColorList.getColorCode().equalsIgnoreCase(borderceilingColorHash)) {
                borderceilingColorName = aGlobalPaintColorList.getName();
            }
        }

        //Ceiling
        String ceilingColorHash = RgbToHex(colors[3], colors[4], colors[5]);
        String ceilingColorName = null;
        for (PaintColor aGlobalPaintColorList : globalPaintColorList) {
            if (aGlobalPaintColorList.getColorCode().equalsIgnoreCase(ceilingColorHash)) {
                ceilingColorName = aGlobalPaintColorList.getName();
            }
        }

        // rightwall
        String righttwallColorHash = RgbToHex(colors[6], colors[7], colors[8]);
        String righttwallColorName = null;
        for (PaintColor aGlobalPaintColorList : globalPaintColorList) {
            if (aGlobalPaintColorList.getColorCode().equalsIgnoreCase(righttwallColorHash)) {
                righttwallColorName = aGlobalPaintColorList.getName();
            }
        }

        PaintedImage paintImage = new PaintedImage();
        paintImage.setName(fileId);
        paintImage.setLocation(fileLocation);
        List<WallDescription> wallDescriptionList = new ArrayList<WallDescription>();
        if (borderceilingColorName != null) {
            wallDescriptionList.add(new WallDescription("Ceiling border",
                    borderceilingColorName,
                    borderceilingColorHash));
        }
        if (ceilingColorName != null) {
            wallDescriptionList.add(new WallDescription("Ceiling",
                    ceilingColorName,
                    ceilingColorHash));
        }
        if (righttwallColorName != null) {
            wallDescriptionList.add(new WallDescription("Right Wall",
                    righttwallColorName,
                    righttwallColorHash));
        }
        paintImage.setWallDescriptionList(wallDescriptionList);
        paintImage.setDate(Utility.getCurrentDate());
        return paintImage;
    }

    private static PaintedImage saveTemplateExterior3ToDb(Context context, Global global, String fileId, String fileLocation, int[] colors) {
        PaintedImage paintImage = new PaintedImage();
        //img1
        String iv_1_hash = RgbToHex(colors[0], colors[1], colors[2]);
        String iv_1_colorname = null;
        for (int i = 0; i < global.globalPaintColorList.size(); i++) {
            if (global.globalPaintColorList.get(i).getColorCode().equalsIgnoreCase(iv_1_hash)) {
                iv_1_colorname = global.globalPaintColorList.get(i).getName();
            }
        }
        AppLog.showLog(CLASS_TAG, "Ext3 color1: " + iv_1_colorname);

        //img2
        String iv_2_hash = RgbToHex(colors[3], colors[4], colors[5]);
        String iv_2_colorname = null;
        for (int i = 0; i < global.globalPaintColorList.size(); i++) {
            if (global.globalPaintColorList.get(i).getColorCode().equalsIgnoreCase(iv_2_hash)) {
                iv_2_colorname = global.globalPaintColorList.get(i).getName();
            }
        }

        AppLog.showLog(CLASS_TAG, "Ext3 color2: " + iv_2_colorname);
        //img3
        String iv_3_hash = RgbToHex(colors[6], colors[7], colors[8]);
        String iv_3_colorname = null;
        for (int i = 0; i < global.globalPaintColorList.size(); i++) {
            if (global.globalPaintColorList.get(i).getColorCode().equalsIgnoreCase(iv_3_hash)) {
                iv_3_colorname = global.globalPaintColorList.get(i).getName();
            }
        }
        AppLog.showLog(CLASS_TAG, "Ext3 color3: " + iv_3_colorname);
        //img4
        String iv_4_hash = RgbToHex(colors[9], colors[10], colors[11]);
        String iv_4_colorname = null;
        for (int i = 0; i < global.globalPaintColorList.size(); i++) {
            if (global.globalPaintColorList.get(i).getColorCode().equalsIgnoreCase(iv_4_hash)) {
                iv_4_colorname = global.globalPaintColorList.get(i).getName();
            }
        }
        AppLog.showLog(CLASS_TAG, "Ext3 color4: " + iv_4_colorname);

        //img5
        String iv_5_hash = RgbToHex(colors[12], colors[13], colors[14]);
        String iv_5_colorname = null;
        for (int i = 0; i < global.globalPaintColorList.size(); i++) {
            if (global.globalPaintColorList.get(i).getColorCode().equalsIgnoreCase(iv_5_hash)) {
                iv_5_colorname = global.globalPaintColorList.get(i).getName();
            }
        }
        AppLog.showLog(CLASS_TAG, "Ext3 color5: " + iv_5_colorname);

        //img6
        String iv_6_hash = RgbToHex(colors[15], colors[16], colors[17]);
        String iv_6_colorname = null;
        for (int i = 0; i < global.globalPaintColorList.size(); i++) {
            if (global.globalPaintColorList.get(i).getColorCode().equalsIgnoreCase(iv_6_hash)) {
                iv_6_colorname = global.globalPaintColorList.get(i).getName();
            }
        }
        AppLog.showLog(CLASS_TAG, "Ext3 color6: " + iv_6_colorname);

        //img7
        String iv_7_hash = RgbToHex(colors[18], colors[19], colors[20]);
        String iv_7_colorname = null;
        for (int i = 0; i < global.globalPaintColorList.size(); i++) {
            if (global.globalPaintColorList.get(i).getColorCode().equalsIgnoreCase(iv_7_hash)) {
                iv_7_colorname = global.globalPaintColorList.get(i).getName();
            }
        }
        AppLog.showLog(CLASS_TAG, "Ext3 color7: " + iv_7_colorname);
        //img8
        String iv_8_hash = RgbToHex(colors[21], colors[22], colors[23]);
        String iv_8_colorname = null;
        for (int i = 0; i < global.globalPaintColorList.size(); i++) {
            if (global.globalPaintColorList.get(i).getColorCode().equalsIgnoreCase(iv_8_hash)) {
                iv_8_colorname = global.globalPaintColorList.get(i).getName();
            }
        }
        AppLog.showLog(CLASS_TAG, "Ext3 color8: " + iv_8_colorname);
        //img9
        String iv_9_hash = RgbToHex(colors[24], colors[25], colors[26]);
        String iv_9_colorname = null;
        for (int i = 0; i < global.globalPaintColorList.size(); i++) {
            if (global.globalPaintColorList.get(i).getColorCode().equalsIgnoreCase(iv_9_hash)) {
                iv_9_colorname = global.globalPaintColorList.get(i).getName();
            }
        }
        AppLog.showLog(CLASS_TAG, "Ext3 color9: " + iv_9_colorname);
        //img10
        String iv_10_hash = RgbToHex(colors[27], colors[28], colors[29]);
        String iv_10_colorname = null;
        for (int i = 0; i < global.globalPaintColorList.size(); i++) {
            if (global.globalPaintColorList.get(i).getColorCode().equalsIgnoreCase(iv_10_hash)) {
                iv_10_colorname = global.globalPaintColorList.get(i).getName();
            }
        }
        AppLog.showLog(CLASS_TAG, "Ext3 color10: " + iv_10_colorname);
        //img11
        String iv_11_hash = RgbToHex(colors[30], colors[31], colors[32]);
        String iv_11_colorname = null;
        for (int i = 0; i < global.globalPaintColorList.size(); i++) {
            if (global.globalPaintColorList.get(i).getColorCode().equalsIgnoreCase(iv_11_hash)) {
                iv_11_colorname = global.globalPaintColorList.get(i).getName();
            }
        }
        AppLog.showLog(CLASS_TAG, "Ext3 color11: " + iv_11_colorname);
        //img12
        String iv_12_hash = RgbToHex(colors[33], colors[34], colors[35]);
        String iv_12_colorname = null;
        for (int i = 0; i < global.globalPaintColorList.size(); i++) {
            if (global.globalPaintColorList.get(i).getColorCode().equalsIgnoreCase(iv_12_hash)) {
                iv_12_colorname = global.globalPaintColorList.get(i).getName();
            }
        }
        AppLog.showLog(CLASS_TAG, "Ext3 color12: " + iv_12_colorname);


        paintImage.setName(fileId);
        paintImage.setLocation(fileLocation);
        List<WallDescription> wallDescriptionList = new ArrayList<WallDescription>();
        if (iv_1_colorname != null) {
            wallDescriptionList.add(new WallDescription("Wall 1",
                    (iv_1_colorname != null ? iv_1_colorname : context.getResources().getString(R.string.text_wall_not_painted)),
                    iv_1_hash));
        }
        if (iv_2_colorname != null) {
            wallDescriptionList.add(new WallDescription("Wall 2",
                    (iv_2_colorname != null ? iv_2_colorname : context.getResources().getString(R.string.text_wall_not_painted)),
                    iv_2_hash));
        }
        if (iv_3_colorname != null) {
            wallDescriptionList.add(new WallDescription("Wall 3",
                    (iv_3_colorname != null ? iv_3_colorname : context.getResources().getString(R.string.text_wall_not_painted)),
                    iv_3_hash));
        }
        if (iv_4_colorname != null) {
            wallDescriptionList.add(new WallDescription("Wall 4",
                    (iv_4_colorname != null ? iv_4_colorname : context.getResources().getString(R.string.text_wall_not_painted)),
                    iv_4_hash));
        }
        if (iv_5_colorname != null) {
            wallDescriptionList.add(new WallDescription("Wall 5",
                    (iv_5_colorname != null ? iv_5_colorname : context.getResources().getString(R.string.text_wall_not_painted)),
                    iv_5_hash));
        }
        if (iv_6_colorname != null) {
            wallDescriptionList.add(new WallDescription("Wall 6",
                    (iv_6_colorname != null ? iv_6_colorname : context.getResources().getString(R.string.text_wall_not_painted)),
                    iv_6_hash));
        }

        if (iv_7_colorname != null) {
            wallDescriptionList.add(new WallDescription("Wall 7",
                    (iv_7_colorname != null ? iv_7_colorname : context.getResources().getString(R.string.text_wall_not_painted)),
                    iv_7_hash));
        }
        if (iv_8_colorname != null) {
            wallDescriptionList.add(new WallDescription("Wall 8",
                    (iv_8_colorname != null ? iv_8_colorname : context.getResources().getString(R.string.text_wall_not_painted)),
                    iv_8_hash));
        }
        if (iv_9_colorname != null) {
            wallDescriptionList.add(new WallDescription("Wall 9",
                    (iv_9_colorname != null ? iv_9_colorname : context.getResources().getString(R.string.text_wall_not_painted)),
                    iv_9_hash));
        }
        if (iv_10_colorname != null) {
            wallDescriptionList.add(new WallDescription("Wall 10",
                    (iv_10_colorname != null ? iv_10_colorname : context.getResources().getString(R.string.text_wall_not_painted)),
                    iv_10_hash));
        }
        if (iv_11_colorname != null) {
            wallDescriptionList.add(new WallDescription("Wall 11",
                    (iv_11_colorname != null ? iv_11_colorname : context.getResources().getString(R.string.text_wall_not_painted)),
                    iv_11_hash));
        }
        if (iv_12_colorname != null) {
            wallDescriptionList.add(new WallDescription("Wall 12",
                    (iv_12_colorname != null ? iv_12_colorname : context.getResources().getString(R.string.text_wall_not_painted)),
                    iv_12_hash));
        }
        paintImage.setWallDescriptionList(wallDescriptionList);
        paintImage.setDate(Utility.getCurrentDate());
        return paintImage;
    }

    private static PaintedImage saveTemplateExterior2ToDb(Global global, String fileId, String fileLocation, int... colors) {
        PaintedImage paintImage = new PaintedImage();
        //leftwall
        String topColorHash2 = RgbToHex(colors[0], colors[1], colors[2]);
        String topColorName2 = null;
        for (int i = 0; i < global.globalPaintColorList.size(); i++) {
            if (global.globalPaintColorList.get(i).getColorCode().equalsIgnoreCase(topColorHash2)) {
                topColorName2 = global.globalPaintColorList.get(i).getName();
            }
        }
        AppLog.showLog(CLASS_TAG, "Ext2 color1:" + topColorName2);
        // rightwall
        String bottomColorHash = RgbToHex(colors[3], colors[4], colors[5]);
        String bottomColorName = null;
        for (int i = 0; i < global.globalPaintColorList.size(); i++) {
            if (global.globalPaintColorList.get(i).getColorCode().equalsIgnoreCase(bottomColorHash)) {
                bottomColorName = global.globalPaintColorList.get(i).getName();
            }
        }
        AppLog.showLog(CLASS_TAG, "Ext2 color1:" + bottomColorName);
        paintImage.setName(fileId);
        paintImage.setLocation(fileLocation);
        List<WallDescription> wallDescriptionList = new ArrayList<WallDescription>();
        if (topColorName2 != null) {
            wallDescriptionList.add(new WallDescription("Top Wall",
                    topColorName2,
                    topColorHash2));
        }
        if (bottomColorName != null) {
            wallDescriptionList.add(new WallDescription("Bottom Wall",
                    bottomColorName,
                    bottomColorHash));
        }
        paintImage.setWallDescriptionList(wallDescriptionList);
        paintImage.setDate(Utility.getCurrentDate());
        return paintImage;
    }

    private static PaintedImage saveTemplateExterior1ToDb(Global global, String fileId, String fileLocation, int... colors) {
        PaintedImage paintImage = new PaintedImage();
        AppLog.showLog(CLASS_TAG, "front (" + colors[0] + "," + colors[1] + "," + colors[2]);
        String frontColorHash = RgbToHex(colors[0], colors[1],
                colors[2]);
        AppLog.showLog(CLASS_TAG, "front color hash:" + frontColorHash);
        String frontColorName = null;
        for (int i = 0; i < global.globalPaintColorList.size(); i++) {
            if (global.globalPaintColorList.get(i).getColorCode()
                    .equalsIgnoreCase(frontColorHash)) {
                frontColorName = global.globalPaintColorList.get(i).getName();
            }
        }
        AppLog.showLog(CLASS_TAG, "front color:" + frontColorName);

        // border top
        String borderTopColorHash = RgbToHex(colors[3],
                colors[4], colors[5]);
        AppLog.showLog(CLASS_TAG, "border color hash:" + borderTopColorHash);
        String borderTopColorName = null;
        for (PaintColor aGlobalPaintColorList : global.globalPaintColorList) {
            if (aGlobalPaintColorList.getColorCode()
                    .equalsIgnoreCase(borderTopColorHash)) {
                borderTopColorName = aGlobalPaintColorList.getName();
            }
        }
        AppLog.showLog(CLASS_TAG, "border top color2:" + borderTopColorName);
        // back
        String backColorHash = RgbToHex(colors[6], colors[7], colors[8]);
        AppLog.showLog(CLASS_TAG, "back color hash:" + backColorHash);
        String backColorName = null;
        for (PaintColor aGlobalPaintColorList : global.globalPaintColorList) {
            if (aGlobalPaintColorList.getColorCode()
                    .equalsIgnoreCase(backColorHash)) {
                backColorName = aGlobalPaintColorList.getName();
            }
        }
        AppLog.showLog(CLASS_TAG, "back color3:" + backColorName);
        // top
        String topColorHash = RgbToHex(colors[9], colors[10], colors[11]);
        String topColorName = null;
        for (PaintColor aGlobalPaintColorList : global.globalPaintColorList) {
            if (aGlobalPaintColorList.getColorCode()
                    .equalsIgnoreCase(topColorHash)) {
                topColorName = aGlobalPaintColorList.getName();
            }
        }
        AppLog.showLog(CLASS_TAG, "top color4:" + topColorName);

        paintImage.setName(fileId);
        paintImage.setLocation(fileLocation);
        List<WallDescription> wallDescriptionList = new ArrayList<WallDescription>();
        if (frontColorName != null) {
            wallDescriptionList.add(new WallDescription("Front Wall",
                    frontColorName,
                    frontColorHash));
        }
        if (borderTopColorName != null) {
            wallDescriptionList.add(new WallDescription("Border Top",
                    borderTopColorName,
                    borderTopColorHash));
        }
        if (backColorName != null) {
            wallDescriptionList.add(new WallDescription("Back Wall",
                    backColorName,
                    backColorHash));
        }
        if (topColorName != null) {
            wallDescriptionList.add(new WallDescription("Top Wall",
                    topColorName,
                    topColorHash));
        }
        paintImage.setWallDescriptionList(wallDescriptionList);
        paintImage.setDate(Utility.getCurrentDate());
        return paintImage;
    }

    public static void share(Context context, View viewToBeShared, List<PaintColor> usedColorList) {
        Bitmap bm1;
        viewToBeShared.setDrawingCacheEnabled(true);
        viewToBeShared.buildDrawingCache();
        bm1 = viewToBeShared.getDrawingCache();

        try {
            String path = Environment.getExternalStorageDirectory().toString();
            File imgDirectory = new File("/sdcard/PashupatiPaints/");
            imgDirectory.mkdirs();
            OutputStream fOut;
            File file;
            String fileId = System.currentTimeMillis() + "";
            String fileLocation = "/PashupatiPaints/" + fileId + ".png";
            file = new File(path, fileLocation);

            //Toast.makeText(context, "saved at: " + file.getAbsolutePath(), Toast.LENGTH_SHORT).show();

            //SAVE THE data to database
            saveImageDetailsToDb(context, fileId, fileLocation, usedColorList);

            String path1 = MediaStore.Images.Media.insertImage(context.getContentResolver(), bm1, "title", null);
            Uri screenShotUri = Uri.parse(path1);

            fOut = new FileOutputStream(file);
            bm1.compress(Bitmap.CompressFormat.PNG, 100, fOut);
            fOut.flush();
            fOut.close();

            final Intent shareIntent = new Intent(Intent.ACTION_SEND);
            shareIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            shareIntent.putExtra(Intent.EXTRA_STREAM, screenShotUri);
            shareIntent.setType("image/png");
            context.startActivity(Intent.createChooser(shareIntent, context.getResources().getString(R.string.text_share_heading)));
        } catch (Exception e) {
            e.printStackTrace();
            Toast.makeText(context, "can't be saved", Toast.LENGTH_SHORT).show();
        }

    }

    private static void saveImageDetailsToDb(Context context, String fileId, String fileLocation, List<PaintColor> usedColorList) {
        AppLog.showLog(CLASS_TAG, "saving image details to database!");

        PaintedImage paintImage = new PaintedImage();
        List<WallDescription> wallDescriptionList = new ArrayList<WallDescription>();
        for (int i = 0; i < usedColorList.size(); i++) {
            wallDescriptionList.add(new WallDescription("Color " + (i + 1),
                    usedColorList.get(i).getName().isEmpty() ? context.getResources().getString(R.string.text_wall_not_painted)
                            : usedColorList.get(i).getName(),
                    usedColorList.get(i).getColorCode().isEmpty()
                            ? "#ffffff" : usedColorList.get(i).getColorCode()
            ));
        }
        paintImage.setName(fileId);
        paintImage.setLocation(fileLocation);
        paintImage.setWallDescriptionList(wallDescriptionList);

        PaintedImageDao paintedImageDao = new PaintedImageDao(context);
        try {
            if (paintedImageDao.insertRecord(paintImage) > 0) {
                AppLog.showLog(CLASS_TAG, "inserted successfully!");
            } else {
                AppLog.showLog(CLASS_TAG, "cannot be inserted");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void save(Context context, View viewToBeSaved, List<PaintColor> usedColorList) {
        Bitmap bm;
        viewToBeSaved.setDrawingCacheEnabled(true);
        viewToBeSaved.buildDrawingCache();
        bm = viewToBeSaved.getDrawingCache();

        try {
            String path = Environment.getExternalStorageDirectory().toString();
            File imgDirectory = new File("/sdcard/PashupatiPaints/");
            imgDirectory.mkdirs();
            OutputStream fOut;
            File file;
            String fileid = System.currentTimeMillis() + "";
            String filelocation = "/PashupatiPaints/" + fileid + ".png";
            file = new File(path, filelocation);

            Toast.makeText(context, "Image Saved", Toast.LENGTH_SHORT).show();

            fOut = new FileOutputStream(file);
            bm.compress(Bitmap.CompressFormat.PNG, 100, fOut);
            fOut.flush();
            fOut.close();

            //SAVE THE data to database
            saveImageDetailsToDb(context, fileid, filelocation, usedColorList);
        } catch (Exception e) {
            e.printStackTrace();
            Toast.makeText(context, "can't be saved", Toast.LENGTH_SHORT).show();
        }
    }

    public static String RgbToHex(int red, int green, int blue) {
        AppLog.showLog(CLASS_TAG, "...converting RGB to Hex");
        String hexValue;

        hexValue = Integer.toHexString(Color.rgb(red, green, blue) & 0x00ffffff);

        if (hexValue.length() < 6) {
            hexValue = "0" + hexValue;
        }

        if (hexValue.length() < 6) {
            hexValue = "0" + hexValue;
        }

        hexValue = "#" + hexValue;


        AppLog.showLog(CLASS_TAG, "rgb to hex:: " + hexValue);
        return hexValue;
    }

    public static Float[] hexToRgb(String hexColor) {
        int red, green, blue, alpha;
        int color = Color.parseColor(hexColor);

        red = (color >> 16) & 0xFF;
        green = (color >> 8) & 0xFF;
        blue = (color) & 0xFF;
        alpha = (color >> 24) & 0xFF;

        float redScaled = (float) red / 255;
        float greenScaled = (float) green / 255;
        float blueScaled = (float) blue / 255;
        float alphaScaled = (float) alpha / 255;

        AppLog.showLog(CLASS_TAG, "COLOR SELECTED hex " + " + hexColor rgb::" + color);
        AppLog.showLog(CLASS_TAG, "red:: " + red + " green:: " + green + " blue:: " + blue + " aplha:: " + alpha);
        AppLog.showLog(CLASS_TAG, "redScaled:: " + redScaled + " greenScaled:: " + greenScaled + " blueScaled:: " + blueScaled + " alphaScaled:: " + alphaScaled);

        return new Float[]{redScaled, greenScaled, blueScaled, alphaScaled};
    }

    public static Bitmap getBitmapFromAsset(Context context, String strName, int scaleType) {
        //scaleType: 0 for list images-> low quality
        //scaleType: 1 for list painting images-> lil high quality
        AssetManager assetManager = context.getAssets();
        InputStream inputStream;
        Bitmap bitmap = null;


        try {
            inputStream = assetManager.open(strName);
            if (scaleType == 0) {
                bitmap = decodeFile(inputStream);
            } else if (scaleType == 1) {
                bitmap = BitmapFactory.decodeStream(inputStream);
            } else {
                bitmap = BitmapFactory.decodeStream(inputStream);
            }

        } catch (IOException e) {
            e.printStackTrace();
        }
        return bitmap;
    }

    //decodes image and scales it InputStream reduce memory consumption
    public static Bitmap decodeFile(InputStream is) {
        try {
            //Decode image size
            BitmapFactory.Options o = new BitmapFactory.Options();
            o.inJustDecodeBounds = true;
            BitmapFactory.decodeStream(is, null, o);

            //The new size we want to scale to
            final int REQUIRED_SIZE = 250;

            //Find the correct scale value. It should be the power of 2.
            int scale = 2;
            while (o.outWidth / scale / 2 >= REQUIRED_SIZE && o.outHeight / scale / 2 >= REQUIRED_SIZE)
                scale *= 2;

            //Decode with inSampleSize
            BitmapFactory.Options o2 = new BitmapFactory.Options();
            o2.inSampleSize = scale;
            return BitmapFactory.decodeStream(is, null, o2);
        } catch (Exception e) {
            AppLog.showLog(CLASS_TAG, "decode file error: " + e.getLocalizedMessage());
        }
        return null;
    }

    public static int[] startPainting(Context context, int className, int x, int y, View mainImage,
                                      View parent, float[] colorScale, ImageView... imageViews) {
        switch (className) {
            case EXTERIOR_TEMPLATE_1:
                return paintExterior1(context, mainImage, parent, x, y, colorScale, imageViews);
            case EXTERIOR_TEMPLATE_2:
                return paintExterior2(context, mainImage, parent, x, y, colorScale, imageViews);
            case EXTERIOR_TEMPLATE_3:
                return paintExterior3(context, mainImage, parent, x, y, colorScale, imageViews);
            case LIVINGROOM_TEMPLATE_1:
                return paintLivingRoom1(context, mainImage, parent, x, y, colorScale, imageViews);
            case LIVINGROOM_TEMPLATE_2:
                return paintLivingRoom2(context, mainImage, parent, x, y, colorScale, imageViews);
            case LIVINGROOM_TEMPLATE_3:
                return paintLIvingRoom3(context, mainImage, parent, x, y, colorScale, imageViews);
            case LIVINGROOM_TEMPLATE_4:
                return paintLivingRoom4(context, mainImage, parent, x, y, colorScale, imageViews);
            case BEDROOM_TEMPLATE_1:
                return paintBedRoom1(context, mainImage, parent, x, y, colorScale, imageViews);
            case BEDROOM_TEMPLATE_2:
                return paintBedRoom2(context, mainImage, parent, x, y, colorScale, imageViews);
            case BEDROOM_TEMPLATE_3:
                return paintBedRoom3(context, mainImage, parent, x, y, colorScale, imageViews);
            case BEDROOM_TEMPLATE_4:
                return paintBedRoom4(context, mainImage, parent, x, y, colorScale, imageViews);
            case BEDROOM_TEMPLATE_5:
                return paintBedRoom5(context, mainImage, parent, x, y, colorScale, imageViews);
            case BEDROOM_TEMPLATE_6:
                return paintBedRoom6(context, mainImage, parent, x, y, colorScale, imageViews);
            case KITCHEN_TEMPLATE_1:
                return paintKitchen1(context, mainImage, parent, x, y, colorScale, imageViews);
            case KITCHEN_TEMPLATE_2:
                return paintKitchen2(context, mainImage, parent, x, y, colorScale, imageViews);
            case KITCHEN_TEMPLATE_3:
                return paintKitchen3(context, mainImage, parent, x, y, colorScale, imageViews);
            case KITCHEN_TEMPLATE_4:
                return paintKitchen4(context, mainImage, parent, x, y, colorScale, imageViews);
            case KITCHEN_TEMPLATE_5:
                return paintKitchen5(context, mainImage, parent, x, y, colorScale, imageViews);
            case KITCHEN_TEMPLATE_6:
                return paintKitchen6(context, mainImage, parent, x, y, colorScale, imageViews);
            case KITCHEN_TEMPLATE_7:
                return paintKitchen7(context, mainImage, parent, x, y, colorScale, imageViews);
        }
        return null;
    }

    private static int[] paintKitchen7(Context context, View mainImage, View parent, int x, int y, float[] colorScale, ImageView[] imageViews) {


        int imageId = getImageId((RelativeLayout) parent, x, y);
        float redScale = colorScale[0];
        float greenScale = colorScale[1];
        float blueScale = colorScale[2];
        int leftwall_r, leftwall_g, leftwall_b,
                ceiling_r, ceiling_g, ceiling_b,
                rightwall_r, rightwall_g, rightwall_b,
                bottom_r, bottom_g, bottom_b,
                corner_r, corner_g, corner_b;
        int[] colors = new int[4];
        if (imageId >= 0) {

            switch (imageId) {
                case R.id.iv_kitchen7_main_image:
                    AppLog.showLog(CLASS_TAG, "main image clicked");
                    break;

                case R.id.iv_kitchen7_left_wall:
                    AppLog.showLog(CLASS_TAG, "left wall clicked!!");

                    leftwall_r = (int) (redScale * 255);
                    colors[0] = leftwall_r;
                    leftwall_g = (int) (greenScale * 255);
                    colors[1] = leftwall_g;
                    leftwall_b = (int) (blueScale * 255);
                    colors[2] = leftwall_b;
                    colors[3] = R.id.iv_kitchen7_left_wall;
                    colorTransform = getColorMatrix(redScale,
                            greenScale, blueScale);
                    colorWall(getBitmapFromAsset(
                                    context, AppText.KITCHEN7_LOCATION_LEFT, 1),
                            colorTransform, imageViews[0]
                    );
                    return colors;

                case R.id.iv_kitchen7_ceiling:
                    AppLog.showLog(CLASS_TAG, "Ceiling clicked!!");

                    ceiling_r = (int) (redScale * 255);
                    colors[0] = ceiling_r;
                    ceiling_g = (int) (greenScale * 255);
                    colors[1] = ceiling_g;
                    ceiling_b = (int) (blueScale * 255);
                    colors[2] = ceiling_b;
                    colors[3] = R.id.iv_kitchen7_ceiling;
                    colorTransform = getColorMatrix(redScale,
                            greenScale, blueScale);
                    colorWall(getBitmapFromAsset(
                            context, AppText.KITCHEN7_LOCATION_TOP,
                            1), colorTransform, imageViews[1]);
                    return colors;

                case R.id.iv_kitchen7_right_wall:
                    AppLog.showLog(CLASS_TAG, "right wall clicked!!");

                    rightwall_r = (int) (redScale * 255);
                    colors[0] = rightwall_r;
                    rightwall_g = (int) (greenScale * 255);
                    colors[1] = rightwall_g;
                    rightwall_b = (int) (blueScale * 255);
                    colors[2] = rightwall_b;
                    colors[3] = R.id.iv_kitchen7_right_wall;

                    colorTransform = getColorMatrix(redScale,
                            greenScale, blueScale);
                    colorWall(
                            getBitmapFromAsset(
                                    context,
                                    AppText.KITCHEN7_LOCATION_RIGHT, 1), colorTransform,
                            imageViews[2]
                    );
                    return colors;

                case R.id.iv_kitchen7_bottom:
                    AppLog.showLog(CLASS_TAG, "bottom wall clicked!!");

                    bottom_r = (int) (redScale * 255);
                    colors[0] = bottom_r;
                    bottom_g = (int) (greenScale * 255);
                    colors[1] = bottom_g;
                    bottom_b = (int) (blueScale * 255);
                    colors[2] = bottom_b;
                    colors[3] = R.id.iv_kitchen7_bottom;
                    colorTransform = getColorMatrix(redScale,
                            greenScale, blueScale);
                    colorWall(
                            getBitmapFromAsset(
                                    context,
                                    AppText.KITCHEN7_LOCATION_BOTTOM, 1), colorTransform,
                            imageViews[3]
                    );
                    return colors;

                case R.id.iv_kitchen7_corner:
                    AppLog.showLog(CLASS_TAG, "corner wall clicked!!");

                    corner_r = (int) (redScale * 255);
                    colors[0] = corner_r;
                    corner_g = (int) (greenScale * 255);
                    colors[1] = corner_g;
                    corner_b = (int) (blueScale * 255);
                    colors[2] = corner_b;
                    colors[3] = R.id.iv_kitchen7_corner;
                    colorTransform = getColorMatrix(redScale,
                            greenScale, blueScale);
                    colorWall(
                            getBitmapFromAsset(
                                    context,
                                    AppText.KITCHEN7_LOCATION_CORNER, 1), colorTransform,
                            imageViews[4]
                    );
                    return colors;

                default:
                    AppLog.showLog(CLASS_TAG, "WTF!");
                    break;
            }

        }
        return colors;
    }

    private static int[] paintKitchen6(Context context, View mainImage, View parent, int x, int y, float[] colorScale, ImageView[] imageViews) {

        int imageId = getImageId((RelativeLayout) parent, x, y);
        float redScale = colorScale[0];
        float greenScale = colorScale[1];
        float blueScale = colorScale[2];

        int leftwall_r, leftwall_g, leftwall_b,
                ceiling_r, ceiling_g, ceiling_b,
                rightwall_r, rightwall_g, rightwall_b;
        int[] colors = new int[4];
        if (imageId >= 0) {
            switch (imageId) {
                case R.id.iv_kitchen6_main_image:
                    AppLog.showLog(CLASS_TAG, "main image clicked");
                    break;

                case R.id.iv_kitchen6_left_wall:
                    AppLog.showLog(CLASS_TAG, "left wall clicked!!");

                    leftwall_r = (int) (redScale * 255);
                    colors[0] = leftwall_r;
                    leftwall_g = (int) (greenScale * 255);
                    colors[1] = leftwall_g;
                    leftwall_b = (int) (blueScale * 255);
                    colors[2] = leftwall_b;
                    colors[3] = R.id.iv_kitchen6_left_wall;

                    colorTransform = getColorMatrix(redScale, greenScale, blueScale);
                    colorWall(getBitmapFromAsset(context, AppText.KITCHEN6_LOCATION_LEFT, 1), colorTransform, imageViews[0]);
                    return colors;

                case R.id.iv_kitchen6_ceiling:
                    AppLog.showLog(CLASS_TAG, "left wall clicked!!");

                    ceiling_r = (int) (redScale * 255);
                    colors[0] = ceiling_r;
                    ceiling_g = (int) (greenScale * 255);
                    colors[1] = ceiling_g;
                    ceiling_b = (int) (blueScale * 255);
                    colors[2] = ceiling_b;
                    colors[3] = R.id.iv_kitchen6_ceiling;
                    colorTransform = getColorMatrix(redScale, greenScale, blueScale);
                    colorWall(getBitmapFromAsset(context, AppText.KITCHEN6_LOCATION_CEILING, 1), colorTransform, imageViews[1]);
                    return colors;

                case R.id.iv_kitchen6_right_wall:
                    AppLog.showLog(CLASS_TAG, "mid wall clicked!!");

                    rightwall_r = (int) (redScale * 255);
                    colors[0] = rightwall_r;
                    rightwall_g = (int) (greenScale * 255);
                    colors[1] = rightwall_g;
                    rightwall_b = (int) (blueScale * 255);
                    colors[2] = rightwall_b;
                    colors[3] = R.id.iv_kitchen6_right_wall;

                    colorTransform = getColorMatrix(redScale, greenScale, blueScale);
                    colorWall(getBitmapFromAsset(context, AppText.KITCHEN6_LOCATION_RIGHT, 1), colorTransform, imageViews[2]);
                    return colors;

                default:
                    AppLog.showLog(CLASS_TAG, "WTF!");
                    break;
            }

        }
        return colors;
    }

    private static int[] paintKitchen5(Context context, View mainImage, View parent, int x, int y, float[] colorScale, ImageView[] imageViews) {
        int imageId = getImageId((RelativeLayout) parent, x, y);
        float redScale = colorScale[0];
        float greenScale = colorScale[1];
        float blueScale = colorScale[2];
        int leftwall_r, leftwall_g, leftwall_b,
                ceiling_r, ceiling_g, ceiling_b,
                rightwall_r, rightwall_g, rightwall_b;

        int[] colors = new int[4];
        if (imageId >= 0) {

            switch (imageId) {
                case R.id.iv_kitchen5_main_image:
                    AppLog.showLog(CLASS_TAG, "main image clicked");
                    break;

                case R.id.iv_kitchen5_left_wall:
                    AppLog.showLog(CLASS_TAG, "left wall clicked!!");

                    leftwall_r = (int) (redScale * 255);
                    colors[0] = leftwall_r;
                    leftwall_g = (int) (greenScale * 255);
                    colors[1] = leftwall_g;
                    leftwall_b = (int) (blueScale * 255);
                    colors[2] = leftwall_b;
                    colors[3] = R.id.iv_kitchen5_left_wall;
                    colorTransform = getColorMatrix(redScale, greenScale, blueScale);
                    colorWall(getBitmapFromAsset(context, AppText.KITCHEN5_LOCATION_LEFT, 1), colorTransform, imageViews[0]);
                    return colors;

                case R.id.iv_kitchen5_ceiling:
                    AppLog.showLog(CLASS_TAG, "left wall clicked!!");

                    ceiling_r = (int) (redScale * 255);
                    colors[0] = ceiling_r;
                    ceiling_g = (int) (greenScale * 255);
                    colors[1] = ceiling_g;
                    ceiling_b = (int) (blueScale * 255);
                    colors[2] = ceiling_b;
                    colors[3] = R.id.iv_kitchen5_ceiling;

                    colorTransform = getColorMatrix(redScale, greenScale, blueScale);
                    colorWall(getBitmapFromAsset(context, AppText.KITCHEN5_LOCATION_CEILING, 1), colorTransform, imageViews[1]);
                    return colors;

                case R.id.iv_kitchen5_right_wall:
                    AppLog.showLog(CLASS_TAG, "mid wall clicked!!");

                    rightwall_r = (int) (redScale * 255);
                    colors[0] = rightwall_r;
                    rightwall_g = (int) (greenScale * 255);
                    colors[1] = rightwall_g;
                    rightwall_b = (int) (blueScale * 255);
                    colors[2] = rightwall_b;
                    colors[3] = R.id.iv_kitchen5_right_wall;
                    colorTransform = getColorMatrix(redScale, greenScale, blueScale);
                    colorWall(getBitmapFromAsset(context, AppText.KITCHEN5_LOCATION_RIGHT, 1), colorTransform, imageViews[2]);
                    return colors;

                default:
                    AppLog.showLog(CLASS_TAG, "WTF!");
                    break;
            }
        }
        return colors;
    }

    private static int[] paintKitchen4(Context context, View mainImage, View parent, int x, int y, float[] colorScale, ImageView[] imageViews) {
        int imageId = getImageId((RelativeLayout) parent, x, y);

        float redScale = colorScale[0];
        float greenScale = colorScale[1];
        float blueScale = colorScale[2];

        int ceiling_r, ceiling_g, ceiling_b,
                rightwall_r, rightwall_g, rightwall_b;
        int[] colors = new int[4];
        if (imageId >= 0) {

            switch (imageId) {
                case R.id.iv_kitchen4_main_image:
                    AppLog.showLog(CLASS_TAG, "main image clicked");
                    break;

                case R.id.iv_kitchen4_ceiling_wall:
                    AppLog.showLog(CLASS_TAG, "left wall clicked!!");

                    ceiling_r = (int) (redScale * 255);
                    colors[0] = ceiling_r;
                    ceiling_g = (int) (greenScale * 255);
                    colors[1] = ceiling_g;
                    ceiling_b = (int) (blueScale * 255);
                    colors[2] = ceiling_b;
                    colors[3] = R.id.iv_kitchen4_ceiling_wall;
                    colorTransform = getColorMatrix(redScale, greenScale, blueScale);
                    colorWall(getBitmapFromAsset(context, AppText.KITCHEN4_LOCATION_CEILING, 1), colorTransform, imageViews[0]);
                    return colors;

                case R.id.iv_kitchen4_right_wall:
                    AppLog.showLog(CLASS_TAG, "mid wall clicked!!");

                    rightwall_r = (int) (redScale * 255);
                    colors[0] = rightwall_r;
                    rightwall_g = (int) (greenScale * 255);
                    colors[1] = rightwall_g;
                    rightwall_b = (int) (blueScale * 255);
                    colors[2] = rightwall_b;
                    colors[3] = R.id.iv_kitchen4_right_wall;
                    colorTransform = getColorMatrix(redScale, greenScale, blueScale);
                    colorWall(getBitmapFromAsset(context, AppText.KITCHEN4_LOCATION_RIGHT, 1), colorTransform, imageViews[1]);
                    return colors;

                default:
                    AppLog.showLog(CLASS_TAG, "WTF!");
                    break;
            }
        }
        for (int i = 0; i < colors.length; i++) {
            AppLog.showLog(CLASS_TAG, "kitchen 4 colors-" + (i + 1) + ") " + colors[i]);
        }
        return colors;
    }

    private static int[] paintKitchen3(Context context, View mainImage, View parent, int x, int y, float[] colorScale, ImageView[] imageViews) {

        int imageId = getImageId((RelativeLayout) parent, x, y);

        float redScale = colorScale[0];
        float greenScale = colorScale[1];
        float blueScale = colorScale[2];

        int leftwall_r, leftwall_g, leftwall_b,
                midwall_r, midwall_g, midwall_b,
                ceiling_r, ceiling_g, ceiling_b,
                border_ceiling_r, border_ceiling_g, border_ceiling_b,
                rightwall_r, rightwall_g, rightwall_b;
        int[] colors = new int[4];
        if (imageId >= 0) {

            switch (imageId) {
                case R.id.iv_kitchen3_main_image:
                    AppLog.showLog(CLASS_TAG, "main image clicked");
                    break;

                case R.id.iv_kitchen3_left_wall:
                    AppLog.showLog(CLASS_TAG, "left wall clicked!!");

                    leftwall_r = (int) (redScale * 255);
                    colors[0] = leftwall_r;
                    leftwall_g = (int) (greenScale * 255);
                    colors[1] = leftwall_g;
                    leftwall_b = (int) (blueScale * 255);
                    colors[2] = leftwall_b;
                    colors[3] = R.id.iv_kitchen3_left_wall;
                    colorTransform = getColorMatrix(redScale, greenScale, blueScale);
                    colorWall(getBitmapFromAsset(context, AppText.KITCHEN3_LOCATION_LEFT, 1), colorTransform, imageViews[0]);
                    return colors;

                case R.id.iv_kitchen3_mid_wall:
                    AppLog.showLog(CLASS_TAG, "mid wall clicked!!");

                    midwall_r = (int) (redScale * 255);
                    colors[0] = midwall_r;
                    midwall_g = (int) (greenScale * 255);
                    colors[1] = midwall_g;
                    midwall_b = (int) (blueScale * 255);
                    colors[2] = midwall_b;
                    colors[3] = R.id.iv_kitchen3_mid_wall;
                    colorTransform = getColorMatrix(redScale, greenScale, blueScale);
                    colorWall(getBitmapFromAsset(context, AppText.KITCHEN3_LOCATION_MID, 1), colorTransform, imageViews[1]);
                    return colors;

                case R.id.iv_kitchen3_border_ceiling:
                    AppLog.showLog(CLASS_TAG, "mid wall clicked!!");

                    border_ceiling_r = (int) (redScale * 255);
                    colors[0] = border_ceiling_r;
                    border_ceiling_g = (int) (greenScale * 255);
                    colors[1] = border_ceiling_g;
                    border_ceiling_b = (int) (blueScale * 255);
                    colors[2] = border_ceiling_b;
                    colors[3] = R.id.iv_kitchen3_border_ceiling;
                    colorTransform = getColorMatrix(redScale, greenScale, blueScale);
                    colorWall(getBitmapFromAsset(context, AppText.KITCHEN3_LOCATION_BORDER_CEILING, 1), colorTransform, imageViews[2]);
                    return colors;

                case R.id.iv_kitchen3_ceiling:
                    AppLog.showLog(CLASS_TAG, "left wall clicked!!");

                    ceiling_r = (int) (redScale * 255);
                    colors[0] = ceiling_r;
                    ceiling_g = (int) (greenScale * 255);
                    colors[1] = ceiling_g;
                    ceiling_b = (int) (blueScale * 255);
                    colors[2] = ceiling_b;
                    colors[3] = R.id.iv_kitchen3_ceiling;
                    colorTransform = getColorMatrix(redScale, greenScale, blueScale);
                    colorWall(getBitmapFromAsset(context, AppText.KITCHEN3_LOCATION_CEILING, 1), colorTransform, imageViews[3]);
                    return colors;

                case R.id.iv_kitchen3_right_wall:
                    AppLog.showLog(CLASS_TAG, "mid wall clicked!!");

                    rightwall_r = (int) (redScale * 255);
                    colors[0] = rightwall_r;
                    rightwall_g = (int) (greenScale * 255);
                    colors[1] = rightwall_g;
                    rightwall_b = (int) (blueScale * 255);
                    colors[2] = rightwall_b;
                    colors[3] = R.id.iv_kitchen3_right_wall;
                    colorTransform = getColorMatrix(redScale, greenScale, blueScale);
                    colorWall(getBitmapFromAsset(context, AppText.KITCHEN3_LOCATION_RIGHT, 1), colorTransform, imageViews[4]);
                    return colors;

                default:
                    AppLog.showLog(CLASS_TAG, "WTF!");
                    break;
            }
        }
        return colors;
    }

    private static int[] paintKitchen2(Context context, View mainImage, View parent, int x, int y, float[] colorScale, ImageView[] imageViews) {
        int imageId = getImageId((RelativeLayout) parent, x, y);
        float redScale = colorScale[0];
        float greenScale = colorScale[1];
        float blueScale = colorScale[2];

        int leftwall_r, leftwall_g, leftwall_b,
                midwall_r, midwall_g, midwall_b,
                ceiling_r, ceiling_g, ceiling_b,
                rightwall_r, rightwall_g, rightwall_b;

        int[] colors = new int[4];
        if (imageId >= 0) {
            switch (imageId) {
                case R.id.iv_kitchen2_main_image:
                    AppLog.showLog(CLASS_TAG, "main image clicked");
                    break;

                case R.id.iv_kitchen2_left_wall:
                    AppLog.showLog(CLASS_TAG, "left wall clicked!!");

                    leftwall_r = (int) (redScale * 255);
                    colors[0] = leftwall_r;
                    leftwall_g = (int) (greenScale * 255);
                    colors[1] = leftwall_g;
                    leftwall_b = (int) (blueScale * 255);
                    colors[2] = leftwall_b;
                    colors[3] = R.id.iv_kitchen2_left_wall;
                    colorTransform = getColorMatrix(redScale, greenScale, blueScale);
                    colorWall(getBitmapFromAsset(context, AppText.KITCHEN2_LOCATION_LEFT, 1), colorTransform, imageViews[0]);
                    return colors;

                case R.id.iv_kitchen2_mid_wall:
                    AppLog.showLog(CLASS_TAG, "mid wall clicked!!");

                    midwall_r = (int) (redScale * 255);
                    colors[0] = midwall_r;
                    midwall_g = (int) (greenScale * 255);
                    colors[1] = midwall_g;
                    midwall_b = (int) (blueScale * 255);
                    colors[2] = midwall_b;
                    colors[3] = R.id.iv_kitchen2_mid_wall;
                    colorTransform = getColorMatrix(redScale, greenScale, blueScale);
                    colorWall(getBitmapFromAsset(context, AppText.KITCHEN2_LOCATION_MID, 1), colorTransform, imageViews[1]);
                    return colors;

                case R.id.iv_kitchen2_ceiling:
                    AppLog.showLog(CLASS_TAG, "left wall clicked!!");

                    ceiling_r = (int) (redScale * 255);
                    colors[0] = ceiling_r;
                    ceiling_g = (int) (greenScale * 255);
                    colors[1] = ceiling_g;
                    ceiling_b = (int) (blueScale * 255);
                    colors[2] = ceiling_b;
                    colors[3] = R.id.iv_kitchen2_ceiling;
                    colorTransform = getColorMatrix(redScale, greenScale, blueScale);
                    colorWall(getBitmapFromAsset(context, AppText.KITCHEN2_LOCATION_CEILING, 1), colorTransform, imageViews[2]);
                    return colors;

                case R.id.iv_kitchen2_right_wall:
                    AppLog.showLog(CLASS_TAG, "mid wall clicked!!");

                    rightwall_r = (int) (redScale * 255);
                    colors[0] = rightwall_r;
                    rightwall_g = (int) (greenScale * 255);
                    colors[1] = rightwall_g;
                    rightwall_b = (int) (blueScale * 255);
                    colors[2] = rightwall_b;
                    colors[3] = R.id.iv_kitchen2_right_wall;
                    colorTransform = getColorMatrix(redScale, greenScale, blueScale);
                    colorWall(getBitmapFromAsset(context, AppText.KITCHEN2_LOCATION_RIGHT, 1), colorTransform, imageViews[3]);
                    return colors;

                default:
                    AppLog.showLog(CLASS_TAG, "WTF!");
                    break;
            }
        }

        return colors;
    }

    private static int[] paintKitchen1(Context context, View mainImage, View parent, int x, int y, float[] colorScale, ImageView[] imageViews) {
        int imageId = getImageId((RelativeLayout) parent, x, y);
        float redScale = colorScale[0];
        float greenScale = colorScale[1];
        float blueScale = colorScale[2];
        int leftwall_r, leftwall_g, leftwall_b,
                midwall_r, midwall_g, midwall_b;
        int[] colors = new int[4];
        if (imageId >= 0) {

            switch (imageId) {
                case R.id.iv_kitchen1_main_image:
                    AppLog.showLog(CLASS_TAG, "main image clicked");
                    break;

                case R.id.iv_kitchen1_left_wall:
                    AppLog.showLog(CLASS_TAG, "left wall clicked!!");

                    leftwall_r = (int) (redScale * 255);
                    colors[0] = leftwall_r;
                    leftwall_g = (int) (greenScale * 255);
                    colors[1] = leftwall_g;
                    leftwall_b = (int) (blueScale * 255);
                    colors[2] = leftwall_b;
                    colors[3] = R.id.iv_kitchen1_left_wall;
                    colorTransform = getColorMatrix(redScale, greenScale, blueScale);
                    colorWall(getBitmapFromAsset(context, AppText.KITCHEN1_LOCATION_LEFT, 1), colorTransform, imageViews[0]);
                    return colors;

                case R.id.iv_kitchen1_mid_wall:
                    AppLog.showLog(CLASS_TAG, "mid wall clicked!!");

                    midwall_r = (int) (redScale * 255);
                    colors[0] = midwall_r;
                    midwall_g = (int) (greenScale * 255);
                    colors[1] = midwall_g;
                    midwall_b = (int) (blueScale * 255);
                    colors[2] = midwall_b;
                    colors[3] = R.id.iv_kitchen1_mid_wall;
                    AppLog.showLog(CLASS_TAG, "OnPaint() mid wall (R:" + midwall_r + " G:" + midwall_g + " B:" + midwall_b + ")");
                    colorTransform = getColorMatrix(redScale, greenScale, blueScale);
                    colorWall(getBitmapFromAsset(context, AppText.KITCHEN1_LOCATION_MID, 1), colorTransform, imageViews[1]);
                    return colors;

                default:
                    AppLog.showLog(CLASS_TAG, "WTF!");
                    break;
            }
        }

        return colors;
    }

    private static int[] paintBedRoom6(Context context, View mainImage, View parent, int x, int y, float[] colorScale, ImageView[] imageViews) {
        int imageId = getImageId((RelativeLayout) parent, x, y);
        float redScale = colorScale[0];
        float greenScale = colorScale[1];
        float blueScale = colorScale[2];
        int iv_front_wall_r, iv_front_wall_g, iv_front_wall_b,
                iv_corner_r, iv_corner_g, iv_corner_b,
                iv_leftwall_r, iv_leftwall_g, iv_leftwall_b;
        int[] colors = new int[4];
        if (imageId >= 0) {

            switch (imageId) {
                case R.id.iv_bed6_main_image:
                    AppLog.showLog(CLASS_TAG, "Bedroom 6 main image clicked");
                    break;

                case R.id.iv_bed6_left_wall:
                    AppLog.showLog(CLASS_TAG, "Bedroom 6 left wall clicked!!");

                    iv_leftwall_r = (int) (redScale * 255);
                    colors[0] = iv_leftwall_r;
                    iv_leftwall_g = (int) (greenScale * 255);
                    colors[1] = iv_leftwall_g;
                    iv_leftwall_b = (int) (blueScale * 255);
                    colors[2] = iv_leftwall_b;
                    colors[3] = R.id.iv_bed6_left_wall;
                    colorTransform = getColorMatrix(redScale, greenScale, blueScale);
                    colorWall(getBitmapFromAsset(context, AppText.BEDROOM6_LOCATION_LEFT, 1), colorTransform, imageViews[0]);
                    return colors;

                case R.id.iv_bed6_front_wall:
                    AppLog.showLog(CLASS_TAG, "Bedroom 6 front wall clicked!!");

                    iv_front_wall_r = (int) (redScale * 255);
                    colors[0] = iv_front_wall_r;
                    iv_front_wall_g = (int) (greenScale * 255);
                    colors[1] = iv_front_wall_g;
                    iv_front_wall_b = (int) (blueScale * 255);
                    colors[2] = iv_front_wall_b;
                    colors[3] = R.id.iv_bed6_front_wall;
                    colorTransform = getColorMatrix(redScale, greenScale, blueScale);
                    colorWall(getBitmapFromAsset(context, AppText.BEDROOM6_LOCATION_FRONT, 1), colorTransform, imageViews[1]);
                    return colors;

                case R.id.iv_bed6_corner:
                    AppLog.showLog(CLASS_TAG, "Bedroom 6 corner wall clicked!!");

                    iv_corner_r = (int) (redScale * 255);
                    colors[0] = iv_corner_r;
                    iv_corner_g = (int) (greenScale * 255);
                    colors[1] = iv_corner_g;
                    iv_corner_b = (int) (blueScale * 255);
                    colors[2] = iv_corner_b;
                    colors[3] = R.id.iv_bed6_corner;

                    colorTransform = getColorMatrix(redScale, greenScale, blueScale);
                    colorWall(getBitmapFromAsset(context, AppText.BEDROOM6_LOCATION_CORNER, 1), colorTransform, imageViews[2]);
                    return colors;

                default:
                    AppLog.showLog(CLASS_TAG, "WTF!");
                    break;
            }
        }

        return colors;
    }

    private static int[] paintBedRoom5(Context context, View mainImage, View parent, int x, int y, float[] colorScale, ImageView[] imageViews) {
        int imageId = getImageId((RelativeLayout) parent, x, y);
        float redScale = colorScale[0];
        float greenScale = colorScale[1];
        float blueScale = colorScale[2];
        int leftwall_r, leftwall_g, leftwall_b,
                iv_midwall_left_r, iv_midwall_left_g, iv_midwall_left_b,
                iv_midwall_right_r, iv_midwall_right_g, iv_midwall_right_b,
                rightwall_r, rightwall_g, rightwall_b;
        int[] colors = new int[4];
        if (imageId >= 0) {

            switch (imageId) {
                case R.id.iv_bed5_main_image:
                    AppLog.showLog(CLASS_TAG, "main image clicked");
                    break;

                case R.id.iv_bed5_left_wall:
                    AppLog.showLog(CLASS_TAG, "left wall clicked!!");

                    leftwall_r = (int) (redScale * 255);
                    colors[0] = leftwall_r;
                    leftwall_g = (int) (greenScale * 255);
                    colors[1] = leftwall_g;
                    leftwall_b = (int) (blueScale * 255);
                    colors[2] = leftwall_b;
                    colors[3] = R.id.iv_bed5_left_wall;
                    colorTransform = getColorMatrix(redScale, greenScale, blueScale);
                    colorWall(getBitmapFromAsset(context, AppText.BEDROOM5_LOCATION_LEFT, 1), colorTransform, imageViews[0]);
                    return colors;

                case R.id.iv_bed5_midwall_left:
                    AppLog.showLog(CLASS_TAG, "left wall clicked!!");

                    iv_midwall_left_r = (int) (redScale * 255);
                    colors[0] = iv_midwall_left_r;
                    iv_midwall_left_g = (int) (greenScale * 255);
                    colors[1] = iv_midwall_left_g;
                    iv_midwall_left_b = (int) (blueScale * 255);
                    colors[2] = iv_midwall_left_b;
                    colors[3] = R.id.iv_bed5_midwall_left;

                    colorTransform = getColorMatrix(redScale, greenScale, blueScale);
                    colorWall(getBitmapFromAsset(context, AppText.BEDROOM5_LOCATION_MID_LEFT, 1), colorTransform, imageViews[1]);
                    return colors;

                case R.id.iv_bed5_midwall_right:
                    AppLog.showLog(CLASS_TAG, "left wall clicked!!");

                    iv_midwall_right_r = (int) (redScale * 255);
                    colors[0] = iv_midwall_right_r;
                    iv_midwall_right_g = (int) (greenScale * 255);
                    colors[1] = iv_midwall_right_g;
                    iv_midwall_right_b = (int) (blueScale * 255);
                    colors[2] = iv_midwall_right_b;
                    colors[3] = R.id.iv_bed5_midwall_right;
                    colorTransform = getColorMatrix(redScale, greenScale, blueScale);
                    colorWall(getBitmapFromAsset(context, AppText.BEDROOM5_LOCATION_MID_RIGHT, 1), colorTransform, imageViews[2]);
                    return colors;

                case R.id.iv_bed5_right_wall:
                    AppLog.showLog(CLASS_TAG, "mid wall clicked!!");

                    rightwall_r = (int) (redScale * 255);
                    colors[0] = rightwall_r;
                    rightwall_g = (int) (greenScale * 255);
                    colors[1] = rightwall_g;
                    rightwall_b = (int) (blueScale * 255);
                    colors[2] = rightwall_b;
                    colors[3] = R.id.iv_bed5_right_wall;
                    colorTransform = getColorMatrix(redScale, greenScale, blueScale);
                    colorWall(getBitmapFromAsset(context, AppText.BEDROOM5_LOCATION_RIGHT, 1), colorTransform, imageViews[3]);
                    return colors;

                default:
                    AppLog.showLog(CLASS_TAG, "WTF!");
                    break;
            }

        }

        return colors;
    }

    private static int[] paintBedRoom4(Context context, View mainImage, View parent, int x, int y, float[] colorScale, ImageView[] imageViews) {
        int imageId = getImageId((RelativeLayout) parent, x, y);
        float redScale = colorScale[0];
        float greenScale = colorScale[1];
        float blueScale = colorScale[2];
        int leftwall_r, leftwall_g, leftwall_b,
                ceiling_r, ceiling_g, ceiling_b,
                rightwall_r, rightwall_g, rightwall_b;
        int[] colors = new int[4];
        if (imageId >= 0) {

            switch (imageId) {
                case R.id.iv_bed4_main_image:
                    AppLog.showLog(CLASS_TAG, "main image clicked");
                    break;

                case R.id.iv_bed4_left_wall:
                    AppLog.showLog(CLASS_TAG, "left wall clicked!!");

                    leftwall_r = (int) (redScale * 255);
                    colors[0] = leftwall_r;
                    leftwall_g = (int) (greenScale * 255);
                    colors[1] = leftwall_g;
                    leftwall_b = (int) (blueScale * 255);
                    colors[2] = leftwall_b;
                    colors[3] = R.id.iv_bed4_left_wall;
                    colorTransform = getColorMatrix(redScale, greenScale, blueScale);
                    colorWall(getBitmapFromAsset(context, AppText.BEDROOM4_LOCATION_LEFT, 1), colorTransform, imageViews[0]);
                    return colors;

                case R.id.iv_bed4_ceiling:
                    AppLog.showLog(CLASS_TAG, "left wall clicked!!");

                    ceiling_r = (int) (redScale * 255);
                    colors[0] = ceiling_r;
                    ceiling_g = (int) (greenScale * 255);
                    colors[1] = ceiling_g;
                    ceiling_b = (int) (blueScale * 255);
                    colors[2] = ceiling_b;
                    colors[3] = R.id.iv_bed4_ceiling;
                    colorTransform = getColorMatrix(redScale, greenScale, blueScale);
                    colorWall(getBitmapFromAsset(context, AppText.BEDROOM4_LOCATION_CEILING, 1), colorTransform, imageViews[1]);
                    return colors;

                case R.id.iv_bed4_right_wall:
                    AppLog.showLog(CLASS_TAG, "mid wall clicked!!");

                    rightwall_r = (int) (redScale * 255);
                    colors[0] = rightwall_r;
                    rightwall_g = (int) (greenScale * 255);
                    colors[1] = rightwall_g;
                    rightwall_b = (int) (blueScale * 255);
                    colors[2] = rightwall_b;
                    colors[3] = R.id.iv_bed4_right_wall;
                    colorTransform = getColorMatrix(redScale, greenScale, blueScale);
                    colorWall(getBitmapFromAsset(context, AppText.BEDROOM4_LOCATION_RIGHT, 1), colorTransform, imageViews[2]);
                    return colors;

                default:
                    AppLog.showLog(CLASS_TAG, "WTF!");
                    break;
            }
        }

        return colors;
    }

    private static int[] paintBedRoom3(Context context, View mainImage, View parent, int x, int y, float[] colorScale, ImageView[] imageViews) {
        int imageId = getImageId((RelativeLayout) parent, x, y);
        float redScale = colorScale[0];
        float greenScale = colorScale[1];
        float blueScale = colorScale[2];

        int leftwall_r, leftwall_g, leftwall_b,
                rightwall_r, rightwall_g, rightwall_b;
        int[] colors = new int[4];
        if (imageId >= 0) {

            switch (imageId) {
                case R.id.iv_bedroom3_main_image:
                    AppLog.showLog(CLASS_TAG, "main image clicked");
                    break;

                case R.id.iv_bedroom3_left_wall:
                    AppLog.showLog(CLASS_TAG, "left wall clicked!!");

                    leftwall_r = (int) (redScale * 255);
                    colors[0] = leftwall_r;
                    leftwall_g = (int) (greenScale * 255);
                    colors[1] = leftwall_g;
                    leftwall_b = (int) (blueScale * 255);
                    colors[2] = leftwall_b;
                    colors[3] = R.id.iv_bedroom3_left_wall;
                    colorTransform = getColorMatrix(redScale, greenScale, blueScale);
                    colorWall(getBitmapFromAsset(context, AppText.BEDROOM3_LOCATION_LEFT, 1), colorTransform, imageViews[0]);
                    return colors;

                case R.id.iv_bedroom3_right_wall:
                    AppLog.showLog(CLASS_TAG, "mid wall clicked!!");

                    rightwall_r = (int) (redScale * 255);
                    colors[0] = rightwall_r;
                    rightwall_g = (int) (greenScale * 255);
                    colors[1] = rightwall_g;
                    rightwall_b = (int) (blueScale * 255);
                    colors[2] = rightwall_b;
                    colors[3] = R.id.iv_bedroom3_right_wall;

                    colorTransform = getColorMatrix(redScale, greenScale, blueScale);
                    colorWall(getBitmapFromAsset(context, AppText.BEDROOM3_LOCATION_RIGHT, 1), colorTransform, imageViews[1]);
                    return colors;

                default:
                    AppLog.showLog(CLASS_TAG, "WTF!");
                    break;
            }

        }

        return colors;

    }

    private static int[] paintBedRoom2(Context context, View mainImage, View parent, int x, int y, float[] colorScale, ImageView[] imageViews) {

        int imageId = getImageId((RelativeLayout) parent, x, y);
        int rightwall_r, rightwall_g, rightwall_b;
        float redScale = colorScale[0];
        float greenScale = colorScale[1];
        float blueScale = colorScale[2];
        int[] colors = new int[4];
        if (imageId >= 0) {

            switch (imageId) {
                case R.id.iv_bed2_main_image:
                    AppLog.showLog(CLASS_TAG, "main image clicked");
                    break;

                case R.id.iv_bed2_right_wall:
                    AppLog.showLog(CLASS_TAG, "mid wall clicked!!");

                    rightwall_r = (int) (redScale * 255);
                    colors[0] = rightwall_r;
                    rightwall_g = (int) (greenScale * 255);
                    colors[1] = rightwall_g;
                    rightwall_b = (int) (blueScale * 255);
                    colors[2] = rightwall_b;
                    colors[3] = R.id.iv_bed2_right_wall;
                    colorTransform = getColorMatrix(redScale, greenScale, blueScale);
                    colorWall(getBitmapFromAsset(context, AppText.BEDROOM2_LOCATION_RIGHT, 1), colorTransform, imageViews[0]);
                    return colors;

                default:
                    AppLog.showLog(CLASS_TAG, "WTF!");
                    break;
            }
        }

        return colors;
    }

    private static int[] paintBedRoom1(Context context, View mainImage, View parent, int x, int y, float[] colorScale, ImageView[] imageViews) {
        int imageId = getImageId((RelativeLayout) parent, x, y);

        float redScale = colorScale[0];
        float greenScale = colorScale[1];
        float blueScale = colorScale[2];

        int leftRed, leftGreen, leftBlue, ceilingRed, ceilingGreen, ceilingBlue,
                rightRed, rightGreen, rightBlue;
        int[] colors = new int[4];
        if (imageId >= 0) {

            switch (imageId) {
                case R.id.iv_bed1_main_image:
                    AppLog.showLog(CLASS_TAG + "CLICKED", "main image clicked");
                    break;

                case R.id.iv_bed1_left_wall:
                    AppLog.showLog(CLASS_TAG + "CLICKED", "left wall clicked!!");

                    leftRed = (int) (redScale * 255);
                    colors[0] = leftRed;
                    leftGreen = (int) (greenScale * 255);
                    colors[1] = leftGreen;
                    leftBlue = (int) (blueScale * 255);
                    colors[2] = leftBlue;
                    colors[3] = R.id.iv_bed1_left_wall;

                    colorTransform = getColorMatrix(redScale, greenScale, blueScale);
                    colorWall(getBitmapFromAsset(context, AppText.BEDROOM1_LOCATION_LEFT, 1), colorTransform, imageViews[0]);
                    return colors;
                case R.id.iv_bed1_ceiling:
                    AppLog.showLog(CLASS_TAG + "CLICKED", "ceiling wall clicked!!");

                    ceilingRed = (int) (redScale * 255);
                    colors[0] = ceilingRed;
                    ceilingGreen = (int) (greenScale * 255);
                    colors[1] = ceilingGreen;
                    ceilingBlue = (int) (blueScale * 255);
                    colors[2] = ceilingBlue;
                    colors[3] = R.id.iv_bed1_ceiling;
                    colorTransform = getColorMatrix(redScale, greenScale, blueScale);
                    colorWall(getBitmapFromAsset(context, AppText.BEDROOM1_LOCATION_CEILING, 1), colorTransform, imageViews[1]);
                    return colors;

                case R.id.iv_bed1_right_wall:
                    AppLog.showLog(CLASS_TAG + "CLICKED", "right wall clicked!!");

                    rightRed = (int) (redScale * 255);
                    colors[0] = rightRed;
                    rightGreen = (int) (greenScale * 255);
                    colors[1] = rightGreen;
                    rightBlue = (int) (blueScale * 255);
                    colors[2] = rightBlue;
                    colors[3] = R.id.iv_bed1_right_wall;

                    colorTransform = getColorMatrix(redScale, greenScale, blueScale);
                    colorWall(getBitmapFromAsset(context, AppText.BEDROOM1_LOCATION_RIGHT, 1), colorTransform, imageViews[2]);
                    return colors;

                default:
                    AppLog.showLog(CLASS_TAG + "CLICKED", "WTF!");
                    break;
            }
        }

        return colors;
    }

    private static int[] paintLivingRoom4(Context context, View mainImage, View parent, int x, int y, float[] colorScale, ImageView[] imageViews) {
        int imageId = getImageId((RelativeLayout) parent, x, y);
        int leftwall_r, leftwall_g, leftwall_b;
        int midwall_r, midwall_g, midwall_b;
        int ceiling_r, ceiling_g, ceiling_b;
        int border_ceiling_r, border_ceiling_g, border_ceiling_b;
        int rightwall_r, rightwall_g, rightwall_b;
        int[] colors = new int[4];
        float redScale = colorScale[0];
        float greenScale = colorScale[1];
        float blueScale = colorScale[2];
        if (imageId >= 0) {
            switch (imageId) {
                case R.id.iv_living4_main_image:
                    AppLog.showLog(CLASS_TAG, "main image clicked");
                    break;

                case R.id.iv_living4_left_wall:
                    AppLog.showLog(CLASS_TAG, "left wall clicked!!");

                    leftwall_r = (int) (redScale * 255);
                    colors[0] = leftwall_r;
                    leftwall_g = (int) (greenScale * 255);
                    colors[1] = leftwall_g;
                    leftwall_b = (int) (blueScale * 255);
                    colors[2] = leftwall_b;
                    colors[3] = R.id.iv_living4_left_wall;
                    colorTransform = getColorMatrix(redScale, greenScale, blueScale);
                    colorWall(getBitmapFromAsset(context, AppText.LIVINGROOM4_LOCATION_LEFT, 1), colorTransform, imageViews[0]);
                    return colors;

                case R.id.iv_living4_mid_wall:
                    AppLog.showLog(CLASS_TAG, "mid wall clicked!!");

                    midwall_r = (int) (redScale * 255);
                    colors[0] = midwall_r;
                    midwall_g = (int) (greenScale * 255);
                    colors[1] = midwall_g;
                    midwall_b = (int) (blueScale * 255);
                    colors[2] = midwall_b;
                    colors[3] = R.id.iv_living4_mid_wall;
                    colorTransform = getColorMatrix(redScale, greenScale, blueScale);
                    colorWall(getBitmapFromAsset(context, AppText.LIVINGROOM4_LOCATION_MID, 1), colorTransform, imageViews[1]);
                    return colors;

                case R.id.iv_living4_border_left:
                    AppLog.showLog(CLASS_TAG, "mid wall clicked!!");

                    border_ceiling_r = (int) (redScale * 255);
                    colors[0] = border_ceiling_r;
                    border_ceiling_g = (int) (greenScale * 255);
                    colors[1] = border_ceiling_g;
                    border_ceiling_b = (int) (blueScale * 255);
                    colors[2] = border_ceiling_b;
                    colors[3] = R.id.iv_living4_border_left;
                    colorTransform = getColorMatrix(redScale, greenScale, blueScale);
                    colorWall(getBitmapFromAsset(context, AppText.LIVINGROOM4_LOCATION_BORDER_LEFT, 1), colorTransform, imageViews[2]);
                    return colors;

                case R.id.iv_living4_ceiling:
                    AppLog.showLog(CLASS_TAG, "left wall clicked!!");

                    ceiling_r = (int) (redScale * 255);
                    colors[0] = ceiling_r;
                    ceiling_g = (int) (greenScale * 255);
                    colors[1] = ceiling_g;
                    ceiling_b = (int) (blueScale * 255);
                    colors[2] = ceiling_b;
                    colors[3] = R.id.iv_living4_ceiling;
                    colorTransform = getColorMatrix(redScale, greenScale, blueScale);
                    colorWall(getBitmapFromAsset(context, AppText.LIVINGROOM4_LOCATION_CEILING, 1), colorTransform, imageViews[3]);
                    return colors;

                case R.id.iv_living4_right_wall:
                    AppLog.showLog(CLASS_TAG, "mid wall clicked!!");

                    rightwall_r = (int) (redScale * 255);
                    colors[0] = rightwall_r;
                    rightwall_g = (int) (greenScale * 255);
                    colors[1] = rightwall_g;
                    rightwall_b = (int) (blueScale * 255);
                    colors[2] = rightwall_b;
                    colors[3] = R.id.iv_living4_right_wall;
                    colorTransform = getColorMatrix(redScale, greenScale, blueScale);
                    colorWall(getBitmapFromAsset(context, AppText.LIVINGROOM4_LOCATION_RIGHT, 1), colorTransform, imageViews[4]);
                    return colors;

                default:
                    AppLog.showLog(CLASS_TAG, "WTF!");
                    break;
            }

        }

        return colors;
    }

    private static int[] paintLIvingRoom3(Context context, View mainImage, View parent, int x, int y, float[] colorScale, ImageView[] imageViews) {
        int imageId = getImageId((RelativeLayout) parent, x, y);
        int rightwall_r, rightwall_g, rightwall_b;
        float redScale = colorScale[0];
        float greenScale = colorScale[1];
        float blueScale = colorScale[2];
        int[] colors = new int[4];
        if (imageId >= 0) {

            switch (imageId) {
                case R.id.iv_kitchen5_main_image:
                    AppLog.showLog(CLASS_TAG, "main image clicked");
                    break;

                case R.id.iv_living3_right_wall:
                    AppLog.showLog(CLASS_TAG, "mid wall clicked!!");

                    rightwall_r = (int) (redScale * 255);
                    colors[0] = rightwall_r;
                    rightwall_g = (int) (greenScale * 255);
                    colors[1] = rightwall_g;
                    rightwall_b = (int) (blueScale * 255);
                    colors[2] = rightwall_b;
                    colors[3] = R.id.iv_living3_right_wall;
                    colorTransform = getColorMatrix(redScale, greenScale, blueScale);
                    colorWall(getBitmapFromAsset(context, AppText.LIVINGROOM3_LOCATION_RIGHT, 1), colorTransform, imageViews[0]);
                    return colors;

                default:
                    AppLog.showLog(CLASS_TAG, "WTF!");
                    break;
            }
        }

        return colors;
    }

    private static int[] paintLivingRoom2(Context context, View mainImage, View parent, int x, int y, float[] colorScale, ImageView[] imageViews) {
        int imageId = getImageId((RelativeLayout) parent, x, y);
        float redScale = colorScale[0];
        float greenScale = colorScale[1];
        float blueScale = colorScale[2];
        int rightwall_r, rightwall_g, rightwall_b;
        int[] colors = new int[4];
        if (imageId >= 0) {
            switch (imageId) {
                case R.id.iv_kitchen5_main_image:
                    AppLog.showLog(CLASS_TAG, "main image clicked");
                    break;

                case R.id.iv_living2_right_wall:
                    AppLog.showLog(CLASS_TAG, "mid wall clicked!!");

                    rightwall_r = (int) (redScale * 255);
                    colors[0] = rightwall_r;
                    rightwall_g = (int) (greenScale * 255);
                    colors[1] = rightwall_g;
                    rightwall_b = (int) (blueScale * 255);
                    colors[2] = rightwall_b;
                    colors[3] = R.id.iv_living2_right_wall;
                    colorTransform = getColorMatrix(redScale, greenScale, blueScale);
                    colorWall(getBitmapFromAsset(context, AppText.LIVINGROOM2_LOCATION_RIGHT, 1), colorTransform, imageViews[0]);
                    return colors;

                default:
                    AppLog.showLog(CLASS_TAG, "WTF!");
                    break;
            }

        }

        return colors;
    }

    private static int[] paintLivingRoom1(Context context,
                                          View mainImage, View parent,
                                          int x, int y,
                                          float[] colorScale, ImageView[] imageViews) {
        int imageId = getImageId((RelativeLayout) parent, x, y);
        int[] colors = new int[4];
        float redScale = colorScale[0];
        float greenScale = colorScale[1];
        float blueScale = colorScale[2];
        int borderceiling_r, borderceiling_g, borderceiling_b,
                ceiling_r, ceiling_g, ceiling_b,
                rightwall_r, rightwall_g, rightwall_b;
        if (imageId >= 0) {
            AppLog.showLog(CLASS_TAG, " x: " + x + " y:: " + y +
                    "main image id:: " + mainImage.getId()
                    + " imageId:: " + imageId + " iv_leftwall id:: " + imageViews[0].getId());

            switch (imageId) {
                case R.id.iv_kitchen5_main_image:
                    AppLog.showLog(CLASS_TAG, "main image clicked");
                    break;

                case R.id.iv_living1_border_ceiling:
                    AppLog.showLog(CLASS_TAG, "border ceiling clicked!!");

                    borderceiling_r = (int) (redScale * 255);
                    colors[0] = borderceiling_r;
                    borderceiling_g = (int) (greenScale * 255);
                    colors[1] = borderceiling_g;
                    borderceiling_b = (int) (blueScale * 255);
                    colors[2] = borderceiling_b;
                    colors[3] = R.id.iv_living1_border_ceiling;
                    colorTransform = getColorMatrix(redScale, greenScale, blueScale);
                    colorWall(getBitmapFromAsset(context, AppText.LIVINGROOM1_LOCATION_BORDER_CEILING, 1), colorTransform, imageViews[0]);
                    return colors;

                case R.id.iv_living1_ceiling:
                    AppLog.showLog(CLASS_TAG, "ceiling clicked!!");

                    ceiling_r = (int) (redScale * 255);
                    colors[0] = ceiling_r;
                    ceiling_g = (int) (greenScale * 255);
                    colors[1] = ceiling_g;
                    ceiling_b = (int) (blueScale * 255);
                    colors[2] = ceiling_b;
                    colors[3] = R.id.iv_living1_ceiling;
                    colorTransform = getColorMatrix(redScale, greenScale, blueScale);
                    colorWall(getBitmapFromAsset(context, AppText.LIVINGROOM1_LOCATION_CEILING, 1), colorTransform, imageViews[1]);
                    return colors;

                case R.id.iv_living1_right_wall:
                    AppLog.showLog(CLASS_TAG, "right wall clicked!!");

                    rightwall_r = (int) (redScale * 255);
                    colors[0] = rightwall_r;
                    rightwall_g = (int) (greenScale * 255);
                    colors[1] = rightwall_g;
                    rightwall_b = (int) (blueScale * 255);
                    colors[2] = rightwall_b;
                    colors[3] = R.id.iv_living1_right_wall;
                    colorTransform = getColorMatrix(redScale, greenScale, blueScale);
                    colorWall(getBitmapFromAsset(context, AppText.LIVINGROOM1_LOCATION_RIGHT, 1), colorTransform, imageViews[2]);
                    return colors;

                default:
                    AppLog.showLog(CLASS_TAG, "default clicked!");
                    break;
            }

        }
        return colors;
    }

    private static int[] paintExterior3(Context context, View mainImage, View parent, int x, int y, float[] colorScale, ImageView[] imageViews) {
        int imageId = getImageId((RelativeLayout) parent, x, y);
        float redScale = colorScale[0];
        float greenScale = colorScale[1];
        float blueScale = colorScale[2];
        AppLog.showLog(CLASS_TAG, "red Scale: " + redScale
                + " greenScale: " + greenScale + " blueScale: " + blueScale);
        int iv_1_r, iv_1_g, iv_1_b, iv_2_r, iv_2_g, iv_2_b, iv_3_r, iv_3_g, iv_3_b,
                iv_4_r, iv_4_g, iv_4_b, iv_5_r, iv_5_g, iv_5_b, iv_6_r, iv_6_g, iv_6_b,
                iv_7_r, iv_7_g, iv_7_b, iv_8_r, iv_8_g, iv_8_b, iv_9_r, iv_9_g, iv_9_b,
                iv_10_r, iv_10_g, iv_10_b, iv_11_r, iv_11_g, iv_11_b, iv_12_r, iv_12_g, iv_12_b;
        int[] colors = new int[4];
        if (imageId >= 0) {
            AppLog.showLog(CLASS_TAG, " x: " + x + " y:: " + y + "main image id:: " + mainImage.getId());

            switch (imageId) {
                case R.id.iv_exterior3_main_image:
                    AppLog.showLog(CLASS_TAG, "main image clicked");
                    break;

                case R.id.iv_exterior3_1:
                    AppLog.showLog(CLASS_TAG, "left wall clicked!!");

                    iv_1_r = (int) (redScale * 255);
                    colors[0] = iv_1_r;
                    iv_1_g = (int) (greenScale * 255);
                    colors[1] = iv_1_g;
                    iv_1_b = (int) (blueScale * 255);
                    colors[2] = iv_1_b;
                    colors[3] = R.id.iv_exterior3_1;
                    colorTransform = getColorMatrix(redScale, greenScale, blueScale);
                    colorWall(getBitmapFromAsset(context, AppText.EXTERIOR3_LOCATION_IMAGE1, 1), colorTransform, imageViews[0]);
                    return colors;

                case R.id.iv_exterior3_2:
                    AppLog.showLog(CLASS_TAG, "mid wall clicked!!");

                    iv_2_r = (int) (redScale * 255);
                    colors[0] = iv_2_r;
                    iv_2_g = (int) (greenScale * 255);
                    colors[1] = iv_2_g;
                    iv_2_b = (int) (blueScale * 255);
                    colors[2] = iv_2_b;
                    colors[3] = R.id.iv_exterior3_2;
                    colorTransform = getColorMatrix(redScale, greenScale, blueScale);
                    colorWall(getBitmapFromAsset(context, AppText.EXTERIOR3_LOCATION_IMAGE2, 1), colorTransform, imageViews[1]);
                    return colors;

                case R.id.iv_exterior3_3:
                    AppLog.showLog(CLASS_TAG, "left wall clicked!!");

                    iv_3_r = (int) (redScale * 255);
                    colors[0] = iv_3_r;
                    iv_3_g = (int) (greenScale * 255);
                    colors[1] = iv_3_g;
                    iv_3_b = (int) (blueScale * 255);
                    colors[2] = iv_3_b;
                    colors[3] = R.id.iv_exterior3_3;
                    colorTransform = getColorMatrix(redScale, greenScale, blueScale);
                    colorWall(getBitmapFromAsset(context, AppText.EXTERIOR3_LOCATION_IMAGE3, 1), colorTransform, imageViews[2]);
                    return colors;

                case R.id.iv_exterior3_4:
                    AppLog.showLog(CLASS_TAG, "left wall clicked!!");

                    iv_4_r = (int) (redScale * 255);
                    colors[0] = iv_4_r;
                    iv_4_g = (int) (greenScale * 255);
                    colors[1] = iv_4_g;
                    iv_4_b = (int) (blueScale * 255);
                    colors[2] = iv_4_b;
                    colors[3] = R.id.iv_exterior3_4;
                    colorTransform = getColorMatrix(redScale, greenScale, blueScale);
                    colorWall(getBitmapFromAsset(context, AppText.EXTERIOR3_LOCATION_IMAGE4, 1), colorTransform, imageViews[3]);
                    return colors;

                case R.id.iv_exterior3_5:
                    AppLog.showLog(CLASS_TAG, "left wall clicked!!");

                    iv_5_r = (int) (redScale * 255);
                    colors[0] = iv_5_r;
                    iv_5_g = (int) (greenScale * 255);
                    colors[1] = iv_5_g;
                    iv_5_b = (int) (blueScale * 255);
                    colors[2] = iv_5_b;
                    colors[3] = R.id.iv_exterior3_5;
                    colorTransform = getColorMatrix(redScale, greenScale, blueScale);
                    colorWall(getBitmapFromAsset(context, AppText.EXTERIOR3_LOCATION_IMAGE5, 1), colorTransform, imageViews[4]);
                    return colors;

                case R.id.iv_exterior3_6:
                    AppLog.showLog(CLASS_TAG, "left wall clicked!!");

                    iv_6_r = (int) (redScale * 255);
                    colors[0] = iv_6_r;
                    iv_6_g = (int) (greenScale * 255);
                    colors[1] = iv_6_g;
                    iv_6_b = (int) (blueScale * 255);
                    colors[2] = iv_6_b;
                    colors[3] = R.id.iv_exterior3_6;
                    colorTransform = getColorMatrix(redScale, greenScale, blueScale);
                    colorWall(getBitmapFromAsset(context, AppText.EXTERIOR3_LOCATION_IMAGE6, 1), colorTransform, imageViews[5]);
                    return colors;

                case R.id.iv_exterior3_7:
                    AppLog.showLog(CLASS_TAG, "left wall clicked!!");

                    iv_7_r = (int) (redScale * 255);
                    colors[0] = iv_7_r;
                    iv_7_g = (int) (greenScale * 255);
                    colors[1] = iv_7_g;
                    iv_7_b = (int) (blueScale * 255);
                    colors[2] = iv_7_b;
                    colors[3] = R.id.iv_exterior3_7;
                    colorTransform = getColorMatrix(redScale, greenScale, blueScale);
                    colorWall(getBitmapFromAsset(context, AppText.EXTERIOR3_LOCATION_IMAGE7, 1), colorTransform, imageViews[6]);
                    return colors;
                case R.id.iv_exterior3_8:
                    AppLog.showLog(CLASS_TAG, "left wall clicked!!");

                    iv_8_r = (int) (redScale * 255);
                    colors[0] = iv_8_r;
                    iv_8_g = (int) (greenScale * 255);
                    colors[1] = iv_8_g;
                    iv_8_b = (int) (blueScale * 255);
                    colors[2] = iv_8_b;
                    colors[3] = R.id.iv_exterior3_8;
                    colorTransform = getColorMatrix(redScale, greenScale, blueScale);
                    colorWall(getBitmapFromAsset(context, AppText.EXTERIOR3_LOCATION_IMAGE8, 1), colorTransform, imageViews[7]);
                    return colors;
                case R.id.iv_exterior3_9:
                    AppLog.showLog(CLASS_TAG, "left wall clicked!!");

                    iv_9_r = (int) (redScale * 255);
                    colors[0] = iv_9_r;
                    iv_9_g = (int) (greenScale * 255);
                    colors[1] = iv_9_g;
                    iv_9_b = (int) (blueScale * 255);
                    colors[2] = iv_9_b;
                    colors[3] = R.id.iv_exterior3_9;
                    colorTransform = getColorMatrix(redScale, greenScale, blueScale);
                    colorWall(getBitmapFromAsset(context, AppText.EXTERIOR3_LOCATION_IMAGE9, 1), colorTransform, imageViews[8]);
                    return colors;
                case R.id.iv_exterior3_10:
                    AppLog.showLog(CLASS_TAG, "left wall clicked!!");

                    iv_10_r = (int) (redScale * 255);
                    colors[0] = iv_10_r;
                    iv_10_g = (int) (greenScale * 255);
                    colors[1] = iv_10_g;
                    iv_10_b = (int) (blueScale * 255);
                    colors[2] = iv_10_b;
                    colors[3] = R.id.iv_exterior3_10;
                    colorTransform = getColorMatrix(redScale, greenScale, blueScale);
                    colorWall(getBitmapFromAsset(context, AppText.EXTERIOR3_LOCATION_IMAGE10, 1), colorTransform, imageViews[9]);
                    return colors;

                case R.id.iv_exterior3_11:
                    AppLog.showLog(CLASS_TAG, "left wall clicked!!");
                    iv_11_r = (int) (redScale * 255);
                    colors[0] = iv_11_r;
                    iv_11_g = (int) (greenScale * 255);
                    colors[1] = iv_11_g;
                    iv_11_b = (int) (blueScale * 255);
                    colors[2] = iv_11_b;
                    colors[3] = R.id.iv_exterior3_11;
                    colorTransform = getColorMatrix(redScale, greenScale, blueScale);
                    colorWall(getBitmapFromAsset(context, AppText.EXTERIOR3_LOCATION_IMAGE11, 1), colorTransform, imageViews[10]);
                    return colors;

                case R.id.iv_exterior3_12:
                    AppLog.showLog(CLASS_TAG, "left wall clicked!!");
                    iv_12_r = (int) (redScale * 255);
                    colors[0] = iv_12_r;
                    iv_12_g = (int) (greenScale * 255);
                    colors[1] = iv_12_g;
                    iv_12_b = (int) (blueScale * 255);
                    colors[2] = iv_12_b;
                    colors[3] = R.id.iv_exterior3_12;
                    colorTransform = getColorMatrix(redScale, greenScale, blueScale);
                    colorWall(getBitmapFromAsset(context, AppText.EXTERIOR3_LOCATION_IMAGE12, 1), colorTransform, imageViews[11]);
                    return colors;

                default:
                    AppLog.showLog(CLASS_TAG, "WTF!");
                    break;
            }
        }

        return colors;
    }

    private static int[] paintExterior2(Context context, View mainImage, View parent, int x, int y, float[] colorScale, ImageView[] imageViews) {
        int imageId = getImageId((RelativeLayout) parent, x, y);
        float redScale = colorScale[0];
        float greenScale = colorScale[1];
        float blueScale = colorScale[2];
        int bottom_r, bottom_g, bottom_b, top_r, top_g, top_b;
        int[] colors = new int[4];
        if (imageId >= 0) {
            AppLog.showLog(CLASS_TAG, " x: " + x + " y:: " + y + "main image id:: "
                    + mainImage.getId() + " imageId:: " + imageId +
                    " iv_leftwall id:: " + imageViews[0].getId());

            switch (imageId) {
                case R.id.iv_exterior2_main_image:
                    AppLog.showLog(CLASS_TAG, "main image clicked");
                    break;

                case R.id.iv_exterior2_top:
                    AppLog.showLog(CLASS_TAG, "left wall clicked!!");

                    top_r = (int) (redScale * 255);
                    colors[0] = top_r;
                    top_g = (int) (greenScale * 255);
                    colors[1] = top_g;
                    top_b = (int) (blueScale * 255);
                    colors[2] = top_b;
                    colors[3] = R.id.iv_exterior2_top;

                    colorTransform = getColorMatrix(redScale, greenScale, blueScale);
                    colorWall(getBitmapFromAsset(context, AppText.EXTERIOR2_LOCATION_BOTTOM, 1),
                            colorTransform, imageViews[0]
                    );
                    return colors;

                case R.id.iv_exterior2_bottom:
                    AppLog.showLog(CLASS_TAG, "mid wall clicked!!");

                    bottom_r = (int) (redScale * 255);
                    colors[0] = bottom_r;
                    bottom_g = (int) (greenScale * 255);
                    colors[1] = bottom_g;
                    bottom_b = (int) (blueScale * 255);
                    colors[2] = bottom_b;
                    colors[3] = R.id.iv_exterior2_bottom;
                    colorTransform = getColorMatrix(redScale, greenScale, blueScale);
                    colorWall(getBitmapFromAsset(context, AppText.EXTERIOR2_LOCATION_TOP, 1),
                            colorTransform, imageViews[1]
                    );
                    return colors;

                default:
                    AppLog.showLog(CLASS_TAG, "WTF!");
                    break;
            }
        }
        return colors;
    }

    private static int[] paintExterior1(Context context, View mainImage, View parent, int x, int y,
                                        float[] colorScale, ImageView... imageViews) {
        int imageId = getImageId((RelativeLayout) parent, x, y);
        float redScale = colorScale[0];
        float greenScale = colorScale[1];
        float blueScale = colorScale[2];
        int front_r, front_g, front_b, border_top_r, border_top_g, border_top_b,
                back_r, back_g, back_b, top_r, top_g, top_b;
        int[] colors = new int[4];
        if (imageId >= 0) {
            switch (imageId) {
                case R.id.iv_exterior1_main_image:
                    AppLog.showLog(CLASS_TAG, "main image clicked");
                    break;

                case R.id.iv_exterior1_front:
                    AppLog.showLog(CLASS_TAG, "front wall clicked!!");

                    front_r = (int) (redScale * 255);
                    colors[0] = front_r;
                    front_g = (int) (greenScale * 255);
                    colors[1] = front_g;
                    front_b = (int) (blueScale * 255);
                    colors[2] = front_b;
                    colors[3] = R.id.iv_exterior1_front;
                    colorTransform = getColorMatrix(redScale,
                            greenScale, blueScale);
                    colorWall(getBitmapFromAsset(
                                    context, AppText.EXTERIOR1_LOCATION_FRONT, 1),
                            colorTransform, imageViews[0]
                    );
                    return colors;

                case R.id.iv_exterior1_border:
                    AppLog.showLog(CLASS_TAG, "border clicked!!");

                    border_top_r = (int) (redScale * 255);
                    colors[0] = border_top_r;
                    border_top_g = (int) (greenScale * 255);
                    colors[1] = border_top_g;
                    border_top_b = (int) (blueScale * 255);
                    colors[2] = border_top_b;
                    colors[3] = R.id.iv_exterior1_border;
                    colorTransform = getColorMatrix(redScale,
                            greenScale, blueScale);
                    colorWall(getBitmapFromAsset(
                                    context,
                                    AppText.EXTERIOR1_LOCATION_BORDER_TOP, 1), colorTransform,
                            imageViews[1]
                    );
                    return colors;

                case R.id.iv_exterior1_back:
                    AppLog.showLog(CLASS_TAG, "back wall clicked!!");

                    back_r = (int) (redScale * 255);
                    colors[0] = back_r;
                    back_g = (int) (greenScale * 255);
                    colors[1] = back_g;
                    back_b = (int) (blueScale * 255);
                    colors[2] = back_b;
                    colors[3] = R.id.iv_exterior1_back;
                    colorTransform = getColorMatrix(redScale,
                            greenScale, blueScale);
                    colorWall(getBitmapFromAsset(
                                    context, AppText.EXTERIOR1_LOCATION_BACK, 1),
                            colorTransform, imageViews[2]
                    );
                    return colors;

                case R.id.iv_exterior1_top:
                    AppLog.showLog(CLASS_TAG, "top wall clicked!!");

                    top_r = (int) (redScale * 255);
                    colors[0] = top_r;
                    top_g = (int) (greenScale * 255);
                    colors[1] = top_g;
                    top_b = (int) (blueScale * 255);
                    colors[2] = top_b;
                    colors[3] = R.id.iv_exterior1_top;
                    colorTransform = getColorMatrix(redScale,
                            greenScale, blueScale);
                    colorWall(getBitmapFromAsset(
                                    context, AppText.EXTERIOR1_LOCATION_TOP, 1),
                            colorTransform, imageViews[3]
                    );
                    return colors;

                default:
                    AppLog.showLog(CLASS_TAG, "WTF!");
                    break;
            }
        }
        return colors;
    }

    public static float[] getColorMatrix(float dr, float dg, float db) {
        return new float[]{0, 0, 0, dr, 0, 0, 0, 0, dg, 0, 0, 0, 0, db, 0, 0,
                0, 0, 0.8f, 0};
    }

    public static int getImageId(RelativeLayout parent, int pointerX, int pointerY) {
        for (int a = parent.getChildCount() - 1; a >= 0; a--) {
            if (parent.getChildAt(a) instanceof ImageView) {
                if (!checkPixelTransparent((ImageView) parent.getChildAt(a), pointerX, pointerY)) {
                    AppLog.showLog(CLASS_TAG, "pixel is not transparent for child::" + parent.getChildAt(a));
                    return parent.getChildAt(a).getId();
                } else {
                    AppLog.showLog(CLASS_TAG, "Pixel is transparent for child::" + parent.getChildAt(a));
                }
            } else {
                AppLog.showLog(CLASS_TAG, "Child is not instance of imageView");
            }
        }
//        imageViewMain.setLayoutParams(new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.WRAP_CONTENT, RelativeLayout.LayoutParams.WRAP_CONTENT));
        return -1;
    }

    public static void colorWall(Bitmap sourceBitmap, float[] colorTransform, ImageView imageViewFront) {
        ColorMatrix colorMatrix = new ColorMatrix();
        colorMatrix.setSaturation(0f); // Remove Colour //Grayscale?!
        colorMatrix.set(colorTransform); // Apply the Color

        ColorMatrixColorFilter colorFilter = new ColorMatrixColorFilter(
                colorMatrix);
        Paint paint = new Paint();
        paint.setColorFilter(colorFilter);

        Bitmap resultBitmap = Bitmap.createBitmap(sourceBitmap.copy(
                Bitmap.Config.ARGB_8888, true)); // bitmap == image
        sourceBitmap.recycle();
        Canvas canvas = new Canvas(resultBitmap);
        paint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.SRC_IN));
        canvas.drawBitmap(resultBitmap, 0, 0, paint);

        imageViewFront.setImageBitmap(resultBitmap);
    }

    public static boolean checkPixelTransparent(ImageView imageView, int x, int y) {
        Bitmap bitmap = ((BitmapDrawable) imageView.getDrawable()).getBitmap();
        AppLog.showLog(CLASS_TAG, "check pixel transparent for imageview:: " + imageView);
        if (x < bitmap.getWidth() && y < bitmap.getHeight()) {
            if (Color.alpha(bitmap.getPixel(x, y)) == 0) {
                AppLog.showLog(CLASS_TAG, "TRANSPARENT!");
                return true;
            } else {
                return false;
            }
        } else {
            return true;
        }
    }

    public static Bitmap getBitmapFromSdFromName(Context mContext, String imageName) {

        Bitmap bitmap = null;

        try {
            if (android.os.Environment.getExternalStorageState().equals(android.os.Environment.MEDIA_MOUNTED)) {

                // It have to be matched with the directory in SDCard
                boolean exist = new File(Environment.getExternalStorageDirectory() + File.separator + "PashupatiPaints").exists();
                if (exist) {
                    String imageInSD = Environment.getExternalStorageDirectory().getAbsolutePath() + "/PashupatiPaints/" + imageName + ".PNG";
                    System.out.println("image path in SD CARD:: " + imageInSD);

                    try {
                        bitmap = BitmapFactory.decodeFile(imageInSD);
                        //bitmap = decodeScaledBitmapFromSdCard(imageInSD, 200, 200);
                    } catch (Exception e) {
                    } finally {
                        System.gc(); //CLEAR MEMORY
                    }


                } else {
                    Toast.makeText(mContext, "Gallery is empty!", Toast.LENGTH_SHORT).show();
                }
            } else {
                Toast.makeText(mContext, "External Storage not available!", Toast.LENGTH_SHORT).show();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return bitmap;
    }

    public static void loadImageUsingLoader(ImageLoader imageLoader, final ImageView viewToLoad, String imageUrl) {
        DisplayImageOptions options = new DisplayImageOptions.Builder()
                .cacheInMemory(true)
                .cacheOnDisk(true)
                .build();

        imageLoader.loadImage(imageUrl, options, new ImageLoadingListener() {
            @Override
            public void onLoadingStarted(String s, View view) {

            }

            @Override
            public void onLoadingFailed(String s, View view, FailReason failReason) {

            }

            @Override
            public void onLoadingComplete(String s, View view, Bitmap bitmap) {
                viewToLoad.setImageBitmap(bitmap);
            }

            @Override
            public void onLoadingCancelled(String s, View view) {

            }
        });
    }

    public static int[] undoPainting(Context context, ImageView imageView, String imageLocation, Float[] colorScale) {
        float redScale = colorScale[0];
        float greenScale = colorScale[1];
        float blueScale = colorScale[2];
        int[] colors = new int[4];
        colors[0] = (int) (redScale * 255);
        colors[1] = (int) (greenScale * 255);
        colors[2] = (int) (blueScale * 255);
        colors[3] = imageView.getId();
        colorTransform = getColorMatrix(redScale, greenScale, blueScale);
        colorWall(getBitmapFromAsset(context, imageLocation, 1), colorTransform, imageView);
        return colors;
    }

    public static Bitmap decodeSampledBitmapFromResource(Resources res, int resId,
                                                         int reqWidth, int reqHeight) {

        // First decode with inJustDecodeBounds=true to check dimensions
        final BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true;
        BitmapFactory.decodeResource(res, resId, options);

        // Calculate inSampleSize
        options.inSampleSize = calculateInSampleSize(options, reqWidth, reqHeight);

        // Decode bitmap with inSampleSize set
        options.inJustDecodeBounds = false;
        return BitmapFactory.decodeResource(res, resId, options);
    }

    public static int calculateInSampleSize(
            BitmapFactory.Options options, int reqWidth, int reqHeight) {
        // Raw height and width of image
        final int height = options.outHeight;
        final int width = options.outWidth;
        int inSampleSize = 1;

        if (height > reqHeight || width > reqWidth) {

            final int halfHeight = height / 2;
            final int halfWidth = width / 2;

            // Calculate the largest inSampleSize value that is a power of 2 and keeps both
            // height and width larger than the requested height and width.
            while ((halfHeight / inSampleSize) > reqHeight
                    && (halfWidth / inSampleSize) > reqWidth) {
                inSampleSize *= 2;
            }
        }

        return inSampleSize;
    }
}
