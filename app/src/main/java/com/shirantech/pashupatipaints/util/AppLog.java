package com.shirantech.pashupatipaints.util;

import android.util.Log;

/**
 * Created by susan on 11/28/14.
 */
public class AppLog {
    private static final String APP_TAG = "PASHUPATI_PAINTS";

    public static void showLog(String classTag, String message) {
        Log.i(APP_TAG, classTag + "::" + message);
    }
    public static void showLog(String message) {
        Log.i(APP_TAG, message);
    }
}
