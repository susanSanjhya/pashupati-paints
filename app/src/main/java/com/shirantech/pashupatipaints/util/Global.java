package com.shirantech.pashupatipaints.util;

import android.app.Activity;
import android.app.Application;
import android.graphics.Bitmap;
import android.util.DisplayMetrics;

import com.shirantech.pashupatipaints.model.PaintColor;

import java.util.ArrayList;
import java.util.List;


public class Global extends Application {

    public List<PaintColor> globalPaintColorList = new ArrayList<PaintColor>();
    public Bitmap snapAndPaintBitmap;

    public Bitmap getSnapAndPaintBitmap() {
        return snapAndPaintBitmap;
    }

    public void setSnapAndPaintBitmap(Bitmap snapAndPaintBitmap) {
        this.snapAndPaintBitmap = snapAndPaintBitmap;
    }

    public List<PaintColor> getGlobalPaintColorList() {
        return globalPaintColorList;
    }

    public void setGlobalPaintColorList(List<PaintColor> globalPaintColorList) {
        this.globalPaintColorList = globalPaintColorList;
    }

    private static Global m_Instance;
    float m_fFrameS = 0;

    public int m_nFrameW = 0,
            m_nFrameH = 0,
            m_nTotalW = 0,
            m_nTotalH = 0,
            m_nPaddingX = 0,
            m_nPaddingY = 0;

    // Application constants
    public static final String PRJNAME = "Carousel";
    public Activity m_GUI = null;

    public Global() {
        super();
        m_Instance = this;
        //
    }

    public static Global getInstance() {
        // init instance
        if (m_Instance == null) {
            synchronized (Global.class) {
                if (m_Instance == null) {
                    new Global();
                }
            }
        }
        return m_Instance;
    }

    @Override
    public void onCreate() {
        super.onCreate();

    }

    @Override
    public void onTerminate() {
        super.onTerminate();
    }

    public void InitGUIFrame(Activity context) {
        m_GUI = context;
        DisplayMetrics dm = new DisplayMetrics();
        context.getWindowManager().getDefaultDisplay().getMetrics(dm);
        m_nTotalW = dm.widthPixels;
        m_nTotalH = dm.heightPixels;
        // scale factor
        m_fFrameS = (float) m_nTotalH / 640.0f;
        // compute our frame
        m_nFrameW = (int) (960.0f * m_fFrameS);
        m_nFrameH = m_nTotalH;
        // compute padding for our frame inside the total screen size

        m_nPaddingY = 0;
        m_nPaddingX = (m_nTotalW - m_nFrameW) / 2;
    }

    public int Scale(int v) {
        float s = (float) v * m_fFrameS;
        int rs;

        if (s - (int) s >= 0.5) {
            rs = ((int) s) + 1;
        } else {
            rs = (int) s;
        }
        AppLog.showLog("Scale", "" + s + ": return" + rs);
        return rs;
    }

    int maxW, maxh;

    public void setMaxW(int maxW) {
        this.maxW = maxW;
    }

    public int getMaxh() {
        return maxh;
    }

    public void setMaxh(int maxh) {
        this.maxh = maxh;
    }

    public int getMaxW() {
        return maxW;
    }

}
