package com.shirantech.pashupatipaints.util;

/**
 * Created by susan on 12/2/14.
 */
public class AppText {

    public static final String PREF_KEY_HAS_RUN_BEFORE = "hasRunBefore";
    public static final String KEY_HAS_RUN = "hasRun";

    //exterior 1 image locations.
    public static final String EXTERIOR1_LOCATION_MAIN_IMAGE = "Exterior/exterior1/exterior1_main.png";
    public static final String EXTERIOR1_LOCATION_FRONT = "Exterior/exterior1/exterior1_front.png";
    public static final String EXTERIOR1_LOCATION_BORDER_TOP = "Exterior/exterior1/exterior1_border_top.png";
    public static final String EXTERIOR1_LOCATION_BACK = "Exterior/exterior1/exterior1_back.png";
    public static final String EXTERIOR1_LOCATION_TOP = "Exterior/exterior1/exterior1_top.png";

    //exterior 2 image locations.
    public static final String EXTERIOR2_LOCATION_MAIN_IMAGE = "Exterior/exterior2/exterior2_main.png";
    public static final String EXTERIOR2_LOCATION_BOTTOM = "Exterior/exterior2/exterior2_bottom.png";
    public static final String EXTERIOR2_LOCATION_TOP = "Exterior/exterior2/exterior2_top.png";

    //exterior 3 image locations.
    public static final String EXTERIOR3_LOCATION_MAIN_IMAGE = "Exterior/exterior3/exterior3_main.png";
    public static final String EXTERIOR3_LOCATION_IMAGE1 = "Exterior/exterior3/exterior3_1.png";
    public static final String EXTERIOR3_LOCATION_IMAGE2 = "Exterior/exterior3/exterior3_2.png";
    public static final String EXTERIOR3_LOCATION_IMAGE3 = "Exterior/exterior3/exterior3_3.png";
    public static final String EXTERIOR3_LOCATION_IMAGE4 = "Exterior/exterior3/exterior3_4.png";
    public static final String EXTERIOR3_LOCATION_IMAGE5 = "Exterior/exterior3/exterior3_5.png";
    public static final String EXTERIOR3_LOCATION_IMAGE6 = "Exterior/exterior3/exterior3_6.png";
    public static final String EXTERIOR3_LOCATION_IMAGE7 = "Exterior/exterior3/exterior3_7.png";
    public static final String EXTERIOR3_LOCATION_IMAGE8 = "Exterior/exterior3/exterior3_8.png";
    public static final String EXTERIOR3_LOCATION_IMAGE9 = "Exterior/exterior3/exterior3_9.png";
    public static final String EXTERIOR3_LOCATION_IMAGE10 = "Exterior/exterior3/exterior3_10.png";
    public static final String EXTERIOR3_LOCATION_IMAGE11 = "Exterior/exterior3/exterior3_11.png";
    public static final String EXTERIOR3_LOCATION_IMAGE12 = "Exterior/exterior3/exterior3_12.png";

    //Living 1 image locations.
    public static final String LIVINGROOM1_LOCATION_MAIN = "LivingRoom/livingroom1/livingroom1_main.png";
    public static final String LIVINGROOM1_LOCATION_BORDER_CEILING = "LivingRoom/livingroom1/livingroom1_border_ceiling.png";
    public static final String LIVINGROOM1_LOCATION_CEILING = "LivingRoom/livingroom1/livingroom1_ceiling.png";
    public static final String LIVINGROOM1_LOCATION_RIGHT = "LivingRoom/livingroom1/livingroom1_right.png";

    //Living 2 image locations.
    public static final String LIVINGROOM2_LOCATION_MAIN = "LivingRoom/livingroom2/livingroom2_main.png";
    public static final String LIVINGROOM2_LOCATION_RIGHT = "LivingRoom/livingroom2/livingroom2_right.png";

    //Living 3 image locations.
    public static final String LIVINGROOM3_LOCATION_MAIN = "LivingRoom/livingroom3/livingroom3_main.png";
    public static final String LIVINGROOM3_LOCATION_RIGHT = "LivingRoom/livingroom3/livingroom3_right.png";

    //Living 4 image locations.
    public static final String LIVINGROOM4_LOCATION_MAIN = "LivingRoom/livingroom4/livingroom4_main.png";
    public static final String LIVINGROOM4_LOCATION_LEFT = "LivingRoom/livingroom4/livingroom4_left.png";
    public static final String LIVINGROOM4_LOCATION_MID = "LivingRoom/livingroom4/livingroom4_mid.png";
    public static final String LIVINGROOM4_LOCATION_CEILING = "LivingRoom/livingroom4/livingroom4_ceiling.png";
    public static final String LIVINGROOM4_LOCATION_BORDER_LEFT = "LivingRoom/livingroom4/livingroom4_border_left.png";
    public static final String LIVINGROOM4_LOCATION_RIGHT = "LivingRoom/livingroom4/livingroom4_right.png";

    //Bedroom 1 image locations.
    public static final String BEDROOM1_LOCATION_MAIN = "Bedroom/bedroom1/bed1_main.png";
    public static final String BEDROOM1_LOCATION_LEFT = "Bedroom/bedroom1/bed1_left.png";
    public static final String BEDROOM1_LOCATION_CEILING = "Bedroom/bedroom1/bed1_ceiling.png";
    public static final String BEDROOM1_LOCATION_RIGHT = "Bedroom/bedroom1/bed1_right.png";

    //Bedroom 2 image locations.
    public static final String BEDROOM2_LOCATION_MAIN = "Bedroom/bedroom2/bedroom2_main.png";
    public static final String BEDROOM2_LOCATION_RIGHT = "Bedroom/bedroom2/bedroom2_right.png";

    //Bedroom 3 image locations.
    public static final String BEDROOM3_LOCATION_MAIN = "Bedroom/bedroom3/bedroom3_main.png";
    public static final String BEDROOM3_LOCATION_LEFT = "Bedroom/bedroom3/bedroom3_left.png";
    public static final String BEDROOM3_LOCATION_RIGHT = "Bedroom/bedroom3/bedroom3_right.png";

    //Bedroom 4 image locations.
    public static final String BEDROOM4_LOCATION_MAIN = "Bedroom/bedroom4/bedroom4_main.png";
    public static final String BEDROOM4_LOCATION_LEFT = "Bedroom/bedroom4/bedroom4_left.png";
    public static final String BEDROOM4_LOCATION_CEILING = "Bedroom/bedroom4/bedroom4_ceiling.png";
    public static final String BEDROOM4_LOCATION_RIGHT = "Bedroom/bedroom4/bedroom4_mid.png";

    //Bedroom 5 image locations
    public static final String BEDROOM5_LOCATION_MAIN = "Bedroom/bedroom5/bed5_main.png";
    public static final String BEDROOM5_LOCATION_LEFT = "Bedroom/bedroom5/bed5_left.png";
    public static final String BEDROOM5_LOCATION_MID_LEFT = "Bedroom/bedroom5/bed5_mid_left.png";
    public static final String BEDROOM5_LOCATION_MID_RIGHT = "Bedroom/bedroom5/bed5_mid_right.png";
    public static final String BEDROOM5_LOCATION_RIGHT = "Bedroom/bedroom5/bed5_right.png";

    //Bedroom 5 image locations.
    public static final String BEDROOM6_LOCATION_MAIN = "Bedroom/bedroom6/bedroom6_main.png";
    public static final String BEDROOM6_LOCATION_LEFT = "Bedroom/bedroom6/bedroom6_left_wall.png";
    public static final String BEDROOM6_LOCATION_FRONT = "Bedroom/bedroom6/bedroom6_front_wall.png";
    public static final String BEDROOM6_LOCATION_CORNER = "Bedroom/bedroom6/bedroom6_corner.png";

    //Kitchen 1 image locations.
    public static final String KITCHEN1_LOCATION_MAIN = "Kitchen/kitchen1/kitchen1_main.png";
    public static final String KITCHEN1_LOCATION_LEFT = "Kitchen/kitchen1/kitchen1_left.png";
    public static final String KITCHEN1_LOCATION_MID = "Kitchen/kitchen1/kitchen1_mid.png";

    //Kitchen 2 image locations.
    public static final String KITCHEN2_LOCATION_MAIN = "Kitchen/kitchen2/kitchen2_main.png";
    public static final String KITCHEN2_LOCATION_LEFT = "Kitchen/kitchen2/kitchen2_left.png";
    public static final String KITCHEN2_LOCATION_MID = "Kitchen/kitchen2/kitchen2_mid.png";
    public static final String KITCHEN2_LOCATION_CEILING = "Kitchen/kitchen2/kitchen2_ceiling.png";
    public static final String KITCHEN2_LOCATION_RIGHT = "Kitchen/kitchen2/kitchen2_right.png";

    //Kitchen 3 image locations.
    public static final String KITCHEN3_LOCATION_MAIN = "Kitchen/kitchen3/kitchen3_main.png";
    public static final String KITCHEN3_LOCATION_LEFT = "Kitchen/kitchen3/kitchen3_left.png";
    public static final String KITCHEN3_LOCATION_MID = "Kitchen/kitchen3/kitchen3_mid.png";
    public static final String KITCHEN3_LOCATION_CEILING = "Kitchen/kitchen3/kitchen3_ceiling.png";
    public static final String KITCHEN3_LOCATION_BORDER_CEILING = "Kitchen/kitchen3/kitchen3_border_ceiling.png";
    public static final String KITCHEN3_LOCATION_RIGHT = "Kitchen/kitchen3/kitchen3_right.png";

    //Kitchen 4 image locations
    public static final String KITCHEN4_LOCATION_MAIN = "Kitchen/kitchen4/kitchen4_main.png";
    public static final String KITCHEN4_LOCATION_CEILING = "Kitchen/kitchen4/kitchen4_ceiling.png";
    public static final String KITCHEN4_LOCATION_RIGHT = "Kitchen/kitchen4/kitchen4_right.png";

    //Kitchen 5 image locations.
    public static final String KITCHEN5_LOCATION_MAIN = "Kitchen/kitchen5/kitchen5_main.png";
    public static final String KITCHEN5_LOCATION_LEFT = "Kitchen/kitchen5/kitchen5_left.png";
    public static final String KITCHEN5_LOCATION_CEILING = "Kitchen/kitchen5/kitchen5_ceiling.png";
    public static final String KITCHEN5_LOCATION_RIGHT = "Kitchen/kitchen5/kitchen5_right.png";

    //Kitchen 6 image locations.
    public static final String KITCHEN6_LOCATION_MAIN = "Kitchen/kitchen6/kitchen6_main.png";
    public static final String KITCHEN6_LOCATION_LEFT = "Kitchen/kitchen6/kitchen6_frontwall.png";
    public static final String KITCHEN6_LOCATION_CEILING = "Kitchen/kitchen6/kitchen6_top.png";
    public static final String KITCHEN6_LOCATION_RIGHT = "Kitchen/kitchen6/kitchen6_rightwall.png";

    //Kitchen 7 image locations.
    public static final String KITCHEN7_LOCATION_MAIN = "Dining/dining1/dining1_main.png";
    public static final String KITCHEN7_LOCATION_LEFT = "Dining/dining1/dining1_leftwall.png";
    public static final String KITCHEN7_LOCATION_TOP = "Dining/dining1/dining1_top.png";
    public static final String KITCHEN7_LOCATION_RIGHT = "Dining/dining1/dining1_rightwall.png";
    public static final String KITCHEN7_LOCATION_BOTTOM = "Dining/dining1/dining1_bottom.png";
    public static final String KITCHEN7_LOCATION_CORNER = "Dining/dining1/dining1_corner.png";
}
