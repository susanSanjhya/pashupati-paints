package com.shirantech.pashupatipaints.util;

import android.content.Context;
import android.util.Log;

import com.shirantech.pashupatipaints.model.PaintColor;
import com.shirantech.pashupatipaints.model.PaintDealer;

import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.poifs.filesystem.POIFSFileSystem;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.Iterator;

public class ExcelHelper {

    private static final String LOG_TAG = "ExcelHelper";

    public static ArrayList<PaintColor> readExcelFile(Context context, int fileId) {

        ArrayList<PaintColor> paintColorList = new ArrayList<PaintColor>();

        if (!Utility.isExternalStorageAvailable() || Utility.isExternalStorageReadOnly()) {
            Log.w(LOG_TAG, "Storage not available or read only");
            return null;
        }

        try {
            // Creating Input Stream 
            /*File file = new File(context.getExternalFilesDir(null), filename); 
            FileInputStream myInput = new FileInputStream(file);*/

            InputStream myInput = context.getResources().openRawResource(fileId);

            // Create a POIFSFileSystem object 
            POIFSFileSystem myFileSystem = new POIFSFileSystem(myInput);

            // Create a workbook using the File System 
            HSSFWorkbook myWorkBook = new HSSFWorkbook(myFileSystem);

            // Get the first sheet from workbook 
            HSSFSheet mySheet = myWorkBook.getSheetAt(0);

            /** We now need something to iterate through the cells.**/
            Iterator<Row> rowIter = mySheet.rowIterator();

            while (rowIter.hasNext()) {

                HSSFRow myRow = (HSSFRow) rowIter.next();
                //SKIP the first 1 row: titles
                if (myRow.getRowNum() < 1) {
                    continue;
                }

                PaintColor paintColor = new PaintColor();

                Iterator<Cell> cellIter = myRow.cellIterator();
                while (cellIter.hasNext()) {
                    HSSFCell myCell = (HSSFCell) cellIter.next();

                    String cellValue;

                    //Check for all Cell Type
                    if (myCell.getCellType() == HSSFCell.CELL_TYPE_STRING) {
                        cellValue = myCell.getStringCellValue();
                    } else {
                        cellValue = String.valueOf(myCell.getNumericCellValue());
                    }

                    Log.w(LOG_TAG, "Cell Value: " + myCell.toString());

                    //Push the parsed data in the Java Object
                    //Check for cell index
                    switch (myCell.getColumnIndex()) {
                        case 0:
                            paintColor.setCategoryId(cellValue);
                            break;

                        case 1:
                            paintColor.setName(cellValue);
                            break;

                        case 2:
                            paintColor.setColorCode("#" + cellValue);
                            break;
                        case 3:
                            paintColor.setPaintCode(cellValue);
                            break;
                        default:
                            break;
                    }

                }
                //Add Objects to List
                paintColorList.add(paintColor);
                System.out.println("paint colors added to the list. size:: " + paintColorList.size());
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

        return paintColorList;
    }

    public static ArrayList<PaintDealer> readExcelDealers(Context context, int fileId) {

        AppLog.showLog(LOG_TAG, "readExcelDealsers method entered");
        ArrayList<PaintDealer> dealerList = new ArrayList<PaintDealer>();

        if (!Utility.isExternalStorageAvailable() || Utility.isExternalStorageReadOnly()) {
            Log.w(LOG_TAG, "Storage not available or read only");
            return null;
        }

        try {
            // Creating Input Stream 
            /*File file = new File(context.getExternalFilesDir(null), filename); 
            FileInputStream myInput = new FileInputStream(file);*/

            InputStream myInput = context.getResources().openRawResource(fileId);

            // Create a POIFSFileSystem object 
            POIFSFileSystem myFileSystem = new POIFSFileSystem(myInput);

            // Create a workbook using the File System 
            HSSFWorkbook myWorkBook = new HSSFWorkbook(myFileSystem);

            // Get the first sheet from workbook 
            HSSFSheet mySheet = myWorkBook.getSheetAt(0);

            /** We now need something to iterate through the cells.**/
            Iterator<Row> rowIter = mySheet.rowIterator();

            while (rowIter.hasNext()) {

                HSSFRow myRow = (HSSFRow) rowIter.next();
                //SKIP the first 2 row: titles
                /*if(myRow.getRowNum() < 2){
                    continue;
            	}*/

                PaintDealer dealer = new PaintDealer();

                Iterator<Cell> cellIter = myRow.cellIterator();
                while (cellIter.hasNext()) {
                    HSSFCell myCell = (HSSFCell) cellIter.next();

                    String cellValue;

                    //Check for all Cell Type
                    /*if(myCell.getCellType() == HSSFCell.CELL_TYPE_STRING){
                        cellValue = myCell.getStringCellValue();
                    } else {
                    	cellValue = String.valueOf(myCell.getNumericCellValue());
                    }*/

                    myCell.setCellType(Cell.CELL_TYPE_STRING);
                    cellValue = myCell.getStringCellValue();


                    //Push the parsed data in the Java Object
                    //Check for cell index
                    switch (myCell.getColumnIndex()) {
                        case 0:
                            break;
                        case 1:
                            cellValue = cellValue.toUpperCase();
                            dealer.setDealerName(cellValue);
                            AppLog.showLog(LOG_TAG, "dealer name: " + dealer.getDealerName());
                            break;
                        case 2:
                            AppLog.showLog(LOG_TAG, "Dealer branch: " + cellValue);
                            break;
                        case 3:
                            cellValue = cellValue.substring(0, 1).toUpperCase() + cellValue.substring(1).toLowerCase();
                            dealer.setCity(cellValue);
                            AppLog.showLog(LOG_TAG, "dealer city: " + dealer.getCity());
                            break;
                        case 4:
                            cellValue = cellValue.substring(0, 1).toUpperCase() + cellValue.substring(1).toLowerCase();
                            dealer.setDistrict(cellValue);
                            AppLog.showLog(LOG_TAG, "dealer district: " + dealer.getDistrict());
                            break;
                        case 5:
                            dealer.setTelephone(cellValue);
                            AppLog.showLog(LOG_TAG, "dealer phone: " + dealer.getTelephone());
                            break;

                        default:
                            break;
                    }

                }
                //Add Objects to List
                dealerList.add(dealer);
                System.out.println("kathmandu dealers added to the list. size:: " + dealerList.size());
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

        return dealerList;
    }

}
