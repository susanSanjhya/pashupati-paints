package com.shirantech.pashupatipaints.controller;

import android.os.AsyncTask;

import com.shirantech.pashupatipaints.interfaces.ModelListGetter;
import com.shirantech.pashupatipaints.model.PaintColor;
import com.shirantech.pashupatipaints.util.Global;

import java.util.List;

/**
 * Created by susan on 12/4/14.
 */
public class GetGlobalColorListTask extends AsyncTask<Void, Void, Void> {

    private Global global;
    private ModelListGetter<PaintColor> listener;
    private List<PaintColor> paintColorList;

    public GetGlobalColorListTask(Global global, ModelListGetter<PaintColor> listener) {
        this.global = global;
        this.listener = listener;
    }

    @Override
    protected void onPreExecute() {
        listener.onTaskStarted();
    }

    @Override
    protected Void doInBackground(Void... voids) {
        paintColorList = global.getGlobalPaintColorList();
        return null;
    }

    @Override
    protected void onPostExecute(Void aVoid) {
        listener.onTaskFinished(paintColorList);
    }
}
