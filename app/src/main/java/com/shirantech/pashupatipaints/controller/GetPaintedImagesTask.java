package com.shirantech.pashupatipaints.controller;

import android.content.Context;
import android.os.AsyncTask;

import com.shirantech.pashupatipaints.database.dao.PaintedImageDao;
import com.shirantech.pashupatipaints.interfaces.ModelListGetter;
import com.shirantech.pashupatipaints.model.PaintedImage;
import com.shirantech.pashupatipaints.util.AppLog;

import java.util.List;

/**
 * Created by susan on 12/3/14.
 */
public class GetPaintedImagesTask extends AsyncTask<Void, Void, Void> {
    private static final String CLASS_TAG = GetPaintedImagesTask.class.getSimpleName();
    private ModelListGetter<PaintedImage> listener;
    private Context context;
    private List<PaintedImage> paintedImageList;

    public GetPaintedImagesTask(Context context, ModelListGetter<PaintedImage> listener) {
        this.context = context;
        this.listener = listener;
    }

    @Override
    protected void onPreExecute() {
        listener.onTaskStarted();
    }

    @Override
    protected Void doInBackground(Void... voids) {
        PaintedImageDao paintedImageDao = new PaintedImageDao(context);
        paintedImageList = paintedImageDao.getAllPaintedImages();
        AppLog.showLog(CLASS_TAG, "Size of painted image list : " + paintedImageList.size());
        return null;
    }

    @Override
    protected void onPostExecute(Void aVoid) {
        listener.onTaskFinished(paintedImageList);
    }
}
