package com.shirantech.pashupatipaints.controller;

import android.content.Context;
import android.os.AsyncTask;

import com.shirantech.pashupatipaints.database.dao.DealerDao;
import com.shirantech.pashupatipaints.interfaces.ModelListGetter;
import com.shirantech.pashupatipaints.model.PaintDealer;

import java.util.List;

/**
 * Created by susan on 12/15/14.
 */
public class GetDistinctDistrictTask extends AsyncTask<String, Void, Void> {
    private Context context;
    private ModelListGetter<PaintDealer> listener;
    private List<PaintDealer> distinctDistrictPaintDealerList;

    public GetDistinctDistrictTask(Context context, ModelListGetter<PaintDealer> listener) {
        this.context = context;
        this.listener = listener;
    }

    @Override
    protected void onPreExecute() {
        listener.onTaskStarted();
    }

    @Override
    protected Void doInBackground(String... strings) {
        distinctDistrictPaintDealerList = new DealerDao(context).getDistinctDistrict(strings[0]);
        return null;
    }

    @Override
    protected void onPostExecute(Void aVoid) {
        listener.onTaskFinished(distinctDistrictPaintDealerList);
    }
}
