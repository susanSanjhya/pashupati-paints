package com.shirantech.pashupatipaints.controller;

import android.content.Context;
import android.os.AsyncTask;

import com.shirantech.pashupatipaints.database.dao.DealerDao;
import com.shirantech.pashupatipaints.interfaces.ModelListGetter;
import com.shirantech.pashupatipaints.model.PaintDealer;

import java.util.List;

/**
 * Created by susan on 12/17/14.
 */
public class GetDealerByBranchDistrictCityTask extends AsyncTask<String, Void, Void> {
    private Context context;
    private ModelListGetter<PaintDealer> listener;
    private List<PaintDealer> paintDealerList;

    public GetDealerByBranchDistrictCityTask(Context context, ModelListGetter<PaintDealer> listener) {
        this.context = context;
        this.listener = listener;
    }

    @Override
    protected void onPreExecute() {
        listener.onTaskStarted();
    }

    @Override
    protected Void doInBackground(String... strings) {
        paintDealerList = new DealerDao(context)
                .getDealersByBranchDistrictCity(strings[0], strings[1], strings[2]);
        return null;
    }

    @Override
    protected void onPostExecute(Void aVoid) {
        listener.onTaskFinished(paintDealerList);
    }
}
