package com.shirantech.pashupatipaints.controller;

import android.content.Context;
import android.os.AsyncTask;

import com.shirantech.pashupatipaints.database.dao.DealerDao;
import com.shirantech.pashupatipaints.interfaces.ModelListGetter;
import com.shirantech.pashupatipaints.model.PaintDealer;
import com.shirantech.pashupatipaints.util.AppLog;

import java.util.List;

/**
 * Created by susan on 12/3/14.
 */
public class GetDealerTask extends AsyncTask<String, Void, Void> {

    private static final String CLASS_TAG = GetDealerTask.class.getSimpleName();
    private ModelListGetter<PaintDealer> listener;
    private Context context;
    private List<PaintDealer> paintDealerList;

    public GetDealerTask(Context context, ModelListGetter<PaintDealer> listener) {
        this.context = context;
        this.listener = listener;
    }

    @Override
    protected void onPreExecute() {
        listener.onTaskStarted();
    }

    @Override
    protected Void doInBackground(String... strings) {
        DealerDao dealerDao = new DealerDao(context);

        //DEALERS FROM KTM LIST GET
        paintDealerList = dealerDao.getAllDealers(strings[0]);
        AppLog.showLog(CLASS_TAG, "Dealers List ktm size:: " + paintDealerList.size());
        return null;
    }

    @Override
    protected void onPostExecute(Void aVoid) {
        listener.onTaskFinished(paintDealerList);
    }
}
