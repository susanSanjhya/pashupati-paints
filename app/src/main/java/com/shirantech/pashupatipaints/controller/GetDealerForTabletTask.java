package com.shirantech.pashupatipaints.controller;

import android.content.Context;
import android.os.AsyncTask;

import com.shirantech.pashupatipaints.activities.list.DealersDetailActivity;
import com.shirantech.pashupatipaints.database.dao.DealerDao;
import com.shirantech.pashupatipaints.interfaces.ModelListGetter;
import com.shirantech.pashupatipaints.model.PaintDealer;
import com.shirantech.pashupatipaints.util.AppLog;

import java.util.List;

/**
 * Created by susan on 12/14/14.
 */
public class GetDealerForTabletTask extends AsyncTask<Void, Void, Void> {
    private static final String CLASS_TAG = GetDealerForTabletTask.class.getSimpleName();
    private ModelListGetter<PaintDealer> listener;
    private Context context;
    private List<PaintDealer> paintDealerList;

    public GetDealerForTabletTask(Context context, ModelListGetter<PaintDealer> listener) {
        this.context = context;
        this.listener = listener;
    }

    @Override
    protected void onPreExecute() {
        listener.onTaskStarted();
    }

    @Override
    protected Void doInBackground(Void... strings) {
        DealerDao dealerDao = new DealerDao(context);

        //DEALERS FROM KTM LIST GET
        paintDealerList = dealerDao.getAllDealers();
        AppLog.showLog(CLASS_TAG, "Dealers List size:: " + paintDealerList.size());
        return null;
    }

    @Override
    protected void onPostExecute(Void aVoid) {
        listener.onTaskFinished(paintDealerList);
    }
}
