package com.shirantech.pashupatipaints.controller;

import android.content.Context;
import android.os.AsyncTask;

import com.shirantech.pashupatipaints.interfaces.ModelListGetter;
import com.shirantech.pashupatipaints.model.RoomCategory;
import com.shirantech.pashupatipaints.util.AppLog;

import java.util.ArrayList;
import java.util.List;

/**
 * Controller task for getting room title and Bitmap.
 * Created by susan on 12/3/14.
 */
public class GetRoomCategoriesTask extends AsyncTask<String, Void, Void> {
    private static final String CLASS_TAG = GetRoomCategoriesTask.class.getSimpleName();
    private ModelListGetter<RoomCategory> listener;
    private Context context;
    private List<RoomCategory> roomCategoryList;
    private String categoryTitle;

    public GetRoomCategoriesTask(Context context, ModelListGetter<RoomCategory> listener, String categoryTitle) {
        this.context = context;
        this.listener = listener;
        this.categoryTitle = categoryTitle;
    }

    @Override
    protected void onPreExecute() {
        listener.onTaskStarted();
    }

    @Override
    protected Void doInBackground(String... drawableArray) {
        roomCategoryList = getRoomCategories(drawableArray);
        return null;
    }

    private List<RoomCategory> getRoomCategories(String... drawableArray) {
        List<RoomCategory> roomCategoryList = new ArrayList<RoomCategory>();
        for (String aDrawableArray : drawableArray) {
            RoomCategory roomCategory = new RoomCategory();
            roomCategory.setTitle(categoryTitle);
            roomCategory.setDrawable(aDrawableArray);
            roomCategoryList.add(roomCategory);
        }
        AppLog.showLog(CLASS_TAG, "Room Category count:: " + roomCategoryList.size());

        return roomCategoryList;
    }

    @Override
    protected void onPostExecute(Void aVoid) {
        listener.onTaskFinished(roomCategoryList);
    }
}
