package com.shirantech.pashupatipaints.controller;

import android.content.Context;
import android.os.AsyncTask;

import com.shirantech.pashupatipaints.R;
import com.shirantech.pashupatipaints.database.dao.DealerDao;
import com.shirantech.pashupatipaints.database.dao.PaintColorDao;
import com.shirantech.pashupatipaints.interfaces.DbLoadListener;
import com.shirantech.pashupatipaints.model.PaintColor;
import com.shirantech.pashupatipaints.model.PaintDealer;
import com.shirantech.pashupatipaints.util.AppLog;
import com.shirantech.pashupatipaints.util.ExcelHelper;

import java.util.ArrayList;

/**
 * Created by susan on 12/2/14.
 */
public class FromExcelToDbTask extends AsyncTask<Void, Void, Void> {

    private static final String CLASS_TAG = FromExcelToDbTask.class.getSimpleName();
    private DbLoadListener listener;
    private Context context;

    public FromExcelToDbTask(Context context, DbLoadListener listener) {
        this.context = context;
        this.listener = listener;
    }

    @Override
    protected void onPreExecute() {
        listener.onTaskStarted();
    }

    @Override
    protected Void doInBackground(Void... voids) {
        //PAINT COLOR LIST GET
        ArrayList<PaintColor> paintColorList = ExcelHelper.readExcelFile(context, R.raw.new_color_codes);
        AppLog.showLog(CLASS_TAG, "paint color size:: Colorchooser:: " + paintColorList.size());

        ArrayList<PaintDealer> dealerListKathmandu = ExcelHelper.readExcelDealers(context, R.raw.pp_dealer_kathmandu);
        ArrayList<PaintDealer> dealerListPokhara = ExcelHelper.readExcelDealers(context, R.raw.pp_dealer_pokhara);
        ArrayList<PaintDealer> dealerListNarayanghat = ExcelHelper.readExcelDealers(context, R.raw.pp_dealer_narayanghat);
        ArrayList<PaintDealer> dealerListBiratnagar = ExcelHelper.readExcelDealers(context, R.raw.pp_dealer_biratnagar);

        //PAINT COLOR LIST INSERT

        PaintColorDao paintColorDao = new PaintColorDao(context);
        try {
            if (paintColorDao.insertRecord(paintColorList) > 0) {
                AppLog.showLog(CLASS_TAG, "inserted successfully.");
            } else {
                AppLog.showLog(CLASS_TAG, "Cannot be inserted.");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        //INSERT DEALERS: kathmandu
        DealerDao dealerDao = new DealerDao(context);

        for (PaintDealer dealer : dealerListKathmandu) {
            AppLog.showLog(CLASS_TAG, "Dealer name ktm::" + dealer.getDealerName());
        }
        try {

            if (dealerDao.insertRecord(dealerListKathmandu, "Kathmandu") > 0) {
                AppLog.showLog(CLASS_TAG, "dealer kathmandu successfully!");
            } else {
                AppLog.showLog(CLASS_TAG, "dealer kathmandu cannot be inserted");
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

        //INSERT DEALERS: Pokhara
        try {

            if (dealerDao.insertRecord(dealerListPokhara, "Pokhara") > 0) {
                AppLog.showLog(CLASS_TAG, "dealer POkhara successfully!");
            } else {
                AppLog.showLog(CLASS_TAG, "dealer Pokhara cannot be inserted");
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

        //INSERT DEALERS: Naranghat
        try {

            if (dealerDao.insertRecord(dealerListNarayanghat, "Narayanghat") > 0) {
                AppLog.showLog(CLASS_TAG, "dealer Naranghat successfully!");
            } else {
                AppLog.showLog(CLASS_TAG, "dealer narayanghat cannot be inserted");
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

        //INSERT DEALERS: Biratnagar
        try {

            if (dealerDao.insertRecord(dealerListBiratnagar, "Biratnagar") > 0) {
                AppLog.showLog(CLASS_TAG, "dealer Biratnagar successfully!");
            } else {
                AppLog.showLog(CLASS_TAG, "dealer Biratnagar cannot be inserted");
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

        return null;
    }

    @Override
    protected void onPostExecute(Void aVoid) {
        listener.onTaskFinished();
    }
}
