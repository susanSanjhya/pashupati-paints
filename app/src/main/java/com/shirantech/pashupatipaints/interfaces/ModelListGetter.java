package com.shirantech.pashupatipaints.interfaces;

import java.util.List;

/**
 * Created by susan on 12/3/14.
 */
public interface ModelListGetter<T> {
    void onTaskStarted();
    void onTaskFinished(List<T> modelList);
}
