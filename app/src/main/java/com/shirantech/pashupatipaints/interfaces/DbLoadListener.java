package com.shirantech.pashupatipaints.interfaces;

/**
 * Created by susan on 12/2/14.
 */
public interface DbLoadListener {
    void onTaskStarted();
    void onTaskFinished();
}
