package com.shirantech.pashupatipaints.interfaces;

import com.shirantech.pashupatipaints.model.PaintColor;

/**
 * Created by susan on 1/12/15.
 */
public interface ViewOnTouchListener {
    public void onViewTouch(PaintColor paintColor);
}
