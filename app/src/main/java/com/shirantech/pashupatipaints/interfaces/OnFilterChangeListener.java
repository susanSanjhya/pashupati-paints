package com.shirantech.pashupatipaints.interfaces;

/**
 * Created by susan on 12/17/14.
 */
public interface OnFilterChangeListener {
    void onFilterFinished(String toSearch);
}
