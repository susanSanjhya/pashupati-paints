package com.shirantech.pashupatipaints.database;

/**
 * Created by susan on 12/2/14.
 */
public class DbText {

    public static final String DATABASE_NAME = "PASHUPATI_PAINTS";
    public static final int DATABASE_VERSION = 3;
    //PAINT_COLORS TABLE
    public static final String TABLE_PAINT_COLORS = "PAINT_COLORS";
    public static final String COLUMN_ID = "_id";
    public static final String COLUMN_CATEGORY_ID = "category_id";
    public static final String COLUMN_NAME = "name";
    public static final String COLUMN_CODE = "code";

    //PAINTED_IMAGE_DETAILS TABLE
    public static final String TABLE_PAINTED_IMAGE_DETAILS = "PAINTED_IMAGE_DETAILS";
    public static final String COLUMN_IMAGE_ID = "_id";
    public static final String COLUMN_IMAGE_NAME = "name";
    public static final String COLUMN_IMAGE_LOCATION = "location";
    public static final String COLUMN_IMAGE_WALL_TYPES = "wall_types";
    public static final String COLUMN_IMAGE_DATE = "DATE";
    public static final String COLUMN_IMAGE_WALL_COLOR_NAMES = "wall_color_names";
    public static final String COLUMN_IMAGE_WALL_COLOR_CODES = "wall_color_codes";

    //DEALERS TABLE
    public static final String TABLE_DEALER = "DEALER";
    public static final String DEALER_COLUMN_ID = "_id";
    public static final String DEALER_COLUMN_DEALER_NAME = "dealer_name";
    public static final String DEALER_COLUMN_DISTRICT = "district";
    public static final String DEALER_COLUMN_CITY = "city";
    public static final String DEALER_COLUMN_TELEPHONE = "telephone";
    public static final String DEALER_COLUMN_BRANCH = "dealer_category";
    public static final String DEALER_COLUMN_KEY_SEARCH = "searchData";

    public static final String COLUMN_PAINT_CODE = "paint_code";
}
