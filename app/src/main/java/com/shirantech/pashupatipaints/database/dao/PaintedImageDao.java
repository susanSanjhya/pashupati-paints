package com.shirantech.pashupatipaints.database.dao;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import com.shirantech.pashupatipaints.database.DbOpenHelper;
import com.shirantech.pashupatipaints.database.DbText;
import com.shirantech.pashupatipaints.model.PaintedImage;
import com.shirantech.pashupatipaints.model.WallDescription;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Created by susan on 12/2/14.
 */
public class PaintedImageDao {
    private Context context;
    private DbOpenHelper dbOpenHelper;
    private SQLiteDatabase database;
    private String[] ALL_COLUMNS = {
            DbText.COLUMN_IMAGE_ID,
            DbText.COLUMN_IMAGE_NAME,
            DbText.COLUMN_IMAGE_LOCATION,
            DbText.COLUMN_IMAGE_WALL_TYPES,
            DbText.COLUMN_IMAGE_WALL_COLOR_NAMES,
            DbText.COLUMN_IMAGE_WALL_COLOR_CODES,
    };

    public PaintedImageDao(Context context) {
        this.context = context;
        dbOpenHelper = new DbOpenHelper(context);
    }

    public long insertRecord(PaintedImage paintImage) {
        database = dbOpenHelper.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(DbText.COLUMN_IMAGE_NAME, paintImage.getName());
        values.put(DbText.COLUMN_IMAGE_LOCATION, paintImage.getLocation());
        String wallTypes = concatWallType(paintImage.getWallDescriptionList());
        values.put(DbText.COLUMN_IMAGE_WALL_TYPES, wallTypes);

        String colorNames = concatColorNames(paintImage.getWallDescriptionList());
        values.put(DbText.COLUMN_IMAGE_WALL_COLOR_NAMES, colorNames);

        String colorCodes = concatColorCodes(paintImage.getWallDescriptionList());
        values.put(DbText.COLUMN_IMAGE_WALL_COLOR_CODES, colorCodes);
        values.put(DbText.COLUMN_IMAGE_DATE, paintImage.getDate());
        long insertId;
        try {
            insertId = database.insert(DbText.TABLE_PAINTED_IMAGE_DETAILS, null, values);
        } catch (Exception e) {
            insertId = 0;
        }
        database.close();
        return insertId;
    }

    private String concatColorCodes(List<WallDescription> wallDescriptionList) {
        String[] colorCodesArray = new String[wallDescriptionList.size()];
        for (int i = 0; i < wallDescriptionList.size(); i++) {
            colorCodesArray[i] = wallDescriptionList.get(i).getColorCode();
        }
        Log.i("ImageDetailsDao", "Concat color codes: " + ("" + Arrays.asList(colorCodesArray)).replaceAll("(^.|.$)", "").replace(", ", ","));
        return ("" + Arrays.asList(colorCodesArray)).replaceAll("(^.|.$)", "").replace(", ", ",");
    }

    private String concatColorNames(List<WallDescription> wallDescriptionList) {
        String[] colorNameArray = new String[wallDescriptionList.size()];
        for (int i = 0; i < wallDescriptionList.size(); i++) {
            colorNameArray[i] = wallDescriptionList.get(i).getColorName();
        }
        Log.i("ImageDetailsDao", "Concat color names: " + ("" + Arrays.asList(colorNameArray)).replaceAll("(^.|.$)", "").replace(", ", ","));
        return ("" + Arrays.asList(colorNameArray)).replaceAll("(^.|.$)", "").replace(", ", ",");
    }

    private String concatWallType(List<WallDescription> wallDescriptionList) {
        String[] wallTypesArray = new String[wallDescriptionList.size()];
        for (int i = 0; i < wallDescriptionList.size(); i++) {
            wallTypesArray[i] = wallDescriptionList.get(i).getWallType();
        }
        Log.i("ImageDetailsDao", "Concat wall types: " + ("" + Arrays.asList(wallTypesArray)).replaceAll("(^.|.$)", "").replace(", ", ","));
        return ("" + Arrays.asList(wallTypesArray)).replaceAll("(^.|.$)", "").replace(", ", ",");
    }

    public List<PaintedImage> getAllPaintedImages() {
        List<PaintedImage> records = new ArrayList<PaintedImage>();
        database = dbOpenHelper.getReadableDatabase();
        /*Cursor cursor = database.query(DbText.TABLE_PAINTED_IMAGE_DETAILS,
                ALL_COLUMNS, null, null, null, null, null);*/
        Cursor cursor = database.rawQuery("SELECT * FROM " + DbText.TABLE_PAINTED_IMAGE_DETAILS +
                " ORDER BY " + DbText.COLUMN_IMAGE_ID + " DESC", null);
        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            PaintedImage record = new PaintedImage();
            record.setId(cursor.getLong(cursor.getColumnIndex(DbText.COLUMN_IMAGE_ID)));
            record.setName(cursor.getString(cursor.getColumnIndex(DbText.COLUMN_IMAGE_NAME)));
            record.setLocation(cursor.getString(cursor.getColumnIndex(DbText.COLUMN_IMAGE_LOCATION)));
            String wallTypes = cursor.getString(cursor.getColumnIndex(DbText.COLUMN_IMAGE_WALL_TYPES));
            String date = cursor.getString(cursor.getColumnIndex(DbText.COLUMN_IMAGE_DATE));
            record.setDate(date);
            String colorNames = cursor.getString(cursor.getColumnIndex(DbText.COLUMN_IMAGE_WALL_COLOR_NAMES));
            String colorCodes = cursor.getString(cursor.getColumnIndex(DbText.COLUMN_IMAGE_WALL_COLOR_CODES));
            List<WallDescription> wallDescriptionList = getListOfWallDescription(wallTypes, colorNames, colorCodes);

            record.setWallDescriptionList(wallDescriptionList);
            records.add(record);
            cursor.moveToNext();
        }
        cursor.close();
        database.close();
        return records;
    }

    private List<WallDescription> getListOfWallDescription(String wallTypes, String colorNames, String colorCodes) {
        List<WallDescription> wallDescriptionList = new ArrayList<WallDescription>();
        String[] wallTypesArray = wallTypes.split(",");
        String[] colorNamesArray = colorNames.split(",");
        String[] colorCodeNamesArray = colorCodes.split(",");
        for (int i = 0; i < colorNamesArray.length; i++) {
            WallDescription wallDescription = new WallDescription();
            wallDescription.setWallType(wallTypesArray[i]);
            wallDescription.setColorCode(colorCodeNamesArray[i]);
            wallDescription.setColorName(colorNamesArray[i]);
            wallDescriptionList.add(wallDescription);
        }
        return wallDescriptionList;
    }

    public void deletePaintedImage(PaintedImage paintedImage) {
        database = dbOpenHelper.getWritableDatabase();
        long id = paintedImage.getId();
        database.delete(DbText.TABLE_PAINTED_IMAGE_DETAILS, DbText.COLUMN_IMAGE_ID + "=" + id, null);
        database.close();
    }
}
