package com.shirantech.pashupatipaints.database.dao;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.widget.Toast;

import com.shirantech.pashupatipaints.database.DbOpenHelper;
import com.shirantech.pashupatipaints.database.DbText;
import com.shirantech.pashupatipaints.model.PaintColor;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by susan on 12/2/14.
 */
public class PaintColorDao {
    private static final String CLASS_TAG = PaintColorDao.class.getSimpleName();
    private String[] ALL_COLUMNS = {DbText.COLUMN_ID,
            DbText.COLUMN_NAME,
            DbText.COLUMN_CATEGORY_ID,
            DbText.COLUMN_CODE,
            DbText.COLUMN_PAINT_CODE};
    private DbOpenHelper dbOpenHelper;
    private SQLiteDatabase database;
    private Context context;

    public PaintColorDao(Context context) {
        this.context = context;
        dbOpenHelper = new DbOpenHelper(context);
    }

    public long insertRecord(List<PaintColor> paintColorList) {
        database = dbOpenHelper.getWritableDatabase();
        long id = 0;

        database.beginTransaction();
        try {
            for (PaintColor paintColor : paintColorList) {
                try {
                    ContentValues values = new ContentValues();
                    values.put(DbText.COLUMN_CATEGORY_ID, paintColor.getCategoryId());
                    values.put(DbText.COLUMN_NAME, paintColor.getName());
                    values.put(DbText.COLUMN_CODE, paintColor.getColorCode());
                    values.put(DbText.COLUMN_PAINT_CODE, paintColor.getPaintCode());
                    id = database.insert(DbText.TABLE_PAINT_COLORS, null, values);
                } catch (Exception e) {
                    id = 0;
                    Toast.makeText(context, "Color name exists already!", Toast.LENGTH_SHORT).show();
                }
            }
            database.setTransactionSuccessful();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            database.endTransaction();
        }
        return id;
    }

    public List<PaintColor> getAllCategory1Paints(String categoryId) {
        List<PaintColor> records = new ArrayList<PaintColor>();
        database = dbOpenHelper.getWritableDatabase();
        Cursor cursor = database.query(DbText.TABLE_PAINT_COLORS, ALL_COLUMNS, DbText.COLUMN_CATEGORY_ID + "=?",
                new String[]{categoryId}, null, null, null, null);
        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            PaintColor record = new PaintColor();
            record.setId(cursor.getLong(0));
            record.setName(cursor.getString(1));
            record.setCategoryId(cursor.getString(2));
            record.setColorCode(cursor.getString(3));
            record.setPaintCode(cursor.getString(4));
            records.add(record);
            cursor.moveToNext();
        }
        cursor.close();
        database.close();
        return records;
    }
}
