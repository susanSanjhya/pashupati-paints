package com.shirantech.pashupatipaints.database.dao;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.widget.Toast;

import com.shirantech.pashupatipaints.database.DbOpenHelper;
import com.shirantech.pashupatipaints.database.DbText;
import com.shirantech.pashupatipaints.model.PaintDealer;
import com.shirantech.pashupatipaints.util.AppLog;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by susan on 12/2/14.
 */
public class DealerDao {
    private static final String CLASS_TAG = DealerDao.class.getSimpleName();
    private Context context;
    private DbOpenHelper dbOpenHelper;
    private SQLiteDatabase database;
    private String[] ALL_COLUMNS = {DbText.DEALER_COLUMN_ID,
            DbText.DEALER_COLUMN_DEALER_NAME,
            DbText.DEALER_COLUMN_CITY,
            DbText.DEALER_COLUMN_DISTRICT,
            DbText.DEALER_COLUMN_TELEPHONE,
            DbText.DEALER_COLUMN_BRANCH,
            DbText.DEALER_COLUMN_KEY_SEARCH};

    public DealerDao(Context context) {
        this.context = context;
        dbOpenHelper = new DbOpenHelper(context);
    }

    public long insertRecord(List<PaintDealer> dealerList, String cityId) {
        database = dbOpenHelper.getWritableDatabase();
        long id = 0;
        database.beginTransaction();
        try {
            for (PaintDealer paintDealer : dealerList) {
                try {
                    ContentValues values = new ContentValues();
                    String searchValue = paintDealer.getDealerName()
                            + " " + paintDealer.getCity() + " " +
                            paintDealer.getTelephone() + " " + paintDealer.getDistrict();
                    values.put(DbText.DEALER_COLUMN_DEALER_NAME, paintDealer.getDealerName());

                    values.put(DbText.DEALER_COLUMN_CITY, paintDealer.getCity());
                    values.put(DbText.DEALER_COLUMN_DISTRICT, paintDealer.getDistrict());
                    values.put(DbText.DEALER_COLUMN_TELEPHONE, paintDealer.getTelephone());
                    values.put(DbText.DEALER_COLUMN_BRANCH, cityId); //cityId are CONSTANTS for
                    values.put(DbText.DEALER_COLUMN_KEY_SEARCH, searchValue);
                    AppLog.showLog(CLASS_TAG, DbText.DEALER_COLUMN_DEALER_NAME + ": " + paintDealer.getDealerName() + "\t" +
                            DbText.DEALER_COLUMN_CITY + ": " + paintDealer.getCity() + "\t" +
                            DbText.DEALER_COLUMN_DISTRICT + ": " + paintDealer.getDistrict() + "\t" +
                            DbText.DEALER_COLUMN_TELEPHONE + ": " + paintDealer.getTelephone() + "\t" +
                            DbText.DEALER_COLUMN_BRANCH + ": " + cityId + "\t" +
                            DbText.DEALER_COLUMN_KEY_SEARCH + ": " + searchValue);
                    AppLog.showLog(CLASS_TAG, "\n");

                    id = database.insert(DbText.TABLE_DEALER, null, values);
                } catch (Exception e) {
                    id = 0;
                    Toast.makeText(context, "Dealer name exists already", Toast.LENGTH_SHORT).show();
                }
            }
            database.setTransactionSuccessful();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            database.endTransaction();
        }
        return id;
    }

    public List<PaintDealer> searchDealer(String inputText) throws SQLException {
        database = dbOpenHelper.getReadableDatabase();
        List<PaintDealer> records = new ArrayList<PaintDealer>();
        String query = "SELECT " +
                DbText.DEALER_COLUMN_ID + ", " +
                DbText.DEALER_COLUMN_DEALER_NAME + ", " +
                DbText.DEALER_COLUMN_CITY + ", " +
                DbText.DEALER_COLUMN_DISTRICT + ", " +
                DbText.DEALER_COLUMN_TELEPHONE + ", " +
                DbText.DEALER_COLUMN_BRANCH + ", " +
                DbText.DEALER_COLUMN_KEY_SEARCH +
                " from " + DbText.TABLE_DEALER + " where " + DbText.DEALER_COLUMN_KEY_SEARCH + " MATCH '" + inputText + "';";
        Cursor cursor = database.rawQuery(query, null);
        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            PaintDealer record = new PaintDealer();
            record.setId(cursor.getLong(0));
            record.setDealerName(cursor.getString(1));
            record.setCity(cursor.getString(2));
            record.setDistrict(cursor.getString(3));
            record.setTelephone(cursor.getString(4));
            record.setDealerCategory(cursor.getString(5));
            record.setSearchValue(cursor.getString(6));
            records.add(record);
            cursor.moveToNext();
        }
        cursor.close();
        database.close();
        return records;
    }

    public List<PaintDealer> getAllDealers() {
        List<PaintDealer> records = new ArrayList<PaintDealer>();

        database = dbOpenHelper.getReadableDatabase();

        /*Cursor cursor = database.query(DbText.TABLE_DEALER, ALL_COLUMNS,
                null, null,
                null, null, null, null);*/

        Cursor cursor = database.rawQuery("SELECT * FROM " + DbText.TABLE_DEALER + " ORDER BY " + DbText.DEALER_COLUMN_DEALER_NAME + " ASC", null);
        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            PaintDealer record = new PaintDealer();
            record.setId(cursor.getLong(0));
            record.setDealerName(cursor.getString(1));
            record.setCity(cursor.getString(2));
            record.setDistrict(cursor.getString(3));
            record.setTelephone(cursor.getString(6));
            record.setDealerCategory(cursor.getString(4));
            record.setSearchValue(cursor.getString(5));
            records.add(record);
            cursor.moveToNext();
        }

        //closing the cursor
        cursor.close();
        database.close();
        return records;
    }

    public List<PaintDealer> getAllDealers(String cityId) {
        List<PaintDealer> records = new ArrayList<PaintDealer>();

        database = dbOpenHelper.getReadableDatabase();

        Cursor cursor = database.query(DbText.TABLE_DEALER, ALL_COLUMNS,
                DbText.DEALER_COLUMN_BRANCH + "=?", new String[]{cityId},
                null, null, null, null);

        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            PaintDealer record = new PaintDealer();
            record.setId(cursor.getLong(0));
            record.setDealerName(cursor.getString(1));
            record.setCity(cursor.getString(2));
            record.setDistrict(cursor.getString(3));
            record.setTelephone(cursor.getString(4));
            record.setDealerCategory(cursor.getString(5));
            record.setSearchValue(cursor.getString(6));
            records.add(record);
            cursor.moveToNext();
        }

        //closing the cursor
        cursor.close();
        database.close();
        return records;
    }

    public List<PaintDealer> getDistinctDistrict(String cityId) {
        List<PaintDealer> records = new ArrayList<PaintDealer>();

        database = dbOpenHelper.getReadableDatabase();
        AppLog.showLog(CLASS_TAG, "Distinct District query is " +
                "SELECT distinct district" +
                " from " + DbText.TABLE_DEALER + " where " + DbText.DEALER_COLUMN_BRANCH + "='" + cityId + "' order by district asc");
        Cursor cursor = database.rawQuery("SELECT distinct district" +
                " from " + DbText.TABLE_DEALER + " where " + DbText.DEALER_COLUMN_BRANCH + "='" + cityId + "' order by district asc", null);
        /*Cursor cursor1 = database.query(true, DbText.TABLE_DEALER, ALL_COLUMNS,
                DbText.DEALER_COLUMN_BRANCH + "=?", new String[]{cityId},
                null, null, null, null);*/

        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            PaintDealer record = new PaintDealer();
//            record.setId(cursor.getLong(0));
//            record.setDealerName(cursor.getString(1));
//            record.setCity(cursor.getString(2));
            record.setDistrict(cursor.getString(0));
//            record.setTelephone(cursor.getString(4));
//            record.setDealerCategory(cursor.getString(5));
//            record.setSearchValue(cursor.getString(6));
            records.add(record);
            cursor.moveToNext();
        }

        //closing the cursor
        cursor.close();
        database.close();
        return records;
    }

    public List<PaintDealer> getDistinctCity(String branch, String district) {
        List<PaintDealer> records = new ArrayList<PaintDealer>();

        database = dbOpenHelper.getReadableDatabase();

        Cursor cursor = database.rawQuery("SELECT distinct city from " + DbText.TABLE_DEALER +
                        " where dealer_category='" + branch + "' and district='" + district + "'" +
                        " order by city asc", null
        );
        /*Cursor cursor1 = database.query(true, DbText.TABLE_DEALER, ALL_COLUMNS,
                DbText.DEALER_COLUMN_BRANCH + "=?", new String[]{cityId},
                null, null, null, null);*/
        AppLog.showLog(CLASS_TAG, "CURSOR COUNT IS " + cursor.getCount());
        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            PaintDealer record = new PaintDealer();
            record.setCity(cursor.getString(0));
//            record.setId(cursor.getLong(1));
//            record.setDealerName(cursor.getString(2));
//            record.setDistrict(cursor.getString(3));
//            record.setTelephone(cursor.getString(4));
//            record.setDealerCategory(cursor.getString(5));
//            record.setSearchValue(cursor.getString(6));
            records.add(record);
            cursor.moveToNext();
        }

        //closing the cursor
        cursor.close();
        database.close();
        return records;
    }

    public List<PaintDealer> getDealersByBranchDistrictCity(String branch, String district, String city) {
        List<PaintDealer> records = new ArrayList<PaintDealer>();

        database = dbOpenHelper.getReadableDatabase();

        /*Cursor cursor1 = database.rawQuery("SELECT distinct city from " + DbText.TABLE_DEALER +
                        " where dealer_category='" + branch + "' and district='" + district + "'" +
                        " order by city asc", null
        );*/

        Cursor cursor = database.rawQuery("SELECT "
                        + DbText.DEALER_COLUMN_ID + ", " +
                        DbText.DEALER_COLUMN_DEALER_NAME + ", " +
                        DbText.DEALER_COLUMN_CITY + ", " +
                        DbText.DEALER_COLUMN_DISTRICT + ", " +
                        DbText.DEALER_COLUMN_TELEPHONE + ", " +
                        DbText.DEALER_COLUMN_BRANCH + ", " +
                        DbText.DEALER_COLUMN_KEY_SEARCH + " FROM " + DbText.TABLE_DEALER +
                        " WHERE (" + DbText.DEALER_COLUMN_BRANCH + "='" + branch + "'" +
                        " AND " + DbText.DEALER_COLUMN_DISTRICT + "='" + district + "'" +
                        " AND " + DbText.DEALER_COLUMN_CITY + "='" + city + "') " +
                        " ORDER BY " + DbText.DEALER_COLUMN_DEALER_NAME + " ASC",
                null
        );
        AppLog.showLog(CLASS_TAG, "CURSOR COUNT IS " + cursor.getCount());
        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            PaintDealer record = new PaintDealer();
            record.setId(cursor.getLong(0));
            record.setDealerName(cursor.getString(1));
            record.setCity(cursor.getString(2));
            record.setDistrict(cursor.getString(3));
            record.setTelephone(cursor.getString(4));
            record.setDealerCategory(cursor.getString(5));
            record.setSearchValue(cursor.getString(6));
            records.add(record);
            cursor.moveToNext();
        }

        //closing the cursor
        cursor.close();
        database.close();
        return records;
    }
}
