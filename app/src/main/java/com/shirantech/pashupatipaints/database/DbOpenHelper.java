package com.shirantech.pashupatipaints.database;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import com.shirantech.pashupatipaints.activities.list.SplashScreenActivity;
import com.shirantech.pashupatipaints.util.AppText;

/**
 * Created by susan on 12/2/14.
 */
public class DbOpenHelper extends SQLiteOpenHelper {

    private static final String CLASS_TAG = DbOpenHelper.class.getSimpleName();

    private Context mContext;

    /*//PAINT_COLORS TABLE
    public static final String TABLE_PAINT_COLORS = "PAINT_COLORS";
    public static final String COLUMN_ID = "_id";
    public static final String COLUMN_CATEGORY_ID = "category_id";
    public static final String COLUMN_NAME = "name";
    public static final String COLUMN_CODE = "code";*/

    // Database creation sql statement create table PAINT_COLORS
    private static final String CREATE_TABLE_PAINT_COLORS = "create table "
            + DbText.TABLE_PAINT_COLORS + "("
            + DbText.COLUMN_ID + " integer primary key autoincrement, "
            + DbText.COLUMN_NAME + " text, "
            + DbText.COLUMN_CATEGORY_ID + " text, "
            + DbText.COLUMN_CODE + " text, " +
            DbText.COLUMN_PAINT_CODE + " TEXT); ";
    //+ "UNIQUE (name)" + ");";

    /*//PAINTED_IMAGE_DETAILS TABLE
    public static final String TABLE_PAINTED_IMAGE_DETAILS = "PAINTED_IMAGE_DETAILS";
    public static final String COLUMN_IMAGE_ID = "_id";
    public static final String COLUMN_IMAGE_NAME = "name";
    public static final String COLUMN_IMAGE_LOCATION = "location";
    public static final String COLUMN_IMAGE_WALL1 = "wall1";
    public static final String COLUMN_IMAGE_WALL2 = "wall2";
    public static final String COLUMN_IMAGE_WALL3 = "wall3";
    public static final String COLUMN_IMAGE_WALL4 = "wall4";
    public static final String COLUMN_IMAGE_WALL5 = "wall5";
    public static final String COLUMN_IMAGE_WALL6 = "wall6";
    public static final String COLUMN_IMAGE_WALL7 = "wall7";
    public static final String COLUMN_IMAGE_WALL8 = "wall8";
    public static final String COLUMN_IMAGE_WALL9 = "wall9";
    public static final String COLUMN_IMAGE_WALL10 = "wall10";
    public static final String COLUMN_IMAGE_WALL11 = "wall11";
    public static final String COLUMN_IMAGE_WALL12 = "wall12";*/

    // Database creation sql statement create table PAINT_COLORS
    private static final String CREATE_TABLE_PAINTED_IMAGE_DETAILS = "create table "
            + DbText.TABLE_PAINTED_IMAGE_DETAILS + "("
            + DbText.COLUMN_IMAGE_ID + " integer primary key autoincrement, "
            + DbText.COLUMN_IMAGE_NAME + " text, "
            + DbText.COLUMN_IMAGE_LOCATION + " text, "
            + DbText.COLUMN_IMAGE_WALL_TYPES + " text, "
            + DbText.COLUMN_IMAGE_DATE + " text, "
            + DbText.COLUMN_IMAGE_WALL_COLOR_NAMES + " text, "
            + DbText.COLUMN_IMAGE_WALL_COLOR_CODES + " text, "
            + "UNIQUE (name)" + ");";

    /*//DEALERS TABLE
    public static final String TABLE_DEALER = "DEALER";
    public static final String DEALER_COLUMN_ID = "_id";
    public static final String DEALER_COLUMN_DEALER_NAME = "dealer_name";
    public static final String DEALER_COLUMN_CITY = "city";
    public static final String DEALER_COLUMN_TELEPHONE = "telephone";
    public static final String DEALER_COLUMN_BRANCH = "dealer_category";
    public static final String DEALER_COLUMN_KEY_SEARCH = "searchData";
*/
    // Database creation sql statement create table DEALER
    private static final String CREATE_TABLE_DEALER = "create VIRTUAL table "
            + DbText.TABLE_DEALER
            + " USING fts3("
            + DbText.DEALER_COLUMN_ID + " integer primary key autoincrement, "
            + DbText.DEALER_COLUMN_DEALER_NAME + " text, "
            + DbText.DEALER_COLUMN_CITY + " text, "
            + DbText.DEALER_COLUMN_DISTRICT + " text, "
            + DbText.DEALER_COLUMN_BRANCH + " text, "
            + DbText.DEALER_COLUMN_KEY_SEARCH + " text, "
            + DbText.DEALER_COLUMN_TELEPHONE + " text) ; ";

    public DbOpenHelper(Context context) {
        super(context, DbText.DATABASE_NAME, null, DbText.DATABASE_VERSION);
        mContext = context;
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(CREATE_TABLE_PAINT_COLORS);
        db.execSQL(CREATE_TABLE_PAINTED_IMAGE_DETAILS);
        db.execSQL(CREATE_TABLE_DEALER);

    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        Log.w(CLASS_TAG, "Upgrading database from version " + oldVersion + " to " + newVersion + ", which will destroy all old data");
        db.execSQL("DROP TABLE IF EXISTS " + DbText.TABLE_PAINT_COLORS); //do the same for other tables!!!
        db.execSQL("DROP TABLE IF EXISTS " + DbText.TABLE_PAINTED_IMAGE_DETAILS);
        db.execSQL("DROP TABLE IF EXISTS " + DbText.TABLE_DEALER);

        onCreate(db);

        SharedPreferences settings = mContext.getSharedPreferences(AppText.PREF_KEY_HAS_RUN_BEFORE, 0);
        SharedPreferences.Editor edit = settings.edit();
        edit.putBoolean(AppText.KEY_HAS_RUN, false); //set to has run
        edit.commit(); //apply

        mContext.startActivity(new Intent(mContext, SplashScreenActivity.class));

    }
}
