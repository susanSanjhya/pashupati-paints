package com.shirantech.pashupatipaints.adapter;

import android.content.ClipData;
import android.content.Context;
import android.graphics.Color;
import android.os.Build;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.ImageButton;
import android.widget.TextView;

import com.shirantech.pashupatipaints.R;
import com.shirantech.pashupatipaints.customwidgets.ColorDragShadowBuilder;
import com.shirantech.pashupatipaints.interfaces.ViewOnTouchListener;
import com.shirantech.pashupatipaints.model.PaintColor;
import com.shirantech.pashupatipaints.util.AppLog;
import com.shirantech.pashupatipaints.util.Utility;

import java.util.List;

/**
 * Created by susan on 1/12/15.
 */
public class TabletColorHorizontalListAdapter extends BaseAdapter {
    private static final String CLASS_TAG = TabletColorHorizontalListAdapter.class.getSimpleName();
    private Context context;
    private List<PaintColor> paintColorList;
    private ViewOnTouchListener listener;

    public TabletColorHorizontalListAdapter(Context context, List<PaintColor> paintColorList, ViewOnTouchListener listener) {
        this.context = context;
        this.paintColorList = paintColorList;
        this.listener = listener;
    }


    @Override
    public int getCount() {
        return paintColorList.size();
    }

    @Override
    public PaintColor getItem(int position) {
        return paintColorList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    public class ViewHolder {
        View imageView;
        TextView textViewColorCode, textViewColorName;
        public ImageButton imageButtonDelete;
    }

    @SuppressWarnings("deprecation")
    @Override
    public View getView(final int position, View view, ViewGroup viewGroup) {
        final ViewHolder holder;
        if (view == null) {
            holder = new ViewHolder();
            view = LayoutInflater.from(context)
                    .inflate(R.layout.view_color_chooser, viewGroup, false);

            holder.imageView = view.findViewById(R.id.iv_color);
            holder.imageButtonDelete = (ImageButton) view.findViewById(R.id.image_button_delete);
            holder.textViewColorName = (TextView) view.findViewById(R.id.tv_color_name);
            holder.textViewColorCode = (TextView) view.findViewById(R.id.tv_color_code);
            /*view.setOnLongClickListener(new View.OnLongClickListener() {
                @Override
                public boolean onLongClick(View v) {
                    holder.imageButtonDelete.setVisibility(View.VISIBLE);
                    return false;
                }
            });*/
            view.setTag(holder);
        } else {
            holder = (ViewHolder) view.getTag();
        }

        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.HONEYCOMB) {
            ChoiceTouchListener listener = new ChoiceTouchListener(position);
            holder.imageView.setOnTouchListener(listener);
        }
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
            holder.imageView.setBackground(Utility.getCustomShape(Color.parseColor(getItem(position).getColorCode())));
        } else {
            holder.imageView.setBackgroundDrawable(Utility.getCustomShape(Color.parseColor(getItem(position).getColorCode())));
        }
        holder.textViewColorCode.setText(getItem(position).getPaintCode());
        holder.textViewColorName.setText(getItem(position).getName());
        return view;
    }

    private final class ChoiceTouchListener implements View.OnTouchListener {

        int position;

        public ChoiceTouchListener(int position) {
            this.position = position;
        }

        @Override
        public boolean onTouch(View v, MotionEvent event) {

            if (event.getAction() == MotionEvent.ACTION_DOWN) {

                if (position > AdapterView.INVALID_POSITION) {

                    PaintColor paintColor = paintColorList.get(position);

                    listener.onViewTouch(paintColor);
                    AppLog.showLog(CLASS_TAG, "Clicked on " + position);
                    ClipData data = ClipData.newPlainText("color", paintColor.getColorCode());
                    ColorDragShadowBuilder shadowBuilder = new ColorDragShadowBuilder(context,paintColor.getColorCode());
                    //start dragging the item touched
                    v.startDrag(data, shadowBuilder, v, 0);

                    return true;
                }
            }
            return true;
        }
    }
}

