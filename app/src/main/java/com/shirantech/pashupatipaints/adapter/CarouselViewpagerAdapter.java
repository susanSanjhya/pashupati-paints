package com.shirantech.pashupatipaints.adapter;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;

import com.shirantech.pashupatipaints.R;
import com.shirantech.pashupatipaints.activities.list.StarterV2Activity;
import com.shirantech.pashupatipaints.customwidgets.CarouselItemLayout;
import com.shirantech.pashupatipaints.fragments.CarouselItemFragment;

/**
 * Created by susan on 7/8/15.
 */
public class CarouselViewpagerAdapter extends FragmentPagerAdapter implements ViewPager.OnPageChangeListener {
    private CarouselItemLayout cur = null;
    private CarouselItemLayout next = null;
    private Context context;
    private FragmentManager fm;
    private float scale;
    public CarouselViewpagerAdapter(Context context, FragmentManager fm) {
        super(fm);
        this.fm = fm;
        this.context = context;
    }

    @Override
    public Fragment getItem(int position) {
        if(position == StarterV2Activity.FIRST_PAGE){
            scale = StarterV2Activity.BIG_SCALE;
        } else {
            scale = StarterV2Activity.SMALL_SCALE;
        }

        Bundle bundle = new Bundle();
        bundle.putInt("pager_position", position);
        position = position % StarterV2Activity.PAGES;
        CarouselItemFragment fragment = new CarouselItemFragment();
        bundle.putInt("position", position);
        bundle.putFloat("scale", scale);
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    public int getCount() {
        return StarterV2Activity.PAGES * StarterV2Activity.LOOPS;
    }

    @Override
    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
        if(positionOffset >= 0f && positionOffset <= 1f){
            cur = getRootView(position);
            next = getRootView(position +1);

            cur.setScaleBoth(StarterV2Activity.BIG_SCALE
                    - StarterV2Activity.DIFF_SCALE * positionOffset);
            next.setScaleBoth(StarterV2Activity.SMALL_SCALE
                    + StarterV2Activity.DIFF_SCALE * positionOffset);
        }
    }

    @Override
    public void onPageSelected(int position) {

    }

    @Override
    public void onPageScrollStateChanged(int state) {

    }

    private CarouselItemLayout getRootView(int position){
        return (CarouselItemLayout)fm.findFragmentByTag(this.getFragmentTag(position))
                .getView().findViewById(R.id.root);
    }
    private String getFragmentTag(int position){
        return "android:switcher:" + ((StarterV2Activity)context).pager.getId()+":"+position;
    }
}
