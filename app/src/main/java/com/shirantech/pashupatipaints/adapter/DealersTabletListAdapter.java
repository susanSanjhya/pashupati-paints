package com.shirantech.pashupatipaints.adapter;

import android.content.Context;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.shirantech.pashupatipaints.R;
import com.shirantech.pashupatipaints.model.PaintDealer;

import java.util.List;

/**
 * Created by susan on 3/20/15.
 */
public class DealersTabletListAdapter extends BaseAdapter {
    private Context context;
    private List<PaintDealer> paintDealerList;

    public DealersTabletListAdapter(Context context, List<PaintDealer> paintDealerList) {
        this.context = context;
        this.paintDealerList = paintDealerList;
    }

    @Override
    public int getCount() {
        return paintDealerList.size();
    }

    @Override
    public PaintDealer getItem(int position) {
        return paintDealerList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    private class ViewHolder {
        private TextView textViewName, textViewPhone, textViewCity;
    }

    @Override
    public View getView(int position, View view, ViewGroup viewGroup) {
        ViewHolder holder;
        if (view == null) {
            holder = new ViewHolder();
            view = LayoutInflater.from(context)
                    .inflate(R.layout.row_dealers_list, viewGroup, false);
            holder.textViewName = (TextView) view.findViewById(R.id.tv_name);
            holder.textViewPhone = (TextView) view.findViewById(R.id.tv_phone);
            holder.textViewCity = (TextView) view.findViewById(R.id.tv_city);
            view.setTag(holder);
        } else {
            holder = (ViewHolder) view.getTag();
        }

        holder.textViewName.setText(getItem(position).getDealerName());
        holder.textViewPhone.setText(TextUtils.isEmpty(getItem(position).getTelephone())
                ? "N/A" : getItem(position).getTelephone());
        holder.textViewCity.setText(getItem(position).getCity() + ", " + getItem(position).getDistrict());
        return view;
    }

    public void remove(PaintDealer record) {
        paintDealerList.remove(record);
    }
}
