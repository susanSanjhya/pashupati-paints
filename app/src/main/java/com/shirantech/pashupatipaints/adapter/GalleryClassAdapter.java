package com.shirantech.pashupatipaints.adapter;

import android.content.Context;
import android.graphics.Color;
import android.os.Build;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TableLayout;
import android.widget.TableRow;

import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.shirantech.pashupatipaints.R;
import com.shirantech.pashupatipaints.customwidgets.CustomBoldTextView;
import com.shirantech.pashupatipaints.model.PaintedImage;
import com.shirantech.pashupatipaints.model.WallDescription;
import com.shirantech.pashupatipaints.util.AppLog;
import com.shirantech.pashupatipaints.util.ImageUtils;
import com.shirantech.pashupatipaints.util.Utility;

import java.util.List;

public class GalleryClassAdapter extends BaseAdapter {

    private static final String CLASS_TAG = GalleryClassAdapter.class.getSimpleName();
    private Context mContext;
    private List<PaintedImage> mGallaryList;
    private ImageLoader imagLoader = ImageLoader.getInstance();

    public GalleryClassAdapter(Context context, List<PaintedImage> items) {
        this.mContext = context;
        this.mGallaryList = items;
        imagLoader.init(ImageLoaderConfiguration.createDefault(context));
    }

    class ViewHolder {
        private ImageView imageViewThumbNail;
        private TableLayout tableLayoutImageDetails;
        private List<WallDescription> wallDescriptionList;
    }

    public int getCount() {
        return mGallaryList.size();
    }

    public PaintedImage getItem(int position) {
        return mGallaryList.get(position);
    }

    public long getItemId(int position) {
        return position;
    }

    @SuppressWarnings("deprecation")
    public View getView(int position, View view, ViewGroup viewGroup) {

        ViewHolder holder;
        if (view == null) {
            holder = new ViewHolder();
            view = LayoutInflater.from(mContext)
                    .inflate(R.layout.gallary_list_row, viewGroup, false);
            holder.imageViewThumbNail = (ImageView) view.findViewById(R.id.imageview_painted);
            holder.tableLayoutImageDetails = (TableLayout) view.findViewById(R.id.tablelayout_image_details);
            holder.wallDescriptionList = getItem(position).getWallDescriptionList();
            ImageUtils.loadImageUsingLoader(imagLoader,
                    holder.imageViewThumbNail, "file:///mnt/sdcard/PashupatiPaints/"
                            + getItem(position).getName() + ".png"
            );
            if (holder.wallDescriptionList.size() != 0) {
                if (holder.wallDescriptionList.size() % 2 != 0) {
                    holder.wallDescriptionList.add(new WallDescription("", "", ""));
                }
                AppLog.showLog(CLASS_TAG, "The Description size " + holder.wallDescriptionList.size());
                for (int i = 0; i < holder.wallDescriptionList.size() / 2; i++) {
                    TableRow tableRow = new TableRow(mContext);
                    tableRow.setLayoutParams(new TableRow.LayoutParams(TableRow.LayoutParams.MATCH_PARENT,
                            TableRow.LayoutParams.WRAP_CONTENT));
                    for (int j = 0; j < 2; j++) {
                        LinearLayout mainLayout = new LinearLayout(mContext);
                        mainLayout.setOrientation(LinearLayout.VERTICAL);
                        CustomBoldTextView textViewWallName = new CustomBoldTextView(mContext);
                        textViewWallName.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT,
                                LinearLayout.LayoutParams.WRAP_CONTENT));

                        textViewWallName.setTextColor(mContext.getResources().getColor(R.color.primary_text));
                        textViewWallName.setText(holder.wallDescriptionList.get((2 * i) + j).getWallType());

                        mainLayout.addView(textViewWallName);

                        LinearLayout linearLayoutContainer = new LinearLayout(mContext);
                        linearLayoutContainer.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT,
                                LinearLayout.LayoutParams.WRAP_CONTENT));
                        linearLayoutContainer.setOrientation(LinearLayout.HORIZONTAL);

                        View colorView = new View(mContext);
                        colorView.setLayoutParams(new LinearLayout.LayoutParams(mContext.getResources().getInteger(R.integer.gallery_color_dim), mContext.getResources().getInteger(R.integer.gallery_color_dim)));
                        AppLog.showLog(CLASS_TAG, "The Color is " + holder.wallDescriptionList.get((2 * i) + j).getColorCode());
                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
                            if (!holder.wallDescriptionList.get((2 * i) + j).getColorCode().isEmpty()) {
                                colorView.setBackground(Utility.getCustomShape(Color.parseColor(holder.wallDescriptionList.get((2 * i) + j).getColorCode())));
                            }
                        } else {
                            if (!holder.wallDescriptionList.get((2 * i) + j).getColorCode().isEmpty()) {
                                colorView.setBackgroundDrawable(Utility.getCustomShape(Color.parseColor(holder.wallDescriptionList.get((2 * i) + j).getColorCode())));
                            }
                        }
                        linearLayoutContainer.addView(colorView);

                        LinearLayout linearLayoutNameAndCode = new LinearLayout(mContext);
                        linearLayoutNameAndCode.setOrientation(LinearLayout.VERTICAL);
                        linearLayoutNameAndCode.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT,
                                LinearLayout.LayoutParams.MATCH_PARENT));

                        CustomBoldTextView textViewColorName = new CustomBoldTextView(mContext);
                        textViewColorName.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT,
                                LinearLayout.LayoutParams.WRAP_CONTENT));
                        textViewColorName.setTextColor(mContext.getResources().getColor(R.color.secondary_text));
                        textViewColorName.setText(holder.wallDescriptionList.get((2 * i) + j).getColorName());
                        textViewColorName.setSingleLine();
                        textViewColorName.setEllipsize(TextUtils.TruncateAt.END);
                        linearLayoutNameAndCode.addView(textViewColorName);
                        CustomBoldTextView textViewColorCode = new CustomBoldTextView(mContext);
                        textViewColorCode.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT,
                                LinearLayout.LayoutParams.WRAP_CONTENT));
                        textViewColorCode.setTextColor(mContext.getResources().getColor(R.color.secondary_text));
                        textViewColorCode.setText(holder.wallDescriptionList.get((2 * i) + j).getColorCode());
                        linearLayoutNameAndCode.addView(textViewColorCode);
                        linearLayoutContainer.addView(linearLayoutNameAndCode);
                        mainLayout.addView(linearLayoutContainer);
                        tableRow.addView(mainLayout);
                    }
                    holder.tableLayoutImageDetails.addView(tableRow);
                }
            }
            viewGroup.setTag(holder);
        } else {
            holder = (ViewHolder) viewGroup.getTag();
        }

        return view;
    }

    public void remove(PaintedImage record) {
        mGallaryList.remove(record);
    }

}
