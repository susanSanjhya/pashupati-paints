package com.shirantech.pashupatipaints.adapter;

import android.content.Context;
import android.graphics.Color;
import android.os.Build;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;

import com.shirantech.pashupatipaints.R;
import com.shirantech.pashupatipaints.model.PaintColor;
import com.shirantech.pashupatipaints.util.Utility;

import java.util.List;

/**
 * Created by susan on 12/3/14.
 */
public class ColorPickerAdapter extends BaseAdapter {
    private Context context;
    private List<PaintColor> paintColorList;

    public ColorPickerAdapter(Context context, List<PaintColor> paintColorList) {
        this.context = context;
        this.paintColorList = paintColorList;
    }

    @Override
    public int getCount() {
        return paintColorList.size();
    }

    @Override
    public PaintColor getItem(int position) {
        return paintColorList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    private class ViewHolder {
        private View imageViewIcon;
    }

    @SuppressWarnings("deprecation")
    @Override
    public View getView(int position, View view, ViewGroup viewGroup) {
        ViewHolder holder;
        if (view == null) {
            holder = new ViewHolder();
            view = LayoutInflater.from(context)
                    .inflate(R.layout.dashboard_item, viewGroup, false);
            holder.imageViewIcon = view.findViewById(R.id.dashboard_image);
            view.setTag(holder);
        } else {
            holder = (ViewHolder) view.getTag();
        }
        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
            holder.imageViewIcon.setBackground(Utility.getCustomShape(Color.parseColor(getItem(position).getColorCode())));
        } else{
            holder.imageViewIcon.setBackgroundDrawable(Utility.getCustomShape(Color.parseColor(getItem(position).getColorCode())));
        }
        return view;
    }
}
