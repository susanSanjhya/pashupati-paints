package com.shirantech.pashupatipaints.adapter;

import android.content.Context;
import android.os.AsyncTask;

import com.shirantech.pashupatipaints.database.dao.PaintColorDao;
import com.shirantech.pashupatipaints.interfaces.ModelListGetter;
import com.shirantech.pashupatipaints.model.PaintColor;

import java.util.List;

/**
 * Created by susan on 12/3/14.
 */
public class GetColorListTask extends AsyncTask<String, Void, Void> {
    private Context context;
    private List<PaintColor> paintColorList;
    private ModelListGetter<PaintColor> listener;

    public GetColorListTask(Context context, ModelListGetter<PaintColor> listener) {
        this.context = context;
        this.listener = listener;
    }

    @Override
    protected void onPreExecute() {
        listener.onTaskStarted();
    }

    @Override
    protected Void doInBackground(String... strings) {
        PaintColorDao paintColorDao = new PaintColorDao(context);
        paintColorList = paintColorDao.getAllCategory1Paints(strings[0]);
        return null;
    }

    @Override
    protected void onPostExecute(Void aVoid) {
        listener.onTaskFinished(paintColorList);
    }
}
