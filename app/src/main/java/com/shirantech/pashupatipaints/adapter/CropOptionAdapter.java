package com.shirantech.pashupatipaints.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.shirantech.pashupatipaints.R;
import com.shirantech.pashupatipaints.model.CropOption;

import java.util.List;

/**
 * Created by susan on 12/2/14.
 */
public class CropOptionAdapter extends BaseAdapter {

    private List<CropOption> cropOptionsList;
    private LayoutInflater layoutInflater;

    public CropOptionAdapter(Context context, List<CropOption> cropOptionsList) {
        this.cropOptionsList = cropOptionsList;
        this.layoutInflater = LayoutInflater.from(context);
    }

    @Override
    public int getCount() {
        return cropOptionsList.size();
    }

    @Override
    public CropOption getItem(int position) {
        return cropOptionsList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    private class ViewHolder {
        private ImageView imageView;
        private TextView textView;
    }

    @Override
    public View getView(int position, View view, ViewGroup viewGroup) {
        ViewHolder holder;
        if (view == null) {
            view = layoutInflater.inflate(R.layout.crop_selector, viewGroup, false);
            holder = new ViewHolder();
            holder.imageView = (ImageView) view.findViewById(R.id.iv_icon);
            holder.textView = (TextView) view.findViewById(R.id.tv_name);
            view.setTag(holder);
        } else {
            holder = (ViewHolder) view.getTag();
        }

        holder.imageView.setImageDrawable(getItem(position).icon);
        holder.textView.setText(getItem(position).title);
        return view;
    }
}
