package com.shirantech.pashupatipaints.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;

import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.shirantech.pashupatipaints.R;
import com.shirantech.pashupatipaints.model.PaintedImage;
import com.shirantech.pashupatipaints.util.ImageUtils;

import java.util.List;

/**
 * Created by susan on 12/14/14.
 */
public class TabletGalleryImageAdapter extends BaseAdapter {
    private Context context;
    private List<PaintedImage> paintedImageList;
    private ImageLoader imageLoader = ImageLoader.getInstance();

    public TabletGalleryImageAdapter(Context context, List<PaintedImage> paintedImageList) {
        this.context = context;
        this.paintedImageList = paintedImageList;
        imageLoader.init(ImageLoaderConfiguration.createDefault(this.context));
    }

    @Override
    public int getCount() {
        return paintedImageList.size();
    }

    @Override
    public PaintedImage getItem(int position) {
        return paintedImageList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    public void remove(PaintedImage record) {
        paintedImageList.remove(record);
    }

    private class ViewHolder {
        private ImageView imageViewGallery;
    }

    @Override
    public View getView(int position, View view, ViewGroup viewGroup) {
        ViewHolder holder;
        if (view == null) {
            holder = new ViewHolder();
            view = LayoutInflater.from(context)
                    .inflate(R.layout.row_gallery_image, viewGroup, false);
            holder.imageViewGallery = (ImageView) view.findViewById(R.id.imageview_gallery_image);
            view.setTag(holder);
        } else {
            holder = (ViewHolder) view.getTag();
        }
        ImageUtils.loadImageUsingLoader(imageLoader, holder.imageViewGallery, "file:///mnt/sdcard/PashupatiPaints/" + getItem(position).getName() + ".png");
        return view;
    }
}
