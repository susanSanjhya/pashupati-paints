package com.shirantech.pashupatipaints.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.shirantech.pashupatipaints.R;
import com.shirantech.pashupatipaints.model.PaintDealer;

import java.util.List;

/**
 * Created by susan on 12/15/14.
 */
public class CitySpinnerAdapter extends BaseAdapter {

    private Context context;
    private List<PaintDealer> paintDealerList;

    public CitySpinnerAdapter(Context context, List<PaintDealer> paintDealerList) {
        this.context = context;
        this.paintDealerList = paintDealerList;
    }

    @Override
    public int getCount() {
        return paintDealerList.size();
    }

    @Override
    public PaintDealer getItem(int position) {
        return paintDealerList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    private class ViewHolder {
        TextView textViewCity;
    }

    @Override
    public View getView(int position, View view, ViewGroup viewGroup) {
        ViewHolder holder;
        if (view == null) {
            holder = new ViewHolder();
            view = LayoutInflater.from(context)
                    .inflate(R.layout.row_spinner_filter, viewGroup, false);
            holder.textViewCity = (TextView) view.findViewById(R.id.textview_filter_single_row);
            view.setTag(holder);
        } else {
            holder = (ViewHolder) view.getTag();
        }

        holder.textViewCity.setText(getItem(position).getCity());
        return view;
    }
}
