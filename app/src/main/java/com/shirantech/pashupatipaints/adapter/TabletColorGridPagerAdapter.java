package com.shirantech.pashupatipaints.adapter;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.shirantech.pashupatipaints.fragments.BeigeFragment;
import com.shirantech.pashupatipaints.fragments.BlackFragment;
import com.shirantech.pashupatipaints.fragments.BlueFragment;
import com.shirantech.pashupatipaints.fragments.BrownFragment;
import com.shirantech.pashupatipaints.fragments.GrayFragment;
import com.shirantech.pashupatipaints.fragments.GreenFragment;
import com.shirantech.pashupatipaints.fragments.IndigoFragment;
import com.shirantech.pashupatipaints.fragments.OrangeFragment;
import com.shirantech.pashupatipaints.fragments.RedFragment;
import com.shirantech.pashupatipaints.fragments.VioletFragment;
import com.shirantech.pashupatipaints.fragments.WhiteFragment;
import com.shirantech.pashupatipaints.fragments.YellowFragment;
import com.shirantech.pashupatipaints.util.AppLog;

/**
 * Created by susan on 12/17/14.
 */
public class TabletColorGridPagerAdapter extends FragmentPagerAdapter {

    private static final String CLASS_TAG = ColorGridPagerAdapter.class.getSimpleName();
    private static final String[] ICONS = new String[]{
            "VIOLET",
            "INDIGO",
            "BLUE",
            "GREEN",
            "YELLOW",
            "ORANGE",
            "RED",
            "BROWN",
            "BEIGE",
            "GREY",
            "WHITE",
            "BLACK"
    };

    public TabletColorGridPagerAdapter(FragmentManager fm) {
        super(fm);
    }

    @Override
    public Fragment getItem(int position) {
        switch (position) {
            case 0:
                VioletFragment violetFragment = new VioletFragment();
                AppLog.showLog(CLASS_TAG, "fragment 0 clicked!!");
                return violetFragment;
            case 1:
                IndigoFragment indigoFragment = new IndigoFragment();
                AppLog.showLog(CLASS_TAG, "fragment 1 clicked!!");
                return indigoFragment;
            case 2:
                BlueFragment blueFragment = new BlueFragment();
                AppLog.showLog(CLASS_TAG, "fragment 2 clicked!!");
                return blueFragment;
            case 3:
                GreenFragment greenFragment = new GreenFragment();
                AppLog.showLog(CLASS_TAG, "fragment 3 clicked!!");
                return greenFragment;
            case 4:
                YellowFragment yellowFragment = new YellowFragment();
                AppLog.showLog(CLASS_TAG, "fragment 4 clicked!!");
                return yellowFragment;
            case 5:
                OrangeFragment orangeFragment = new OrangeFragment();
                AppLog.showLog(CLASS_TAG, "fragment 5 clicked!!");
                return orangeFragment;
            case 6:
                return new RedFragment();
            case 7:
                return new BrownFragment();
            case 8:
                return new BeigeFragment();
            case 9:
                return new GrayFragment();
            case 10:
                return new WhiteFragment();
            case 11:
                return new BlackFragment();
            default:
                return null;
        }
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return ICONS[position];
    }

    @Override
    public int getCount() {
        return ICONS.length;
    }
}
