package com.shirantech.pashupatipaints.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.nostra13.universalimageloader.core.ImageLoader;
import com.shirantech.pashupatipaints.R;
import com.shirantech.pashupatipaints.model.RoomCategory;
import com.shirantech.pashupatipaints.util.AppLog;
import com.shirantech.pashupatipaints.util.ImageUtils;

import java.util.List;

/**
 * Created by susan on 3/25/15.
 */
public class RoomCategoryAdapter extends BaseAdapter {
    private static final String CLASS_TAG = RoomCategory.class.getSimpleName();
    private Context context;
    private List<RoomCategory> roomCategoryList;
    private ImageLoader imageLoader;

    public RoomCategoryAdapter(Context context, List<RoomCategory> roomCategoryList, ImageLoader imageLoader) {
        this.context = context;
        this.roomCategoryList = roomCategoryList;
        this.imageLoader = imageLoader;
    }

    @Override
    public int getCount() {
        return roomCategoryList.size();
    }

    @Override
    public RoomCategory getItem(int position) {
        return roomCategoryList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    private class ViewHolder {
        private ImageView imageView;
        private TextView textView;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder;
        if (convertView == null) {
            holder = new ViewHolder();
            convertView = LayoutInflater.from(context)
                    .inflate(R.layout.row_room_category, parent, false);
            holder.imageView = (ImageView) convertView.findViewById(R.id.image_view_room_category);
            holder.textView = (TextView) convertView.findViewById(R.id.text_view_room_category);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        AppLog.showLog(CLASS_TAG, "Drawable " + getItem(position).getDrawable());
        holder.textView.setText(getItem(position).getTitle());
        ImageUtils.loadImageUsingLoader(imageLoader, holder.imageView,"assets://"+ getItem(position).getDrawable());
        return convertView;
    }
}
