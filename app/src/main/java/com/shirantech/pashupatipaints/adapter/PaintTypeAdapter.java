package com.shirantech.pashupatipaints.adapter;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.shirantech.pashupatipaints.R;
import com.shirantech.pashupatipaints.fragments.PaintTypeFragment;
import com.shirantech.pashupatipaints.model.PaintCalculatorType;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by susan on 3/22/15.
 */
public class PaintTypeAdapter extends FragmentPagerAdapter {
    private List<PaintCalculatorType> paintCalculatorTypeList;

    public PaintTypeAdapter(FragmentManager fm) {
        super(fm);
        paintCalculatorTypeList = new ArrayList<PaintCalculatorType>();
        paintCalculatorTypeList.add(new PaintCalculatorType(R.drawable.excoat_plus, 70));
        paintCalculatorTypeList.add(new PaintCalculatorType(R.drawable.excoat, 65));
        paintCalculatorTypeList.add(new PaintCalculatorType(R.drawable.eco_safe, 90));
        paintCalculatorTypeList.add(new PaintCalculatorType(R.drawable.nepolin_exterior, 55));
        paintCalculatorTypeList.add(new PaintCalculatorType(R.drawable.luxuria_interior, 80));
        paintCalculatorTypeList.add(new PaintCalculatorType(R.drawable.oneway_interior, 75));
        paintCalculatorTypeList.add(new PaintCalculatorType(R.drawable.nepolin_interior, 60));
        paintCalculatorTypeList.add(new PaintCalculatorType(R.drawable.nepolin_iacrylic_distemper, 45));
        paintCalculatorTypeList.add(new PaintCalculatorType(R.drawable.super_synthetic_enamel, 65));
        paintCalculatorTypeList.add(new PaintCalculatorType(R.drawable.danfe_gp_enamel, 60));
    }

    @Override
    public Fragment getItem(int i) {
        PaintTypeFragment paintTypeFragment = new PaintTypeFragment();
        Bundle bundle = new Bundle();
        bundle.putInt("POSITION", i);
        bundle.putInt("IMAGE_RESOURCE", paintCalculatorTypeList.get(i).getProductsDrawable());
        paintTypeFragment.setArguments(bundle);
        return paintTypeFragment;
    }

    @Override
    public int getCount() {
        return paintCalculatorTypeList.size();
    }
}
