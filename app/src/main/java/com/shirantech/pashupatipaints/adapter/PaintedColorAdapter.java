package com.shirantech.pashupatipaints.adapter;

import android.content.Context;
import android.graphics.Color;
import android.os.Build;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;

import com.shirantech.pashupatipaints.R;
import com.shirantech.pashupatipaints.customwidgets.CustomBoldTextView;
import com.shirantech.pashupatipaints.model.WallDescription;
import com.shirantech.pashupatipaints.util.Utility;

import java.util.List;

/**
 * Created by susan on 12/26/14.
 */
@SuppressWarnings("deprecation")
public class PaintedColorAdapter extends BaseAdapter {
    private Context context;
    private List<WallDescription> wallDescriptionList;

    public PaintedColorAdapter(Context context, List<WallDescription> wallDescriptionList) {
        this.context = context;
        this.wallDescriptionList = wallDescriptionList;
    }

    @Override
    public int getCount() {
        return wallDescriptionList.size();
    }

    @Override
    public WallDescription getItem(int position) {
        return wallDescriptionList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    private class ViewHolder {
        private View viewColor;
        private CustomBoldTextView textViewWallType, textViewName, textViewColorCode;
    }

    @Override
    public View getView(int position, View view, ViewGroup viewGroup) {
        ViewHolder holder;
        if (view == null) {
            holder = new ViewHolder();
            view = LayoutInflater.from(context)
                    .inflate(R.layout.row_used_colors, viewGroup, false);
            holder.viewColor = view.findViewById(R.id.view_used_color);
            holder.textViewWallType = (CustomBoldTextView) view.findViewById(R.id.textview_used_color_wall_type);
            holder.textViewName = (CustomBoldTextView) view.findViewById(R.id.textview_used_color_name);
            holder.textViewColorCode = (CustomBoldTextView) view.findViewById(R.id.textview_used_color_code);
            view.setTag(holder);
        } else {
            holder = (ViewHolder) view.getTag();
        }

        holder.textViewWallType.setText(getItem(position).getWallType());
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
            holder.viewColor.setBackground(Utility.getCustomShape(Color.parseColor(getItem(position).getColorCode())));
        } else {
            holder.viewColor.setBackgroundDrawable(Utility.getCustomShape(Color.parseColor(getItem(position).getColorCode())));
        }
        holder.textViewName.setText(getItem(position).getColorName());
        holder.textViewColorCode.setText(getItem(position).getColorCode());
        return view;
    }
}
