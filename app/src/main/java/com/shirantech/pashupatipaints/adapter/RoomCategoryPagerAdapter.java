package com.shirantech.pashupatipaints.adapter;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.shirantech.pashupatipaints.R;
import com.shirantech.pashupatipaints.fragments.BedroomsFragment;
import com.shirantech.pashupatipaints.fragments.DrawingHallsFragment;
import com.shirantech.pashupatipaints.fragments.ExteriorsFragment;
import com.shirantech.pashupatipaints.fragments.KitchensFragment;
import com.shirantech.pashupatipaints.util.AppLog;
import com.viewpagerindicator.IconPagerAdapter;

/**
 * View Pager Adapter for Room Category List.
 * Created by susan on 12/3/14.
 */
public class RoomCategoryPagerAdapter extends FragmentPagerAdapter implements IconPagerAdapter {
    private static final String CLASS_TAG = RoomCategoryPagerAdapter.class.getSimpleName();
    private static final int[] ICONS = new int[]{
            R.drawable.vp_tab_group_exteriors,
            R.drawable.vp_tab_group_drawingrooms, //living room!
            R.drawable.vp_tab_group_bedrooms,
            R.drawable.vp_tab_group_kitchens,
    };

    public RoomCategoryPagerAdapter(FragmentManager fm) {
        super(fm);
    }

    @Override
    public Fragment getItem(int position) {
        switch (position) {
            case 0:
                ExteriorsFragment exteriorsFragment = new ExteriorsFragment();
                AppLog.showLog(CLASS_TAG, "fragment 0 clicked!!");
                return exteriorsFragment;
            case 1:
                DrawingHallsFragment drawingHallFragment = new DrawingHallsFragment();
                AppLog.showLog(CLASS_TAG, "fragment 1 clicked!!");
                return drawingHallFragment;
            case 2:
                BedroomsFragment bedroomsFragment = new BedroomsFragment();
                AppLog.showLog(CLASS_TAG, "fragment 2 clicked!!");
                return bedroomsFragment;
            case 3:
                KitchensFragment kitchensFragment = new KitchensFragment();
                AppLog.showLog(CLASS_TAG, "fragment 3 clicked!!");
                return kitchensFragment;
            default:
                return null;

        }
    }

    @Override
    public int getIconResId(int index) {
        return ICONS[index];
    }

    @Override
    public int getCount() {
        return ICONS.length;
    }
}
