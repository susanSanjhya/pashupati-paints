package com.shirantech.pashupatipaints.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.v4.view.PagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.shirantech.pashupatipaints.R;
import com.shirantech.pashupatipaints.activities.list.DealersDetailActivity;
import com.shirantech.pashupatipaints.activities.list.GalleryActivity;
import com.shirantech.pashupatipaints.activities.list.PainingWaysActivity;
import com.shirantech.pashupatipaints.activities.list.PaintCalculatorTabletActivity;
import com.shirantech.pashupatipaints.animatedViewPager.JazzyViewPager;
import com.shirantech.pashupatipaints.util.AppLog;

/**
 * Created by binod on 3/20/15.
 */
public class DashBoardPagerAdapter extends PagerAdapter {

    private Context mContext;
    JazzyViewPager mJazzy;

    public int[] dashboardDrawables = {R.drawable.start_painting,
            R.drawable.paint_calculator,
            R.drawable.authorized_dealers,
            R.drawable.ic_gallery
    };


    private final String[] imageTitle;


    public DashBoardPagerAdapter(Context mContext, JazzyViewPager mJazzy) {
        this.mContext = mContext;
        this.mJazzy = mJazzy;

        imageTitle = new String[]{
                mContext.getResources().getString(R.string.start_painting),
                mContext.getResources().getString(R.string.text_paint_calculator),
                mContext.getResources().getString(R.string.text_authorised_dealers),
                mContext.getResources().getString(R.string.text_gallery)
        };

    }

    @Override
    public Object instantiateItem(ViewGroup container, int position) {
        if (position >= imageTitle.length) {
            position = position % imageTitle.length;
        }
        LayoutInflater inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = inflater.inflate(R.layout.dashboard_pager_item, container, false);
        ImageView dashBoardImage = (ImageView) view.findViewById(R.id.imageView_pager_item);
        dashBoardImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startCategoryActivity(mJazzy.getCurrentItem());
            }
        });
        TextView dashBoardText = (TextView) view.findViewById(R.id.textView_pager_item);

        dashBoardImage.setImageResource(dashboardDrawables[position]);
        dashBoardText.setText(imageTitle[position]);

        container.addView(view, 0);
        mJazzy.setObjectForPosition(view, position);

        return view;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((View) object);
    }

    @Override
    public int getCount() {
//        return imageTitle.length;
        return 100;
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return (view == object);
    }

    private void startCategoryActivity(int position) {
        if (position >= imageTitle.length) {
            position = position % imageTitle.length;
        }
        AppLog.showLog("position::" + position);
        switch (position) {
            case 0:
                Intent paintingWaysIntent = new Intent(mContext, PainingWaysActivity.class);
                mContext.startActivity(paintingWaysIntent);
                break;
            case 1:
                Intent paintCalculatorIntent = new Intent(mContext.getApplicationContext(), PaintCalculatorTabletActivity.class);
                mContext.startActivity(paintCalculatorIntent);
                break;
            case 2:

                Intent dealersDetailIntent = new Intent(mContext.getApplicationContext(), DealersDetailActivity.class);
                mContext.startActivity(dealersDetailIntent);
                break;
            case 3:

                Intent galleryIntent = new Intent(mContext.getApplicationContext(), GalleryActivity.class);
                mContext.startActivity(galleryIntent);

                break;
            default:
                break;
        }
    }
}
