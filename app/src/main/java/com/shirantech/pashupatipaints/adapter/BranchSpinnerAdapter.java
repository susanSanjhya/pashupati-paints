package com.shirantech.pashupatipaints.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.shirantech.pashupatipaints.R;

/**
 * Created by susan on 12/15/14.
 */
public class BranchSpinnerAdapter extends BaseAdapter {
    private Context context;
    private String[] paintDealerList;

    public BranchSpinnerAdapter(Context context, String[] paintDealerList) {
        this.context = context;
        this.paintDealerList = paintDealerList;
    }

    @Override
    public int getCount() {
        return paintDealerList.length;
    }

    @Override
    public String getItem(int position) {
        return paintDealerList[position];
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    private class ViewHolder {
        private TextView textViewBranch;
    }

    @Override
    public View getView(int position, View view, ViewGroup viewGroup) {
        ViewHolder holder;

        if (view == null) {
            holder = new ViewHolder();
            view = LayoutInflater.from(context)
                    .inflate(R.layout.row_spinner_filter, viewGroup, false);
            holder.textViewBranch = (TextView) view.findViewById(R.id.textview_filter_single_row);
            view.setTag(holder);
        } else {
            holder = (ViewHolder) view.getTag();
        }

        holder.textViewBranch.setText(getItem(position));
        return view;
    }
}
