package com.shirantech.pashupatipaints.adapter;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;

import com.shirantech.pashupatipaints.R;
import com.shirantech.pashupatipaints.activities.list.GalleryV2Activity;
import com.shirantech.pashupatipaints.customwidgets.CarouselItemLayout;
import com.shirantech.pashupatipaints.fragments.GalleryFragment;
import com.shirantech.pashupatipaints.model.PaintedImage;
import com.shirantech.pashupatipaints.util.AppLog;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by susan on 7/9/15.
 */
@SuppressWarnings("ConstantConditions")
public class GalleryPagerAdapter extends FragmentPagerAdapter implements ViewPager.OnPageChangeListener {
    private static final String CLASS_TAG = GalleryPagerAdapter.class.getSimpleName();
    private List<PaintedImage> paintedImagesList;
    private float scale;
    private FragmentManager fm;
    public int page;
    public int firstPage;
    private Context context;
    private CarouselItemLayout cur = null;
    private CarouselItemLayout next = null;
    private List<Fragment> fragmentList = new ArrayList<Fragment>();

    public GalleryPagerAdapter(FragmentManager fm, Context context, List<PaintedImage> paintedImagesList) {
        super(fm);
        this.fm = fm;
        this.context = context;
        this.paintedImagesList = paintedImagesList;
        this.page = paintedImagesList.size();
        this.firstPage = page / 2;
    }

    public void clearAll(){
        for(Fragment fragment : fragmentList){
            fm.beginTransaction().remove(fragment).commit();
        }
        fragmentList.clear();
    }

    @Override
    public Fragment getItem(int position) {
        if (position == firstPage) {
            scale = 1.0f;
        } else {
            scale = 0.7f;
        }
        GalleryFragment fragment = new GalleryFragment();
        Bundle bundle = new Bundle();
        bundle.putString("paint_name", paintedImagesList.get(position).getName());
        bundle.putString("paint_date", paintedImagesList.get(position).getDate());
        bundle.putInt("position", position);
        bundle.putFloat("scale", scale);
        fragment.setArguments(bundle);
        fragmentList.add(fragment);
        return fragment;
    }

    @Override
    public int getCount() {
        return paintedImagesList.size();
    }

    @Override
    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
        if (positionOffset >= 0f && positionOffset <= 1f) {
            if (paintedImagesList.size() > 0) {
                cur = getRootView(position);
            } else {
                cur = null;
            }
            AppLog.showLog(CLASS_TAG, "Position: " + position);
            AppLog.showLog(CLASS_TAG, "Image list size: " + paintedImagesList.size());
            if (position + 1 < paintedImagesList.size() && paintedImagesList.size() != 1) {
                next = getRootView(position + 1);
            } else {
                next = null;
            }
            if (cur != null) {
                cur.setScaleBoth(1.0f
                        - (1.0f - 0.7f) * positionOffset);
            }
            if (next != null) {
                next.setScaleBoth(0.7f
                        + (1.0f - 0.7f) * positionOffset);
            }
        }
    }

    @Override
    public void onPageSelected(int position) {

    }

    @Override
    public void onPageScrollStateChanged(int state) {

    }

    private CarouselItemLayout getRootView(int position) {
        return (CarouselItemLayout) fm.findFragmentByTag(this.getFragmentTag(position))
                .getView().findViewById(R.id.root);
    }

    private String getFragmentTag(int position) {
        return "android:switcher:" + ((GalleryV2Activity) context).pager.getId() + ":" + position;
    }

    public void remove(PaintedImage record) {
        paintedImagesList.remove(record);
    }
}
