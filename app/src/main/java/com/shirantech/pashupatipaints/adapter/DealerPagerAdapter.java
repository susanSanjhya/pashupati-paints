package com.shirantech.pashupatipaints.adapter;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.shirantech.pashupatipaints.fragments.DealersBiratnagarFragment;
import com.shirantech.pashupatipaints.fragments.DealersKathmanduFragment;
import com.shirantech.pashupatipaints.fragments.DealersNarayanghatFragment;
import com.shirantech.pashupatipaints.fragments.DealersPokharaFragment;
import com.shirantech.pashupatipaints.util.AppLog;

/**
 * Created by susan on 12/3/14.
 */
public class DealerPagerAdapter extends FragmentPagerAdapter {

    private static final String CLASS_TAG = DealerPagerAdapter.class.getSimpleName();
    private static final String[] CONTENT = new String[]{"Kathmandu", "Pokhara", "Narayanghat", "Biratnagar"};

    public DealerPagerAdapter(FragmentManager fm) {
        super(fm);
    }

    @Override
    public Fragment getItem(int position) {
        switch (position) {
            case 0:
                DealersKathmanduFragment dealersKathmanduFragment = new DealersKathmanduFragment();
                AppLog.showLog(CLASS_TAG, "fragment ktmDealers clicked!!");
                return dealersKathmanduFragment;
            case 1:
                DealersPokharaFragment dealersPokharaFragment = new DealersPokharaFragment();
                AppLog.showLog(CLASS_TAG, "fragment POkDealers clicked!!");
                return dealersPokharaFragment;
            case 2:
                DealersNarayanghatFragment dealersNarayanghatFragment = new DealersNarayanghatFragment();
                AppLog.showLog(CLASS_TAG, "fragment nghDealers clicked!!");
                return dealersNarayanghatFragment;
            case 3:
                DealersBiratnagarFragment dealersBiratnagarFragment = new DealersBiratnagarFragment();
                AppLog.showLog(CLASS_TAG, "fragment brtDealers clicked!!");
                return dealersBiratnagarFragment;
            default:
                return null;
        }
        /*
        if (position == 0) {
            DealersKathmanduFragment dealersKathmanduFragment = new DealersKathmanduFragment();
            AppLog.showLog(CLASS_TAG, "fragment ktmDealers clicked!!");
            return dealersKathmanduFragment;
        } else if (position == 1) {
            DealersPokharaFragment dealersPokharaFragment = new DealersPokharaFragment();
            AppLog.showLog(CLASS_TAG, "fragment POkDealers clicked!!");
            return dealersPokharaFragment;
        } else if (position == 2) {
            DealersNarayanghatFragment dealersNarayanghatFragment = new DealersNarayanghatFragment();
            AppLog.showLog(CLASS_TAG, "fragment nghDealers clicked!!");
            return dealersNarayanghatFragment;
        } else if (position == 3) {
            DealersBiratnagarFragment dealersBiratnagarFragment = new DealersBiratnagarFragment();
            AppLog.showLog(CLASS_TAG, "fragment brtDealers clicked!!");
            return dealersBiratnagarFragment;
        }
        return null;*/
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return CONTENT[position % CONTENT.length].toUpperCase();
    }

    @Override
    public int getCount() {
        return CONTENT.length;
    }
}
