package com.shirantech.pashupatipaints.adapter;

import android.content.Context;
import android.graphics.Color;
import android.os.Build;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.shirantech.pashupatipaints.R;
import com.shirantech.pashupatipaints.model.PaintColor;
import com.shirantech.pashupatipaints.util.AppLog;
import com.shirantech.pashupatipaints.util.Utility;

import java.util.List;

/**
 * Created by susan on 12/19/14.
 */
@SuppressWarnings("deprecation")
public class UsedColorAdapter extends BaseAdapter {
    private static final String CLASS_TAG = UsedColorAdapter.class.getSimpleName();
    private Context context;
    private List<PaintColor> usedColorList;

    public UsedColorAdapter(Context context, List<PaintColor> usedColorList) {
        this.context = context;
        this.usedColorList = usedColorList;
    }

    @Override
    public int getCount() {
        return usedColorList.size();
    }

    @Override
    public PaintColor getItem(int position) {
        return usedColorList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    private class ViewHolder {
        private View colorView;
        private TextView textViewName, textViewCode, textViewWallType;
    }

    @Override
    public View getView(int position, View view, ViewGroup viewGroup) {
        ViewHolder holder;
        if (view == null) {
            holder = new ViewHolder();
            view = LayoutInflater.from(context)
                    .inflate(R.layout.row_used_colors, viewGroup, false);
            holder.colorView = view.findViewById(R.id.view_used_color);
            holder.textViewName = (TextView) view.findViewById(R.id.textview_used_color_name);
            holder.textViewCode = (TextView) view.findViewById(R.id.textview_used_color_code);
            holder.textViewWallType = (TextView) view.findViewById(R.id.textview_used_color_wall_type);
            view.setTag(holder);
        } else {
            holder = (ViewHolder) view.getTag();
        }

        AppLog.showLog(CLASS_TAG, "COLOR CODE: " + getItem(position).getColorCode());
        holder.textViewWallType.setText(getItem(position).getTypeWall());
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
            holder.colorView.setBackground(Utility.getCustomShape(Color.parseColor(getItem(position).getColorCode())));
        } else {
            holder.colorView.setBackgroundDrawable(Utility.getCustomShape(Color.parseColor(getItem(position).getColorCode())));
        }
        holder.textViewName.setText(getItem(position).getName());
        holder.textViewCode.setText(getItem(position).getColorCode());
        return view;
    }
}
