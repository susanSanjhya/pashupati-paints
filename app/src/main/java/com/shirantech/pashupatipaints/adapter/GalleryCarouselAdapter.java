package com.shirantech.pashupatipaints.adapter;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.LinearGradient;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffXfermode;
import android.graphics.Shader;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.actionbarsherlock.app.SherlockFragmentActivity;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.shirantech.pashupatipaints.R;
import com.shirantech.pashupatipaints.customwidgets.carousel.ScalableImageView;
import com.shirantech.pashupatipaints.model.PaintedImage;
import com.shirantech.pashupatipaints.util.Global;

import java.util.List;

/**
 * Created by susan on 3/25/15.
 */
public class GalleryCarouselAdapter extends BaseAdapter {
    private Context context;
    private List<PaintedImage> paintedImageList;
    int m_w, m_h;
    private ImageLoader imageLoader;

    public GalleryCarouselAdapter(Context context, ImageLoader imageLoader, List<PaintedImage> paintedImageList, int m_w, int m_h) {
        this.context = context;
        this.paintedImageList = paintedImageList;
        this.imageLoader = imageLoader;
        this.m_w = m_w;
        this.m_h = m_h;
    }

    @Override
    public int getCount() {
        return paintedImageList.size();
    }

    @Override
    public Object getItem(int position) {
        return paintedImageList.get(position);
    }

    @Override
    public long getItemId(int position) {
       /* if (paintedImageList.size() == 0) return 0; //fix divide by zero on filtering
        if (position >= paintedImageList.size()) {
            position = position % paintedImageList.size();
        }*/
        return position;
    }

    private Bitmap createReflectedImage(Bitmap bmp) {
        if (bmp == null) return null;
        // The gap we want between the reflection and the original image
        final int reflectionGap = 0;
        int width = bmp.getWidth();
        int height = bmp.getHeight();


        //This will not scale but will flip on the Y axis
        Matrix matrix = new Matrix();
        matrix.preScale(1, -1);

        //Create a Bitmap with the flip matrix applied to it.
        int reflection_part = 4;
        //We only want the bottom half of the image
        Bitmap reflectionImage = Bitmap.createBitmap(bmp, 0,
                height - height / reflection_part, width, height / reflection_part, matrix, false);
        //Create a new bitmap with same width but taller to fit reflection
        Bitmap bitmapWithReflection = Bitmap.createBitmap(width, (height + height / reflection_part), Bitmap.Config.ARGB_8888);
        //Create a new Canvas with the bitmap that's big enough for
        //the image plus gap plus reflection
        Canvas canvas = new Canvas(bitmapWithReflection);
        //Draw in the original image
        canvas.drawBitmap(bmp, 0, 0, null);
        //Draw in the gap
        Paint deafaultPaint = new Paint();
        canvas.drawRect(0, height, width, height + reflectionGap, deafaultPaint);
        //Draw in the reflection
        canvas.drawBitmap(reflectionImage, 0, height + reflectionGap, null);
        //Create a shader that is a linear gradient that covers the reflection
        Paint paint = new Paint();
        LinearGradient shader = new LinearGradient(0, height, 0,
                bitmapWithReflection.getHeight() + reflectionGap, 0x70ffffff, 0x00ffffff,
                Shader.TileMode.CLAMP);
        //Set the paint to use this shader (linear gradient)
        paint.setShader(shader);
        //Set the Transfer mode to be porter duff and destination in
        paint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.DST_IN));
        //Draw a rectangle using the paint with our linear gradient
        canvas.drawRect(0, height, width,
                bitmapWithReflection.getHeight() + reflectionGap, paint);

        return bitmapWithReflection;

    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ((Global) ((SherlockFragmentActivity) context).getApplication()).setMaxh(m_h);
        ((Global) ((SherlockFragmentActivity) context).getApplication()).setMaxW(m_w);
        ViewHolder holder;
        if (convertView == null) {
            holder = new ViewHolder();
            convertView = LayoutInflater.from(context)
                    .inflate(R.layout.carousel_gallery_item, parent, false);
            holder.imageView = (ScalableImageView) convertView.findViewById(R.id.image_view);
            holder.textView = (TextView) convertView.findViewById(R.id.text_view);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }
      /*  if (paintedImageList.size() > 0) {
            if (position >= paintedImageList.size()) {
                position = position % paintedImageList.size();
            }
        }
*/

        holder.imageView.setM_h(m_h);
        holder.imageView.setM_w(m_w);/*
        ImageUtils.loadImageUsingLoader(imageLoader, holder.imageView, "file:///mnt/sdcard/PashupatiPaints/"
                + paintedImageList.get(position).getName() + ".png");*/
        Bitmap originalImage = BitmapFactory.decodeFile("/mnt/sdcard/PashupatiPaints/" + paintedImageList.get(position).getName() + ".png");
        Bitmap reflection = createReflectedImage(originalImage);
        if (originalImage != null) {
            originalImage.recycle();
        }
        holder.imageView.setImageBitmap(reflection);
        holder.textView.setText(paintedImageList.get(position).getDate());
        return convertView;
    }

    private class ViewHolder {
        private ScalableImageView imageView;
        private TextView textView;
    }

    public void remove(PaintedImage record) {
        paintedImageList.remove(record);
    }

}
