package com.shirantech.pashupatipaints.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.nostra13.universalimageloader.core.ImageLoader;
import com.shirantech.pashupatipaints.R;
import com.shirantech.pashupatipaints.model.RoomCategory;
import com.shirantech.pashupatipaints.util.ImageUtils;

import java.util.List;

/**
 * Adapter class for Room Category.
 * Created by susan on 12/3/14.
 */
public class RoomCategoryListAdapter extends BaseAdapter {
    private Context context;
    private List<RoomCategory> roomCategoryList;
    private ImageLoader imageLoader;

    public RoomCategoryListAdapter(Context context, ImageLoader imageLoader, List<RoomCategory> roomCategoryList) {
        this.context = context;
        this.roomCategoryList = roomCategoryList;
        this.imageLoader = imageLoader;
    }

    @Override
    public int getCount() {
        return roomCategoryList.size();
    }

    @Override
    public RoomCategory getItem(int position) {
        return roomCategoryList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    private class ViewHolder {
        private TextView textView;
        private ImageView imageView;
    }

    @Override
    public View getView(int position, View view, ViewGroup viewGroup) {
        ViewHolder holder;
        if (view == null) {
            holder = new ViewHolder();
            view = LayoutInflater.from(context)
                    .inflate(R.layout.row, viewGroup, false);
            holder.textView = (TextView) view.findViewById(R.id.toptext);
            holder.imageView = (ImageView) view.findViewById(R.id.ivRoom);
            view.setTag(holder);
        } else {
            holder = (ViewHolder) view.getTag();
        }

        holder.textView.setText(getItem(position).getTitle());
        ImageUtils.loadImageUsingLoader(imageLoader, holder.imageView, "assets://" + getItem(position).getDrawable());
        return view;
    }

    public void remove(RoomCategory record) {
        roomCategoryList.remove(record);
    }
}
